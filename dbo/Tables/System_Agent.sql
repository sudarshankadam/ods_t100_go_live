CREATE TABLE [dbo].[System_Agent] (
    [System_Customer_Id] INT           NOT NULL,
    [System_Id]          INT           NULL,
    [Account_Agent_id]   VARCHAR (15)  NULL,
    [Agent_Name]         VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([System_Customer_Id] ASC)
);


GO

