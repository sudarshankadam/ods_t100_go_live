CREATE TABLE [dbo].[User_Role] (
    [User_Role_Id]            INT          NOT NULL,
    [User_Info_Id]            INT          NULL,
    [Role_Id]                 INT          NULL,
    [Role_Assignement_Status] VARCHAR (50) NULL,
    [Role_Assigned_date]      DATETIME     NULL,
    [Role_Last_Update_date]   DATETIME     NULL,
    CONSTRAINT [PK_User_Role] PRIMARY KEY CLUSTERED ([User_Role_Id] ASC),
    CONSTRAINT [FK_User_Role_Role_Id_Master] FOREIGN KEY ([Role_Id]) REFERENCES [dbo].[Role] ([Role_Id]),
    CONSTRAINT [FK_User_Role_User_Master] FOREIGN KEY ([User_Info_Id]) REFERENCES [dbo].[User_Master] ([User_Info_Id])
);


GO

