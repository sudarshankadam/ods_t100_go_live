CREATE TABLE [dbo].[Account_Type] (
    [Account_Type_Id]          INT          NOT NULL,
    [Account_Type]             VARCHAR (6)  NULL,
    [Account_type_Description] VARCHAR (50) NULL,
    [Account_Type_Group]       VARCHAR (15) NULL,
    CONSTRAINT [PK_Account_Type] PRIMARY KEY CLUSTERED ([Account_Type_Id] ASC)
);


GO

