CREATE TABLE [dbo].[UI_Account_Type_Function_Access] (
    [Account_Type_Id_Function_ID]         INT          NOT NULL,
    [System_Id]                           INT          NOT NULL,
    [Account_Type_id]                     INT          NOT NULL,
    [Function_Tab_id]                     INT          NOT NULL,
    [Function_Id]                         INT          NOT NULL,
    [Access_Level]                        VARCHAR (20) NULL,
    [Access_Description]                  VARCHAR (50) NULL,
    [Next_Mandatory_Function_Id]          INT          NULL,
    [Next_Mandatory_Function_Description] VARCHAR (50) NULL,
    [Next_Mandatory_Function_Label]       VARCHAR (50) NULL,
    [Prev_Brokerage_Code]                 CHAR (8)     NULL,
    CONSTRAINT [PK_ui_Account_Type_Function_Access_1] PRIMARY KEY CLUSTERED ([System_Id] ASC, [Account_Type_id] ASC, [Function_Tab_id] ASC, [Function_Id] ASC),
    CONSTRAINT [FK_ui_Account_Type_Function_Access_ui_Account_Type_Function_Tab_Access] FOREIGN KEY ([Account_Type_id], [Function_Tab_id], [System_Id]) REFERENCES [dbo].[UI_Account_Type_Function_Tab_Access] ([Account_Type_id], [Function_tab_Id], [System_Id]),
    CONSTRAINT [FK_ui_Account_Type_Function_Access_ui_Function] FOREIGN KEY ([Function_Id]) REFERENCES [dbo].[UI_Function] ([Function_Id])
);


GO

