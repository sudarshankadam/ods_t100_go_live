CREATE TABLE [dbo].[Role_Function_Field] (
    [Role_Function_Field_ID] INT          NULL,
    [Role_id]                INT          NULL,
    [Function_Field_Id]      INT          NULL,
    [Access_Level]           VARCHAR (50) NULL,
    CONSTRAINT [FK_Role_Function_Field_Role] FOREIGN KEY ([Role_id]) REFERENCES [dbo].[Role] ([Role_Id])
);


GO

