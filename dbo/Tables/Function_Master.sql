CREATE TABLE [dbo].[Function_Master] (
    [function_id]          INT           NOT NULL,
    [function_description] VARCHAR (100) NULL,
    CONSTRAINT [PK_Function] PRIMARY KEY CLUSTERED ([function_id] ASC)
);


GO

