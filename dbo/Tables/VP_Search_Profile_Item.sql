CREATE TABLE [dbo].[VP_Search_Profile_Item] (
    [Search_Profile_Id]                INT           NOT NULL,
    [Search_Profile_Item_Id]           INT           NOT NULL,
    [DB_Table_View_Name]               VARCHAR (100) NULL,
    [Column_Group_Screen_Label]        VARCHAR (100) NULL,
    [DB_Column_Group]                  VARCHAR (100) NULL,
    [Db_Column_Name]                   VARCHAR (100) NULL,
    [DB_Comparison_Allowed]            VARCHAR (50)  NULL,
    [DB_Comparison_Allowed_Additional] VARCHAR (50)  NULL,
    [Profile_Item_Create_Date]         DATE          NULL,
    [Profile_Item_Update_Date]         DATE          NULL,
    [system_id]                        INT           NULL
);


GO

