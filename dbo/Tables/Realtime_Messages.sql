CREATE TABLE [dbo].[Realtime_Messages] (
    [USER_ID]      VARCHAR (100) NULL,
    [SUBJECT]      VARCHAR (300) NULL,
    [MESSAGE_DATE] VARCHAR (40)  NULL,
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [ACTIVE]       VARCHAR (1)   NOT NULL,
    [cc_list]      VARCHAR (300) NULL,
    [bcc_list]     VARCHAR (300) NULL,
    [to_list]      VARCHAR (300) NULL,
    [BODY]         TEXT          NULL,
    CONSTRAINT [PK_REALTIME_MESSAGES] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO

