CREATE TABLE [dbo].[User_Profile_Item] (
    [User_profile_item_id] INT IDENTITY (1, 1) NOT NULL,
    [User_profile]         INT NOT NULL,
    [User_search_profile]  INT NOT NULL,
    CONSTRAINT [PK_User_Profile_Item_Id_Master] PRIMARY KEY CLUSTERED ([User_profile_item_id] ASC),
    CONSTRAINT [User_Profile_Item_FK] FOREIGN KEY ([User_profile]) REFERENCES [dbo].[User_Profile] ([User_profile_id]),
    CONSTRAINT [User_Profile_Item_FK_1] FOREIGN KEY ([User_search_profile]) REFERENCES [dbo].[User_Search_Profile] ([User_search_profile_id])
);


GO

