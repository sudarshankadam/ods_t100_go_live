CREATE TABLE [dbo].[User_Search_Profile_Column] (
    [User_search_profile_Column_Id] INT            IDENTITY (1, 1) NOT NULL,
    [User_search_profile_Col_Descr] VARCHAR (8000) NULL,
    [Search_Profile_Id]             INT            NOT NULL,
    [User_Search_Profile_Id]        VARCHAR (500)  NULL,
    [User_Id]                       VARCHAR (100)  NULL,
    [User_Profile_Create_Date]      DATE           NULL,
    [User_Profile_Update_Date]      DATE           NULL,
    [User_Search_Profile_Status]    VARCHAR (30)   NULL,
    [User_Search_Organization_Id]   VARCHAR (80)   NULL,
    [User_Type]                     VARCHAR (30)   NULL,
    [agency_id]                     VARCHAR (30)   NULL,
    [Customer_id]                   VARCHAR (30)   NULL,
    [system_id]                     INT            NULL,
    CONSTRAINT [PK_User_Search_Profile_Coulmn] PRIMARY KEY CLUSTERED ([User_search_profile_Column_Id] ASC)
);


GO

CREATE NONCLUSTERED INDEX [index_Creator_User_Search_Profile]
    ON [dbo].[User_Search_Profile_Column]([User_Id] ASC) WITH (FILLFACTOR = 100);


GO

