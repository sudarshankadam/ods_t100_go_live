CREATE TABLE [dbo].[System] (
    [System_Id]          INT           NOT NULL,
    [System_Code]        VARCHAR (15)  NULL,
    [System_DESCRIPTION] VARCHAR (100) NULL,
    [Navigation_URL]     VARCHAR (200) NULL,
    [Image_src]          VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([System_Id] ASC)
);


GO

