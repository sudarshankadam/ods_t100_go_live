CREATE TABLE [dbo].[UI_Function_Field_Value] (
    [Function_Field_Collate_Id]   INT          NOT NULL,
    [function_field_id]           INT          NOT NULL,
    [Field_Value]                 VARCHAR (50) NULL,
    [Field_Value_Description]     VARCHAR (50) NULL,
    [Field_Collate_Outcome_Value] VARCHAR (50) NULL,
    [Field_Value_Status]          VARCHAR (10) NULL,
    [Field_Value_Status_Desc]     VARCHAR (80) NULL,
    CONSTRAINT [PK_ui_Function_Field_Value] PRIMARY KEY CLUSTERED ([Function_Field_Collate_Id] ASC, [function_field_id] ASC),
    CONSTRAINT [FK_ui_Function_Field_Value_ui_Function_Field] FOREIGN KEY ([function_field_id]) REFERENCES [dbo].[UI_Function_Field] ([function_field_id])
);


GO

