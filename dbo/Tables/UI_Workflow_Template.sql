CREATE TABLE [dbo].[UI_Workflow_Template] (
    [Workflow_Template_Id] INT           IDENTITY (1, 1) NOT NULL,
    [Template_Short_Desc]  VARCHAR (50)  NULL,
    [Template_Long_Desc]   VARCHAR (500) NULL,
    [Function_Tab_id]      INT           NULL,
    [System_Id]            INT           NULL,
    PRIMARY KEY CLUSTERED ([Workflow_Template_Id] ASC),
    CONSTRAINT [FK_Function_Tab_id] FOREIGN KEY ([Function_Tab_id]) REFERENCES [dbo].[UI_Function_Tab] ([Function_Tab_Id])
);


GO

