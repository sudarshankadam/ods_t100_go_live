CREATE TABLE [dbo].[User_Master] (
    [User_Info_Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [Login_Id]                      VARCHAR (80)  NOT NULL,
    [email]                         VARCHAR (60)  NULL,
    [First_Name]                    VARCHAR (25)  NOT NULL,
    [Last_Name]                     VARCHAR (25)  NOT NULL,
    [Organization_Id]               VARCHAR (80)  NULL,
    [Department]                    VARCHAR (25)  NULL,
    [User_Type]                     VARCHAR (100) NULL,
    [User_Type_Id]                  VARCHAR (30)  NULL,
    [User_Status]                   VARCHAR (15)  NULL,
    [Created_Date_Time]             DATETIME      NULL,
    [Modified_Date_Time]            DATETIME      NULL,
    [Primary_Role_Id]               INT           NULL,
    [ip_Address]                    VARCHAR (50)  NULL,
    [referer_Url]                   VARCHAR (30)  NULL,
    [Is_Online]                     VARCHAR (10)  NULL,
    [messages_token]                VARCHAR (250) NULL,
    [messages_feature]              VARCHAR (1)   NULL,
    [Organization_Name]             VARCHAR (80)  NULL,
    [Login_Attempt_Number]          INT           DEFAULT ((0)) NULL,
    [Login_Attempt_Failed_Number]   INT           CONSTRAINT [df_Login_Attempt_Failed_Number] DEFAULT ((0)) NULL,
    [Login_Status_Password_Issue]   VARCHAR (15)  NULL,
    [Login_Last_Failed_Attempt_Dt]  DATETIME      NULL,
    [Login_Last_Success_Dt]         DATETIME      NULL,
    [Login_Lifetime_Success_Number] INT           CONSTRAINT [df_Login_Lifetime_Success_Number] DEFAULT ((0)) NULL,
    [Login_First_Failed_Attempt_Dt] DATETIME      NULL,
    [Last_Logged_In]                DATETIME      NULL,
    [Customer_Prime_Agency_Id]      VARCHAR (25)  NULL,
    [Created_By]                    VARCHAR (80)  NULL,
    [Approved_By]                   VARCHAR (80)  NULL,
    [Primary_System_Id]             INT           NULL,
    CONSTRAINT [PK__User_Mas__579FA59C5629CD9C] PRIMARY KEY CLUSTERED ([User_Info_Id] ASC)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [ix_User_Master_Login_ID]
    ON [dbo].[User_Master]([Login_Id] ASC);


GO

