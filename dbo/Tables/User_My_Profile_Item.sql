CREATE TABLE [dbo].[User_My_Profile_Item] (
    [my_profile_id_item]       INT          IDENTITY (1, 1) NOT NULL,
    [my_profile_id]            INT          NOT NULL,
    [item_property_value]      VARCHAR (80) NULL,
    [item_property_value_Desc] VARCHAR (80) NULL,
    [update_timestamp]         DATE         NULL,
    [system_id]                INT          NULL,
    CONSTRAINT [PK_my_profile_id_item_Id_Master] PRIMARY KEY CLUSTERED ([my_profile_id_item] ASC),
    CONSTRAINT [FK_my_profile_id] FOREIGN KEY ([my_profile_id]) REFERENCES [dbo].[User_My_Profile] ([my_profile_id]),
    CONSTRAINT [FK_User_My_Profile_Item_SPID] FOREIGN KEY ([my_profile_id]) REFERENCES [dbo].[User_My_Profile] ([my_profile_id])
);


GO

