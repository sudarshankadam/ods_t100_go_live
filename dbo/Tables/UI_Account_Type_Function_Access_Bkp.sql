CREATE TABLE [dbo].[UI_Account_Type_Function_Access_Bkp] (
    [Account_Type_Id_Function_ID]         INT          NOT NULL,
    [System_Id]                           INT          NOT NULL,
    [Account_Type_id]                     INT          NOT NULL,
    [Function_Tab_id]                     INT          NOT NULL,
    [Function_Id]                         INT          NOT NULL,
    [Access_Level]                        VARCHAR (20) NULL,
    [Access_Description]                  VARCHAR (50) NULL,
    [Next_Mandatory_Function_Id]          INT          NULL,
    [Next_Mandatory_Function_Description] VARCHAR (50) NULL,
    [Next_Mandatory_Function_Label]       VARCHAR (50) NULL,
    [Prev_Brokerage_Code]                 CHAR (8)     NULL
);


GO

