CREATE TABLE [dbo].[UI_Field] (
    [Field_Id]          INT           NOT NULL,
    [Field_Code]        VARCHAR (30)  NULL,
    [Field_Description] VARCHAR (500) NULL,
    CONSTRAINT [PK_low_level_field] PRIMARY KEY CLUSTERED ([Field_Id] ASC)
);


GO

