CREATE TABLE [dbo].[Account_Item] (
    [Account_Id_Item]             INT           NOT NULL,
    [Account_Id]                  INT           NULL,
    [ACCN_AGNT_Id]                VARCHAR (20)  NULL,
    [Account_It_Item_Description] VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([Account_Id_Item] ASC),
    CONSTRAINT [FK_Account_Id_Item_Account_Id_Master] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[Account_Master] ([Account_Id])
);


GO

