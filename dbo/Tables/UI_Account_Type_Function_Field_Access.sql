CREATE TABLE [dbo].[UI_Account_Type_Function_Field_Access] (
    [Account_Type_Id_Function_Field_ID] INT          NULL,
    [System_id]                         INT          NOT NULL,
    [Account_Type_id]                   INT          NOT NULL,
    [Function_Tab_id]                   INT          NOT NULL,
    [Function_Id]                       INT          NOT NULL,
    [Field_id]                          INT          NOT NULL,
    [Access_Level]                      VARCHAR (30) NULL,
    [Access_Description]                VARCHAR (50) NULL,
    CONSTRAINT [PK_Ui_Account_Type_Function_Field_Access] PRIMARY KEY CLUSTERED ([System_id] ASC, [Account_Type_id] ASC, [Function_Tab_id] ASC, [Function_Id] ASC, [Field_id] ASC),
    CONSTRAINT [FK_Ui_Account_Type_Function_Field_Access_ui_Account_Type_Function_Access] FOREIGN KEY ([System_id], [Account_Type_id], [Function_Tab_id], [Function_Id]) REFERENCES [dbo].[UI_Account_Type_Function_Access] ([System_Id], [Account_Type_id], [Function_Tab_id], [Function_Id]),
    CONSTRAINT [FK_Ui_Account_Type_Function_Field_Access_ui_field] FOREIGN KEY ([Field_id]) REFERENCES [dbo].[UI_Field] ([Field_Id])
);


GO

