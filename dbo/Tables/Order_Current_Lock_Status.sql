CREATE TABLE [dbo].[Order_Current_Lock_Status] (
    [order_id]                      VARCHAR (100) NOT NULL,
    [company_id]                    VARCHAR (100) NOT NULL,
    [current_edit_lock_status]      VARCHAR (100) NULL,
    [current_last_update_date_time] DATETIME      NULL,
    [current_user_id_lock]          VARCHAR (100) NULL,
    [current_unlock_reason]         VARCHAR (100) NULL,
    CONSTRAINT [Order_Current_Lock_Status_PK] PRIMARY KEY CLUSTERED ([order_id] ASC, [company_id] ASC)
);


GO

