CREATE TABLE [dbo].[UI_Account_Type_Function_Tab_Access] (
    [Account_Type_Id_Function_ID] INT          NOT NULL,
    [System_Id]                   INT          NOT NULL,
    [Account_Type_id]             INT          NOT NULL,
    [Function_tab_Id]             INT          NOT NULL,
    [Access_Level]                VARCHAR (20) NULL,
    [Access_Description]          VARCHAR (50) NULL,
    CONSTRAINT [PK_ui_Account_Type_Function_Tab_Access] PRIMARY KEY CLUSTERED ([Account_Type_id] ASC, [Function_tab_Id] ASC, [System_Id] ASC),
    CONSTRAINT [FK_ui_Account_Type_Function_Tab_Access_System_Account_Type] FOREIGN KEY ([System_Id], [Account_Type_id]) REFERENCES [dbo].[System_Account_Type] ([System_Id], [Account_Type_Id]),
    CONSTRAINT [FK_ui_Account_Type_Function_Tab_Access_ui_Function_Tab] FOREIGN KEY ([Function_tab_Id]) REFERENCES [dbo].[UI_Function_Tab] ([Function_Tab_Id])
);


GO

