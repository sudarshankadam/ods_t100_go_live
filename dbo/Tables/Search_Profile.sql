CREATE TABLE [dbo].[Search_Profile] (
    [Search_Profile_Id]       INT           NOT NULL,
    [Search_Profile_Name]     VARCHAR (200) NULL,
    [Profile_DB_Source]       VARCHAR (200) NULL,
    [Profile_Create_Date]     DATE          NULL,
    [Profile_Update_date]     DATE          NULL,
    [Profile_Created_User_Id] VARCHAR (40)  NULL,
    [system_id]               INT           NULL,
    CONSTRAINT [PK_Search_Profile_Id_Master] PRIMARY KEY CLUSTERED ([Search_Profile_Id] ASC)
);


GO

