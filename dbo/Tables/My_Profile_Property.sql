CREATE TABLE [dbo].[My_Profile_Property] (
    [property_id]              INT           NOT NULL,
    [property_Desc]            VARCHAR (80)  NULL,
    [property_Type]            VARCHAR (80)  NULL,
    [property_DB_Refrence]     VARCHAR (120) NULL,
    [system_id]                INT           NULL,
    [Parent_Property_id]       INT           NULL,
    [Property_Hierarchy_Level] INT           NULL,
    [Property_Status]          VARCHAR (8)   NULL,
    [Property_Access_Status]   VARCHAR (8)   NULL,
    [default_value]            VARCHAR (200) NULL,
    CONSTRAINT [PK_property_id_Master] PRIMARY KEY CLUSTERED ([property_id] ASC)
);


GO

