CREATE TABLE [dbo].[TempAccTypeFuncAccess] (
    [Account_Type_Id_Function_ID]         FLOAT (53)     NULL,
    [System_Id]                           FLOAT (53)     NULL,
    [Account_Type_id]                     FLOAT (53)     NULL,
    [Function_Tab_id]                     FLOAT (53)     NULL,
    [Function_Id]                         FLOAT (53)     NULL,
    [Access_Level]                        NVARCHAR (255) NULL,
    [Access_Description]                  NVARCHAR (255) NULL,
    [Next_Mandatory_Function_Id]          FLOAT (53)     NULL,
    [Next_Mandatory_Function_Description] NVARCHAR (255) NULL,
    [Next_Mandatory_Function_Label]       NVARCHAR (255) NULL,
    [Prev_Brokerage_Code]                 NVARCHAR (255) NULL,
    [F12]                                 NVARCHAR (255) NULL,
    [Account_type_Description]            NVARCHAR (255) NULL,
    [Function_Tab_Description]            NVARCHAR (255) NULL,
    [Function_Description]                NVARCHAR (255) NULL
);


GO

