CREATE TABLE [dbo].[Search_Profile_Item] (
    [Search_Profile_Id]                INT           NOT NULL,
    [Search_Profile_Item_Id]           INT           NOT NULL,
    [DB_Table_View_Name]               VARCHAR (100) NULL,
    [Column_Group_Screen_Label]        VARCHAR (100) NULL,
    [DB_Column_Group]                  VARCHAR (100) NULL,
    [Db_Column_Name]                   VARCHAR (100) NULL,
    [DB_Comparison_Allowed]            VARCHAR (50)  NULL,
    [DB_Comparison_Allowed_Additional] VARCHAR (50)  NULL,
    [Profile_Item_Create_Date]         DATE          NULL,
    [Profile_Item_Update_Date]         DATE          NULL,
    [system_id]                        INT           NULL,
    CONSTRAINT [PK_Search_Profile_Item_Id_Master] PRIMARY KEY CLUSTERED ([Search_Profile_Id] ASC, [Search_Profile_Item_Id] ASC),
    CONSTRAINT [FK_Search_Profile_Item] FOREIGN KEY ([Search_Profile_Id]) REFERENCES [dbo].[Search_Profile] ([Search_Profile_Id])
);


GO

