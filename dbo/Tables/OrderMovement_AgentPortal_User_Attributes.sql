CREATE TABLE [dbo].[OrderMovement_AgentPortal_User_Attributes] (
    [order_id]                          CHAR (8)     NOT NULL,
    [movement_id]                       CHAR (32)    NOT NULL,
    [agent_portal_capacity_rep]         VARCHAR (80) NULL,
    [agent_portal_capacity_rep_date]    DATETIME     NULL,
    [agent_portal_dispatcher]           VARCHAR (80) NULL,
    [agent_portal_dispatcher_date]      DATETIME     NULL,
    [agent_portal_operations_user]      VARCHAR (80) NULL,
    [agent_portal_operations_user_date] DATETIME     NULL,
    [agent_portal_movement_lock_user]   VARCHAR (80) NULL,
    [agent_portal_movement_lock_ind]    CHAR (1)     NULL,
    [agent_portal_movement_unlock_time] DATETIME     NULL
);


GO

