CREATE TABLE [dbo].[User_Profile] (
    [User_profile_id]            INT           IDENTITY (1, 1) NOT NULL,
    [User_profile_name]          VARCHAR (200) NULL,
    [created_by]                 VARCHAR (100) NULL,
    [Profile_Create_Date]        DATE          NULL,
    [User_Profile_Status]        VARCHAR (30)  NULL,
    [User_Organization_Id]       VARCHAR (80)  NULL,
    [mcld_User_Type]             VARCHAR (30)  NULL,
    [mcld_agency_id]             VARCHAR (30)  NULL,
    [mcld_Customer_id]           VARCHAR (30)  NULL,
    [mcld_system_id]             INT           NULL,
    [User_Profile_Access_Status] VARCHAR (100) NULL,
    CONSTRAINT [PK_User_Profile_Id_Master] PRIMARY KEY CLUSTERED ([User_profile_id] ASC)
);


GO

CREATE NONCLUSTERED INDEX [index_Creator_User_Profile]
    ON [dbo].[User_Profile]([created_by] ASC) WITH (FILLFACTOR = 100);


GO

