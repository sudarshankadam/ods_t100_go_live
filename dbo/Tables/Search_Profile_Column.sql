CREATE TABLE [dbo].[Search_Profile_Column] (
    [Search_Profile_Column_Id]   INT            NOT NULL,
    [Search_Profile_Column_Name] VARCHAR (200)  NULL,
    [Search_Profile_Id]          INT            NOT NULL,
    [Profile_Create_Date]        DATE           NULL,
    [Profile_Update_date]        DATE           NULL,
    [Profile_Created_User_Id]    VARCHAR (40)   NULL,
    [system_id]                  INT            NULL,
    [Type]                       VARCHAR (10)   NULL,
    [Logic]                      VARCHAR (2000) NULL,
    CONSTRAINT [PK_Search_Profile_Column] PRIMARY KEY CLUSTERED ([Search_Profile_Column_Id] ASC)
);


GO

