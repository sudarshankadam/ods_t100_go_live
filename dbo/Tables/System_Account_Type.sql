CREATE TABLE [dbo].[System_Account_Type] (
    [System_Id]             INT NOT NULL,
    [Account_Type_Id]       INT NOT NULL,
    [Relative_Access_Level] INT NULL,
    PRIMARY KEY CLUSTERED ([System_Id] ASC, [Account_Type_Id] ASC),
    CONSTRAINT [FK_System_Account_Type_Account_Type] FOREIGN KEY ([Account_Type_Id]) REFERENCES [dbo].[Account_Type] ([Account_Type_Id]),
    CONSTRAINT [FK_System_Account_Type_System] FOREIGN KEY ([System_Id]) REFERENCES [dbo].[System] ([System_Id])
);


GO

