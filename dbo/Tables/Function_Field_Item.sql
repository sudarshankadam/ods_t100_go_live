CREATE TABLE [dbo].[Function_Field_Item] (
    [Function_Field_Id_Item]             INT           NOT NULL,
    [Function_Field_Id]                  INT           NULL,
    [Dependent_Field_Code]               VARCHAR (50)  NULL,
    [Contract_OMS_Status_Code]           VARCHAR (100) NULL,
    [Function_Field_Item_Description_UI] VARCHAR (100) NULL,
    [Function_Field_Description]         VARCHAR (100) NULL,
    CONSTRAINT [PK_Function_Field_Item] PRIMARY KEY CLUSTERED ([Function_Field_Id_Item] ASC),
    CONSTRAINT [FK_Function_Field_Item_Function_Field] FOREIGN KEY ([Function_Field_Id]) REFERENCES [dbo].[Function_Field] ([Function_Field_Id])
);


GO

