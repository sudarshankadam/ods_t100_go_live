CREATE TABLE [dbo].[Order_AgentPortal_User_Attributes] (
    [order_id]                          CHAR (8)     NOT NULL,
    [agent_portal_create_user]          VARCHAR (80) NULL,
    [agent_portal_create_user_date]     DATETIME     NULL,
    [agent_portal_operations_user]      VARCHAR (80) NULL,
    [agent_portal_operations_user_date] DATETIME     NULL
);


GO

