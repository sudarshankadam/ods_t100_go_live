CREATE TABLE [dbo].[Field] (
    [field_id]          INT          NOT NULL,
    [field_code]        VARCHAR (50) NULL,
    [field_description] VARCHAR (50) NULL,
    CONSTRAINT [PK_Field] PRIMARY KEY CLUSTERED ([field_id] ASC)
);


GO

