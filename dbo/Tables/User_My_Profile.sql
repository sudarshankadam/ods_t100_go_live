CREATE TABLE [dbo].[User_My_Profile] (
    [my_profile_id]      INT          IDENTITY (1, 1) NOT NULL,
    [User_info_id]       INT          NOT NULL,
    [property_id]        INT          NOT NULL,
    [property_value]     VARCHAR (80) NULL,
    [update_timestamp]   DATE         NULL,
    [property_id_status] VARCHAR (8)  NULL,
    [system_id]          INT          NULL,
    CONSTRAINT [PK_User_My_Profile_Id_Master] PRIMARY KEY CLUSTERED ([my_profile_id] ASC),
    CONSTRAINT [FK_Property_id] FOREIGN KEY ([property_id]) REFERENCES [dbo].[My_Profile_Property] ([property_id]),
    CONSTRAINT [FK_User_Info_id] FOREIGN KEY ([User_info_id]) REFERENCES [dbo].[User_Master] ([User_Info_Id])
);


GO

