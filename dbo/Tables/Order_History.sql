CREATE TABLE [dbo].[Order_History] (
    [mcleod_ref_id]        CHAR (32)      NOT NULL,
    [order_id]             CHAR (8)       NOT NULL,
    [movement_id]          CHAR (32)      NULL,
    [order_history_json]   VARCHAR (MAX)  NULL,
    [agent_portal_user_id] VARCHAR (50)   NULL,
    [mcleod_user_id]       VARCHAR (50)   NULL,
    [updated_datetime]     DATETIME       NULL,
    [action_id]            INT            NULL,
    [customer_id]          CHAR (8)       NULL,
    [agent_id]             VARCHAR (50)   NULL,
    [remarks]              VARCHAR (8000) NULL
);


GO

