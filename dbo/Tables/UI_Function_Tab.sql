CREATE TABLE [dbo].[UI_Function_Tab] (
    [Function_Tab_Id]      INT          NOT NULL,
    [Function_Description] VARCHAR (50) NULL,
    [Screen_Label]         VARCHAR (50) NULL,
    [Has_Workflow]         VARCHAR (8)  NULL,
    CONSTRAINT [PK_ui_Function_Tab_1] PRIMARY KEY CLUSTERED ([Function_Tab_Id] ASC)
);


GO

