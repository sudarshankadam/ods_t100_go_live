CREATE TABLE [dbo].[User_search_profile_item] (
    [User_search_profile_id]          INT           NOT NULL,
    [User_search_profile_item_id]     INT           NOT NULL,
    [Search_Profile_id]               INT           NOT NULL,
    [Search_Profile_Item_id]          INT           NOT NULL,
    [DB_Compare_Condition]            VARCHAR (50)  NULL,
    [DB_Compare_Condition_additional] VARCHAR (50)  NULL,
    [DB_Compare_Value]                VARCHAR (200) NULL,
    [User_Profile_Create_Date]        DATE          NULL,
    [User_Profile_Update_Date]        DATE          NULL,
    [system_id]                       INT           NULL,
    CONSTRAINT [PK_User_search_profile_item_Id_Master] PRIMARY KEY CLUSTERED ([User_search_profile_id] ASC, [User_search_profile_item_id] ASC),
    CONSTRAINT [FK_User_search_profile_item_SPID] FOREIGN KEY ([Search_Profile_id], [Search_Profile_Item_id]) REFERENCES [dbo].[Search_Profile_Item] ([Search_Profile_Id], [Search_Profile_Item_Id]),
    CONSTRAINT [FK_User_search_profile_item_UPID] FOREIGN KEY ([User_search_profile_id]) REFERENCES [dbo].[User_Search_Profile] ([User_search_profile_id])
);


GO

