CREATE TABLE [dbo].[Order_Template_SK] (
    [Template_Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Template_Name]     VARCHAR (20)   NOT NULL,
    [customer_id]       VARCHAR (15)   NOT NULL,
    [organization_type] VARCHAR (15)   NOT NULL,
    [Template_Details]  VARCHAR (8000) NOT NULL,
    [Created_By]        VARCHAR (20)   NOT NULL,
    [Created_Date]      VARCHAR (20)   NOT NULL,
    [Active]            VARCHAR (2)    NOT NULL,
    [Agency_Id]         VARCHAR (15)   NULL,
    CONSTRAINT [PK_Order_Template_SK] PRIMARY KEY CLUSTERED ([Template_Id] ASC)
);


GO

