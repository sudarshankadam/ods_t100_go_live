CREATE TABLE [dbo].[UI_Function] (
    [Function_Id]                         INT           NOT NULL,
    [Function_Tab_Id]                     INT           NOT NULL,
    [Function_Description]                VARCHAR (50)  NULL,
    [Screen_Label]                        VARCHAR (50)  NULL,
    [Next_Mandatory_Function_Id]          INT           NULL,
    [Next_Mandatory_Function_Description] VARCHAR (50)  NULL,
    [Next_Mandatory_Function_Label]       VARCHAR (50)  NULL,
    [Prev_Brokerage_Code]                 CHAR (8)      NULL,
    [Addl_Function_Id_Open]               VARCHAR (100) NULL,
    [Function_Access_Level]               VARCHAR (20)  NULL,
    [Function_Group_Description]          VARCHAR (50)  NULL,
    [Brokerage_Status_Range_for_Action]   VARCHAR (30)  NULL,
    [Function_Id_Type]                    VARCHAR (8)   NULL,
    CONSTRAINT [PK_ui_Function_1] PRIMARY KEY CLUSTERED ([Function_Id] ASC),
    CONSTRAINT [FK_ui_Function_ui_Function_Tab] FOREIGN KEY ([Function_Tab_Id]) REFERENCES [dbo].[UI_Function_Tab] ([Function_Tab_Id])
);


GO

