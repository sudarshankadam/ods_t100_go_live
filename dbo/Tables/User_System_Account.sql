CREATE TABLE [dbo].[User_System_Account] (
    [User_Info_Id]               INT          NOT NULL,
    [Account_Id]                 INT          NOT NULL,
    [Account_Assignement_Status] VARCHAR (50) NULL,
    [Account_Assigned_date]      DATETIME     NULL,
    [Account_Last_Update_date]   DATETIME     NULL,
    CONSTRAINT [PK_User_System_Account] PRIMARY KEY CLUSTERED ([User_Info_Id] ASC, [Account_Id] ASC),
    CONSTRAINT [FK_User_System_Account_Account_Id_Master] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[Account_Master] ([Account_Id]),
    CONSTRAINT [FK_User_System_Account_User_Master] FOREIGN KEY ([User_Info_Id]) REFERENCES [dbo].[User_Master] ([User_Info_Id])
);


GO

