CREATE TABLE [dbo].[UI_Workflow_Template_Functions] (
    [Workflow_Template_Function_Id] INT         IDENTITY (1, 1) NOT NULL,
    [Workflow_Template_Id]          INT         NULL,
    [Function_id]                   INT         NULL,
    [Function_id_status]            VARCHAR (8) NULL,
    [Function_id_type]              VARCHAR (8) NULL,
    [System_Id]                     INT         NULL,
    PRIMARY KEY CLUSTERED ([Workflow_Template_Function_Id] ASC),
    CONSTRAINT [FK_Function_Id] FOREIGN KEY ([Function_id]) REFERENCES [dbo].[UI_Function] ([Function_Id]),
    CONSTRAINT [FK_Workflow_Template_Id] FOREIGN KEY ([Workflow_Template_Id]) REFERENCES [dbo].[UI_Workflow_Template] ([Workflow_Template_Id])
);


GO

