CREATE TABLE [dbo].[template_usage] (
    [template_id]             INT          IDENTITY (1, 1) NOT NULL,
    [template_used_by_UserId] VARCHAR (50) NULL,
    [template_used_date]      DATETIME     NULL,
    CONSTRAINT [PK_template_usage] PRIMARY KEY CLUSTERED ([template_id] ASC)
);


GO

