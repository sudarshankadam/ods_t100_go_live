CREATE TABLE [dbo].[User_Search_Profile] (
    [User_search_profile_id]      INT           IDENTITY (1, 1) NOT NULL,
    [User_search_profile_name]    VARCHAR (200) NULL,
    [Search_Profile_Id]           INT           NOT NULL,
    [User_Id]                     VARCHAR (100) NULL,
    [User_Profile_Create_Date]    DATE          NULL,
    [User_Profile_Update_Date]    DATE          NULL,
    [User_Search_Profile_Status]  VARCHAR (30)  NULL,
    [User_Search_Organization_Id] VARCHAR (80)  NULL,
    [User_Type]                   VARCHAR (30)  NULL,
    [agency_id]                   VARCHAR (30)  NULL,
    [Customer_id]                 VARCHAR (30)  NULL,
    [system_id]                   INT           NULL,
    CONSTRAINT [PK_User_Search_Profile_Id_Master] PRIMARY KEY CLUSTERED ([User_search_profile_id] ASC),
    CONSTRAINT [FK_User_Search_Profile] FOREIGN KEY ([Search_Profile_Id]) REFERENCES [dbo].[Search_Profile] ([Search_Profile_Id])
);


GO

CREATE NONCLUSTERED INDEX [index_Creator_User_Search_Profile]
    ON [dbo].[User_Search_Profile]([User_Id] ASC);


GO

