CREATE TABLE [dbo].[Function_Field] (
    [Function_Field_Id]              INT           NOT NULL,
    [Function_Id]                    INT           NULL,
    [Field_id]                       INT           NULL,
    [Function_Field_Description]     VARCHAR (100) NULL,
    [Function_Field_Description_UI]  VARCHAR (100) NULL,
    [Field_Dashboard_Classification] VARCHAR (100) NULL,
    CONSTRAINT [PK_dbo1.Table_2] PRIMARY KEY CLUSTERED ([Function_Field_Id] ASC),
    CONSTRAINT [FK_Function_Field_Field] FOREIGN KEY ([Field_id]) REFERENCES [dbo].[Field] ([field_id]),
    CONSTRAINT [FK_Function_Field_Function_Master] FOREIGN KEY ([Function_Id]) REFERENCES [dbo].[Function_Master] ([function_id])
);


GO

