CREATE TABLE [dbo].[UI_Function_Field] (
    [function_field_id]          INT          NULL,
    [System_Id]                  INT          NOT NULL,
    [function_tab_id]            INT          NOT NULL,
    [Function_Id]                INT          NOT NULL,
    [Field_id]                   INT          NOT NULL,
    [Function_Field_Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_ui_Function_Field] PRIMARY KEY CLUSTERED ([System_Id] ASC, [function_tab_id] ASC, [Function_Id] ASC, [Field_id] ASC),
    CONSTRAINT [AK_ui_function_field_id] UNIQUE NONCLUSTERED ([function_field_id] ASC)
);


GO

