CREATE TABLE [dbo].[Account_Master] (
    [Account_Id]              INT           NOT NULL,
    [System_Id]               INT           NOT NULL,
    [Account_Type_Id]         INT           NOT NULL,
    [Account_Code_Group_Name] VARCHAR (100) NULL,
    [Account_Description]     VARCHAR (100) NULL,
    CONSTRAINT [PK_Account_Id_Master] PRIMARY KEY CLUSTERED ([Account_Id] ASC),
    CONSTRAINT [FK_Account_Id_Master_System_Account_Type] FOREIGN KEY ([System_Id], [Account_Type_Id]) REFERENCES [dbo].[System_Account_Type] ([System_Id], [Account_Type_Id])
);


GO

