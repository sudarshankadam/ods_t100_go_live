CREATE TABLE [dbo].[User_Credentials] (
    [User_Info_Id]      INT           NOT NULL,
    [Password]          VARCHAR (150) NULL,
    [Last_updated_date] DATETIME      NULL,
    [Number_of_update]  INT           NULL,
    [Email_token]       VARCHAR (150) NULL,
    CONSTRAINT [PK_User_Credentials] PRIMARY KEY CLUSTERED ([User_Info_Id] ASC)
);


GO

