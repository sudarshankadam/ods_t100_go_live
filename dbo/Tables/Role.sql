CREATE TABLE [dbo].[Role] (
    [Role_Id]          INT           NOT NULL,
    [Role_Name]        VARCHAR (20)  NOT NULL,
    [Role_Description] VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([Role_Id] ASC)
);


GO

