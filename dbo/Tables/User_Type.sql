CREATE TABLE [dbo].[User_Type] (
    [User_Type]  VARCHAR (50) NOT NULL,
    [User_Group] VARCHAR (50) NULL,
    CONSTRAINT [PK_User_Type] PRIMARY KEY CLUSTERED ([User_Type] ASC)
);


GO

