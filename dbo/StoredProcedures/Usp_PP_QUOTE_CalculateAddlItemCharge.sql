        
CREATE Procedure [dbo].[Usp_PP_QUOTE_CalculateAddlItemCharge]                                              
@ProductTypeIDs text ,                                              
@AccountNumber int,                                          
@Zip varchar(50),                                        
@Country int,                                        
--@Region int,                                              
--@StateID varchar(2), 
@QuoteID int,                            
@PartnerID int,                                              
@DelDate  DATETIME,                                              
@CreatedBy varchar(50)                                            
                                              
As                           
                        
                                         
Declare @Rowcount int                                            
                                             
Declare @XMLArrayDoc int                                                
Declare @CustomerCost float                                              
Declare @PartnerCost float                                              
Declare @AddlItemPercenatge float                                              
Declare @CustomerRateValue float                                              
Declare @RateValue float                                              
Declare @ActivityDescription varchar(50)                                              
Declare @Margin smallmoney                                               
Declare @ProjectRateID int                                              
Declare @ProjectActivitiesID int                                        
Declare @ProductID int                                          
                                        
Declare @TotalCustomerCost float                                              
Declare @TotalPartnerCost float                                              
                                        
Declare @wrkCustomerCost float                                        
Declare @wrkPartnerCost float                                        
Declare @ExcessMilesCustomerCost float                                        
Declare @ExcessMilesPartnerCost float                                        
Declare @PlusDelivCustomerCost float                                        
Declare @PlusDelivPartnerCost float                                        
Declare @WasPlusDeliv int                                        
                                        
Declare @wrk2CustomerCost float                                        
Declare @wrk2PartnerCost float                                        
Declare @HighestCustomerCost float                                        
Declare @HighestPartnerCost float                                        
                                        
Declare @CustomerCost1 float                                              
Declare @CustomerCost2 float                                              
Declare @CustomerCost3 float                                              
Declare @CustomerCost4 float                                              
Declare @CustomerCost5 float                                              
Declare @CustomerCost6 float                                              
Declare @CustomerCost7 float                                              
Declare @CustomerCost8 float                                              
Declare @CustomerCost9 float                                              
Declare @CustomerCost10 float                                              
Declare @CustomerCost11 float                                              
Declare @CustomerCost12 float                                              Declare @CustomerCost13 float                                              
Declare @CustomerCost14 float                              
Declare @CustomerCost15 float                                              
Declare @CustomerCost16 float                                              
Declare @CustomerCost17 float                                              
Declare @CustomerCost18 float                                              
Declare @CustomerCost19 float                                            
Declare @CustomerCost20 float                                              
                                          
Declare @PartnerCost1 float                           
Declare @PartnerCost2 float                                              
Declare @PartnerCost3 float                                              
Declare @PartnerCost4 float                                              
Declare @PartnerCost5 float                                              
Declare @PartnerCost6 float                                              
Declare @PartnerCost7 float                                              
Declare @PartnerCost8 float                              Declare @PartnerCost9 float                                              
Declare @PartnerCost10 float                                              
Declare @PartnerCost11 float                                   
Declare @PartnerCost12 float                                              
Declare @PartnerCost13 float                                              
Declare @PartnerCost14 float                                         
Declare @PartnerCost15 float                                              
Declare @PartnerCost16 float                                              
Declare @PartnerCost17 float                                              
Declare @PartnerCost18 float                                              
Declare @PartnerCost19 float                                              
Declare @PartnerCost20 float                                    
                                              
Declare @Err int                                              
                                              
Declare @ProdTypeCode int                                              
Declare @MaxCustomerCost float                                   
                                              
Declare @Typecode  int                                              
Declare @Count int                                              
Declare @InsertFlag int                                              
                                            
Declare @TotalWeight numeric                                            
Declare @MinCharge float                                              
Declare @MinComp float                                              
-- 7/8/2015: added the next 6 variables to use with Dimensional Weight                                          
Declare @ActualWeight numeric                                            
Declare @DimWeight numeric                                           
Declare @CubicInches numeric(18,2)                                         
Declare @DimWeightUsedFlag tinyint                                        
Declare @DimWeightgFlag int                                        
Declare @DimWeightDivisor int                                        
                                        
                                            
Declare @PermAccess int                                            
Declare @PermAccessID int                                            
Declare @PermAccessQTY  int                                            
Declare @PermAccessID2 int                                            
Declare @PermAccessQTY2  int                                            
Declare @PermAccessID3 int                                            
Declare @PermAccessQTY3  int                                            
Declare @PermAccessID4 int           
Declare @PermAccessQTY4  int                                            
Declare @PermAccessID5 int                                            
Declare @PermAccessQTY5  int                                            
Declare @PermAccessID6 int                                            
Declare @PermAccessQTY6  int                                            
Declare @PermAccessID7 int                                            
Declare @PermAccessQTY7  int                             
Declare @PermAccessID8 int                                            
Declare @PermAccessQTY8  int                                            
Declare @PermAccessID9 int                                            
Declare @PermAccessQTY9  int                                         
Declare @PermAccessID10 int                                            
Declare @PermAccessQTY10  int                                            
-- 9/8/2010: newvariable needed                                        
Declare @wrkPermAccessQTY int                                     
--Declare @ProductQty int                                        
                                        
-- 8/4/2016: new variables needed for the Excess Mileage                                        
-- permanent accessorials.                     
Declare @ExcessMileage int                                        
Declare @ExcessMileageID int                                        
Declare @ExcessMileageQty int                                        
Declare @ExcessMileageStartMiles numeric(18,1)                                         
Declare @ExcessMileageEndMiles numeric(18,1)                                        
Declare @ExcessMileageID2 int                                        
Declare @ExcessMileageQty2 int                                        
Declare @ExcessMileageStartMiles2 numeric(18,1)                                        
Declare @ExcessMileageEndMiles2 numeric(18,1)                                        
Declare @ExcessMileageID3 int                                        
Declare @ExcessMileageQty3 int                                        
Declare @ExcessMileageStartMiles3 numeric(18,1)                                        
Declare @ExcessMileageEndMiles3 numeric(18,1)                                    
-- Greater Mileage Variable declaration --                                  
DECLARE @TemplateId INT                                  
DECLARE @GrtMileage TINYINT                                   
DECLARE @GrtMileLastFreeMiles NUMERIC                                   
DECLARE @GrtMileageID INT                                  
DECLARE @GrtMileageQty INT                                  
                                      
-----------------------------------------                                           
Declare @ActivityID int                                            
Declare @UnitOfMeasureCD varchar(3)                                            
Declare @CustomerRateUnitOfMeasure varchar(3)                                            
Declare @RateActionTypeID int                                            
Declare @PartnerUOM varchar(3)                                        
Declare @CustomerMinimumUOM varchar(3)                                        
Declare @CustomerMinimumRateValue float                                        
Declare @PartnerMinimumUOM varchar(3)                                        
Declare @PartnerMinimumRateValue float                                        
                                        
Declare @DeliveryChargeCost int                                            
Declare @CoordEmail varchar(50)                                                          
Declare @RecordCount  int                                          
Declare @ProductCount int                                          
Declare @Diff  int                                          
-- 10/30/2014: added - needed since we now can have                                         
-- 3 Permanent Accessorials on each product                                        
Declare @PermAccessNumber int                                        
                                          
Declare @OneManRateFlag int                                          
Declare @CustomerOneManDisc float                                          
Declare @PartnerOneManDisc float                                          
                                          
Declare @RatingTypeID int                                          
Declare @ServiceLevel int                                          
Declare @MileageRange int                                        
                                   
Declare @UOM varchar(3)                            
Declare @ExpUOM varchar(3)                                          
Declare @UOMMultiplier int                                           
Declare @EXPUOMMultiplier int                                           
Declare @CategoryID int                                        
Declare @PlusDelivChgFlag int                                        
Declare @StartWgt int                           
Declare @EndWgt int                                      
Declare @wrkWeight float                                        
                                          
Declare @StartMiles int                                        
Declare @EndMiles int                                        
Declare @MilesPlusDelivChgFlag int                                        
Declare @ExcessMilesUOM varchar(3)                           
Declare @FMMiles float                                        
Declare @wrkMiles float                                        
                                        
Declare @KeepGoing int                                        
Declare @TotalCube numeric(12,2)                                          
Declare @Quantity int                                           
Declare @Weight int                            
Declare @Pieces int                                  
Declare @Cube numeric(9,2)                                          
Declare @Parts int                                        
                                          
Declare @TotalQuantity int                                        
Declare @TotalPieces int                                        
Declare @Length numeric(18,2)                                          
Declare @Width numeric(18,2)                
Declare @TotalLinearFeet numeric(18,2)                                          
Declare @TotalSquareFeet numeric(18,2)                                          
                                        
-- 9/5/2014: these are new to be used by the product accessorials                                        
Declare @ProductWeight numeric                                              
Declare @ProductCube numeric(12,2)                                          
Declare @ProductQuantity int                   
Declare @ProductPieces int                                        
Declare @ProductSquareFeet numeric(18,2)                                          
Declare @ProductLinearFeet numeric(18,2)                                                   
                                        
Declare @Customer2ndItemDisc numeric(18,2)                                         
Declare @Customer3rdItemDisc numeric(18,2)                                         
Declare @Customer4thItemDisc numeric(18,2)                                         
Declare @Customer5thItemDisc numeric(18,2)                                         
Declare @Customer6thItemDisc numeric(18,2)                                         
                                        
Declare @SecondItemDiscount numeric(18,2)                                         
Declare @ThirdItemDiscount numeric(18,2)                                         
Declare @FourthItemDiscount numeric(18,2)                                         
Declare @FifthItemDiscount numeric(18,2)                                         
Declare @SixthItemDiscount numeric(18,2)                       
                                        
Declare @Region int                                        
Declare @Region2 int                                        
Declare @Region3 int                                        
Declare @Region4 int                                        
Declare @Region5 int                                        
Declare @Region6 int                                        
Declare @Region7 int                            
Declare @Region8 int                                        
Declare @Region9 int                                        
Declare @Region10 int                                        
Declare @Region11 int                                        
Declare @Region12 int                                        
Declare @Region13 int                                        
Declare @Region14 int                                        
Declare @Region15 int                         
Declare @Region16 int                                        
Declare @Region17 int                                        
Declare @Region18 int                                        
Declare @Region19 int                             
Declare @Region20 int                                        
Declare @Region21 int                                        
Declare @Region22 int                                        
Declare @Region23 int                                        
Declare @Region24 int                       
Declare @Region25 int                                        
Declare @Region26 int                                        
Declare @BlendedRegion int                                        
                                        
-- 5/7/2007: we will now recalculate remote charges in this procedure                                        
Declare @RemoteRev float                                          
Declare @RemoteRev2 float                                          
Declare @RemoteRev3 float                                          
Declare @RemoteRev4 float                                          
Declare @RemoteRev5 float                                          
Declare @RemoteRev6 float                                          
Declare @RemoteRev7 float                                          
Declare @RemoteRev8 float                                          
Declare @RemoteRev9 float                                          
Declare @RemoteRev10 float                                          
Declare @RemoteExp float                                          
Declare @RateStateID varchar(2)                              
Declare @VariableRemoteRevFlag int                                        
                                        
--3/10/2011: new fields                                        
Declare @RemoteRevB float                                          
Declare @RemoteRevC float                                          
Declare @RemoteRevD float                                          
Declare @RemoteRevE float                                          
Declare @RemoteRevF float                                          
Declare @RemoteRevG float                                          
Declare @RemoteRevH float                              
Declare @RemoteRevI float                                          
Declare @RemoteRevJ float                                          
Declare @RemoteRevK float                                          
Declare @RemoteRevL float                                          
Declare @RemoteRevM float                                          
Declare @RemoteRevN float                                          
Declare @RemoteRevO float                                          
Declare @RemoteRevP float                                          
Declare @RemoteRevQ float                                          
Declare @RemoteRevR float                                          
Declare @RemoteRevS float                                          
Declare @RemoteRevT float                                          
Declare @RemoteRevU float                                          
Declare @RemoteRevV float                                          
Declare @RemoteRevW float                                          
Declare @RemoteRevX float                                          
Declare @RemoteRevY float                                          
Declare @RemoteRevZ float                                          
Declare @RemoteRevBlended float                                          
                                        
Declare @YellowFlag int                                            
Declare @Network varchar(1)                                        
                               
--Declare @Zip varchar(50)                                        
--Declare @Country int                                        
Declare @RemoteDiscount float                                          
Declare @RemoteCompDiscount float                                          
Declare @BusinessTypeID int                                        
                                        
-- 10/8/2007: new fields for valuation                                        
Declare @ValFlag int                                        
Declare @ValRate float                                        
Declare @ValRateType varchar(10)                                        
Declare @ValMinChg float                                        
Declare @ValFreeCoverage float                                        
Declare @ValFreeCoverageType varchar(10)                                        
Declare @RCPValue float                                        
Declare @ValuationCharge float                                        
Declare @MinimumUsed int                                        
--sELECT * from #tempProducts                                                                              
Declare @ExtractDate datetime                                        
                                        
Declare @ActualDate varchar(50)                                        
Declare @ActualTime varchar(50)                                        
Declare @POD varchar(50)                                        
                                        
Declare @ItemNumber int                                        
                                        
                                      
Declare @DeliveryChargeActivityID int                                        
                                    
Declare @PickupPartner int                                        
Declare @OrderTypeID int                                        
                                      
Declare @LocOfProduct varchar(50)                                        
                                        
                                        
                                  
Declare @SpecialCompFlag int                                        
Declare @SpecialCompAmount float                                        
Declare @SpecialCompActivityID int                                        
Declare @SpecialCompDescription varchar(50)                          
Declare @SpecialCompExists int                                        
                                        
--delete from #tempProducts where productid = 368                      
                  
BEGIN TRY                                      
                                          
Create Table #tempProducts(                                                
 ProductID int,                                          
 Quantity int,                                          
 Weight int,                                          
 Pieces int,                                          
 iCube numeric(9,2),                                        
 Parts int,           
 Height numeric(9,2),                                        
 Width numeric(9,2),                                        
 Length numeric(9,2)                                              
)                                                
                              
Create Table #tempAcc(                                                
 ProductID int,                                          
 PermAccessID int,                                                
 PermAccessQTY int,                                        
 PermAccessNumber int,                          
 PartnerId int                                       
)                                                
                                          
/* Delivery Charge calculation starts */                                          
EXEC sp_xml_preparedocument @XMLArrayDoc output, @ProductTypeIDs                                                
                                          
-- 7/8/2015: V = Height W Width X = Length                                        
                                        
insert into #tempProducts                                              
SELECT  I,J,K,L,M,N,V,W,X FROM OPENXML (@XMLArrayDoc, '/A/E', 1) WITH (I int,J int,K int,L int,M numeric(9,2),N int, V numeric(9,2), W numeric(9,2), X numeric(9,2))                                                
                                               
EXEC sp_xml_removedocument @XMLArrayDoc                                                
                                          
--Select @TotalWeight = sum(p.weight) from #tempProducts tp, product p                                            
--where tp.productid = p.producttypeid                                             
                                          
Select @ActualWeight = sum(tp.weight * tp.quantity) from #tempProducts tp                                          
Select @TotalQuantity = sum(tp.quantity) from #tempProducts tp                                          
Select @TotalPieces = sum(tp.pieces * tp.quantity) from #tempProducts tp                                          
Select @CubicInches = Sum(isnull(tp.Length,0) * isnull(tp.Width,0) * isnull(tp.Height,0))from #tempProducts tp                                          
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@CubicInches=',@CubicInches, @QuoteID, GetDate(),'bergc')                                        
                                        
-- 7/8/2015: get the DimWeightFlag to see if this customer uses it                                        
Select @DimWeightgFlag = isnull(DimensionWgtFlag,0),                                        
  @DimWeightDivisor = isnull(DimensionWgtDivisor,1)                                        
From [HDN_Providence].[dbo].[Customer]                                        
Where CustomerID = @AccountNumber                                        
                                        
If @DimWeightgFlag = 1 and @DimWeightDivisor <> 0                                        
  Begin                                        
 Set @DimWeight = @CubicInches / @DimWeightDivisor                                      
  End                                        
Else                    
  Begin                                     
 Set @DimWeight = 0                                        
  End                                        
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@DimWeight=',@DimWeight, @QuoteID, GetDate(),'bergc')                                        
                                         
Set @DimWeightUsedFlag = 0                                        
Set @TotalWeight = @ActualWeight                                        
                                        
If @ActualWeight < @DimWeight                             
  Begin                             
 Set @TotalWeight = @DimWeight                                        
 Set @DimWeightUsedFlag = 1                                        
  End                                        
                                         
Select @OrderTypeID = OrderTypeID,                                        
  @ServiceLevel = ServiceLevelID,                                         
  @FMMiles = FMMiles,                                        
  @LocOfProduct = LocOfProduct, 
  -- Umakanth change as required                                           
  @OneManRateFlag = OneManRateFlag,                                        
  @RCPValue = isnull(RCPValue, 0)                                        
  --@ActualDate = ActualDate,                                  
  --@ActualTime = ActualTime,                                         
  --@POD = POD                                        
From Quote                                         
Where QuoteID = @QuoteID                                     
                                         
-- just in case, avoid null                                        
Select @OrderTypeID = ISNULL(@OrderTypeID,0)                                        
Select @LocOfProduct = ISNULL(@LocOfProduct,'')                                        
                                        
-- 5/22/2014: change this to < 1                                        
--If @FMMiles <= 0                                        
If @FMMiles < 1                                        
  Begin                                        
 Set @FMMiles = 1                                        
  End                                        
                          
                         
If @OrderTypeID = 4 and @LocOfProduct <> 'Residence'                                        
  Begin                                        
 goto SkipRemoteChargeCalc                                        
  End                                        
                                        
                                         
-- 1/9/2012: we now allow the user to define the delivery charge                                        
-- activity id by service level.                                        
Select @DeliveryChargeActivityID = ActivityID                                        
From [HDN_Providence].[dbo].AccountServiceLevel                                        
Where CustomerID = @AccountNumber                                        
And ServiceLevelID = @ServiceLevel                                        
                                  
If ISNULL(@DeliveryChargeActivityID,0) = 0                                        
  Begin                                        
 Set @DeliveryChargeActivityID = 37                                        
  End          
        
--Add orderswap activity ID for Return Pickups for TempurSealy          
Declare @PO as varchar(255)        
Declare @Cref as varchar(255)        
        
SELECT @PO = PONumber, @Cref = CustRefNumber from Project WITH(NOLOCK) where CustomerId = 2843 AND QuoteID = @QuoteID and CustRefNumber like '5%'  
       
if LTRIM(rtrim(@PO)) = LTRIM(RTRIM(@Cref))        
Begin        
 Set @DeliveryChargeActivityID = 57        
End                                        
                                          
-- *** for RatingTypeIDs 3 and 4, we won't do it this way.                                        
-- we will need to handle each product separately.                                        
SELECT @TypeCode = TypeCode, @UOM = UnitofMeasureCD, @ExpUOM = ExpUnitofMeasureCD,                                        
  @CategoryID = CategoryID, @PlusDelivChgFlag = PlusDelivChgFlag,                                        
  @StartWgt = StartWgt, @EndWgt = EndWgt                                        
FROM [HDN_Providence].[dbo].AccountWgtBreaks                                         
WHERE CustomerID = @AccountNumber                                             
AND @TotalWeight between startwgt and endwgt                                            
ORDER BY TypeCode         
        
 --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@490=',CAST( @TypeCode as Varchar(10)) + ' ' + CAST(@TotalWeight as VARCHAR(10)), @QuoteID, GetDate(),'Anudeep')                                           
                                          
SELECT @MileageRange = TypeCode, @StartMiles = StartMiles, @EndMiles = EndMiles,                                        
  @MilesPlusDelivChgFlag = PlusDelivChgFlag, @ExcessMilesUOM = ExcessMilesUOM                                        
FROM [HDN_Providence].[dbo].AccountMileageBreaks                                    
WHERE CustomerID = @AccountNumber                                          
-- round the final mile miles in case it is something like 50.4 and the mileage                                        
-- ranges are defined as 0-50 and 51-9999.                                        
AND Round(@FMMiles,0) between StartMiles and EndMiles                                            
ORDER BY TypeCode           
        
 --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@502=',CAST(@FMMiles as Varchar(10)), @QuoteID, GetDate(),'Anudeep')                                         
                                           
-- get all of the fields we need from the customer table here                                        
SELECT @RatingTypeID = IsNull(RatingTypeID, 1),                                         
 @VariableRemoteRevFlag = VariableRemoteRevFlag,                                        
 @BusinessTypeID = BusinessTypeID,           
 -- 3/10/2011: need these for RemoteRev by network                                        
 @YellowFlag = IsNull(YellowFlag, 0),                                        
 @Network = NetworkID,                                        
 -- 10/8/2007: get the new fields for valuation while we are at it                                        
 @ValFlag = ValuationFlag,                                        
 @ValRate = isnull(ValuationRate,0),                                        
 @ValRateType = isnull(ValuationRatingType,''),                                        
 @ValMinChg = isnull(ValuationMinimumCharge,0),                                        
 @ValFreeCoverage = isnull(ValuationFreeCoverage,0),                                        
 @ValFreeCoverageType = isnull(ValuationFreeCoverageType,''),                                        
 @MinCharge = MinimumCharge,                                         
 @MinComp = MinimumComp,                                         
 @CustomerOneManDisc = OneManRateDisc,                         
 @Customer2ndItemDisc = SecondItemDiscount,                                        
 @Customer3rdItemDisc = ThirdItemDiscount,                                         
 @Customer4thItemDisc = FourthItemDiscount,                                        
 @Customer5thItemDisc = FifthItemDiscount,                                        
 @Customer6thItemDisc = SixthItemDiscount                                        
FROM [HDN_Providence].[dbo].Customer                                         
WHERE CustomerID = @AccountNumber                                          
                                          
----------------------------------------------------------------------------------------                                        
-- 5/7/2007: calculate remote charges                                        
                                        
/*             
-- but we need zip and country                                
SELECT @Zip = Zip, @Country = CountryID                                        
FROM Project, Address                     
WHERE Project.AddressID = Address.AddressID                                        
AND Project.QuoteID = @QuoteID                                        
*/                                        
                                        
-- 10/2/2012: if US, avoid issues with 9 digit zip                                        
If @Country = 1                                        
 Begin                                        
 Set @Zip = LEFT(@Zip, 5)                                        
 End                                        
                                        
                                        
-- get the Remote Charge value and insert/update the ProjectActivities table                                            
If @Country = 2                                        
 Begin                                        
 SELECT                                         
 @Region=region,                                         
 @RemoteRev=isnull(RemoteRev,0),                                         
 @RemoteExp=isnull(RemoteExp,0), @RateStateID=StateID                            
 FROM [HDN_Providence].[dbo].CanadianRates                                        
 WHERE @Zip between startzip and endzip                                        
                                        
 Select @RowCount = @@RowCount                                        
 End                                        
else                                        
 Begin                                        
 SELECT                                         
 @Region=Region, @Region2=RegionB, @Region3=RegionC, @Region4=RegionD, @Region5=RegionE,                                        
 @Region6=RegionF, @Region7=RegionG, @Region8=RegionH, @Region9=RegionI, @Region10=RegionJ,                                 
 @Region11=RegionK, @Region12=RegionL, @Region13=RegionM, @Region14=RegionN, @Region15=RegionO,                                          
 @Region16=RegionP, @Region17=RegionQ, @Region18=RegionR, @Region19=RegionS, @Region20=RegionT,                                          
 @Region21=RegionU, @Region22=RegionV, @Region23=RegionW, @Region24=RegionX, @Region25=RegionY,                
 @Region26=RegionZ, @BlendedRegion=BlendedRegion,                     
 @RemoteRev=isnull(RemoteRev,0),                                         
 @RemoteRev2=isnull(RemoteRev2,0), @RemoteRev3=isnull(RemoteRev3,0), @RemoteRev4=isnull(RemoteRev4,0),                                        
 @RemoteRev5=isnull(RemoteRev5,0), @RemoteRev6=isnull(RemoteRev6,0), @RemoteRev7=isnull(RemoteRev7,0),                                         
 @RemoteRev8=isnull(RemoteRev8,0), @RemoteRev9=isnull(RemoteRev9,0), @RemoteRev10=isnull(RemoteRev10,0),                                         
 @RemoteRevB=isnull(RemoteRevB,0),@RemoteRevC=isnull(RemoteRevC,0),@RemoteRevD=isnull(RemoteRevD,0),                                           
 @RemoteRevE=isnull(RemoteRevE,0),@RemoteRevF=isnull(RemoteRevF,0),@RemoteRevG=isnull(RemoteRevG,0),                                           
 @RemoteRevH=isnull(RemoteRevH,0),@RemoteRevI=isnull(RemoteRevI,0),@RemoteRevJ=isnull(RemoteRevJ,0),                                           
 @RemoteRevK=isnull(RemoteRevK,0),@RemoteRevL=isnull(RemoteRevL,0),@RemoteRevM=isnull(RemoteRevM,0),                                           
 @RemoteRevN=isnull(RemoteRevN,0),@RemoteRevO=isnull(RemoteRevO,0),@RemoteRevP=isnull(RemoteRevP,0),                                           
 @RemoteRevQ=isnull(RemoteRevQ,0),@RemoteRevR=isnull(RemoteRevR,0),@RemoteRevS=isnull(RemoteRevS,0),                                           
 @RemoteRevT=isnull(RemoteRevT,0),@RemoteRevU=isnull(RemoteRevU,0),@RemoteRevV=isnull(RemoteRevV,0),                                           
 @RemoteRevW=isnull(RemoteRevW,0),@RemoteRevX=isnull(RemoteRevX,0),@RemoteRevY=isnull(RemoteRevY,0),                                           
 @RemoteRevZ=isnull(RemoteRevZ,0),@RemoteRevBlended=isnull(BlendedRemoteRev,0),                                        
 @RemoteExp=isnull(RemoteExp,0), @RateStateID=StateID                        
FROM [HDN_Providence].[dbo].HXRates                                        
 WHERE Zip = @Zip                                        
                                        
 Select @RowCount = @@RowCount                                          
                                        
 If @YellowFlag = 1                                            
  Begin                                              
   Set @Region = @BlendedRegion                                        
  Set @RemoteRev = @RemoteRevBlended                                        
  End                                            
                                        
    If @Network <> 'A' and @YellowFlag <> 1                                          
   Begin                                        
     If @Network = 'B'                                        
       Begin                                        
   Set @Region = @Region2                                        
   Set @RemoteRev = @RemoteRevB                                        
       End                                        
     If @Network = 'C'                                        
       Begin                                        
   Set @Region = @Region3                                        
   Set @RemoteRev = @RemoteRevC                                        
       End                                        
     If @Network = 'D'                                      
       Begin                                        
   Set @Region = @Region4                                    
   Set @RemoteRev = @RemoteRevD                                        
       End                                        
     If @Network = 'E'                                        
       Begin                                        
   Set @Region = @Region5                                        
   Set @RemoteRev = @RemoteRevE                                        
       End                                        
     If @Network = 'F'                                        
       Begin                                        
   Set @Region = @Region6                                        
   Set @RemoteRev = @RemoteRevF                                        
       End                                        
     If @Network = 'G'                              
       Begin                                        
   Set @Region = @Region7                                        
   Set @RemoteRev = @RemoteRevG                        
       End                                        
     If @Network = 'H'                                        
       Begin                                        
   Set @Region = @Region8                                        
   Set @RemoteRev = @RemoteRevH                                        
       End                                        
     If @Network = 'I'      
       Begin                                        
   Set @Region = @Region9                                        
   Set @RemoteRev = @RemoteRevI                                        
       End                                        
     If @Network = 'J'                                        
       Begin                                        
   Set @Region = @Region10                                        
   Set @RemoteRev = @RemoteRevJ                                        
       End                                        
  If @Network = 'K'                                        
       Begin                                        
   Set @Region = @Region11                                        
   Set @RemoteRev = @RemoteRevK                                        
       End                             
  If @Network = 'L'                                        
       Begin                                        
   Set @Region = @Region12                                        
   Set @RemoteRev = @RemoteRevL                                        
       End                                        
  If @Network = 'M'                                        
       Begin                              
   Set @Region = @Region13                                        
   Set @RemoteRev = @RemoteRevM                                        
       End                                        
  If @Network = 'N'                                        
       Begin                                        
   Set @Region = @Region14                                        
   Set @RemoteRev = @RemoteRevN                                        
       End                                        
  If @Network = 'O'                                        
       Begin                        
   Set @Region = @Region15                                        
   Set @RemoteRev = @RemoteRevO                                        
       End                                        
  If @Network = 'P'                                        
       Begin                                        
   Set @Region = @Region16                                        
   Set @RemoteRev = @RemoteRevP                                        
       End                                        
  If @Network = 'Q'                                        
       Begin                                        
   Set @Region = @Region17                                        
   Set @RemoteRev = @RemoteRevQ                                        
       End                                        
 If @Network = 'R'                                        
       Begin                                        
   Set @Region = @Region18                                        
   Set @RemoteRev = @RemoteRevR                                        
       End                                        
  If @Network = 'S'                                        
       Begin                                        
   Set @Region = @Region19                                        
   Set @RemoteRev = @RemoteRevS                                        
       End                                        
  If @Network = 'T'                                        
       Begin                         
   Set @Region = @Region20                                        
   Set @RemoteRev = @RemoteRevT                                        
       End                                        
  If @Network = 'U'                                        
       Begin                                        
   Set @Region = @Region21                                        
   Set @RemoteRev = @RemoteRevU                                        
       End                                      
  If @Network = 'V'                                        
       Begin                                        
   Set @Region = @Region22                                        
   Set @RemoteRev = @RemoteRevV                
       End                                        
  If @Network = 'W'                                        
       Begin                                        
   Set @Region = @Region23                                        
   Set @RemoteRev = @RemoteRevW                                        
       End                                        
  If @Network = 'X'                                        
       Begin                                        
   Set @Region = @Region24                                        
   Set @RemoteRev = @RemoteRevX                                        
       End                                        
  If @Network = 'Y'                                        
       Begin                                        
   Set @Region = @Region25                                        
   Set @RemoteRev = @RemoteRevY                                
       End                                        
  If @Network = 'Z'                                        
       Begin                                        
   Set @Region = @Region26                                        
   Set @RemoteRev = @RemoteRevZ                                        
       End                                        
   End                                        
                                        
 If @VariableRemoteRevFlag > 0                   
   Begin                              
     If @TypeCode = 1                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev                                        
       End                                        
     If @TypeCode = 2                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev2                                        
       End               If @TypeCode = 3                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev3                                        
       End                                        
     If @TypeCode = 4                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev4                                        
       End                                        
     If @TypeCode = 5                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev5                                        
       End                                        
     If @TypeCode = 6                                        
       Begin                                        
 Set @RemoteRev = @RemoteRev6                                        
       End                                        
     If @TypeCode = 7                                        
       Begin                                       
  Set @RemoteRev = @RemoteRev7                                        
       End                                        
     If @TypeCode = 8                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev8                         
       End                                        
     If @TypeCode = 9                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev9                                        
       End                                        
     If @TypeCode = 10                                        
       Begin                                        
  Set @RemoteRev = @RemoteRev10                                        
       End                                        
   End                                        
 End                                        
                                        
                                        
if (@RemoteExp <> 0 or @RemoteRev <> 0)                                        
Begin     
                                        
 If (@RatingTypeID = 1) or (@RatingTypeID = 3) -- Region                                        
 Begin                                          
  Select Top 1 @RemoteDiscount = isnull(RemoteDisc,1)                                        
  From [HDN_Providence].[dbo].CustomerRatesRevenue                                        
  Where CustomerID = @AccountNumber                                        
  And Region = @Region                            
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc                                        
 End                                          
                                          
 If (@RatingTypeID = 2) or (@RatingTypeID = 4) -- State to State                                          
 Begin                                          
  Select Top 1 @RemoteDiscount = isnull(RemoteDisc,1)                                          
  From [HDN_Providence].[dbo].CustomerRatesRevenue                                      
  Where CustomerID = @AccountNumber                                          
  And StateID =  @RateStateID                                          
  And ServiceLevelID = @ServiceLevel                                           
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                  
  Order By EffectiveDate Desc                                        
 End                                          
                                          
 Select Top 1 @RemoteCompDiscount = isnull(RemoteCompDisc,1)                                        
 From [HDN_Providence].[dbo].CustomerRatesComp                                     
 Where CustomerID = @AccountNumber                                        
 And Region = @Region                                        
 And ServiceLevelID = @ServiceLevel                                        
 And MileageRange = @MileageRange                                        
 And EffectiveDate < @DelDate                                        
 Order By EffectiveDate Desc              
         
 --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@798=',CAST(@Region as Varchar(10)) + ' ' + CAST(@MileageRange as VARCHAR(10)) + ' ' + CAST(@DelDate As Varchar(10)), @QuoteID, GetDate(),'Anudeep')                   
                                      
                                        
 Select @RemoteDiscount = isnull(@RemoteDiscount,1)                                          
 Select @RemoteCompDiscount = isnull(@RemoteCompDiscount,1)                                           
                                          
 if @RemoteDiscount > 0                                             
 Begin                                            
  Select @RemoteRev = @RemoteRev - (@RemoteRev * @RemoteDiscount)                                           
 End                                            
                      
  if @RemoteCompDiscount > 0                                           
 Begin                                          
  Select @RemoteExp = @RemoteExp - (@RemoteExp * @RemoteCompDiscount)                                           
 End                                          
End                                          
                                        
-- 27 OCT 2004: For Local type do not calculate the Remote area charges                                        
if @BusinessTypeID  <> 2                                        
Begin                                           
                                        
  -- 8/20/2010: new way to remove a Remote Charge if it exists                                        
 Create Table #RemoveRemoteCharge(                                
   ProjectRateID int                                                
  )                                                
                                        
  Insert into #RemoveRemoteCharge                                          
 Select pr.ProjectRateID from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                        
 where QuoteId = @QuoteID                                        
 and activityid = 39                                        
 and pa.Extracted is null                                        
 -- 2/19/2016: check partner extract also                                        
 and pa.PartnerExtractDate is null                       
                                        
-- and InvoicedFlag <> 1                                        
                                        
  -- remove a Remote Charge if it exists and it is not invoiced                                        
  Delete from ProjectActivities where ProjectRateID in                             
 (Select ProjectRateID from #RemoveRemoteCharge)                                          
                                          
  Delete from ProjectRates where ProjectRateID in                                         
 (Select ProjectRateID from #RemoveRemoteCharge)                                          
                                      
  -- after the discounts, see if there is still a remote expense or remote revenue              
  if (@RemoteExp <> 0 or @RemoteRev <> 0)                                        
  Begin                                          
 Select @ActivityDescription = Description from [HDN_Providence].[dbo].Activity where ActivityID = 39                                          
 if @RemoteExp > 0                                           
   Begin                                          
     Select @Margin  = round(((@RemoteRev - @RemoteExp)*100/@RemoteExp)/100,4)                                          
   End                                          
 Else                                        
   Begin                                        
  Set @Margin = 1                                        
   End                                        
                                        
 Select @RecordCount = Count(*) from ProjectRates where QuoteID = @QuoteID and activityid = 39                                        
                                      
 If @RecordCount = 0                                           
   Begin                                          
  Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                          
   Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,             InvoicedFlag,RateActionTypeID,createdby,createDate)                                          
  Values(@QuoteID,@PartnerID,39,'FR',@RemoteExp,                                        
   @Margin,@RemoteRev,'FR',0,@ActivityDescription,                                        
   0,3,@CreatedBy,getdate())                                          
                                          
  Select  @ProjectRateID = @@IDENTITY, @Err=@@error                                            
                                          
  -- 2/3/04: use freight available date instead of deldate, because deldate is not required                                        
  insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                          
   CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                            
  values (@ProjectRateID,@DelDate,1,0,@RemoteRev,@RemoteExp,0,@CreatedBy,getdate())                                            
   End                                        
 else                                        
   Begin                             
  Select @ProjectRateID = ProjectRateID from ProjectRates where QuoteID = @QuoteID and activityid = 39                                                                      
  Select @ExtractDate = Extracted from ProjectActivities where ProjectRateID = @ProjectRateID                                        
                                          
  -- 8/20/2010: don't update if it is already billed                                        
  If isnull(@ExtractDate,'') = ''                                        
    Begin                                        
   -- update here                                        
   Update ProjectRates                                           
   Set Margin = @Margin,                                          
   CustomerRateValue = @RemoteRev,                                          
   RateValue = @RemoteExp,                                          
   ModifiedBy = @CreatedBy,                                          
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                          
                                              
   Update ProjectActivities                                           
   Set CustomerCost = @RemoteRev,                                          
   PartnerCost = @RemoteExp,                                          
   ModifiedBy = @CreatedBy,                                          
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                           
    End                                        
   End                         
                                         
  End                                           
                                        
  drop table #RemoveRemoteCharge                                          
End                                     
                                        
/* Remote Charges calculation ends */                                          
                                        
                                        
-- 12/18/2012: only calc delivery charge for Returns from Residence                                        
SkipRemoteChargeCalc:                                        
----------------------------------------------------------------------------------------                                        
/* Delivery Charge calculation begins */                                          
                                        
DECLARE Product_Cursor  CURSOR FAST_FORWARD FOR                                                  
  Select ProductID,Quantity,Weight,Pieces,icube,Parts from #tempProducts                                              
                                          
Set @TotalCube = 0                                          
Set @TotalLinearFeet = 0                                        
Set @TotalSquareFeet = 0                                        
                                              
OPEN Product_Cursor                                              
                              FETCH NEXT FROM Product_Cursor                                              
INTO @ProductID,@Quantity,@Weight,@Pieces,@Cube,@Parts                                             
                                                 
WHILE @@FETCH_STATUS = 0                                                  
BEGIN                                             
 if @Cube > 0                 
  Begin                                              
 Set @TotalCube = @TotalCube + (@Quantity * @Cube)                                          
                                        
 Select @Length = Length, @Width = Width                                        
 From [HDN_Providence].[dbo].Product                                           
 Where ProductTypeID = @ProductID                                           
                                        
 Set @TotalLinearFeet = @TotalLinearFeet + (@Quantity * @Length / 12)                                        
 Set @TotalSquareFeet = @TotalSquareFeet + (@Quantity * @Length * @Width / 12 / 12)                                        
  End                                          
                                              
  FETCH NEXT FROM Product_Cursor                                              
 INTO @ProductID,@Quantity,@Weight,@Pieces,@Cube,@Parts                                              
END                                                  
                                                  
CLOSE Product_Cursor                                                      
DEALLOCATE Product_Cursor                                   
                                        
                                    
If @OrderTypeID = 4 and @LocOfProduct <> 'Residence'                                        
  Begin                                        
 goto SkipDeliveryChargeCalc                                        
  End                                        
----------------------------------------------------------------------------------------                                        
If @RatingTypeID = 3 or @RatingTypeID = 4  -- Category ratings                                          
Begin                                        
                                       
-- Since we might be recalculating the charges from the details 2                                        
-- page, we need to delete any current additional item charges                                        
-- that are not billed and were not added as accessorials.                                        
                                        
-- we need to do this for both types of Additional Items Discounts                                        
-- (Account Level and Product Level)                                        
                                        
  Delete from AddlItemActivities where ProjectActivitiesID in (                                          
  Select pa.ProjectActivitiesID from ProjectActivities pa                                         
 inner join ProjectRates pr ON pr.ProjectRateID = pa.ProjectRateID                                        
 where pr.QuoteID = @QuoteID                                         
 and pr.ActivityID = 40                                        
 and isnull(pr.AccessorialFlag,0) = 0                                        
 and pa.Extracted is null                                        
 and pa.PartnerExtractDate is null)                                        
                                        
                                        
  Delete from ProjectActivities where ProjectRateID in (                                          
  Select pr.ProjectRateID from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                      
 where pr.QuoteID = @QuoteID                                         
 and pr.ActivityID = 40                                        
 and isnull(pr.AccessorialFlag,0) = 0                                        
 and pa.Extracted is null                                        
 and pa.PartnerExtractDate is null)                                        
                                          
                                        
  Delete from ProjectRates where ProjectRateID in (                                          
  Select pr.ProjectRateID from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                        
 where pr.QuoteID = @QuoteID                                         
 and pr.ActivityID = 40                                        
 and isnull(pr.AccessorialFlag,0) = 0                                        
 and pa.Extracted is null                                        
 and pa.PartnerExtractDate is null)                     
                                        
  -- ACCOUNT LEVEL ADDITIONAL ITEM DISCOUNTS                                        
  If (@Customer2ndItemDisc + @Customer3rdItemDisc + @Customer4thItemDisc + @Customer5thItemDisc + @Customer6thItemDisc) > 0                                        
  Begin                                        
  -- if the customer has Account Level Additional Item Discounts, execute the following code                                        
                                        
  Set @KeepGoing = 1                                        
  Set @CustomerCost = 0                                        
  Set @PartnerCost = 0                                        
  Set @TotalCustomerCost = 0                                        
  Set @TotalPartnerCost = 0                                        
  Set @ExcessMilesCustomerCost = 0                                        
  Set @ExcessMilesPartnerCost = 0                                        
                                        
-------------------------------------                                        
                          
                                        
                                        
  -- we will need another temporary table so we can sort by costs                                        
  Create Table #tempProductsWithCosts(                                                
 ProductID int,                                          
 Quantity int,                                          
 Weight int,                                          
 Pieces int,                    
 iCube numeric(9,2),                                        
 Parts int,                                 
 CategoryID int,                                        
 CustomerCost float,                                        
 PartnerCost float                                              
 )                                                
                                        
                                        
  WHILE @KeepGoing = 1                                        
  BEGIN                                        
                                        
  DELETE FROM #tempProductsWithCosts                                        
                                        
  DECLARE Category_Cursor  CURSOR FAST_FORWARD FOR                                                  
 SELECT tp.ProductID,tp.Quantity,tp.Weight,tp.Pieces,tp.iCube,tp.Parts,p.CategoryID                                         
 FROM #tempProducts tp                                         
  INNER JOIN Product p ON tp.ProductID = p.ProductTypeID                                        
                                        
  OPEN Category_Cursor                                        
                                        
  FETCH NEXT FROM Category_Cursor                  
  INTO @ProductID,@Quantity,@Weight,@Pieces,@Cube,@Parts,@CategoryID                                             
                                                 
  WHILE @@FETCH_STATUS = 0                                                  
   BEGIN                                           
                                        
 -- we need to get the CustomerCost and the PartnerCost for each and                                        
 -- every product on the order so that we can determine which is the                                         
 -- most expensive to deliver                                        
 -- the cost of the additional items will be the discount % times the                                        
 -- highest cost item.                                        
 -- example: 2 items, 1 costs $500 to deliver, the other costs $400.                                        
 -- if the second item discount is 50%, the first item delivers for                                         
 -- $500, and the second delivers for $250.                                        
                   
 -- the first thing that we need to do is get the customercost and                                        
 -- partnercost of each item on the order, and write that info to                                        
 -- the temp table                                        
                                        
  Set @TypeCode = 0                                        
                                        
 -- find the type code base on the category                                        
 SELECT @TypeCode = TypeCode                                        
 FROM [HDN_Providence].[dbo].AccountWgtBreaks                                         
 WHERE CustomerID = @AccountNumber                                  
 AND CategoryID = @CategoryID                                        
                                        
 If @RatingTypeID = 3 -- Category-Region                                          
 Begin                                          
  Select Top 1 @CustomerCost1 = isnull(Rate1,0),@CustomerCost2 = isnull(Rate2,0),                                        
  @CustomerCost3 = isnull(Rate3,0),@CustomerCost4 = isnull(Rate4,0),                                        
  @CustomerCost5 = isnull(Rate5,0),@CustomerCost6 = isnull(Rate6,0),                                        
  @CustomerCost7 = isnull(Rate7,0),@CustomerCost8 = isnull(Rate8,0),                                        
  @CustomerCost9 = isnull(Rate9,0),@CustomerCost10 = isnull(Rate10,0),                                        
  @CustomerCost11 = isnull(Rate11,0),@CustomerCost12 = isnull(Rate12,0),                                        
  @CustomerCost13 = isnull(Rate13,0),@CustomerCost14 = isnull(Rate14,0),                                        
  @CustomerCost15 = isnull(Rate15,0),@CustomerCost16 = isnull(Rate16,0),                                        
  @CustomerCost17 = isnull(Rate17,0),@CustomerCost18 = isnull(Rate18,0),                                        
  @CustomerCost19 = isnull(RAte19,0),@CustomerCost20 = isnull(Rate20,0)                                        
  From [HDN_Providence].[dbo].CustomerRatesRevenue                                        
  Where CustomerID = @AccountNumber                                         
  And Region = @Region                                        
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc                                        
 End                                          
                                        
 If @RatingTypeID = 4 -- Category-State to State                                          
 Begin                                          
  Select Top 1 @CustomerCost1 = isnull(Rate1,0),@CustomerCost2 = isnull(Rate2,0),                           
  @CustomerCost3 = isnull(Rate3,0),@CustomerCost4 = isnull(Rate4,0),                                        
  @CustomerCost5 = isnull(Rate5,0),@CustomerCost6 = isnull(Rate6,0),                                        
  @CustomerCost7 = isnull(Rate7,0),@CustomerCost8 = isnull(Rate8,0),                                        
  @CustomerCost9 = isnull(Rate9,0),@CustomerCost10 = isnull(Rate10,0),                                        
  @CustomerCost11 = isnull(Rate11,0),@CustomerCost12 = isnull(Rate12,0),                                        
  @CustomerCost13 = isnull(Rate13,0),@CustomerCost14 = isnull(Rate14,0),                                        
  @CustomerCost15 = isnull(Rate15,0),@CustomerCost16 = isnull(Rate16,0),                                        
  @CustomerCost17 = isnull(Rate17,0),@CustomerCost18 = isnull(Rate18,0),                                        
  @CustomerCost19 = isnull(Rate19,0),@CustomerCost20 = isnull(Rate20,0)                                        
  From [HDN_Providence].[dbo].CustomerRatesRevenue                                        
  Where CustomerID = @AccountNumber                                         
  And StateID =  @RateStateID                                         
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc                                        
 End                                          
                                     
 -- Partner Cost is the same for both rating types                                        
 Select Top 1 @PartnerCost1 = isnull(Rate1,0),@PartnerCost2 = isnull(Rate2,0),                                        
  @PartnerCost3 = isnull(Rate3,0),@PartnerCost4 = isnull(Rate4,0),                                        
  @PartnerCost5 = isnull(Rate5,0),@PartnerCost6 = isnull(Rate6,0),                                        
  @PartnerCost7 = isnull(Rate7,0),@PartnerCost8 = isnull(Rate8,0),                                        
  @PartnerCost9 = isnull(Rate9,0),@PartnerCost10 = isnull(Rate10,0),                                       
  @PartnerCost11 = isnull(Rate11,0),@PartnerCost12 = isnull(Rate12,0),                                        
  @PartnerCost13 = isnull(Rate13,0),@PartnerCost14 = isnull(Rate14,0),                                        
  @PartnerCost15 = isnull(Rate15,0),@PartnerCost16 = isnull(Rate16,0),                                        
  @PartnerCost17 = isnull(Rate17,0),@PartnerCost18 = isnull(Rate18,0),                                        
  @PartnerCost19 = isnull(Rate19,0),@PartnerCost20 = isnull(Rate20,0)                                        
 From [HDN_Providence].[dbo].CustomerRatesComp                                        
 Where CustomerID = @AccountNumber                                         
  And Region = @Region                                        
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc          
          
 --  insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@1132=',CAST(@Region as Varchar(10)) + ' ' + CAST(@MileageRange as VARCHAR(10)) + ' ' + CAST(@DelDate As Varchar(10)), @QuoteID, GetDate(),'Anudeep')                                      
                                        
 -- initialize these so that, if we encounter a product that does not                                        
 -- have a category, we won't use the previous product's charge.                                        
 Set @wrkCustomerCost = 0                                        
 Set @wrkPartnerCost = 0                                        
                                        
 If @TypeCode = 1                                        
  Begin                       
   Set @wrkCustomerCost = @CustomerCost1                                        
   Set @wrkPartnerCost = @PartnerCost1                                        
  End                            
 If @TypeCode = 2                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost2                                
   Set @wrkPartnerCost = @PartnerCost2                                        
  End                                        
 If @TypeCode = 3                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost3                                        
 Set @wrkPartnerCost = @PartnerCost3                                        
  End                                        
 If @TypeCode = 4                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost4                                        
   Set @wrkPartnerCost = @PartnerCost4                                        
  End                                        
 If @TypeCode = 5                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost5                                        
   Set @wrkPartnerCost = @PartnerCost5                                        
  End                               
 If @TypeCode = 6                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost6                                        
   Set @wrkPartnerCost = @PartnerCost6                                        
  End                                        
 If @TypeCode = 7                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost7                                        
   Set @wrkPartnerCost = @PartnerCost7                                        
  End                                        
 If @TypeCode = 8                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost8                                        
   Set @wrkPartnerCost = @PartnerCost8                
  End                                        
 If @TypeCode = 9                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost9                                        
   Set @wrkPartnerCost = @PartnerCost9                                        
  End                                        
 If @TypeCode = 10                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost10             
   Set @wrkPartnerCost = @PartnerCost10               
  End                                        
 If @TypeCode = 11                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost11                                        
   Set @wrkPartnerCost = @PartnerCost11                                        
  End                                        
 If @TypeCode = 12                                        
  Begin                                        
Set @wrkCustomerCost = @CustomerCost12                                        
   Set @wrkPartnerCost = @PartnerCost12                                        
  End                                        
 If @TypeCode = 13                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost13                          
   Set @wrkPartnerCost = @PartnerCost13                                        
  End                                        
 If @TypeCode = 14                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost14                                        
   Set @wrkPartnerCost = @PartnerCost14                                        
  End                                        
 If @TypeCode = 15                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost15                                        
   Set @wrkPartnerCost = @PartnerCost15                                        
  End                                        
 If @TypeCode = 16                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost16                                        
 Set @wrkPartnerCost = @PartnerCost16                              
  End                                        
 If @TypeCode = 17                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost17                                        
   Set @wrkPartnerCost = @PartnerCost17                                        
  End                                        
 If @TypeCode = 18                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost18                                        
   Set @wrkPartnerCost = @PartnerCost18                                        
  End                                        
 If @TypeCode = 19                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost19                                        
   Set @wrkPartnerCost = @PartnerCost19                                        
  End                                        
 If @TypeCode = 20                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost20                                     
   Set @wrkPartnerCost = @PartnerCost20                                        
  End                                     
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@wrkCustomerCost=',@wrkCustomerCost, 0, GetDate(),'bergc')                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@wrkPartnerCost=',@wrkPartnerCost, @QuoteID, GetDate(),'Anudeep')                                        
                                        
                                        
 -- it will be easier later on if we save everything                                         
 -- in quantity of 1                                        
 WHILE @Quantity > 0                                        
 Begin                                        
   INSERT INTO #tempProductsWithCosts(                                                
   ProductID,Quantity,Weight,Pieces,iCube,Parts,CategoryID,CustomerCost,PartnerCost)                                                
   VALUES (@ProductID,1,@Weight,@Pieces,@Cube,@Parts,@CategoryID,@wrkCustomerCost,@wrkPartnerCost)                                        
   Set @Quantity = @Quantity - 1                                        
 End                                 
                                         
    FETCH NEXT FROM Category_Cursor                                              
    INTO @ProductID,@Quantity,@Weight,@Pieces,@Cube,@Parts,@CategoryID                                             
                                        
  END -- WHILE @@FETCH_STATUS = 0 (Category_Cursor)                                        
                                        
  CLOSE Category_Cursor                     
  DEALLOCATE Category_Cursor                                          
                                        
                                       
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@MileageRange=',@MileageRange, @QuoteID, GetDate(),'bergc')                                        
                                        
  If @MileageRange > 1 and @MilesPlusDelivChgFlag = 1                                        
  Begin                                        
                                        
                         
 Set @wrkCustomerCost = 0                                        
 Set @wrkPartnerCost = 0                                        
                                        
 SELECT Top 1 @wrkCustomerCost = CustomerCost,                                         
    @wrkPartnerCost = PartnerCost                                        
 FROM #tempProductsWithCosts                                         
  ORDER BY CustomerCost Desc           
                                       
                                                          
 -- Pounds or Hundred Weight                                        
 if @ExcessMilesUOM = 'LB' or @ExcessMilesUOM = 'CWT'                                            
 Begin                                    
    Select @UOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @ExcessMilesUOM                                          
     Set @CustomerCost = (@TotalWeight/@UOMMultiplier ) * @wrkCustomerCost                                          
  Set @PartnerCost = (@TotalWeight/@UOMMultiplier ) * @wrkPartnerCost          
          
                       
 End                                          
                                        
 -- Cubes                                        
 if @ExcessMilesUOM = 'CU'                            
 Begin                                          
     Set @CustomerCost = @TotalCube * @wrkCustomerCost                                          
     Set @PartnerCost = @TotalCube * @wrkPartnerCost                                          
 End                                         
                   
 -- Square Feet                                        
 if @ExcessMilesUOM = 'SF'                                           
 Begin                                          
     Set @CustomerCost = @TotalSquareFeet * @wrkCustomerCost                                          
     Set @PartnerCost = @TotalSquareFeet * @wrkPartnerCost                                          
 End                                          
                                        
 -- Linear Feet                                        
 if @ExcessMilesUOM = 'LF'                                           
 Begin                                          
     Set @CustomerCost = @TotalLinearFeet * @wrkCustomerCost                                          
     Set @PartnerCost = @TotalLinearFeet * @wrkPartnerCost                                          
 End                                          
                                        
 -- Each                                        
 if @ExcessMilesUOM = 'EA'                                         
 Begin                                    
     Set @CustomerCost = @TotalQuantity * @wrkCustomerCost                                          
    Set @PartnerCost = @TotalQuantity * @wrkPartnerCost                                          
 End                                          
                                        
 -- Per Piece                                        
 if @ExcessMilesUOM = 'PC'                                            
 Begin                  
     Set @CustomerCost = @TotalPieces * @wrkCustomerCost                                          
  Set @PartnerCost = @TotalPieces * @wrkPartnerCost                           
 End                                          
                                        
 -- Miles                                        
 if @ExcessMilesUOM = 'MI'                                            
 Begin                                          
 -- cost times excess miles                                        
 -- I need to account for 3 or more mileage breaks where                                        
 -- the mileage falls into the 3rd break and both the 2nd and 3rd                                        
 -- are "Plus Delivery Charge".                                        
   If @FMMiles > @EndMiles                                         
    Begin                                        
  Set @wrkMiles = @EndMiles - @StartMiles + 1                                        
    End                                        
   Else                                        
    Begin                     
  Set @wrkMiles = @FMMiles - @StartMiles + 1                                        
    End                                        
   Set @CustomerCost = @wrkCustomerCost * @wrkMiles                                        
   Set @PartnerCost = @wrkPartnerCost * @wrkMiles           
           
   End                                          
                                        
 -- accumulate excess mileage costs here                                        
 Set @ExcessMilesCustomerCost = @ExcessMilesCustomerCost + @CustomerCost                                        
 Set @ExcessMilesPartnerCost = @ExcessMilesPartnerCost + @PartnerCost                                        
 Set @CustomerCost = 0                                        
 Set @PartnerCost = 0                                        
                                        
 Set @MileageRange = @MileageRange - 1                         
                                        
 SELECT @MilesPlusDelivChgFlag = PlusDelivChgFlag, @StartMiles = StartMiles,                                         
   @EndMiles = EndMiles, @ExcessMilesUOM = ExcessMilesUOM       FROM AccountMileageBreaks                                         
 WHERE CustomerID = @AccountNumber                                             
 AND TypeCode = @MileageRange                                        
                                        
  End -- If @MileageRange > 1 and @MilesPlusDelivChgFlag = 1                                        
                                        
  Else                                        
                                        
   Begin                                        
 Set @KeepGoing = 0                                        
   End                                        
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@KeepGoing=',@KeepGoing, 0, GetDate(),'bergc')                                        
                                        
  END -- WHILE @KeepGoing = 1                                        
                                        
                                        
  -- in the most straight-forward case, they would have only 1 mileage                                        
  -- range, so we only have to write the temp table one time.                                        
  -- let's handle that case here.                                        
                                        
  DECLARE Cost_Cursor  CURSOR FAST_FORWARD FOR                         
 SELECT CustomerCost, PartnerCost                                        
 FROM #tempProductsWithCosts                            
  ORDER BY CustomerCost Desc                                        
                                   
  OPEN Cost_Cursor   
                                        
  FETCH NEXT FROM Cost_Cursor                                              
  INTO @wrkCustomerCost, @wrkPartnerCost                                            
                                         
                                                
  Set @ItemNumber = 1                                        
                                        
  WHILE @@FETCH_STATUS = 0                                                  
   BEGIN                                           
 -- the cost of the additional items will be the discount % times the                                        
 -- highest cost item.                                        
 -- example: 2 items, 1 costs $500 to deliver, the other costs $400.                                        
 -- if the second item discount is 50%, the first item delivers for                                         
 -- $500, and the second delivers for $250.                                        
                                        
 If @ItemNumber = 1                                         
 Begin                                        
   -- the ItemNumber 1 costs will go into the Delivery Charge activity                                        
   Set @HighestCustomerCost = @wrkCustomerCost                                        
   Set @HighestPartnerCost = @wrkPartnerCost                                        
   Set @CustomerCost = @CustomerCost + @wrkCustomerCost                                        
   Set @PartnerCost = @PartnerCost + @wrkPartnerCost                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@ItemNumber=',@ItemNumber, 0, GetDate(),'bergc')                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
 End                                        
                                        
 If @ItemNumber = 2                                        
   Begin                                        
  -- the ItemNumber 2+ costs will go into "Additional Item" activities                                        
  Set @wrk2CustomerCost = ((1 - @Customer2ndItemDisc) * @HighestCustomerCost)                                        
  Set @wrk2PartnerCost = ((1 - @Customer2ndItemDisc) * @HighestPartnerCost)                                        
   End                                        
                                 
 If @ItemNumber = 3                                        
  Begin                                        
  Set @wrk2CustomerCost = ((1 - @Customer3rdItemDisc) * @HighestCustomerCost)                              
  Set @wrk2PartnerCost = ((1 - @Customer3rdItemDisc) * @HighestPartnerCost)                                        
  End                                        
                                        
 If @ItemNumber = 4                                        
  Begin                                        
  Set @wrk2CustomerCost = ((1 - @Customer4thItemDisc) * @HighestCustomerCost)                                        
  Set @wrk2PartnerCost = ((1 - @Customer4thItemDisc) * @HighestPartnerCost)                                        
  End                                        
                                        
 If @ItemNumber = 5                                        
  Begin                                        
  Set @wrk2CustomerCost = ((1 - @Customer5thItemDisc) * @HighestCustomerCost)                                        
  Set @wrk2PartnerCost = ((1 - @Customer5thItemDisc) * @HighestPartnerCost)                                        
  End                                        
                                        
 If @ItemNumber > 5                                        
  Begin                                        
  Set @wrk2CustomerCost = ((1 - @Customer6thItemDisc) * @HighestCustomerCost)                                        
  Set @wrk2PartnerCost = ((1 - @Customer6thItemDisc) * @HighestPartnerCost)                                        
  End                                        
                                        
 If @ItemNumber > 1                                        
   Begin                                        
  -- make sure the discounted price is not higher than the regular price                                        
  If @wrk2CustomerCost < @wrkCustomerCost and @wrkCustomerCost <> 0                                        
   Begin                                        
    Set @wrkCustomerCost = @wrk2CustomerCost                                        
    Set @wrkPartnerCost = @wrk2PartnerCost                                        
   End                                        
                                        
  Select @ActivityDescription = Description from Activity where ActivityID = 40                                            
                                           
  If @wrkPartnerCost = 0                                        
   Begin                                        
    Set @Margin = 1                                        
   End                                        
  Else                                     
   Begin                                        
    Select @Margin  = round(((@wrkCustomerCost - @wrkPartnerCost)*100/@wrkPartnerCost)/100,4)                                              
   End                                        
                                        
  Set @ProjectActivitiesID = 0                                        
                                      
  -- we use the AddlItemActivities table keep track of which activity                                        
  -- corresponds to which additional item                                        
  -- check the AddlItemActivities table to see if this item exists                                        
  Select @ProjectActivitiesID = ProjectActivitiesID                                        
  From AddlItemActivities                                         
  where QuoteID = @QuoteID                                         
  and ItemNumber = @ItemNumber                                        
                                        
  If @ProjectActivitiesID = 0                                           
   Begin                                          
    Insert Into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                              
      Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                              
   InvoicedFlag,RateActionTypeID,CreatedBy,CreateDate,DeliveryChargeFlag)                                              
    Values(@QuoteID,@PartnerID,40,'FR',@wrkPartnerCost,                                        
   @Margin,@wrkCustomerCost,'FR',0,@ActivityDescription,                                        
   0,3,@CreatedBy,GetDate(),0)                                              
                                               
       Select  @ProjectRateID = @@IDENTITY                                              
                                             
    Insert Into ProjectActivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                              
  CustomerCost,PartnerCost,MinimumUsed,CreatedBy,CreateDate)                                                
    Values (@ProjectRateID,@DelDate,1,0,                                        
   @wrkCustomerCost,@wrkPartnerCost,0,@CreatedBy,GetDate())       
                                        
       Select  @ProjectActivitiesID = @@IDENTITY                                          
                  
    Insert Into AddlItemActivities (QuoteID, ProjectActivitiesID, ItemNumber, CreatedBy, CreateDate)                                        
    Values (@QuoteID, @ProjectActivitiesID, @ItemNumber, @CreatedBy, GetDate())                                        
   End                                          
                                        
     Else                                          
                                        
   Begin                                       
    -- never update a billed activity                                        
    Select @ExtractDate = Extracted,                       
    @ProjectRateID = ProjectRateID                                        
    From ProjectActivities                                         
    Where ProjectActivitiesID = @ProjectActivitiesID                                        
                                          
    -- don't update if it is already billed                                        
    If isnull(@ExtractDate,'') = ''                                        
     Begin                                        
   Update ProjectRates                                           
   Set Margin = @Margin,                                          
   CustomerRateValue = @wrkCustomerCost,                                          
   RateValue = @wrkPartnerCost,                              
   ModifiedBy = @CreatedBy,                                          
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                          
       
   Update ProjectActivities                                           
   Set CustomerCost = @wrkCustomerCost,                                          
   PartnerCost = @wrkPartnerCost,                                          
   ModifiedBy = @CreatedBy,                                          
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                           
     End                                        
  End                                          
  End                                        
                                        
 FETCH NEXT FROM Cost_Cursor                                              
 INTO @wrkCustomerCost, @wrkPartnerCost                                            
                                        
 Set @ItemNumber = @ItemNumber + 1                             
                                        
   END -- WHILE @@FETCH_STATUS = 0 (Cost_Cursor)                                        
                                        
  Set @CustomerCost = @CustomerCost + @ExcessMilesCustomerCost                                        
  Set @PartnerCost = @PartnerCost + @ExcessMilesPartnerCost                                        
  Set @TotalCustomerCost = @TotalCustomerCost + @CustomerCost                                        
  Set @TotalPartnerCost = @TotalPartnerCost + @PartnerCost                                        
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
                                        
 DROP TABLE #tempProductsWithCosts                                        
                                        
  End -- If (@Customer2ndItemDisc + @Customer3rdItemDisc + @Customer4thItemDisc + @Customer5thItemDisc + @Customer6thItemDisc) > 0                       
               
                                        
----------------------------------------------------------------------------------------                                        
  If (@Customer2ndItemDisc + @Customer3rdItemDisc + @Customer4thItemDisc + @Customer5thItemDisc + @Customer6thItemDisc) = 0                                        
  Begin                                        
  -- if they do not have account level additional item discounts, execute the following code                                        
                                        
  Set @KeepGoing = 1                                        
  Set @CustomerCost = 0                                        
  Set @PartnerCost = 0                                        
  Set @TotalCustomerCost = 0                                        
  Set @TotalPartnerCost = 0                                        
                      
  WHILE @KeepGoing = 1                                        
  BEGIN                                        
                                        
  DECLARE Category_Cursor  CURSOR FAST_FORWARD FOR                      
 SELECT ProductID, SUM(Quantity) FROM #tempProducts GROUP BY ProductID                                          
                                            
-- *** look here ***                                        
-- *** if the discounts are by category, then we might                                        
-- *** want to use the following cursor                                        
--                                        
--  DECLARE Category_Cursor  CURSOR FAST_FORWARD FOR                                                  
--  SELECT p.CategoryID, SUM(Quantity)                                         
--  FROM #tempProducts tp                                         
--   INNER JOIN Product p ON tp.ProductID = p.ProductTypeID                                        
--   GROUP BY p.CategoryID         
                                            
  OPEN Category_Cursor                                        
                                        
  FETCH NEXT FROM Category_Cursor                                              
 INTO @ProductID,@Quantity                                        
                                                 
  WHILE @@FETCH_STATUS = 0                      
   BEGIN                                           
 -- get the category                                          
 Select @CategoryID = isnull(CategoryID,0),                                        
   @SecondItemDiscount = isnull(SecondItemDiscount,0),                                        
   @ThirdItemDiscount = isnull(ThirdItemDiscount,0),                                        
   @FourthItemDiscount = isnull(FourthItemDiscount,0),                                        
   @FifthItemDiscount = isnull(FifthItemDiscount,0),                                        
   @SixthItemDiscount = isnull(SixthItemDiscount,0)                                        
 From Product            
 Where ProductTypeID = @ProductID                                        
                                        
 -- if category is not found, make sure we don't have an                                        
 -- invalid type code that falls through                                        
 Set @TypeCode = 0                                        
                                        
 -- find the type code base on the category                 
 SELECT @TypeCode = TypeCode                                        
 FROM AccountWgtBreaks                                         
 WHERE CustomerID = @AccountNumber                                             
 AND CategoryID = @CategoryID                                        
                                        
 If @RatingTypeID = 3 -- Category-Region                                          
 Begin                                          
  Select Top 1 @CustomerCost1 = isnull(Rate1,0),@CustomerCost2 = isnull(Rate2,0),                                        
  @CustomerCost3 = isnull(Rate3,0),@CustomerCost4 = isnull(Rate4,0),                                        
  @CustomerCost5 = isnull(Rate5,0),@CustomerCost6 = isnull(Rate6,0),                                        
  @CustomerCost7 = isnull(Rate7,0),@CustomerCost8 = isnull(Rate8,0),                                        
  @CustomerCost9 = isnull(Rate9,0),@CustomerCost10 = isnull(Rate10,0),                                        
  @CustomerCost11 = isnull(Rate11,0),@CustomerCost12 = isnull(Rate12,0),                         
  @CustomerCost13 = isnull(Rate13,0),@CustomerCost14 = isnull(Rate14,0),                                        
  @CustomerCost15 = isnull(Rate15,0),@CustomerCost16 = isnull(Rate16,0),                                        
  @CustomerCost17 = isnull(Rate17,0),@CustomerCost18 = isnull(Rate18,0),                                        
  @CustomerCost19 = isnull(RAte19,0),@CustomerCost20 = isnull(Rate20,0)                                        
  From CustomerRatesRevenue                                        
  Where CustomerID = @AccountNumber                                         
  And Region = @Region                                        
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc                                        
 End                                          
                                        
 If @RatingTypeID = 4 -- Category-State to State                                          
 Begin                                          
  Select Top 1 @CustomerCost1 = isnull(Rate1,0),@CustomerCost2 = isnull(Rate2,0),                                        
  @CustomerCost3 = isnull(Rate3,0),@CustomerCost4 = isnull(Rate4,0),                             
  @CustomerCost5 = isnull(Rate5,0),@CustomerCost6 = isnull(Rate6,0),                                
  @CustomerCost7 = isnull(Rate7,0),@CustomerCost8 = isnull(Rate8,0),                            
  @CustomerCost9 = isnull(Rate9,0),@CustomerCost10 = isnull(Rate10,0),                                        
  @CustomerCost11 = isnull(Rate11,0),@CustomerCost12 = isnull(Rate12,0),                                        
  @CustomerCost13 = isnull(Rate13,0),@CustomerCost14 = isnull(Rate14,0),                                        
  @CustomerCost15 = isnull(Rate15,0),@CustomerCost16 = isnull(Rate16,0),                                        
  @CustomerCost17 = isnull(Rate17,0),@CustomerCost18 = isnull(Rate18,0),                                        
  @CustomerCost19 = isnull(Rate19,0),@CustomerCost20 = isnull(Rate20,0)                                        
  From CustomerRatesRevenue                                        
  Where CustomerID = @AccountNumber                                         
  And StateID =  @RateStateID                                
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc                                        
 End                                          
                                        
 -- Partner Cost is the same for both rating types                                        
 Select Top 1 @PartnerCost1 = isnull(Rate1,0),@PartnerCost2 = isnull(Rate2,0),                                        
  @PartnerCost3 = isnull(Rate3,0),@PartnerCost4 = isnull(Rate4,0),                                        
  @PartnerCost5 = isnull(Rate5,0),@PartnerCost6 = isnull(Rate6,0),                                        
  @PartnerCost7 = isnull(Rate7,0),@PartnerCost8 = isnull(Rate8,0),                                        
  @PartnerCost9 = isnull(Rate9,0),@PartnerCost10 = isnull(Rate10,0),                                        
  @PartnerCost11 = isnull(Rate11,0),@PartnerCost12 = isnull(Rate12,0),                                        
  @PartnerCost13 = isnull(Rate13,0),@PartnerCost14 = isnull(Rate14,0),                                        
  @PartnerCost15 = isnull(Rate15,0),@PartnerCost16 = isnull(Rate16,0),                                        
  @PartnerCost17 = isnull(Rate17,0),@PartnerCost18 = isnull(Rate18,0),                                        
  @PartnerCost19 = isnull(Rate19,0),@PartnerCost20 = isnull(Rate20,0)                                        
 From CustomerRatesComp                                        
 Where CustomerID = @AccountNumber                                         
  And Region = @Region                                        
  And ServiceLevelID = @ServiceLevel                                        
  And MileageRange = @MileageRange                                        
  And EffectiveDate < @DelDate                                        
  Order By EffectiveDate Desc            
          
 --  insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@1666=',CAST(@Region as Varchar(10)) + ' ' + CAST(@MileageRange as VARCHAR(10)) + ' ' + CAST(@DelDate As Varchar(10)), @QuoteID, GetDate(),'Anudeep')            
                                        
 -- initialize these so that, if we encounter a product that does not                           
 -- have a category, we won't use the previous product's charge.                                        
 Set @wrkCustomerCost = 0                                        
 Set @wrkPartnerCost = 0                                        
                                        
 If @TypeCode = 1                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost1                                        
   Set @wrkPartnerCost = @PartnerCost1                                        
  End                                        
 If @TypeCode = 2                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost2                                        
   Set @wrkPartnerCost = @PartnerCost2               
  End                
 If @TypeCode = 3                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost3                                        
   Set @wrkPartnerCost = @PartnerCost3                                        
  End                                        
 If @TypeCode = 4                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost4                                        
   Set @wrkPartnerCost = @PartnerCost4                                        
  End                                        
 If @TypeCode = 5                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost5                                        
   Set @wrkPartnerCost = @PartnerCost5                                       
  End                   
 If @TypeCode = 6                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost6                                        
   Set @wrkPartnerCost = @PartnerCost6                                        
  End                                        
 If @TypeCode = 7                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost7                                        
   Set @wrkPartnerCost = @PartnerCost7                                        
  End                                        
 If @TypeCode = 8                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost8                               
   Set @wrkPartnerCost = @PartnerCost8                                        
  End                                        
 If @TypeCode = 9                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost9                                        
   Set @wrkPartnerCost = @PartnerCost9                                        
  End                                        
 If @TypeCode = 10                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost10                                        
   Set @wrkPartnerCost = @PartnerCost10                                        
  End                                        
 If @TypeCode = 11                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost11                                        
   Set @wrkPartnerCost = @PartnerCost11                                        
  End                                        
 If @TypeCode = 12                                        
  Begin                 
   Set @wrkCustomerCost = @CustomerCost12                                        
   Set @wrkPartnerCost = @PartnerCost12                                        
  End                                 
 If @TypeCode = 13                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost13                                        
   Set @wrkPartnerCost = @PartnerCost13                                        
  End                                        
 If @TypeCode = 14                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost14                                        
   Set @wrkPartnerCost = @PartnerCost14                                        
  End                                        
 If @TypeCode = 15                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost15                                        
   Set @wrkPartnerCost = @PartnerCost15                                        
  End                                        
 If @TypeCode = 16                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost16                                        
   Set @wrkPartnerCost = @PartnerCost16                                        
  End                        
 If @TypeCode = 17                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost17                                        
   Set @wrkPartnerCost = @PartnerCost17             
  End                                        
 If @TypeCode = 18                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost18                                        
   Set @wrkPartnerCost = @PartnerCost18                                        
  End                                        
 If @TypeCode = 19                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost19              
   Set @wrkPartnerCost = @PartnerCost19                                        
  End                                        
 If @TypeCode = 20                                        
  Begin                                        
   Set @wrkCustomerCost = @CustomerCost20                                        
   Set @wrkPartnerCost = @PartnerCost20                                        
  End                                        
                                        
                                        
                                
 If @SecondItemDiscount + @ThirdItemDiscount + @FourthItemDiscount + @FifthItemDiscount + @SixthItemDiscount = 0                                        
  Begin                                        
   Set @wrkCustomerCost = @wrkCustomerCost * @Quantity                                        
   Set @wrkPartnerCost = @wrkPartnerCost * @Quantity                                           
  -- If @QuoteID = 1349388                                        
  -- Begin                                        
  --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  -- Values('@ProductID=',@ProductID, 1349388, GetDate(),'bergc')                                        
  --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  -- Values('@wrkCustomerCost=',@wrkCustomerCost, 1349388, GetDate(),'bergc')                                        
  --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  -- Values('@wrkPartnerCost=',@wrkPartnerCost, 1349388, GetDate(),'bergc')                                        
  -- End                                        
  End                                        
                                        
                                     
 Set @CustomerCost = @CustomerCost + @wrkCustomerCost                                        
 Set @PartnerCost = @PartnerCost + @wrkPartnerCost                                        
                                        
                         
                                      
 If (@Quantity > 1) and (@SecondItemDiscount + @ThirdItemDiscount + @FourthItemDiscount + @FifthItemDiscount + @SixthItemDiscount > 0)                                        
  Begin                                        
-----------------------------------------------------------------------------------------                                        
                                               
   Set @ItemNumber = 2                      
                                        
   WHILE @ItemNumber <= @Quantity                                        
  BEGIN                                           
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@ItemNumber=',@ItemNumber, 0, GetDate(),'bergc')                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                   
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
  If @ItemNumber = 2                                        
  Begin                                        
    -- the ItemNumber 2+ costs will go into "Additional Item" activities                                        
    Set @wrk2CustomerCost = ((1 - @SecondItemDiscount) * @wrkCustomerCost)                                        
    Set @wrk2PartnerCost = ((1 - @SecondItemDiscount) * @wrkPartnerCost)                                        
  End                                        
                                        
  If @ItemNumber = 3                                        
  Begin                                        
    Set @wrk2CustomerCost = ((1 - @ThirdItemDiscount) * @wrkCustomerCost)                                        
    Set @wrk2PartnerCost = ((1 - @ThirdItemDiscount) * @wrkPartnerCost)                                        
  End                                        
                                        
  If @ItemNumber = 4                                        
  Begin                                        
    Set @wrk2CustomerCost = ((1 - @FourthItemDiscount) * @wrkCustomerCost)                                        
    Set @wrk2PartnerCost = ((1 - @FourthItemDiscount) * @wrkPartnerCost)                                        
  End                                        
                                        
  If @ItemNumber = 5                                        
  Begin                                        
    Set @wrk2CustomerCost = ((1 - @FifthItemDiscount) * @wrkCustomerCost)                                       
    Set @wrk2PartnerCost = ((1 - @FifthItemDiscount) * @wrkPartnerCost)                                        
  End                                        
                                        
  If @ItemNumber > 5                                        
  Begin                                        
    Set @wrk2CustomerCost = ((1 - @SixthItemDiscount) * @wrkCustomerCost)                                        
    Set @wrk2PartnerCost = ((1 - @SixthItemDiscount) * @wrkPartnerCost)                                      
  End                                        
                                        
  Select @ActivityDescription = Description from Activity where ActivityID = 40                                                       
  If @wrk2PartnerCost = 0                                        
   Begin                                        
    Set @Margin = 1                                        
   End                                        
  Else                                        
   Begin                                        
    Select @Margin  = round(((@wrk2CustomerCost - @wrk2PartnerCost)*100/@wrk2PartnerCost)/100,4)                                              
   End                                        
                                              
  Set @ProjectActivitiesID = 0                                        
                                        
  -- we use the AddlItemActivities table keep track of which activity                                        
  -- corresponds to which additional item                                        
  -- check the AddlItemActivities table to see if this item exists                                        
  Select @ProjectActivitiesID = ProjectActivitiesID                                        
  From AddlItemActivities                                         
  where QuoteID = @QuoteID                                         
  and ItemNumber = @ItemNumber                                        
                                 
 -- If @ProjectActivitiesID = 0                                           
 --  Begin                             
 --  insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                    
 --  Values('ProjectRates activityid = 0 QuoteID=',@QuoteID, @PartnerID, GetDate(),'Anudeep')                                     
 -- Insert Into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                              
 --  Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                              
 --  InvoicedFlag,RateActionTypeID,CreatedBy,CreateDate,DeliveryChargeFlag)                                              
  --  Values(@QuoteID,@PartnerID,40,'FR',@wrk2PartnerCost,                                        
  -- @Margin,@wrk2CustomerCost,'FR',0,@ActivityDescription,                                        
  -- 0,3,@CreatedBy,GetDate(),0)                        
                                              
  --  Select  @ProjectRateID = @@IDENTITY                                              
                                              
  --  Insert Into ProjectActivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                              
 -- CustomerCost,PartnerCost,MinimumUsed,CreatedBy,CreateDate)                                                
 --   Values (@ProjectRateID,@DelDate,1,0,                                        
 --  @wrk2CustomerCost,@wrk2PartnerCost,0,@CreatedBy,GetDate())                                                
                                        
  --  Select  @ProjectActivitiesID = @@IDENTITY                                
                                        
  --  Insert Into AddlItemActivities (QuoteID, ProjectActivitiesID, ItemNumber, CreatedBy, CreateDate)                                        
  --  Values (@QuoteID, @ProjectActivitiesID, @ItemNumber, @CreatedBy, GetDate())                                        
  -- End                                          
                             
   --  Else                                          
                                        
   Begin                                
    -- never update a billed activity                                        
    Select @ExtractDate = Extracted,                                        
   @ProjectRateID = ProjectRateID                                        
    From ProjectActivities                                         
    Where ProjectActivitiesID = @ProjectActivitiesID                                        
                                         
    -- don't update if it is already billed                                        
    If isnull(@ExtractDate,'') = ''                                        
     Begin                                        
   Update ProjectRates                                           
   Set Margin = @Margin,                                          
   CustomerRateValue = @wrk2CustomerCost,                                          
   RateValue = @wrk2PartnerCost,                                          
   ModifiedBy = @CreatedBy,                                          
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                          
                                              
   Update ProjectActivities                                           
   Set CustomerCost = @wrk2CustomerCost,                                         
   PartnerCost = @wrk2PartnerCost,                                          
   ModifiedBy = @CreatedBy,                                          
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                  
     End                                        
   End                 
                                        
  Set @ItemNumber = @ItemNumber + 1                                        
   End  -- WHILE @ItemNumber <= @Quantity                                        
                                        
    End   -- If (@uantity > 1) and ....                                        
-----------------------------------------------------------------------------------------                                 
                                        
/*                                        
  Set @CustomerCost = @CustomerCost + ((1 - @SecondItemDiscount) * @wrkCustomerCost)                                        
  Set @PartnerCost = @PartnerCost + ((1 - @SecondItemDiscount) * @wrkPartnerCost)                                        
  End                                        
 If @Quantity > 2                                
  Begin                                        
  Set @CustomerCost = @CustomerCost + ((1 - @ThirdItemDiscount) * @wrkCustomerCost)                                        
  Set @PartnerCost = @PartnerCost + ((1 - @ThirdItemDiscount) * @wrkPartnerCost)                                        
  End                                        
 If @Quantity > 3            
  Begin                                        
  Set @CustomerCost = @CustomerCost + ((1 - @FourthItemDiscount) * @wrkCustomerCost)                                        
  Set @PartnerCost = @PartnerCost + ((1 - @FourthItemDiscount) * @wrkPartnerCost)                                        
  End                                        
 If @Quantity > 4                                         
  Begin                                        
  Set @CustomerCost = @CustomerCost + ((1 - @FifthItemDiscount) * @wrkCustomerCost)                                        
  Set @PartnerCost = @PartnerCost + ((1 - @FifthItemDiscount) * @wrkPartnerCost)                                        
  End                                        
 If @Quantity > 5                                         
  Begin                                        
  Set @CustomerCost = @CustomerCost + (((1 - @SixthItemDiscount) * @wrkCustomerCost) * (@Quantity - 5))                                        
  Set @PartnerCost = @PartnerCost + (((1 - @SixthItemDiscount) * @wrkPartnerCost) * (@Quantity - 5))                                        
  End                                        
*/                                        
                                        
 FETCH NEXT FROM Category_Cursor                                              
    INTO @ProductID,@Quantity                                             
   END       --WHILE @@FETCH_STATUS = 0                                          
                                        
  CLOSE Category_Cursor                                        
  DEALLOCATE Category_Cursor                                              
                                        
                                        
  IF @MileageRange > 1 and @MilesPlusDelivChgFlag = 1                                        
  -- NOTE: if it is mileagerange 1, it is not valid that the                                        
  -- Plus Delivery Charge box be checked.                                        
  Begin                                        
                                        
 -- Pounds or Hundred Weight                                        
 if @ExcessMilesUOM = 'LB' or @ExcessMilesUOM = 'CWT'                                            
 Begin                                          
    Select @UOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @ExcessMilesUOM                                          
     Set @CustomerCost = (@TotalWeight/@UOMMultiplier ) * @CustomerCost                                          
  Set @PartnerCost = (@TotalWeight/@UOMMultiplier ) * @PartnerCost                                          
 End                                         
                                        
 -- Cubes                                        
 if @ExcessMilesUOM = 'CU'                                           
 Begin                                          
     Set @CustomerCost = @TotalCube * @CustomerCost                                          
     Set @PartnerCost = @TotalCube * @PartnerCost                                          
 End                                         
                                        
 -- Square Feet                                        
 if @ExcessMilesUOM = 'SF'                                     
 Begin                                          
     Set @CustomerCost = @TotalSquareFeet * @CustomerCost                               Set @PartnerCost = @TotalSquareFeet * @PartnerCost                                          
 End                                          
                                 
 -- Linear Feet                                        
 if @ExcessMilesUOM = 'LF'                                           
 Begin                                          
     Set @CustomerCost = @TotalLinearFeet * @CustomerCost                                          
     Set @PartnerCost = @TotalLinearFeet * @PartnerCost                                          
 End                                          
                                        
 -- Each                                        
 if @ExcessMilesUOM = 'EA'                                         
 Begin             
     Set @CustomerCost = @TotalQuantity * @CustomerCost                                          
    Set @PartnerCost = @TotalQuantity * @PartnerCost                                          
 End                                          
                                        
 -- Per Piece                               
 if @ExcessMilesUOM = 'PC'                                            
 Begin                                          
     Set @CustomerCost = @TotalPieces * @CustomerCost                                          
  Set @PartnerCost = @TotalPieces * @PartnerCost                                          
 End                                          
                                        
 -- Miles                                        
 if @ExcessMilesUOM = 'MI'                                            
 Begin                                          
 -- cost times excess miles                                        
 -- I need to account for 3 or more mileage breaks where                                        
 -- the mileage falls into the 3rd break and both the 2nd and 3rd                                        
 -- are "Plus Delivery Charge".                                        
   If @FMMiles > @EndMiles                                         
    Begin                                        
  Set @wrkMiles = @EndMiles - @StartMiles + 1                                        
    End                                        
   Else                                        
    Begin                                        
  Set @wrkMiles = @FMMiles - @StartMiles + 1                                        
    End                                        
   Set @CustomerCost = @CustomerCost * @wrkMiles                                        
   Set @PartnerCost = @PartnerCost * @wrkMiles                                        
 End                                          
                                        
 Set @MileageRange = @MileageRange - 1                                        
                                        
 SELECT @MilesPlusDelivChgFlag = PlusDelivChgFlag, @StartMiles = StartMiles,                                         
   @EndMiles = EndMiles, @ExcessMilesUOM = ExcessMilesUOM                                        
 FROM AccountMileageBreaks                                         
 WHERE CustomerID = @AccountNumber                                             
 AND TypeCode = @MileageRange                                        
  End                                        
                                        
  Else                                        
                                        
  Begin                                        
 Set @KeepGoing = 0                                        
  End                                        
                                        
  Set @TotalCustomerCost = @TotalCustomerCost + @CustomerCost                                        
  Set @TotalPartnerCost = @TotalPartnerCost + @PartnerCost                                        
                                        
  Set @CustomerCost = 0                                        
  Set @PartnerCost = 0                                        
                   
  END      --WHILE @KeepGoing = 1                                        
                                 
  End -- end if (@Customer2ndItemDisc + @Customer3rdItemDisc + @Customer4thItemDisc + @Customer5thItemDisc + @Customer6thItemDisc) = 0                 
                                        
  -- set these up for processing below                                        
  Set @CustomerCost = @TotalCustomerCost                                        
  Set @PartnerCost = @TotalPartnerCost                                        
                                         
End -- end RatingTypeID 3 and 4                                         
----------------------------------------------------------------------------------------                                        
                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
-- Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                        
--insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                              
-- Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
                            
----------------------------------------------------------------------------------------                                        
If @RatingTypeID = 1 or @RatingTypeID = 2 -- Region or State to State                                        
Begin                                        
                                    
  Set @KeepGoing = 1                                        
  Set @CustomerCost = 0                                        
  Set @PartnerCost = 0                                
  -- in this section, we will use the next 2 variables                                        
  -- so we can keep the excess Mileage costs separate                                        
  Set @ExcessMilesCustomerCost = 0                                        
  Set @ExcessMilesPartnerCost = 0                                        
                                        
 WHILE @KeepGoing = 1                                        
 BEGIN                                        
                                        
  If @RatingTypeID = 1 -- Region                                          
  Begin                                          
   Select Top 1 @CustomerCost1 = isnull(Rate1,0),@CustomerCost2 = isnull(Rate2,0),                                        
  @CustomerCost3 = isnull(Rate3,0),@CustomerCost4 = isnull(Rate4,0),                                        
  @CustomerCost5 = isnull(Rate5,0),@CustomerCost6 = isnull(Rate6,0),                                        
  @CustomerCost7 = isnull(Rate7,0),@CustomerCost8 = isnull(Rate8,0),                                        
  @CustomerCost9 = isnull(Rate9,0),@CustomerCost10 = isnull(Rate10,0),                                        
  @CustomerCost11 = isnull(Rate11,0),@CustomerCost12 = isnull(Rate12,0),                                        
  @CustomerCost13 = isnull(Rate13,0),@CustomerCost14 = isnull(Rate14,0),                                        
  @CustomerCost15 = isnull(Rate15,0),@CustomerCost16 = isnull(Rate16,0),                                        
  @CustomerCost17 = isnull(Rate17,0),@CustomerCost18 = isnull(Rate18,0),                                        
  @CustomerCost19 = isnull(RAte19,0),@CustomerCost20 = isnull(Rate20,0)                                        
   From CustomerRatesRevenue                                        
   Where CustomerID = @AccountNumber                                         
 And Region = @Region                                        
 And ServiceLevelID = @ServiceLevel                                        
 And MileageRange = @MileageRange                                        
 And EffectiveDate < @DelDate                                        
 Order By EffectiveDate Desc                                        
  End                                          
                                          
  If @RatingTypeID = 2 -- State to State                                          
  Begin                                          
   Select Top 1 @CustomerCost1 = isnull(Rate1,0),@CustomerCost2 = isnull(Rate2,0),                 
  @CustomerCost3 = isnull(Rate3,0),@CustomerCost4 = isnull(Rate4,0),                                        
  @CustomerCost5 = isnull(Rate5,0),@CustomerCost6 = isnull(Rate6,0),                                        
  @CustomerCost7 = isnull(Rate7,0),@CustomerCost8 = isnull(Rate8,0),                                        
  @CustomerCost9 = isnull(Rate9,0),@CustomerCost10 = isnull(Rate10,0),                                        
  @CustomerCost11 = isnull(Rate11,0),@CustomerCost12 = isnull(Rate12,0),                                        
  @CustomerCost13 = isnull(Rate13,0),@CustomerCost14 = isnull(Rate14,0),                                        
  @CustomerCost15 = isnull(Rate15,0),@CustomerCost16 = isnull(Rate16,0),                                        
  @CustomerCost17 = isnull(Rate17,0),@CustomerCost18 = isnull(Rate18,0),                                        
  @CustomerCost19 = isnull(Rate19,0),@CustomerCost20 = isnull(Rate20,0)                                        
   From CustomerRatesRevenue                                        
   Where CustomerID = @AccountNumber                                         
 And StateID =  @RateStateID                                         
 And ServiceLevelID = @ServiceLevel                                        
 And MileageRange = @MileageRange                                        
 And EffectiveDate < @DelDate                                        
 Order By EffectiveDate Desc                                        
  End                                          
                 
-- Partner Cost is the same for both rating types           
        
                                  
  Select Top 1 @PartnerCost1 = isnull(Rate1,0),@PartnerCost2 = isnull(Rate2,0),                                        
  @PartnerCost3 = isnull(Rate3,0),@PartnerCost4 = isnull(Rate4,0),                                        
  @PartnerCost5 = isnull(Rate5,0),@PartnerCost6 = isnull(Rate6,0),                                        
  @PartnerCost7 = isnull(Rate7,0),@PartnerCost8 = isnull(Rate8,0),                                        
  @PartnerCost9 = isnull(Rate9,0),@PartnerCost10 = isnull(Rate10,0),                                        
  @PartnerCost11 = isnull(Rate11,0),@PartnerCost12 = isnull(Rate12,0),                                        
  @PartnerCost13 = isnull(Rate13,0),@PartnerCost14 = isnull(Rate14,0),                                        
  @PartnerCost15 = isnull(Rate15,0),@PartnerCost16 = isnull(Rate16,0),                                        
  @PartnerCost17 = isnull(Rate17,0),@PartnerCost18 = isnull(Rate18,0),                                        
  @PartnerCost19 = isnull(Rate19,0),@PartnerCost20 = isnull(Rate20,0)                                  
  From CustomerRatesComp                                        
  Where CustomerID = @AccountNumber                                         
 And Region = @Region                                        
 And ServiceLevelID = @ServiceLevel                                        
 And MileageRange = @MileageRange                                        
 And EffectiveDate < @DelDate                                        
 Order By EffectiveDate Desc            
         
 -- insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@2139=',CAST(@Region as Varchar(10)) + ' ' + CAST(@MileageRange as VARCHAR(10)) + ' ' + CAST(@DelDate As Varchar(12)), @QuoteID, GetDate(),'Anudeep')                                    
                                        
  -- @TypeCode falls through from the beginning of the procedure                                        
  If @TypeCode = 1                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost1                                        
   Set @PartnerCost = @PartnerCost1                                        
  End                                        
  If @TypeCode = 2                                        
  Begin                                        
Set @CustomerCost = @CustomerCost2                                        
   Set @PartnerCost = @PartnerCost2                                        
 End                                        
  If @TypeCode = 3                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost3                                        
   Set @PartnerCost = @PartnerCost3                                        
  End                                        
  If @TypeCode = 4                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost4                                        
   Set @PartnerCost = @PartnerCost4                                        
  End                                        
  If @TypeCode = 5                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost5                                        
   Set @PartnerCost = @PartnerCost5                                        
  End                                        
  If @TypeCode = 6                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost6                                        
   Set @PartnerCost = @PartnerCost6                                        
  End                                        
  If @TypeCode = 7                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost7                                        
   Set @PartnerCost = @PartnerCost7                                       
  End                                        
  If @TypeCode = 8                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost8                                        
   Set @PartnerCost = @PartnerCost8                                        
  End                                        
  If @TypeCode = 9                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost9                                        
   Set @PartnerCost = @PartnerCost9                                        
  End                                        
  If @TypeCode = 10                          
  Begin                                        
   Set @CustomerCost = @CustomerCost10                                        
   Set @PartnerCost = @PartnerCost10                                        
  End                                        
  If @TypeCode = 11                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost11                                        
   Set @PartnerCost = @PartnerCost11                    
  End                                        
  If @TypeCode = 12                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost12                                        
   Set @PartnerCost = @PartnerCost12                         
  End                                        
  If @TypeCode = 13                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost13                                        
   Set @PartnerCost = @PartnerCost13                                        
  End                                        
  If @TypeCode = 14                                        
  Begin                                   
   Set @CustomerCost = @CustomerCost14                                        
   Set @PartnerCost = @PartnerCost14                                        
  End                                        
  If @TypeCode = 15                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost15                                           Set @PartnerCost = @PartnerCost15                                        
  End                                        
  If @TypeCode = 16      
  Begin                                        
   Set @CustomerCost = @CustomerCost16                                        
   Set @PartnerCost = @PartnerCost16                                        
  End                                        
  If @TypeCode = 17                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost17                                        
   Set @PartnerCost = @PartnerCost17                                        
  End                                        
  If @TypeCode = 18                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost18                                        
   Set @PartnerCost = @PartnerCost18                                        
  End                                        
  If @TypeCode = 19                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost19                                        
   Set @PartnerCost = @PartnerCost19                                        
  End                                        
  If @TypeCode = 20                                        
  Begin                                        
   Set @CustomerCost = @CustomerCost20                                        
   Set @PartnerCost = @PartnerCost20                                        
  End                                        
                                        
----------------------------------------------------------------------------------------                                        
  If @MileageRange > 1 and @MilesPlusDelivChgFlag = 1                                        
  -- NOTE: if it is mileagerange 1, it is not valid that the                                        
  -- Plus Delivery Charge box be checked.                                        
  Begin                                        
                                        
 -- Pounds or Hundred Weight                                        
 if @ExcessMilesUOM = 'LB' or @ExcessMilesUOM = 'CWT'                                            
 Begin                                          
    Select @UOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @ExcessMilesUOM                                          
     Set @CustomerCost = (@TotalWeight/@UOMMultiplier ) * @CustomerCost                                          
  Set @PartnerCost = (@TotalWeight/@UOMMultiplier ) * @PartnerCost                                          
 End                                          
                                        
 -- Cubes                                        
 if @ExcessMilesUOM = 'CU'                    
 Begin                                          
     Set @CustomerCost = @TotalCube * @CustomerCost                                          
     Set @PartnerCost = @TotalCube * @PartnerCost                                          
 End                              
                                        
 -- Square Feet                                        
 if @ExcessMilesUOM = 'SF'                                           
 Begin                                          
     Set @CustomerCost = @TotalSquareFeet * @CustomerCost                                          
     Set @PartnerCost = @TotalSquareFeet * @PartnerCost                                          
 End                                          
                                        
 -- Linear Feet                                        
 if @ExcessMilesUOM = 'LF'                                           
 Begin                                          
     Set @CustomerCost = @TotalLinearFeet * @CustomerCost                                
     Set @PartnerCost = @TotalLinearFeet * @PartnerCost               
 End                                          
                                        
 -- Each                                        
 if @ExcessMilesUOM = 'EA'                                         
 Begin                                          
     Set @CustomerCost = @TotalQuantity * @CustomerCost                                          
    Set @PartnerCost = @TotalQuantity * @PartnerCost                                          
 End                                          
                                        
 -- Per Piece                         
 if @ExcessMilesUOM = 'PC'                                            
 Begin                                          
     Set @CustomerCost = @TotalPieces * @CustomerCost                                          
  Set @PartnerCost = @TotalPieces * @PartnerCost                                          
 End                                          
                                        
 -- Miles                               
 if @ExcessMilesUOM = 'MI'                                            
 Begin                                          
 -- cost times excess miles                                        
 -- I need to account for 3 or more mileage breaks where                                        
 -- the mileage falls into the 3rd break and both the 2nd and 3rd                                        
 -- are "Plus Delivery Charge".                                        
   If @FMMiles > @EndMiles                                         
    Begin                                        
  Set @wrkMiles = @EndMiles - @StartMiles + 1                                        
    End                                        
   Else                                        
    Begin                                        
  Set @wrkMiles = @FMMiles - @StartMiles + 1                                        
    End                                        
  Set @CustomerCost = @CustomerCost * @wrkMiles                
  Set @PartnerCost = @PartnerCost * @wrkMiles                                        
 End                                          
                                        
 -- accumulate excess mileage costs here                                        
 Set @ExcessMilesCustomerCost = @ExcessMilesCustomerCost + @CustomerCost                                        
 Set @ExcessMilesPartnerCost = @ExcessMilesPartnerCost + @PartnerCost                                        
 Set @CustomerCost = 0                                        
 Set @PartnerCost = 0                                        
                                        
 Set @MileageRange = @MileageRange - 1                                        
                                        
 SELECT @MilesPlusDelivChgFlag = PlusDelivChgFlag, @StartMiles = StartMiles,                                         
   @EndMiles = EndMiles, @ExcessMilesUOM = ExcessMilesUOM                                        
 FROM AccountMileageBreaks                                         
 WHERE CustomerID = @AccountNumber                                             
 AND TypeCode = @MileageRange                                        
  End                                        
                                        
  Else                                        
                                        
  Begin                                        
 Set @KeepGoing = 0                                       
  End                                        
                                        
 END -- WHILE @KeepGoing = 1                                        
                                  
----------------------------------------------------------------------------------------                                        
                                        
-- the "Keep Going" loop above will handle the Mileage Breaks.  We will "back thru"                                        
-- the mileage breaks until we hit one without the "Plus Delivery Charge" checked,                                        
-- or until we get to the first one.                                        
                                        
-- at this point, @ExcessMilesCustomerCost and @ExcessMilesPartnerCost contain the excess                                         
-- mileage costs, and @CustomerCost and @PartnerCost contain the rate to use/process.                                        
-- of course, that rate could be "Plus Delivery Charge" on the Weight Range.                                        
                                        
                                        
/*                                        
If @AccountNumber = 335                                        
begin                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
end                                        
*/                                        
                                        
  Set @WasPlusDeliv = 0                                        
  Set @PlusDelivCustomerCost = 0                                        
  Set @PlusDelivPartnerCost = 0                                        
                    
  WHILE @TypeCode > 1 and @PlusDelivChgFlag = 1                                        
  BEGIN                                        
 -- we are here if we have a weight range "Plus Delivery Charge"                                        
                                        
 /*                                        
 If @AccountNumber = 2487                                        
 begin                                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  Values('@UOM=',@UOM, 0, GetDate(),'bergc')                                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  Values('@ExpUOM=',@ExpUOM, 0, GetDate(),'bergc')                                        
 end                                        
 */                                        
                                        
 -- pass this flag to avoid overcharging if per pound or CWT calc                                        
 Set @WasPlusDeliv = 1                                        
                                        
 Select @UOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @UOM                                          
 Select @EXPUOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @ExpUOM                                          
                                        
 -- Pounds or Hundred Weight                                        
 if @UOM = 'LB' or @UOM = 'CWT' or @ExpUOM = 'LB' or @ExpUOM = 'CWT'                                         
 Begin                                    
   -- since this is "Plus Delivery Charge", we are dealing with an "excess" charge,                                         
   -- so we need to calculate the excess weight here                                        
   If @TotalWeight > @EndWgt                                         
    Begin                                        
  Set @wrkWeight = @EndWgt - @StartWgt + 1                                        
    End                                        
   Else                                        
    Begin                                        
  Set @wrkWeight = @TotalWeight - @StartWgt + 1                                        
    End                             
 End                           
      
 /*                                        
 If @AccountNumber = 2487                                        
 begin                                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  Values('@StartWgt=',@StartWgt, 0, GetDate(),'bergc')                                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                        
  Values('@EndWgt=',@EndWgt, 0, GetDate(),'bergc')                                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  Values('@wrkWeight=',@wrkWeight, 0, GetDate(),'bergc')                                        
 end                                        
 */                                        
                                        
 if @UOM = 'LB' or @UOM = 'CWT'                                            
 Begin                                          
      Set @CustomerCost = (@wrkWeight/@UOMMultiplier ) * @CustomerCost                       
 End                                          
 if @ExpUOM = 'LB' or @ExpUOM = 'CWT'                                            
 Begin                                          
   Set @PartnerCost = (@wrkWeight/@EXPUOMMultiplier ) * @PartnerCost                                          
 End                                          
                                        
 /*                                      
 If @AccountNumber = 2487                                        
 begin                                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                              
  Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                        
 insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
  Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
 end                                        
 */                                        
                                        
 -- Cubes                                        
 if @UOM = 'CU'                                           
 Begin                                          
      Set @CustomerCost = @TotalCube * @CustomerCost                                          
 End                                          
 if @ExpUOM = 'CU'                                           
 Begin                                          
      Set @PartnerCost = @TotalCube * @PartnerCost                          
 End                                            
                                        
 -- Square Feet                                        
 if @UOM = 'SF'                                           
 Begin                                          
      Set @CustomerCost = @TotalSquareFeet * @CustomerCost                                          
 End                                           
 if @ExpUOM = 'SF'                                           
 Begin                                          
      Set @PartnerCost = @TotalSquareFeet * @PartnerCost                                          
 End                                          
                                        
 -- Linear Feet                                        
 if @UOM = 'LF'                                           
 Begin                                          
      Set @CustomerCost = @TotalLinearFeet * @CustomerCost                                          
 End                                            
 if @ExpUOM = 'LF'                                           
 Begin                                
      Set @PartnerCost = @TotalLinearFeet * @PartnerCost                                          
 End                                          
                                        
 -- Each                                        
 if @UOM = 'EA'                                         
 Begin                                          
      Set @CustomerCost = @TotalQuantity * @CustomerCost                                          
 End                                          
 if @ExpUOM = 'EA'                                        
 Begin                                          
   Set @PartnerCost = @TotalQuantity * @PartnerCost                                          
 End             
                                        
 -- Per Piece                                        
 if @UOM = 'PC'                                            
 Begin               
      Set @CustomerCost = @TotalPieces * @CustomerCost                                          
 End                                          
 if @ExpUOM = 'PC'                                            
 Begin                                          
Set @PartnerCost = @TotalPieces * @PartnerCost                                          
 End                                          
                                        
 -- Flat Rate will fall through (@CustomerCost=@CustomerCost, @PartnerCost=@PartnerCost)                                       
                                        
 Set @PlusDelivCustomerCost = @PlusDelivCustomerCost + @CustomerCost                                    
 Set @PlusDelivPartnerCost = @PlusDelivPartnerCost + @PartnerCost                                        
 Set @CustomerCost = 0                                        
 Set @PartnerCost = 0                                        
                                        
 Set @TypeCode = @TypeCode - 1                                        
                                        
 SELECT @UOM = UnitofMeasureCD, @ExpUOM = ExpUnitofMeasureCD,                                        
  @PlusDelivChgFlag = PlusDelivChgFlag,                                        
  @StartWgt = StartWgt, @EndWgt = EndWgt                                        
 FROM AccountWgtBreaks                                         
 WHERE CustomerID = @AccountNumber                                             
 AND TypeCode = @TypeCode                                           
 ORDER BY TypeCode                                          
                                        
                                        
 -- now get the appropriate rate for the new @TypeCode                                        
 If @TypeCode = 1                                        
 Begin                                        
  Set @CustomerCost = @CustomerCost1                                        
  Set @PartnerCost = @PartnerCost1                                        
 End                                        
 If @TypeCode = 2                                        
 Begin                                        
  Set @CustomerCost = @CustomerCost2                                        
  Set @PartnerCost = @PartnerCost2                                        
 End                                        
 If @TypeCode = 3                                        
 Begin                                        
  Set @CustomerCost = @CustomerCost3                                        
     Set @PartnerCost = @PartnerCost3                                        
 End                                        
 If @TypeCode = 4                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost4                                        
     Set @PartnerCost = @PartnerCost4                                        
 End                                        
 If @TypeCode = 5                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost5                                        
  Set @PartnerCost = @PartnerCost5                                  
 End                                        
 If @TypeCode = 6                                        
 Begin                                     
 Set @CustomerCost = @CustomerCost6                                        
     Set @PartnerCost = @PartnerCost6                                        
 End                                        
 If @TypeCode = 7                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost7                                        
     Set @PartnerCost = @PartnerCost7                                        
 End                                        
 If @TypeCode = 8                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost8                                        
  Set @PartnerCost = @PartnerCost8           
 End                                        
 If @TypeCode = 9                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost9                                        
     Set @PartnerCost = @PartnerCost9                                        
 End                                        
 If @TypeCode = 10                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost10                                        
     Set @PartnerCost = @PartnerCost10                                        
 End                                        
    If @TypeCode = 11                                 
 Begin                                        
     Set @CustomerCost = @CustomerCost11                      
     Set @PartnerCost = @PartnerCost11                                        
 End                                        
 If @TypeCode = 12                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost12                                        
     Set @PartnerCost = @PartnerCost12                                        
 End                                        
    If @TypeCode = 13                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost13                                        
     Set @PartnerCost = @PartnerCost13                                        
 End                                        
 If @TypeCode = 14                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost14                                        
     Set @PartnerCost = @PartnerCost14                                
 End                                        
 If @TypeCode = 15                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost15                                        
     Set @PartnerCost = @PartnerCost15                                        
 End                                        
 If @TypeCode = 16                                      
 Begin                                        
     Set @CustomerCost = @CustomerCost16                                        
     Set @PartnerCost = @PartnerCost16                                        
 End                                        
 If @TypeCode = 17                                        
 Begin                                        
 Set @CustomerCost = @CustomerCost17                                        
     Set @PartnerCost = @PartnerCost17                                        
 End                                        
 If @TypeCode = 18                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost18                                        
     Set @PartnerCost = @PartnerCost18                                        
 End                                        
 If @TypeCode = 19                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost19                                        
     Set @PartnerCost = @PartnerCost19                                        
 End                                        
 If @TypeCode = 20                                        
 Begin                                        
     Set @CustomerCost = @CustomerCost20                                        
     Set @PartnerCost = @PartnerCost20                                        
 End                                        
                                        
  END -- WHILE @TypeCode > 1 and @PlusDelivChgFlag = 1                                        
                                        
                                        
-- at this point, we will have the ExcellMilesCustomerCost,                                        
-- the PlusDelivCustomerCost and the CustomerCost.                                        
-- the CustomerCost needs to be adjusted for UOM.                                        
                                        
                                        
  Select @UOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @UOM                                          
  Select @EXPUOMMultiplier = Multiplier from UnitofMeasure where UnitofMeasureCD = @ExpUOM                                          
                                        
                    
 -- Pounds or Hundred Weight                                        
 if @UOM = 'LB' or @UOM = 'CWT' or @ExpUOM = 'LB' or @ExpUOM = 'CWT'                                         
 Begin                                        
   If @WasPlusDeliv = 1                                        
   Begin                                        
  -- since there was a "Plus Delivery Charge", we already dealt with the "excess" charge,                                         
  -- so our calculation here can only cover the current weight range                                       
  If @TotalWeight > @EndWgt                                         
   Begin                                        
    -- Set @wrkWeight = @EndWgt - @StartWgt + 1                                        
    Set @wrkWeight = @EndWgt                                        
   End                                        
 Else                                        
   Begin                                        
    -- Set @wrkWeight = @TotalWeight - @StartWgt + 1                                        
    Set @wrkWeight = @TotalWeight                                        
   End                                        
   End                                        
 End                                        
                       
  -- Pounds or Hundred Weight                                        
  if @UOM = 'LB' or @UOM = 'CWT'                                            
  Begin                          
     Set @CustomerCost = (@TotalWeight/@UOMMultiplier ) * @CustomerCost                                          
  End                         
  if @ExpUOM = 'LB' or @ExpUOM = 'CWT'                                            
  Begin                                          
 Set @PartnerCost = (@TotalWeight/@EXPUOMMultiplier ) * @PartnerCost                                          
  End                                          
                                        
  -- Cubes                                        
  if @UOM = 'CU'                                           
  Begin                                          
     Set @CustomerCost = @TotalCube * @CustomerCost                                          
  End                                          
  if @ExpUOM = 'CU'                                           
  Begin                                          
    Set @PartnerCost = @TotalCube * @PartnerCost                                          
  End                    
                                        
  -- Square Feet                                        
  if @UOM = 'SF'                                           
  Begin                                          
     Set @CustomerCost = @TotalSquareFeet * @CustomerCost                                          
  End                                           
  if @ExpUOM = 'SF'                                           
  Begin                                          
     Set @PartnerCost = @TotalSquareFeet * @PartnerCost                                          
  End                                          
                       
  -- Linear Feet                                        
  if @UOM = 'LF'                                           
  Begin                                          
     Set @CustomerCost = @TotalLinearFeet * @CustomerCost                                          
  End                                            
  if @ExpUOM = 'LF'                                           
  Begin                                          
     Set @PartnerCost = @TotalLinearFeet * @PartnerCost                                          
  End                                          
                          
  -- Each                                        
  if @UOM = 'EA'                                         
  Begin                               
     Set @CustomerCost = @TotalQuantity * @CustomerCost                                          
  End                                          
  if @ExpUOM = 'EA'                                        
  Begin                                          
 Set @PartnerCost = @TotalQuantity * @PartnerCost                                          
  End                                          
                                        
  -- Per Piece                                        
  if @UOM = 'PC'                                            
  Begin                                          
     Set @CustomerCost = @TotalPieces * @CustomerCost                                          
  End                                          
  if @ExpUOM = 'PC'                                            
  Begin                                          
 Set @PartnerCost = @TotalPieces * @PartnerCost                   
  End                   
                                        
  -- Flat Rate will fall through (@CustomerCost=@CustomerCost, @PartnerCost=@PartnerCost)                                  
                                        
  -- after the CustomerCost and PartnerCost is adjusted, add the ExcessMiles and                                        
  -- PlusDeliv costs                                        
  Set @CustomerCost = @CustomerCost + @ExcessMilesCustomerCost + @PlusDelivCustomerCost                                        
  Set @PartnerCost = @PartnerCost + @ExcessMilesPartnerCost + @PlusDelivPartnerCost           
          
 --insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@2687=',ISNUll(CAST(@PartnerCost as VARCHAR(10)),'') + '  ' + @UOM , @QuoteID, GetDate(),'Anudeep')                                        
                                        
End -- Rating Type 1 and 2                                        
----------------------------------------------------------------------------------------                                        
                                         
                                        
/* 9/30/2003: allow for one man rate discount */                                          
/* 3/15/2011: we now select these fields at the beginning of this stored procedure */                                        
--Select @OneManRateFlag = OneManRateFlag,                                        
-- -- 10/8/2007: while we are at it, get rcpvalue for valuation calculation                                        
-- @RCPValue = isnull(RCPValue, 0)                                        
-- from Project where QuoteID = @QuoteID                                            
                                        
-- 3/15/2011: we now get this at the start                                        
-- Select @MinCharge = MinimumCharge, @MinComp = MinimumComp, @CustomerOneManDisc = OneManRateDisc from Customer where customerid = @AccountNumber                                           
Select @PartnerOneManDisc = OneManRateDisc from Partner where Partnerid = @PartnerID                                           
                                          
if @OneManRateFlag = 1 and @CustomerOneManDisc > 0 and @CustomerCost > 0                                          
 Begin                                      
    Select @CustomerCost = @CustomerCost - (@CustomerCost * @CustomerOneManDisc)                                           
 End                                            
                                          
if @OneManRateFlag = 1 and @PartnerOneManDisc > 0 and  @PartnerCost > 0                                          
 Begin                                            
    Select @PartnerCost = @PartnerCost - (@PartnerCost * @PartnerOneManDisc)                                           
 End                                            
/* end one man rate calculation */                              
                                          
                                            
if ( @CustomerCost > 0 or @PartnerCost > 0 )  AND @DeliveryChargeActivityID <> 57                                           
 Begin                                             
      
 --  INSERT INTO AppErrorLog (Application, Error, QuoteID, CreateDate, CreatedBy)    
 --Values ('Order Swap Calc','2749' + CAST(@DeliveryChargeActivityID AS VARCHAR(10)),@QuoteID,GETDATE(),'Anudeep')                                           
   --Select @ActivityDescription = Description from Activity where ActivityID = 37                                            
   Select @ActivityDescription = Description from Activity where ActivityID = @DeliveryChargeActivityID                                           
                                        
   -- don't do this yet - talk to Kramer about it                                         
   --If @DimWeightUsedFlag = 1                                        
   --  Begin                                        
   --    Set @ActivityDescription = Left(@ActivityDescription + ' (Dim Weight Used)', 50)                                        
   --  End                                        
                                           
   If @PartnerCost = 0                                        
     Begin                                        
  Set @Margin = 1                                        
  End                                        
   Else                                        
  Begin                                        
 Select @Margin  = round(((@CustomerCost - @PartnerCost)*100/@PartnerCost)/100,4)                                              
  End                                        
                                             
   /*Check if record already exists for the given project in ProjectRates table,                                          
     if record exists then control is coming from Add_Project_Wizard2 page                                          
     else either from Final Mile or Final Mile Customer Warehouse screen.                                           
   */                                          
                                          
   Select @RecordCount = Count(*)                                         
   from ProjectRates                                         
   where QuoteID = @QuoteID                                         
   and DeliveryChargeFlag = 1                                          
                                        
   If @RecordCount = 0                                           
 Begin                                          
     Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                       
     Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                 
     InvoicedFlag,RateActionTypeID,createdby,createDate,DeliveryChargeFlag)                                              
     Values(@QuoteID,@PartnerID,@DeliveryChargeActivityID,'FR',@PartnerCost,@Margin,                                              
     @CustomerCost,'FR',0,@ActivityDescription,0,3,@CreatedBy,getdate(),1)                                              
                                               
     Select  @ProjectRateID = @@IDENTITY                                              
                                               
     insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                              
     CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                                
     values (@ProjectRateID,@DelDate,1,0,@CustomerCost,@PartnerCost,0,@CreatedBy,getdate())                                                
    End                                          
   else                                          
 Begin                                          
  -- look here                                        
  -- could select Extracted here also to make sure the item is not Invoiced already                                        
     Select @ProjectRateID = ProjectRateID from ProjectRates where QuoteID = @QuoteID                                       
  and DeliveryChargeFlag = 1                                          
                                                
  -- 8/20/2010: never update a billed activity                                        
  Select @ExtractDate = Extracted from ProjectActivities where ProjectRateID = @ProjectRateID                     
                                   
  -- 8/20/2010: don't update if it is already billed                                        
  If isnull(@ExtractDate,'') = ''                                        
    Begin                                        
  Update ProjectRates                                           
  Set Margin = @Margin,                                          
  CustomerRateValue = @CustomerCost,                                          
  RateValue = @PartnerCost,                                          
  -- 1/9/2012: update the next 2 fields because they might change now                                        
  ActivityID = @DeliveryChargeActivityID,                                        
  ProjectRateDescription = @ActivityDescription,                                        
  ModifiedBy = @CreatedBy,                                          
  ModifiedDate = getdate()                                          
  Where ProjectRateID = @ProjectRateID                                          
                                              
  Update ProjectActivities                                           
  Set CustomerCost = @CustomerCost,                                          
  PartnerCost = @PartnerCost,                                          
  ModifiedBy = @CreatedBy,                                          
  ModifiedDate = getdate()                                          
  Where ProjectRateID = @ProjectRateID                                           
    End                                        
 End                                         
                          
   Set @DeliveryChargeCost=1                                            
 End                                              
                                        
else                                           
 Begin     
 if (@CustomerCost > 0 or @PartnerCost > 0 )  AND @DeliveryChargeActivityID =  57    
     
 BEGIN     
 ------- Order Swap Calc --BEGIN    
 Select @PartnerCost = A.PartnerRateValue , @CustomerCost = A.CustomerRateValue, @ActivityDescription = B.Description from Accessories A INNER JOIN     
 Activity B ON A.ActivityId = B.ActivityId WHERE A.CustomerId = 2843 AND A.ActivityId = 57     
     
 --INSERT INTO AppErrorLog (Application, Error, QuoteID, CreateDate, CreatedBy)    
 --Values ('Order Swap Calc','',@QuoteID,GETDATE(),'Anudeep')    
     
 If @PartnerCost = 0                                        
     Begin                                        
  Set @Margin = 1                                        
  End                                        
   Else                                        
  Begin                                        
 Select @Margin  = round(((@CustomerCost - @PartnerCost)*100/@PartnerCost)/100,4)                                              
  End                                        
                                             
   /*Check if record already exists for the given project in ProjectRates table,                                          
     if record exists then control is coming from Add_Project_Wizard2 page                                          
     else either from Final Mile or Final Mile Customer Warehouse screen.                                           
   */                                          
                                          
   Select @RecordCount = Count(1)                                         
   from ProjectRates WITH(NOLOCK)                                        
   where QuoteID = @QuoteID                                        
   and DeliveryChargeFlag = 1                                          
                                        
   If @RecordCount = 0                                           
 Begin                                          
     Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                       
     Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                              
     InvoicedFlag,RateActionTypeID,createdby,createDate,DeliveryChargeFlag)                                              
     Values(@QuoteID,@PartnerID,@DeliveryChargeActivityID,'FR',@PartnerCost,@Margin,                                              
     @CustomerCost,'FR',0,@ActivityDescription,0,3,@CreatedBy,getdate(),1)                                              
                                               
     Select  @ProjectRateID = @@IDENTITY                                              
                                               
     insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                              
     CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                                
     values (@ProjectRateID,@DelDate,1,0,@CustomerCost,@PartnerCost,0,@CreatedBy,getdate())                                                
    End     
 -------- Order Swap Calc -- END    
     END     
     
       ELSE     
        BEGIN                                    
          Set @DeliveryChargeCost=0             
        END                                           
 End                                            
                        
/* If Delivery Charge is 0 then send mail to Coordinator, if coordiantot mail id null, then send mail to                                          
Heather Wright  -- 9/21/2016 make that Kyle Dodge                                        
*/                                         
                                        
-- 11/21/2014: don't send this email for Electrolux Demand Flow                                        
if (@BusinessTypeID <> 2) and (@RowCount = 0 or @DeliveryChargeCost = 0) and @AccountNumber <> 2025                                                      
Begin                                                  
 -- 10/9/2013: do not send this email for Local orders                                        
 -- 10/10/2005: get coordinator from customer, not partner                                      
 -- 10/9/2013: get the coordinator from the partner again                                        
 -- 11/18/2013: check customer coordinator first and use if there                                      
 -- if not there, get the partner coordinator                                        
 Select  @CoordEmail = s.emailaddress from systemuser s, customer c                                            
 where s.userid = c.coordinator                          
 and c.customerid = @AccountNumber                                        
                                        
 if isnull(@CoordEmail,'Null') = 'Null'                                              
  Begin                                              
 Select  @CoordEmail = s.emailaddress from systemuser s, partner p                                            
 where s.userid = p.coordinator                                            
 and p.partnerid = @PartnerID                        End                                              
  -- Umakanth need to change this after discuss with Anudeep                                      
-- if isnull(@CoordEmail,'Null') = 'Null'                                              
 -- Begin                                              
 --  Set @CoordEmail = 'Kyle.Dodge@STIDelivers.com;anudeep.borugadda@crst.com'                                              
--  End                                              
-- IF   @CoordEmail NOT LIKE '%Gunnar.Kaiser@crst.com%'          
 --   BEGIN                                              
 -- insert into FaxEMailStaging (QuoteID,PartnerID,FaxmailtypeID,StatusID,               
 --  FaxMailFrom,FaxMailValue,Subject,RequestedSendDateTime)                                                  
 -- Values(@QuoteID,@PartnerID,2,0,'Administrator',@CoordEmail,'Failed to create Deliv Charge for Order #' +  convert(varchar(20),@QuoteID) ,getdate())                                                  
  --   END                                              
End                                                  
                              
/* Delivery Charge calculation ends */                                          
                                        
----------------------------------------------------------------------------------------                                        
-- 12/18/2012: only calc delivery charge for Returns from Residence                            
SkipDeliveryChargeCalc:                                        
                        
----------------------------------------------------------------------------------------                                        
/* Accessorial Charge calculation starts */                                          
                                          
Create Table #RemoveAccessorial(                                          
 ProjectRateID int                                                
)                                                
                                          
                                         
/*Delete the Accessorial from ProjectRates and ProjectActivities tables when the product is removed from                                           
add_project_wizard_2 page */                                          
                                          
/*Insert into #RemoveAccessorial                                          
Select pr.projectrateid  from ProjectRates pr where QuoteID = @QuoteID                                           
and ProductId in (Select ProductID from #tempProducts)                                           
                                        
Delete from ProjectActivities where ProjectRateID in (                                          
Select ProjectRateID from #RemoveAccessorial)                                          
                                         
Delete from ProjectRates where ProjectRateID in (                                          
Select ProjectRateID from #RemoveAccessorial)                                    
*/                                          
                                         
                                        
-- 8/20/2010: change query to look at Extracted, as it                                        
-- is a better determinant of whether the activity is billed                                        
-- (the InvoicedFlag doesn't get cleared when invoice is rejected.)                                        
-- 10/1/2010: also look at PartnerExtractDate to see if a partner                                        
-- invoice has been created.                                        
Insert into #RemoveAccessorial                                          
Select pr.ProjectRateID from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                        
 where pr.QuoteID = @QuoteID                                        
 and pr.AccessorialFlag = 1                                         
 and pa.Extracted is null                   
 and pa.PartnerExtractDate is null                                        
 -- and InvoicedFlag <> 1                                          
                                          
Delete from ProjectActivities where ProjectRateID in (                                  
Select ProjectRateID from #RemoveAccessorial)                                          
                                          
-- 10/1/2010: no need to delete the Project Rate                                        
-- 10/5/2010: when I didn't delete it, the @RecordCount below was messed up.                                        
Delete from ProjectRates where ProjectRateID in (                                         
Select ProjectRateID from #RemoveAccessorial)                                          
                                        
                                         
/* Accessorial removal ends */                                          
                                        
----------------------------------------------------------------------------------------                                        
/* first, add customer defined accessorial (from customertemplate) */                                        
                                        
Set @ProductID = 0                                        
                                        
Select  @PermAccess = isnull(PermAccess,0),                                        
 @PermAccessID = isnull(PermAccessID,0),                                        
 @PermAccessQty = isnull(PermAccessQty,0),                                       
 @PermAccessID2 = isnull(PermAccessID2,0),                                        
 @PermAccessQty2 = isnull(PermAccessQty2,0),                                        
 @PermAccessID3 = isnull(PermAccessID3,0),                                        
 @PermAccessQty3 = isnull(PermAccessQty3,0),                                        
 @PermAccessID4 = isnull(PermAccessID4,0),                                        
 @PermAccessQty4 = isnull(PermAccessQty4,0),                                        
 @PermAccessID5 = isnull(PermAccessID5,0),                                        
 @PermAccessQty5 = isnull(PermAccessQty5,0),                                        
 @PermAccessID6 = isnull(PermAccessID6,0),                                        
 @PermAccessQty6 = isnull(PermAccessQty6,0),                                        
 @PermAccessID7 = isnull(PermAccessID7,0),                                        
 @PermAccessQty7 = isnull(PermAccessQty7,0),                                        
 @PermAccessID8 = isnull(PermAccessID8,0),                                        
 @PermAccessQty8 = isnull(PermAccessQty8,0),                                        
 @PermAccessID9 = isnull(PermAccessID9,0),                                        
 @PermAccessQty9 = isnull(PermAccessQty9,0),                                        
 @PermAccessID10 = isnull(PermAccessID10,0),                                        
 @PermAccessQty10 = isnull(PermAccessQty10,0),                                        
 -- 8/4/2016: new fields for excess miles permanent accessorials                      
 @ExcessMileage = isnull(ExcessMileage,0),                              
 @ExcessMileageID = isnull(ExcessMileageID,0),                                        
 @ExcessMileageQty = isnull(ExcessMileageQty,0),                                        
 @ExcessMileageStartMiles = isnull(ExcessMileageStartMiles,0),                     
 @ExcessMileageEndMiles = isnull(ExcessMileageEndMiles,0),                                        
 @ExcessMileageID2 = isnull(ExcessMileageID2,0),                                        
 @ExcessMileageQty2 = isnull(ExcessMileageQty2,0),                                        
 @ExcessMileageStartMiles2 = isnull(ExcessMileageStartMiles2,0),                                        
 @ExcessMileageEndMiles2 = isnull(ExcessMileageEndMiles2,0),                                        
 @ExcessMileageID3 = isnull(ExcessMileageID3,0),                                        
 @ExcessMileageQty3 = isnull(ExcessMileageQty3,0),                                        
 @ExcessMileageStartMiles3 = isnull(ExcessMileageStartMiles3,0),                                        
 @ExcessMileageEndMiles3 = isnull(ExcessMileageEndMiles3,0),              -- Greater Mileage values assignment --                                  
 --@TemplateId = ISNULL([HDN_Providence].[dbo].CustomerTemplate.TemplateId,0),                                  
 @GrtMileage = isnull(grtMileage,0),                                    
 @GrtMileageID = isnull(grtMileageID,0),                                        
 @GrtMileageQty = isnull(grtMileageQty,0),                                        
 @GrtMileLastFreeMiles = isnull(grtMileLastFreeMiles,0)                                    
 ----------------------------------------------------------                                   
-- 8/10/2012: change to get the template from the project                                          
From Quote                                        
INNER JOIN [HDN_Providence].[dbo].CustomerTemplate CT ON Quote.TemplateID = CT.TemplateId                                        
Where QuoteID = @QuoteID                                        
--From CustomerTemplate                                        
--Where CustomerID = @AccountNumber                                        
--And Active = 1                                 
                                        
                                        
If @PermAccess = 1 and @PermAccessID > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @PermAccessID                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID,@PermAccessQty,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                       
 
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID2 > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
/*                                        
  ,                                        
  @ActivityDescription = ProjectDescription,                        
   @UnitOfMeasureCD = UnitOfMeasureCD,                                            
        @CustomerRateUnitOfMeasure = CustomerRateUnitOfMeasure,                                            
        @CustomerRateValue = CustomerRateValue,                                            
        @RateValue = PartnerRateValue,          
  @PartnerUOM = PartnerUOM,                                        
  @CustomerMinimumUOM = CustomerMinimumUOM,                        @CustomerMinimumRateValue = CustomerMinimumRateValue,                                        
  @PartnerMinimumUOM = PartnerMinimumUOM,                                        
  @PartnerMinimumRateValue = PartnerMinimumRateValue,                                        
     @RateActionTypeID = RateActionTypeID                                           
*/                                         
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID2                                            
                                        
 -- the accessorial will not delete if it has already been billed                 
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID2,@PermAccessQty2,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                     
  
   
/*                                        
     --Calc Margin and insert record into ProjectRates and ProjectActivites tables                             
   If @RateValue <> 0                                           
  Begin                                          
    Select @Margin  = round(((@CustomerRateValue - @RateValue)*100/@RateValue)/100,4)                                            
  End                                              
   If @RateValue = 0                                           
   Begin                                          
  Select @Margin  = 1                                           
  End                                              
                                             
    Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                              
    Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                              
    InvoicedFlag,RateActionTypeID,createdby,createDate,             
  --ProductID,                                        
  AccessorialFlag)                                              
    Values(@QuoteID,@PartnerID,@ActivityID,@UnitOfMeasureCD,@RateValue*@PermAccessQTY2,@Margin,                                              
    @CustomerRateValue*@PermAccessQTY2,@CustomerRateUnitOfMeasure,0,@ActivityDescription,0,                                            
    @RateActionTypeID,@CreatedBy,getdate(),                                        
  --@ProductID,                                        
  1)                                        
                                               
    Select  @ProjectRateID = @@IDENTITY                                              
                                               
    Insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                              
    CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                                
    values (@ProjectRateID,@DelDate,1,0,@CustomerRateValue*@PermAccessQTY2,@RateValue*@PermAccessQTY2,0,                                          
    @CreatedBy,getdate())                                         
*/                                                     
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID3 > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID3                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID3,@PermAccessQty3,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                     
   
/*                                        
     --Calc Margin and insert record into ProjectRates and ProjectActivites tables                     
   If @RateValue <> 0                                           
  Begin                                          
    Select @Margin  = round(((@CustomerRateValue - @RateValue)*100/@RateValue)/100,4)                                            
  End                                              
   If @RateValue = 0                                           
   Begin                              
  Select @Margin  = 1                                           
  End                                              
                                             
    Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                              
    Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                              
    InvoicedFlag,RateActionTypeID,createdby,createDate,                                        
  --ProductID,                                        
  AccessorialFlag)                                              
    Values(@QuoteID,@PartnerID,@ActivityID,@UnitOfMeasureCD,@RateValue*@PermAccessQTY3,@Margin,                                              
    @CustomerRateValue*@PermAccessQTY3,@CustomerRateUnitOfMeasure,0,@ActivityDescription,0,                                            
    @RateActionTypeID,@CreatedBy,getdate(),                                        
  --@ProductID,                                        
  1)                                        
                                             
    Select  @ProjectRateID = @@IDENTITY                                              
                                               
    Insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                              
    CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                                
    values (@ProjectRateID,@DelDate,1,0,@CustomerRateValue*@PermAccessQTY3,@RateValue*@PermAccessQTY3,0,                                          
    @CreatedBy,getdate())                                           
*/                                                         
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID4 > 0                                        
Begin        
 Select @ActivityID = ActivityID                                        
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID4                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID4,@PermAccessQty4,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy         
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID5 > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID5                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0               
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID5,@PermAccessQty5,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                     
  
   
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID6 > 0                                    
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID6                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                           
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID6,@PermAccessQty6,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                      
  
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID7 > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID7                                            
            
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID7,@PermAccessQty7,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                      
  
 End                                        
End                                        
----------------------------------------------------------------------------------------                    
--12/18/2012: we will now reserve accessorial 8 to be the handling charges associated                                        
-- with a Return order that is returned from the Warehouse.                                        
-- a return from a residence will calculate a delivery charge.                                        
If @OrderTypeID = 4 and @LocOfProduct = 'Warehouse' and @PermAccess = 1 and @PermAccessID8 > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @PermAccessID8                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                              
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID8,@PermAccessQty8,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                      
 
   
 End                                        
                                         
 -- 12/18/2012: remove the delivery charge when it is a return from warehouse                                        
  Set @ProjectRateID = 0                                        
    Select @ProjectRateID = ProjectRateID from ProjectRates where QuoteID = @QuoteID                                          
  and DeliveryChargeFlag = 1                                          
                                            
                                            
                                            
    If @ProjectRateID > 0                                         
      Begin                                        
  -- remove the delivery charge                                        
  UPDATE  ProjectActivities                                        
   SET Units = 0,                                        
   DoNotPayAgentFlag = 1,              
   ModifiedBy = @CreatedBy,                                 
   ModifiedDate = GetDate()                                        
  WHERE ProjectRateID = @ProjectRateID                                        
   AND Extracted is null                                     
   AND PartnerExtracted is null                                        
                                        
  -- clear delivery charge flag                                        
  UPDATE ProjectRates                                        
   SET DeliveryChargeFlag = 0,                                        
   ModifiedBy = @CreatedBy,                                        
   ModifiedDate = GetDate()                                         
  FROM ProjectRates                                         
   INNER JOIN ProjectActivities pa ON ProjectRates.ProjectRateID = pa.ProjectRateID                                        
  WHERE ProjectRates.ProjectRateID = @ProjectRateID                                        
   -- 11/6/2012: make sure the activity is not billed                                        
   AND Extracted is null                                        
   AND PartnerExtracted is null                                        
   End                                        
End                                        
----------------------------------------------------------------------------------------                                        
-- 8/21/2012: we will now reserve the last 2 permanent accessorials for activities                                         
-- that will go to the Pick Up partner.                                        
Set @PickupPartner = 0                                        
                            
-- only check the order types that involve a pick up                                        
If @OrderTypeID IN (5, 7, 8, 25, 26)                                        
Begin                                        
 Select @PickupPartner = PartnerID                                        
 From ProjectPartner                                        
 Where QuoteID = @QuoteID                                        
 and PickupPartner = 1                           
 -- avoid null                                        
 Select @PickupPartner = ISNULL(@PickupPartner,0)                                        
End                                            
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID9 > 0 and @PickupPartner > 0                    
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @PermAccessID9                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                      
 where QuoteID = @QuoteID  and PartnerId = @PickupPartner                                        
 and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PickupPartner,@DelDate,@PermAccessID9,@PermAccessQty9,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                  
  
   
      
       
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
If @PermAccess = 1 and @PermAccessID10 > 0 and @PickupPartner > 0                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @PermAccessID10                                            
                 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and PartnerId = @PickupPartner                                        
 and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PickupPartner,@DelDate,@PermAccessID10,@PermAccessQty10,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                
  
   
       
       
         
 End                                        
End                                        
----------------------------------------------------------------------------------------                                        
/* new we need to handle the Excess Mileage Permanent Accessorials */                                        
If @ExcessMileage = 1 and @ExcessMileageID > 0 and @FMMiles between @ExcessMileageStartMiles and @ExcessMileageEndMiles                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @ExcessMileageID                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@ExcessMileageID,@ExcessMileageQty,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                  
  
    
      
 End                                        
End                                        
                                        
If @ExcessMileage = 1 and @ExcessMileageID2 > 0 and @FMMiles between @ExcessMileageStartMiles2 and @ExcessMileageEndMiles2                                        
Begin                          
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @ExcessMileageID2                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
                                        
 If @RecordCount = 0                                  
 Begin                         
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@ExcessMileageID2,@ExcessMileageQty2,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                
 
     
      
        
 End                                        
End                                        
                                        
If @ExcessMileage = 1 and @ExcessMileageID3 > 0 and @FMMiles between @ExcessMileageStartMiles3 and @ExcessMileageEndMiles3                                        
Begin                                        
 Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @ExcessMileageID3                                            
                                        
 -- the accessorial will not delete if it has already been billed                     
   Select @RecordCount = count(*) from ProjectRates                                            
 where QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                          
           
 If @RecordCount = 0                                         
 Begin                                           
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@ExcessMileageID3,@ExcessMileageQty3,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                
  
   
       
        
 End                                        
End                                        
                            
-- GREATER MILEAGE CALCULATION STARTS HERE                                        
--------------------------------------------------------------------------------------                                        
                                   
 IF @GrtMileage = 1                                    
 SELECT @GrtMileageID =                                  
 CASE                                    
 WHEN   @FMMiles BETWEEN grtMileageStartMiles AND   grtMileageEndMiles  THEN  grtMileageID                                   
 WHEN   @FMMiles BETWEEN grtMileageStartMiles2 AND  grtMileageEndMiles2 THEN  grtMileageID2                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles3 AND  grtMileageEndMiles3 THEN  grtMileageID3                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles4 AND  grtMileageEndMiles4 THEN  grtMileageID4                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles5 AND  grtMileageEndMiles5 THEN  grtMileageID5                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles6 AND  grtMileageEndMiles6 THEN  grtMileageID6                                  
                                   
 ELSE '0'                                  
 END ,                            
 @GrtMileageQty = CASE                                    
 WHEN   @FMMiles BETWEEN grtMileageStartMiles AND   grtMileageEndMiles  THEN  grtMileageQty                                   
 WHEN   @FMMiles BETWEEN grtMileageStartMiles2 AND  grtMileageEndMiles2 THEN  grtMileageQty2                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles3 AND  grtMileageEndMiles3 THEN  grtMileageQty3                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles4 AND  grtMileageEndMiles4 THEN  grtMileageQty4                                  
 WHEN   @FMMiles BETWEEN grtMileageStartMiles5 AND  grtMileageEndMiles5 THEN  grtMileageQty5                 
 WHEN   @FMMiles BETWEEN grtMileageStartMiles6 AND  grtMileageEndMiles6 THEN  grtMileageQty6                                  
                                   
 ELSE '0'                                
 END                                  
 FROM CustomerTemplate WHERE TemplateId = @TemplateId                                   
BEGIN                                  
Select  @ActivityID = ActivityID                                        
  from Accessories where AccessorialID = @GrtMileageID                                            
                                        
 -- the accessorial will not delete if it has already been billed                                         
   SELECT @RecordCount = COUNT(1) FROM ProjectRates WITH(NOLOCK)                                            
   WHERE QuoteID = @QuoteID and ActivityID = @ActivityID and AccessorialFlag = 1                                  
   IF @FMMiles > @GrtMileLastFreeMiles                              
   BEGIN                                     
   SET @FMMiles =  @FMMiles - @GrtMileLastFreeMiles                               
   END                              
 IF @RecordCount = 0                                         
 Begin                                    
                                   
  -- Exec USP_CalculateGreaterMileageCharge @QuoteID,@PartnerID,@DelDate,@GrtMileageID,@GrtMileageQty,@FMMiles,@GrtMileLastFreeMiles,@ProductID,@CreatedBy                                        
  Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@GrtMileageID,@GrtMileageQty,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                        
 End                                        
 END                                  
 --GREATER MILEAGE ENDS HERE                                 
----------------------------------------------------------------------------------------                                   
                                   
/* now handle product attached accessorials */                    
                  
insert into #tempAcc                                              
/*                                        
Select p.ProductTypeID, PermAccessID, PermAccessQty                                        
  from Product p, #tempProducts tp                                             
  where p.ProductTypeID = tp.ProductID                                             
  and  PermAccess = 1                                         
  and p.ProductTypeid = tp.productid                                            
*/                               
-- 9/8/10: added the sum() and group by                         
                     
                                      
Select p.ProductTypeID, PermAccessID, sum(PermAccessQty), 1, MAX(p.PartnerID)                                    
  from Product p, #tempProducts tp                                             
  where p.ProductTypeID = tp.ProductID                                             
  and  PermAccess = 1                                         
  and p.ProductTypeid = tp.productid                                           
Group By  p.ProductTypeID, PermAccessID                                        
                                         
UNION ALL                                        
                                        
Select p.ProductTypeID, PermAccessID2, sum(PermAccessQty2), 2,MAX(p.PartnerID2)                                   
  from Product p, #tempProducts tp                                             
  where p.ProductTypeID = tp.ProductID                                             
  and  PermAccess = 1                                         
  and p.ProductTypeid = tp.productid                                         
  and  PermAccessID2 > 0                                         
Group By  p.ProductTypeID, PermAccessID2                          
                                                
UNION ALL                        
                                        
Select p.ProductTypeID, PermAccessID3, sum(PermAccessQty3), 3 , MAX(p.PartnerID3)                  
  from Product p, #tempProducts tp                                             
  where p.ProductTypeID = tp.ProductID                                             
  and  PermAccess = 1                            
  and p.ProductTypeid = tp.productid                                         
  and  PermAccessID3 > 0                                         
Group By  p.ProductTypeID, PermAccessID3                           
                                     
UNION ALL                                        
 -- Added for Permenent accessorial for products                                       
Select p.ProductTypeID, PermAccessID4, sum(PermAccessQty4), 4, MAX(p.PartnerID4)                   
  from Product p, #tempProducts tp                                             
  where p.ProductTypeID = tp.ProductID                                             
  and  PermAccess = 1                                    
  and p.ProductTypeid = tp.productid                                         
  and  PermAccessID4 > 0                                         
Group By  p.ProductTypeID, PermAccessID4                           
                        
UNION ALL                                        
                                        
Select p.ProductTypeID, PermAccessID5, sum(PermAccessQty5), 5 , MAX(p.PartnerID5)                                   
  from Product p, #tempProducts tp                                
  where p.ProductTypeID = tp.ProductID                                             
  and  PermAccess = 1                                    
  and p.ProductTypeid = tp.productid                                         
  and  PermAccessID5 > 0                                        
Group By  p.ProductTypeID, PermAccessID5                         
                        
                        
--INSERT INTO  test_productAcc (ProductTypeID , PermAccessID, PermAccessQty , Acc_num, PartnerID )                         
                        
--SELECT * FROM #tempAcc                        
                        
                                            
DECLARE AddlItemCharge_Cursor  CURSOR FAST_FORWARD FOR                                                    
select PermAccessID, PermAccessQTY, ProductID, PermAccessNumber,                   
CASE PartnerId                   
WHEN 0 THEN @PartnerID                  
ELSE PartnerId END As PartnerId from #tempAcc                                            
                                                
OPEN AddlItemCharge_Cursor                                               
                                                
FETCH NEXT FROM AddlItemCharge_Cursor                                                
INTO @PermAccessID, @PermAccessQTY, @ProductID, @PermAccessNumber,@PartnerId                        
                                             
WHILE @@FETCH_STATUS = 0                                                    
BEGIN                                               
                                               
 Select @ActivityID = ActivityID                                        
/*                                        
 ,@ActivityDescription = ProjectDescription,                                            
  @UnitOfMeasureCD = UnitOfMeasureCD,                                            
        @CustomerRateUnitOfMeasure=CustomerRateUnitOfMeasure,                                            
        @CustomerRateValue=CustomerRateValue,                                            
        @RateValue = PartnerRateValue,                                            
    @RateActionTypeID=RateActionTypeID                                           
*/                                         
  from [HDN_Providence].[dbo].Accessories where AccessorialID = @PermAccessID                                            
                        
                        
 --     insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@PartnerIdCursor=',@PartnerId, @QuoteID, GetDate(),'Anudeep')                                       
/*                                        
       --Calc Margin and insert record into ProjectRates and ProjectActivites tables                        
                                            
 If @RateValue <> 0                                           
 Begin                                          
    Select @Margin  = round(((@CustomerRateValue - @RateValue)*100/@RateValue)/100,4)                                            
 End                                              
                                           
 If @RateValue = 0                                           
 Begin                                          
    Select @Margin  = 1                                           
 End       */                                        
                                         
                                          
-- Select @RecordCount = count(*) from ProjectRates where QuoteID = @QuoteID                                           
-- and ProductID = @ProductID                                          
                                        
-- 9/8/10: count the units, not the records                                        
 Select @RecordCount = isnull(Sum(Units),0)                                        
 from ProjectRates                                         
 Inner Join ProjectActivities On ProjectRates.ProjectRateID = ProjectActivities.ProjectRateID                                        
 where QuoteID = @QuoteID                                           
 and ProductID = @ProductID                                          
 and ActivityID = @ActivityID                    
 and PartnerId = @PartnerID                                       
 and AccessorialFlag = 1                                          
                                     
 Select @ProductCount = Count(*)                                         
 from OrderInv where ProductTypeID = @ProductID                                           
 and AccountNumber = @AccountNumber                
 and QuoteID = @QuoteID                                          
                                         
-- *** look here ***                                        
-- *** this is where the problem is ***                                        
                                          
 -- 9/8/10: we need the PermAccessQty from the Product table,                                        
 -- since the one we have is the sum                                        
 If @PermAccessNumber = 1                                        
  Begin                                        
     Select @wrkPermAccessQTY = PermAccessQty                                        
   From [HDN_Providence].[dbo].Product                                        
   Where ProductTypeID = @ProductID                                        
  End                                        
                                          
 If @PermAccessNumber = 2                                 
  Begin                                        
     Select @wrkPermAccessQTY = PermAccessQty2                                        
   From [HDN_Providence].[dbo].Product                                        
   Where ProductTypeID = @ProductID                                        
  End                                        
                                          
 If @PermAccessNumber = 3                                        
  Begin                                        
     Select @wrkPermAccessQTY = PermAccessQty3                                     
   From Product                                        
   Where ProductTypeID = @ProductID                                        
  End                        
                        
   If @PermAccessNumber = 4                                        
  Begin                         
     Select @wrkPermAccessQTY = PermAccessQty4                                        
   From [HDN_Providence].[dbo].Product                                        
   Where ProductTypeID = @ProductID                                        
  End                       
                        
   If @PermAccessNumber = 5                                        
  Begin                                        
     Select @wrkPermAccessQTY = PermAccessQty5                                        
   From [HDN_Providence].[dbo].Product                                        
   Where ProductTypeID = @ProductID                                        
  End                                       
                                        
 -- 9/8/10: this is the number that we need to end up with                                        
 Select @ProductCount = @ProductCount * @wrkPermAccessQTY                                        
                                        
 Set @Diff = @ProductCount - @RecordCount                                           
                                          
 If @RecordCount  = 0 or @Diff > 0                                          
 Begin                                         
   -- 9/5/2014: since these are product attached accessorials, they apply                                        
   -- only to the exact product they are attached to.                                        
   -- So, we need to calculate the Weight, Cube, Square Feet, Linear Feet,                 
   -- Quantity and Pieces only for the specific product that is on this order.                               
   -- The variables calculated earlier in this procedure contain the totals                                        
   -- for all products that on the order.                                        
                                        
   -- @ProductWeight                                        
   -- @ProductCube                                        
   -- @ProductSquareFeet                                        
   -- @ProductLinearFeet                                        
   -- @ProductQuantity                                        
   -- @ProductPieces                                        
                                           
   Select @ProductWeight = Sum(Weight)                                        
   From OrderInv                                        
   Where ProductTypeID = @ProductID                                           
   and AccountNumber = @AccountNumber                                           
   and QuoteID = @QuoteID                               
                                            
   Select @ProductCube = Sum(Cube)                                        
   From OrderInv                                        
   Where ProductTypeID = @ProductID                                           
   and AccountNumber = @AccountNumber                                           
   and QuoteID = @QuoteID                                         
                                        
   Select @ProductLinearFeet = Sum(Pieces * Length / 12)                                        
   From OrderInv                                        
   Where ProductTypeID = @ProductID                                           
   and AccountNumber = @AccountNumber                                           
   and QuoteID = @QuoteID                                         
                                              
   Select @ProductSquareFeet = Sum(Pieces * Length * Width / 12 / 12)                                        
   From OrderInv                                        
   Where ProductTypeID = @ProductID                                           
   and AccountNumber = @AccountNumber                                           
   and QuoteID = @QuoteID                                         
                                        
   Select @ProductQuantity = Count(*)                                         
   from OrderInv                                         
   where ProductTypeID = @ProductID                                           
   and AccountNumber = @AccountNumber                                           
   and QuoteID = @QuoteID                  
                   
 --     insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 --Values('@ProductQuantity=',CAST(@ProductID AS VARCHAR(10)) + ' ' + CAST(@QuoteID AS VARCHAR(10)), @QuoteID, GetDate(),'bergc')                                          
                                 
   Select @ProductPieces = Sum(Pieces)                                        
   From OrderInv                                        
   Where ProductTypeID = @ProductID                                           
   and AccountNumber = @AccountNumber                                           
   and QuoteID = @QuoteID                                         
                                        
                                        
   -- @Diff should be the number of accessorials we need to  create                                        
   -- Exec USP_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID,@Diff,@TotalWeight,@TotalCube,@TotalSquareFeet,@TotalLinearFeet,@TotalQuantity,@TotalPieces,@FMMiles,@ProductID,@CreatedBy                                        
   -- 9/5/2014: pass in the Product Totals                   
                     
 --  insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                         
 --Values('@ProductQuantity111=',CAST(@PermAccessID AS VARCHAR(10)) + ' : ' + CAST(@ProductQuantity AS VARCHAR(10)) + ' : ' + CAST(@ProductPieces AS VARCHAR(10)), @QuoteID, GetDate(),'bergc')                    
  ---  SET @PartnerID = 2264                                      
   Exec USP_QUOTE_CalculateAccessorialCharge @QuoteID,@PartnerID,@DelDate,@PermAccessID,@Diff,@ProductWeight,@ProductCube,                
   @ProductSquareFeet,@ProductLinearFeet,@ProductQuantity,@ProductPieces,@FMMiles,@ProductID,@CreatedBy                                        
                           
   --If exist parnerid and QuoteID                        
                           
    IF NOT EXISTS(SELECT 1 FROM ProjectPartner WITH(NOLOCK) WHERE QuoteID=@QuoteID AND PartnerID=@PartnerID)                        
    BEGIN                        
       INSERT INTO ProjectPartner (QuoteID,PartnerID,PrimaryPartner,PickupPartner,CreatedBy,CreateDate,ModifiedBy,ModifiedDate)                        
         VALUES(@QuoteID,@PartnerID,0,@PickupPartner,@CreatedBy,GETDATE(),NULL,NULL)                            
     END                                       
  -- End                          
/*                                        
 -- 3/7/2006: calc one charge for each product (quantity)                                        
 -- eg, if 5 productIDs 'ABCXYZ', calc rate * 5                                        
     Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                              
     Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                              
     InvoicedFlag,RateActionTypeID,createdby,createDate,ProductID,AccessorialFlag)                                              
     Values(@QuoteID,@PartnerID,@ActivityID,@UnitOfMeasureCD,@RateValue,                                        
   @Margin,@CustomerRateValue,@CustomerRateUnitOfMeasure,0,@ActivityDescription,                                        
   0,@RateActionTypeID,@CreatedBy,getdate(),@ProductID,1)                                              
                                               
     Select  @ProjectRateID = @@IDENTITY                                              
*/                                         
/*                                        
    --Del01 Deluxing for account 889                                        
   if @ProductID = 47790                                        
   Begin                                        
      insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,CustomerCost,                     
    partnercost,MinimumUsed,createdby,createdate)                                                
      values (@ProjectRateID,@DelDate,@PermAccessQTY*1,0,@CustomerRateValue*@PermAccessQTY*1,                                        
    @RateValue*@PermAccessQTY*1,0,@CreatedBy,getdate())                                                
   End                                         
--    if @ProductID <> 47790                                        
--   Begin                                             
      insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,CustomerCost,                                        
   partnercost,MinimumUsed,createdby,createdate)                                                
      values (@ProjectRateID,@DelDate,@PermAccessQTY*@Diff,0,@CustomerRateValue*@PermAccessQTY*@Diff,                                        
    @RateValue*@PermAccessQTY*@Diff,0,@CreatedBy,getdate())                                                
--   End                                        
*/                                        
/*                                        
-- 9/8/10: @Diff now represents the number ofunits we need to add                                        
      insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,CustomerCost,                                        
    partnercost,MinimumUsed,createdby,createdate)                                                
      values (@ProjectRateID,@DelDate,@Diff,0,@CustomerRateValue*@Diff,                                        
    @RateValue*@Diff,0,@CreatedBy,getdate())                                                
                                        
    Set @Diff = @Diff - 1                                          
*/                                        
 End                                                   
                               
 FETCH NEXT FROM AddlItemCharge_Cursor                                                
     INTO @PermAccessID, @PermAccessQTY, @ProductID, @PermAccessNumber, @PartnerId                                             
END                                                    
                               
CLOSE AddlItemCharge_Cursor                                                
                                                    
DEALLOCATE AddlItemCharge_Cursor                                                
                           
/* Accessorial Charge calculation ends */                                          
                                             
drop table #tempProducts                                                
                                              
drop table #tempAcc                                            
                                            
                                            
drop table #RemoveAccessorial                                        
                                        
                               
----------------------------------------------------------------------------------------                                        
-- 3/6/2006: check minimum here because Maytag depends                                        
-- upon permanent accessorials to get the prices right.                                        
                                   
If @MinCharge > 0 and @AccountNumber <> 2025                                        
Begin                                        
                                        
  SELECT @CustomerCost = sum(pa.CustomerCost), @PartnerCost = sum(pa.PartnerCost)                                        
  FROM projectactivities pa, projectrates pr                                        
  WHERE pa.projectrateid = pr.projectrateid                                        
  AND pr.QuoteID = @QuoteID                                        
  --AND pr.activityid = 37                                        
  AND pr.activityid = @DeliveryChargeActivityID                                        
  AND pa.units > 0                                        
                                        
/*                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                 
 Values('@MinCharge=',@MinCharge, 0, GetDate(),'bergc')                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                                        
*/                                        
                                        
  if @MinCharge > @CustomerCost                                        
                                        
   Begin                                             
                                             
   set @CustomerCost = @MinCharge                                        
                                           
   If @MinComp > @PartnerCost                                         
   begin                                        
    set @PartnerCost = @MinComp                                        
   end                                        
                                        
   Select @Margin  = round(((@CustomerCost - @PartnerCost)*100/@PartnerCost)/100,4)                                              
                                               
   Select @ProjectRateID = ProjectRateID                                         
 from ProjectRates                                        
 where QuoteID = @QuoteID                                          
 and DeliveryChargeFlag = 1                                          
                                              
   If isnull(@ProjectRateID, 0) = 0                                        
    begin                                        
   Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                          
  Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                          
  InvoicedFlag,RateActionTypeID,deliverychargeflag,createdby,createDate)                                          
   Values(@QuoteID,@PartnerID,@DeliveryChargeActivityID,'FR',@PartnerCost,                                        
  @Margin,@CustomerCost,'FR',0,'Minimum Delivery Charge',                     
  0,3,1,@CreatedBy,getdate())                                          
                                          
   Select  @ProjectRateID = @@IDENTITY, @Err=@@error                                                    
                                          
   -- 2/3/04: use freight available date instead of deldate, because deldate is not required                                        
   insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                          
 CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                       
   values (@ProjectRateID,@DelDate,1,0,                                        
     @CustomerCost,@PartnerCost,1,@CreatedBy,getdate())                                            
    end                                        
   else                                        
    begin                                             
  Update ProjectRates                                           
     Set Margin = @Margin,                                          
     CustomerRateValue = @CustomerCost,                                          
     RateValue = @PartnerCost,                                          
      -- 1/9/2012: update the next 2 fields because they might change now                                        
   ActivityID = @DeliveryChargeActivityID,                                        
ProjectRateDescription = @ActivityDescription,                                        
     ModifiedBy = @CreatedBy,                                          
     ModifiedDate = getdate()                                          
     Where ProjectRateID = @ProjectRateID                                        
                                              
     Update ProjectActivities                                           
      Set CustomerCost = @CustomerCost,                                          
     PartnerCost = @PartnerCost,                                          
     MinimumUsed = 1,                                        
     ModifiedBy = @CreatedBy,                                          
     ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                
    end                                        
                                         
   Delete from ProjectActivities                                         
 Where ProjectRateID IN                                         
 (                                          
 Select ProjectRateID from ProjectRates                                        
 where QuoteID = @QuoteID                           
 --and activityid = 37                                        
 and activityid = @DeliveryChargeActivityID                                        
 and isnull(deliverychargeflag, 0) = 0                                         
 and InvoicedFlag <> 1                                        
 )                                          
                                          
   Delete from ProjectRates                                         
 Where QuoteID = @QuoteID                                        
 --and activityid = 37                             
 and activityid = @DeliveryChargeActivityID                                        
 and isnull(deliverychargeflag, 0) = 0                                          
 and InvoicedFlag <> 1                                        
  End                                        
End                                        
                                        
                                        
----------------------------------------------------------------------------------------                                        
-- 3/6/2006: check minimum here because Maytag depends                               
-- upon permanent accessorials to get the prices right.                                        
                                        
-- 12/5/2011: I changed this today to make the Minimum Charge be a                                         
-- "Minimum Charge Adjustment" for account 2025 (Electrolux - Demand Flow Dept       
                                        
If @MinCharge > 0 and @AccountNumber = 2025                                        
Begin                                        
                                        
  -----------------------------------------------------------                                        
  -- 3/1/2017: this was working fine on the first calculation,                                        
  -- but if a minimum was added and then the user added a new                                        
  -- product to the order the adjustment did not change.                                        
  -- this customer is charged by the product.  so if they add                                        
  -- another product the charges went up, but the minimum                                        
  -- adjustment was not going down                                        
  Create Table #RemoveMinimumAdjustment(                                          
   ProjectRateID int                                                
  )                                                
                                        
  Insert into #RemoveMinimumAdjustment                    
  Select pr.ProjectRateID from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                        
 where pr.QuoteID = @QuoteID                                         
 AND pr.activityid = @DeliveryChargeActivityID                                        
 and pr.ProjectRateDescription = '1.9MMDBC'   ---  Minimum Delivery Charge Adjustment                                      
 and pa.Extracted is null                                        
 and pa.PartnerExtractDate is null                                        
 -- and InvoicedFlag <> 1                                          
                 
  Delete from ProjectActivities where ProjectRateID in (                                          
  Select ProjectRateID from #RemoveMinimumAdjustment)                                          
                                          
  Delete from ProjectRates where ProjectRateID in (                                          
  Select ProjectRateID from #RemoveMinimumAdjustment)                                          
                                        
  Drop Table #RemoveMinimumAdjustment                                        
  -----------------------------------------------------------                                        
                                        
  SELECT @CustomerCost = sum(pa.CustomerCost), @PartnerCost = sum(pa.PartnerCost)                                        
  FROM projectactivities pa, projectrates pr                                        
  WHERE pa.projectrateid = pr.projectrateid                                        
  AND pr.QuoteID = @QuoteID                                        
  --AND pr.activityid = 37                                        
  AND pr.activityid = @DeliveryChargeActivityID                                        
  AND pa.units > 0                                        
                                        
/*                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@MinCharge=',@MinCharge, 0, GetDate(),'bergc')                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')                                        
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@PartnerCost=',@PartnerCost, 0, GetDate(),'bergc')                      
*/                                        
                                        
                                        
  if @MinCharge > @CustomerCost    OR   @MinComp > @PartnerCost                                   
                                       
   Begin                                             
                                
       if @MinCharge > @CustomerCost                          
       BEGIN                                       
   -- set @CustomerCost = @MinCharge                                        
   set @CustomerCost = @MinCharge - @CustomerCost                           
                             
         END                           
         ELSE                           
         BEGIN                           
          set @CustomerCost = 0                          
         END                                
                                           
   If @MinComp > @PartnerCost                                         
   begin                                      
    -- set @PartnerCost = @MinComp                                        
    set @PartnerCost = @MinComp - @PartnerCost                                        
   end                           
   ELSE                              
  BEGIN                          
   set @PartnerCost = 0                          
  END                                        
                                        
   Select @Margin  = round(((@CustomerCost - @PartnerCost)*100/@PartnerCost)/100,4)                                              
                                               
   Select @ProjectRateID = ProjectRateID                                         
 from ProjectRates                                        
 where QuoteID = @QuoteID                                          
 and DeliveryChargeFlag = 1                                          
            
   If isnull(@ProjectRateID, 0) = 0                                        
    begin                                        
   Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                          
  Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                          
  InvoicedFlag,RateActionTypeID,deliverychargeflag,createdby,createDate)                                          
   Values(@QuoteID,@PartnerID,@DeliveryChargeActivityID,'FR',@PartnerCost,                                        
  @Margin,@CustomerCost,'FR',0,'1.9MMDBC',   --- Minimum Delivery Charge Adjustment                                      
  0,3,1,@CreatedBy,getdate())                                          
                                          
   Select  @ProjectRateID = @@IDENTITY, @Err=@@error                                                    
                                          
   -- 2/3/04: use freight available date instead of deldate, because deldate is not required                                        
   insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                          
  CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                            
   values (@ProjectRateID,@DelDate,1,0,                                        
     @CustomerCost,@PartnerCost,1,@CreatedBy,getdate())                                            
    end                                        
   else                                        
    begin                                             
  Update ProjectRates                                           
     Set Margin = @Margin,                                          
     CustomerRateValue = @CustomerCost,                                          
     RateValue = @PartnerCost,                                          
      -- 1/9/2012: update the next 2 fields because they might change now                                        
   ActivityID = @DeliveryChargeActivityID,                                        
   ProjectRateDescription = @ActivityDescription,                                        
     ModifiedBy = @CreatedBy,                                          
     ModifiedDate = getdate()                                          
     Where ProjectRateID = @ProjectRateID                                          
                                              
     Update ProjectActivities                                           
      Set CustomerCost = @CustomerCost,                                          
     PartnerCost = @PartnerCost,                                         
     MinimumUsed = 1,                                        
     ModifiedBy = @CreatedBy,                                          
     ModifiedDate = getdate()                                          
     Where ProjectRateID = @ProjectRateID                                           
    end                                        
                                         
-- 12/5/2011: do not delete these any more.  we now create an adjustment.                                        
/*                                            
   Delete from ProjectActivities                                         
 Where ProjectRateID IN                                         
 (                                          
 Select ProjectRateID from ProjectRates                                        
 where QuoteID = @QuoteID                                        
 and activityid = 37                                        
 and isnull(deliverychargeflag, 0) = 0                    and InvoicedFlag <> 1                                        
 )                                          
                                          
   Delete from ProjectRates                                         
 Where QuoteID = @QuoteID                                        
 and activityid = 37                                        
 and isnull(deliverychargeflag, 0) = 0                                          
 and InvoicedFlag <> 1                                        
 */                                         
   End                                        
End                                        
                                        
----------------------------------------------------------------------------------------                                        
-- 10/8/2007: calculate valuation if it applies                                 
                                        
-- we have previously selected the following values:                                        
-- @ValFlag                  
-- @ValRate                                        
-- @ValRateType                                    
-- @ValMinChg                                        
-- @ValFreeCoverage                                        
-- @ValFreeCoverageType                                        
-- @TotalWeight                                        
                                        
-- activityid 73 is valuation                                        
set @ValuationCharge = 0                                        
set @MinimumUsed = 0                                        
                                        
-- 10/8/2007: For Local type do not calculate the Valuation charges                                        
if @BusinessTypeID  <> 2                                        
Begin                                           
                                        
  -- 8/20/2010: new way to remove a Valuation Charge if it exists                                          
  Create Table #RemoveValuation(                                          
   ProjectRateID int                                                
  )                                                
                  Insert into #RemoveValuation                                          
 Select pr.ProjectRateID from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                        
 where QuoteID = @QuoteID                                        
 and activityid = 73                                        
 and pa.Extracted is null                                        
-- and InvoicedFlag <> 1                                        
                                        
  -- remove a Valuation Charge if it exists and is not invoiced                                        
  Delete from ProjectActivities where ProjectRateID in                                         
 (Select ProjectRateID from #RemoveValuation)                                          
                  
  Delete from ProjectRates where ProjectRateID in                                         
 (Select ProjectRateID from #RemoveValuation)                                          
                                        
                                        
  If @ValFlag = 1 and @RCPValue > 0                                         
   Begin                                        
                                        
    If @ValRateType = 'Flat Rate'                                        
      Begin                                        
  set @ValuationCharge = @ValRate                                        
      End                   
                                        
    If @ValFreeCoverageType = 'Per Pound'                                        
     Begin                                        
 set @ValFreeCoverage = @ValFreeCoverage * @TotalWeight                                        
     End                                        
                                        
    If @ValRateType = 'Per 1000'                                        
     Begin                                        
 set @ValuationCharge = (@RCPValue - @ValFreeCoverage) * @ValRate / 1000                                        
    End                                        
                                        
    If @ValuationCharge < 0                                        
     Begin                                    
 set @ValuationCharge = 0                                        
     End                                        
                                        
    If (@ValMinChg > @ValuationCharge) and (@ValuationCharge > 0)                                        
     Begin                                        
 set @ValuationCharge = @ValMinChg                                        
 set @MinimumUsed = 1                                        
     End                                        
                               
    if @ValuationCharge <> 0                                        
     Begin                                          
  Select @ActivityDescription = Description from Activity where ActivityID = 73                                          
                                        
  Select @RecordCount = Count(*) from ProjectRates where QuoteID = @QuoteID and activityid = 73                                        
                                        
  If @RecordCount = 0                                           
   Begin                                          
    Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                          
   Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                          
   InvoicedFlag,RateActionTypeID,createdby,createDate)                                       
    Values(@QuoteID,@PartnerID,73,'FR',0,                                        
   1,@ValuationCharge,'FR',0,@ActivityDescription,                         
   0,3,@CreatedBy,getdate())                                          
                                          
    Select  @ProjectRateID = @@IDENTITY, @Err=@@error                                                    
                                          
    -- 2/3/04: use freight available date instead of deldate, because deldate is not required                                        
    insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                          
   CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                            
    values (@ProjectRateID,@DelDate,1,0,@ValuationCharge,0,@MinimumUsed,@CreatedBy,getdate())                                            
   End                                        
                                        
  Else                           
                                        
   Begin                                        
    -- if the activity exists                                        
    Select @ProjectRateID = ProjectRateID from ProjectRates where QuoteID = @QuoteID and activityid = 39                     
          -- see if it is billed                                        
    Select @ExtractDate = Extracted from ProjectActivities where ProjectRateID = @ProjectRateID                                        
                                          
    -- 8/20/2010: don't update if it is already billed                                        
    If isnull(@ExtractDate,'') = ''                                        
     Begin                                        
   -- update here                                      
   Update ProjectRates                              
   Set Margin = 1,                                          
   CustomerRateValue = @ValuationCharge,                                         
   RateValue = 0,                                          
   ModifiedBy = @CreatedBy,                                          
  ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                          
                                              
   Update ProjectActivities                                           
   Set CustomerCost = @ValuationCharge,                                          
   PartnerCost = 0,                                          
   ModifiedBy = @CreatedBy,                       
   ModifiedDate = getdate()                                          
   Where ProjectRateID = @ProjectRateID                                           
     End                    
                                        
   End                                        
     End                                          
  End                                        
                                        
  Drop Table #RemoveValuation                                        
                                        
 End                                        
                                        
                                        
----------------------------------------------------------------------------------------                                        
---- 12/30/2016: Operations has a special situation where they need to give an agent                                        
---- $3 additional comp across the board above the standard comp.                                        
---- so, rather than create 40 new regions, we added fields to the Partner record that                                        
---- allow us to accomplish this.                                        
---- You cannot see the fields from the screens, but that is good since this is a special                                        
---- situation.                                        
                                        
Select @SpecialCompFlag = SpecialCompFlag,                                        
  @SpecialCompAmount = isnull(SpecialCompAmount,0),                                        
  @SpecialCompActivityID = isnull(SpecialCompActivityID,0),                                        
  @SpecialCompDescription = isnull(SpecialCompDescription,'')                                        
From Partner                                        
Where PartnerID = @PartnerID                                        
                                        
                                             
If @SpecialCompFlag = 1 and @SpecialCompAmount > 0 and @SpecialCompActivityID > 0 and @SpecialCompDescription <> ''                                        
  Begin                                        
                                        
 Select @SpecialCompExists = Count(*)                                        
 from ProjectRates pr                                        
 inner join ProjectActivities pa ON pr.ProjectRateID = pa.ProjectRateID                                        
 where pr.QuoteID = @QuoteID                                         
 and pr.PartnerID = @PartnerID                                        
 and pr.ActivityID = @SpecialCompActivityID                             
 and pr.ProjectRateDescription = @SpecialCompDescription                                        
 and pa.Units > 0                                         
                                        
 If @SpecialCompExists = 0                                         
     Begin                                        
  Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,                                          
   Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,                                          
   InvoicedFlag,RateActionTypeID,createdby,createDate)                                          
  Values(@QuoteID,@PartnerID,@SpecialCompActivityID,'FR',@SpecialCompAmount,                                        
    -1,0,'FR',0,@SpecialCompDescription,                                        
   0,3,@CreatedBy,getdate())                                          
                                          
  Select  @ProjectRateID = @@IDENTITY, @Err=@@error                                                    
                                          
    -- 2/3/04: use freight available date instead of deldate, because deldate is not required                                        
  insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,                                          
   CustomerCost,partnercost,MinimumUsed,createdby,createdate)                                            
  values (@ProjectRateID,@DelDate,1,0,                                        
    0,@SpecialCompAmount,0,@CreatedBy,getdate())                                            
    -- Added insert for Permanent Accessories from Product types.                 
                                          
  End                                        
                                        
----------------------------------------------------------------------------------------                                        
-- 9/30/2010: since we now apply the Fuel Surcharge to Additional Items, which can                                        
-- be (and are) setup as permanemnt accessorials, we need to recalculate the fuel                                        
-- surcharge at the end of this procedure.                                        
                                        
-- 3/15/2011: we now get these fields at the beginning                                        
--SELECT @ActualDate = ActualDate, @ActualTime = ActualTime, @POD = POD                                        
--FROM Project                                        
--Where QuoteID = @QuoteID                                        
                                        
                                        
-- 4/17/2013: we will now call the fuel calculation separately from the delivery                                        
-- charge calculation because they want it to calculate without the "Update                                        
-- Delivery Charge" box checked.                                        
-- we only call the procedure if the Delivery information is present                                        
/*                                        
If isnull(@ActualDate,'') <> '' and isnull(@ActualTime,'') <> '' and isnull(@POD,'') <> ''                                        
  Begin                                        
 Exec ProjDAL_UpdProject_ProcessFuelSurchage @QuoteID,@ActualDate,@PartnerId,@CreatedBy                                         
  End                                        
*/                         
END                  
 
 ----This is a temporary stored proc used to remove the Haul away charge for DHL Tempur
 
 exec XXX_Quote_CleanUpActivities_temp               
                  
END TRY                   
                  
BEGIN CATCH                   
                  
insert into AppErrorLog(Application, Error, QuoteID, CreateDate, CreatedBy)                                        
 Values('@Usp_CalculateAddlItemCharge_New=',ERROR_MESSAGE() + ' ' + CAST(ERROR_LINE() as VARCHAR(10)), @QuoteID, GetDate(),'Anudeep')                   
                  
END CATCH

GO

