 
---sp_CalculateWeightBasedDeliveryCharge_Anudeep '2796','43001','10001','300','1','35','70','Anudeep'

---sp_CalculateWeightBasedDeliveryCharge_Anudeep '2805','43001','10001','300','1','0','70','Anudeep'

--sp_CalculateWeightBasedDeliveryCharge 'CustomerId', 'OrginZip', 'DestinationZip', 'Total Weight' , 'ServiceLevel', 'TransTypeId, 'FMMiles' , ' UserID'

CREATE PROCEDURE [dbo].[sp_Quote_CalculateWeightBasedDeliveryCharge]   
@CustomerID int,  
@OriginZip varchar(50),  
@DestZip varchar(50),  
@TotalWeight int,  
@ServiceLevel int,  
@TransTypeID int,  
@FMMiles float,  
@UserID varchar(10),  
@QuoteID int
  
AS  
  
BEGIN   
  
SET NOCOUNT ON  

BEGIN  -- Variable Declaration--
  
  DECLARE @DeliveryCharge float  
  DECLARE @FuelSurcharge float  
  DECLARE @TransCharge float  
  DECLARE @TransFuelSurcharge float  
  
  DECLARE @KeepGoing int  
  DECLARE @ExcessMilesCustomerCost float  
  
  DECLARE @WasPlusDeliv int  
  DECLARE @PlusDelivCustomerCost float  
  
  DECLARE @DeliveryCharge1 float  
  DECLARE @DeliveryCharge2 float  
  DECLARE @DeliveryCharge3 float  
  DECLARE @DeliveryCharge4 float  
  DECLARE @DeliveryCharge5 float  
  DECLARE @DeliveryCharge6 float  
  DECLARE @DeliveryCharge7 float  
  DECLARE @DeliveryCharge8 float  
  DECLARE @DeliveryCharge9 float  
  DECLARE @DeliveryCharge10 float  
  DECLARE @DeliveryCharge11 float  
  DECLARE @DeliveryCharge12 float  
  DECLARE @DeliveryCharge13 float  
  DECLARE @DeliveryCharge14 float  
  DECLARE @DeliveryCharge15 float  
  DECLARE @DeliveryCharge16 float  
  DECLARE @DeliveryCharge17 float  
  DECLARE @DeliveryCharge18 float  
  DECLARE @DeliveryCharge19 float  
  DECLARE @DeliveryCharge20 float  
  
  
  DECLARE @Region int  
  DECLARE @Region2 int  
  DECLARE @Region3 int  
  DECLARE @Region4 int  
  DECLARE @Region5 int  
  DECLARE @Region6 int  
  DECLARE @Region7 int  
  DECLARE @Region8 int  
  DECLARE @Region9 int  
  DECLARE @Region10 int  
  DECLARE @Region11 int  
  DECLARE @Region12 int  
  DECLARE @Region13 int  
  DECLARE @Region14 int  
  DECLARE @Region15 int  
  DECLARE @Region16 int  
  DECLARE @Region17 int  
  DECLARE @Region18 int  
  DECLARE @Region19 int  
  DECLARE @Region20 int  
  DECLARE @Region21 int  
  DECLARE @Region22 int  
  DECLARE @Region23 int  
  DECLARE @Region24 int  
  DECLARE @Region25 int  
  DECLARE @Region26 int  
  DECLARE @BlendedRegion int  
  
  -- 5/7/2007: we will now recalculate remote charges in this procedure    
  DECLARE @RemoteRev float  
  DECLARE @RemoteRev2 float  
  DECLARE @RemoteRev3 float  
  DECLARE @RemoteRev4 float  
  DECLARE @RemoteRev5 float  
  DECLARE @RemoteRev6 float  
  DECLARE @RemoteRev7 float  
  DECLARE @RemoteRev8 float  
  DECLARE @RemoteRev9 float  
  DECLARE @RemoteRev10 float  
  DECLARE @RemoteExp float  
  DECLARE @RateStateID varchar(2)  
  
  --3/10/2011: new fields    
  DECLARE @RemoteRevB float  
  DECLARE @RemoteRevC float  
  DECLARE @RemoteRevD float  
  DECLARE @RemoteRevE float  
  DECLARE @RemoteRevF float  
  DECLARE @RemoteRevG float  
  DECLARE @RemoteRevH float  
  DECLARE @RemoteRevI float  
  DECLARE @RemoteRevJ float  
  DECLARE @RemoteRevK float  
  DECLARE @RemoteRevL float  
  DECLARE @RemoteRevM float  
  DECLARE @RemoteRevN float  
  DECLARE @RemoteRevO float  
  DECLARE @RemoteRevP float  
  DECLARE @RemoteRevQ float  
  DECLARE @RemoteRevR float  
  DECLARE @RemoteRevS float  
  DECLARE @RemoteRevT float  
  DECLARE @RemoteRevU float  
  DECLARE @RemoteRevV float  
  DECLARE @RemoteRevW float  
  DECLARE @RemoteRevX float  
  DECLARE @RemoteRevY float  
  DECLARE @RemoteRevZ float  
  DECLARE @RemoteRevBlended float  
  
  DECLARE @PreferredProvider int  
  DECLARE @NetworkA int  
  DECLARE @NetworkB int  
  DECLARE @NetworkC int  
  DECLARE @NetworkD int  
  DECLARE @NetworkE int  
  DECLARE @NetworkF int  
  DECLARE @NetworkG int  
  DECLARE @NetworkH int  
  DECLARE @NetworkI int  
  DECLARE @NetworkJ int  
  DECLARE @NetworkK int  
  DECLARE @NetworkL int  
  DECLARE @NetworkM int  
  DECLARE @NetworkN int  
  DECLARE @NetworkO int  
  DECLARE @NetworkP int  
  DECLARE @NetworkQ int  
  DECLARE @NetworkR int  
  DECLARE @NetworkS int  
  DECLARE @NetworkT int  
  DECLARE @NetworkU int  
  DECLARE @NetworkV int  
  DECLARE @NetworkW int  
  DECLARE @NetworkX int  
  DECLARE @NetworkY int  
  DECLARE @NetworkZ int  
  DECLARE @BlendedNetwork int  
  
  DECLARE @RatingTypeID int  
  DECLARE @YellowFlag int  
  DECLARE @Network varchar(1)  
  DECLARE @VariableRemoteRevFlag int  
  DECLARE @MinCharge float  
  DECLARE @RemoteDiscount float  
  DECLARE @DelDate datetime  
  DECLARE @Rowcount int  
  
  DECLARE @Typecode int  
  DECLARE @UOM varchar(3)  
  DECLARE @UOMMultiplier int  
  DECLARE @PlusDelivChgFlag int  
  DECLARE @StartWgt int  
  DECLARE @EndWgt int  
  DECLARE @wrkWeight int  
  
  DECLARE @MileageRange int  
  DECLARE @StartMiles int  
  DECLARE @EndMiles int  
  DECLARE @MilesPlusDelivChgFlag int  
  DECLARE @ExcessMilesUOM varchar(3)  
  DECLARE @wrkMiles float  
  
  
  -- Fuel Surcharge variables    
  DECLARE @ApplyFuelSurcharge tinyint  
  DECLARE @DeliveryEffDate datetime  
  DECLARE @TransEffDate datetime  
  DECLARE @FuelSurchargeID int  
  
  DECLARE @Surcharge smallmoney  
  DECLARE @TransSurcharge smallmoney  
  
  --DECLARE @QuoteID AS integer  
  
  ------------------------------------------------------------------------    
  -- these variables are used to calculate the Trans Charge    
  
  -- Declare @TransTypeID int    
  DECLARE @TransProvider varchar(50)  
  DECLARE @OneTimeRate int  
  DECLARE @TransPartnerID int  
  DECLARE @UsingDefaultTransTypeID int  
  
  DECLARE @TransMarkup float  
  DECLARE @CustomerTransDiscount float  
  
  DECLARE @wrkDestState varchar(2)  
  DECLARE @wrkOriginState varchar(2)  
  DECLARE @wrkConsumerDestState varchar(2)  
  DECLARE @wrkConsumerOriginState varchar(2)  
  
  
  -- 7/24/2009: added for WWL tariff (trantype 42)     
  DECLARE @FurnitureTransID int  
  DECLARE @CaseWeight numeric  
  DECLARE @UpholsteryWeight numeric  
  DECLARE @CommodityCode varchar(10)  
  
  DECLARE @Tariff varchar(10)  
  DECLARE @MinimumCharge float  
  DECLARE @Rate1 float  
  DECLARE @Rate2 float  
  DECLARE @Rate3 float  
  DECLARE @Rate4 float  
  DECLARE @Rate5 float  
  DECLARE @Rate6 float  
  DECLARE @Rate7 float  
  DECLARE @Rate8 float  
  DECLARE @Rate9 float  
  DECLARE @Rate10 float  
  DECLARE @Rate11 float  
  DECLARE @Rate12 float  
  DECLARE @Rate13 float  
  
  -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 now need to calculate customer    
  -- charges based on the customer address, and the partner    
  -- charges based on the provider address    
  -- these varibles are needed to do this    
  DECLARE @ConsumerStateTariff varchar(10)  
  DECLARE @CSMinimumCharge float  
  DECLARE @CSRate1 float  
  DECLARE @CSRate2 float  
  DECLARE @CSRate3 float  
  DECLARE @CSRate4 float  
  DECLARE @CSRate5 float  
  DECLARE @CSRate6 float  
  DECLARE @CSRate7 float  
  DECLARE @CSRate8 float  
  DECLARE @CSRate9 float  
  DECLARE @CSRate10 float  
  DECLARE @CSRate11 float  
  DECLARE @CSCustomerTransCharge float  
  
  --10/25/2007: changes for transtype 22    
  DECLARE @TMOriginTransZone varchar(10)  
  DECLARE @TMDestTransZone varchar(10)  
  
  DECLARE @wrkTransCharge1 float  
  DECLARE @wrkTransCharge2 float  
  
  -- added 1/5/2012    
  DECLARE @NbrofTransTypes int  
  ------------------------------------------------------------------------    
  
  -- 10/11/2016: new variables needed for the Excess Mileage    
  -- permanent accessorials.    
  DECLARE @ExcessMileage int  
  DECLARE @ExcessMileageID int  
  DECLARE @ExcessMileageQty int  
  DECLARE @ExcessMileageStartMiles numeric(18, 1)  
  DECLARE @ExcessMileageEndMiles numeric(18, 1)  
  DECLARE @ExcessMileageID2 int  
  DECLARE @ExcessMileageQty2 int  
  DECLARE @ExcessMileageStartMiles2 numeric(18, 1)  
  DECLARE @ExcessMileageEndMiles2 numeric(18, 1)  
  DECLARE @ExcessMileageID3 int  
  DECLARE @ExcessMileageQty3 int  
  DECLARE @ExcessMileageStartMiles3 numeric(18, 1)  
  DECLARE @ExcessMileageEndMiles3 numeric(18, 1)  
  
  DECLARE @PermAccessID int  
  DECLARE @PermAccessQTY int  
  
  DECLARE @ActivityID int  
  DECLARE @ActivityDescription varchar(50)  
  DECLARE @UnitOfMeasureCD varchar(3)  
  DECLARE @CustomerRateUnitOfMeasure varchar(3)  
  DECLARE @CustomerRateValue float  
  DECLARE @CustomerMinimumUOM varchar(3)  
  DECLARE @CustomerMinimumRateValue float  
  
  
  DECLARE @CustomerCost float  
  DECLARE @CustomerMinimum float  
  
  DECLARE @Margin smallmoney  
  DECLARE @ProjectRateID int  
  DECLARE @Units float  
  
  -- 3/24/2017    
  DECLARE @wrkDestZip3 varchar(3)  
  --Declare @wrkOriginState varchar(2)    
  DECLARE @wrkOriginZip varchar(10)  
  Declare @ExactDirectRate float  
 
 END
 
 BEGIN 
  
  SET @DelDate = GETDATE()  
  
  SET @RemoteRev = 0  
  SET @DeliveryCharge = 0  
  SET @FuelSurcharge = 0  
  SET @TransCharge = 0  
  SET @TransFuelSurcharge = 0    
  SET @UsingDefaultTransTypeID = 0   
  
  -- 7/6/2011: don't let the mileage software mess us up    
  IF @FMMiles = -1  
  BEGIN  
    SET @FMMiles = 1  
  END  
  IF @FMMiles = 0  
  BEGIN  
    SET @FMMiles = 1  
  END  
  
  -- NOTE: I probably do need to calculate remote revenue    
  -- and include it with the delivery charge    
  
  
  SELECT   ---AccountWgtBreaks
    @TypeCode = TypeCode,  
    @UOM = UnitofMeasureCD,  
    @PlusDelivChgFlag = PlusDelivChgFlag,  
    @StartWgt = StartWgt,  
    @EndWgt = EndWgt  
  FROM [HDN_Providence].[dbo].AccountWgtBreaks  
  WHERE CustomerID = @CustomerID  
  AND @TotalWeight BETWEEN startwgt AND endwgt  
  ORDER BY TypeCode  
  
PRINT '--AccountWgtBreaks--' + '@TypeCode :' + CONVERT(VARCHAR(5),@TypeCode)+ ' @UOM :' + CONVERT(VARCHAR(5),@UOM)+ ' @PlusDelivChgFlag :' + CONVERT(VARCHAR(5),@PlusDelivChgFlag)+ ' @StartWgt :' + CONVERT(VARCHAR(5),@StartWgt)+ ' @EndWgt :' + CONVERT(VARCHAR(5),@EndWgt)
  
  SELECT  --AccountMileageBreaks
    @MileageRange = TypeCode,  
    @StartMiles = StartMiles,  
    @EndMiles = EndMiles,  
    @MilesPlusDelivChgFlag = PlusDelivChgFlag,  
    @ExcessMilesUOM = ExcessMilesUOM  
  FROM [HDN_Providence].[dbo].AccountMileageBreaks  
  WHERE CustomerID = @CustomerID  
  -- round the final mile miles in case it is something like 50.4 and the mileage    
  -- ranges are defined as 0-50 and 51-9999.    
  AND ROUND(@FMMiles, 0) BETWEEN StartMiles AND EndMiles  
  ORDER BY TypeCode  
  
  PRINT '--AccountMileageBreaks--' + ' @MileageRange :' + CONVERT(VARCHAR(5),@MileageRange)+ ' @StartMiles :' + CONVERT(VARCHAR(5),@StartMiles)+ ' @EndMiles :' + CONVERT(VARCHAR(5),@EndMiles)+ ' @MilesPlusDelivChgFlag :' + CONVERT(VARCHAR(5),@MilesPlusDelivChgFlag)+ ' @ExcessMilesUOM :' + CONVERT(VARCHAR(5),@ExcessMilesUOM)
  
  -- get all of the fields we need from the customer table here    
  
  SELECT  --RatingTypeID, VariableRemoteRevFlag,YellowFlag,Network,MinCharge
    @RatingTypeID = ISNULL(RatingTypeID, 1),  
    @VariableRemoteRevFlag = VariableRemoteRevFlag,  
    -- 3/10/2011: need these for RemoteRev by network    
    @YellowFlag = ISNULL(YellowFlag, 0),  
    @Network = NetworkID,  
    @MinCharge = MinimumCharge  
  FROM [HDN_Providence].[dbo].Customer  
  WHERE CustomerID = @CustomerID   
  
    PRINT ISNULL('--RatingTypeID, VariableRemoteRevFlag,YellowFlag,Network,MinCharge--','') + ' @RatingTypeID :' + ISNULL(CONVERT(VARCHAR(5),@RatingTypeID),'')+ ' @VariableRemoteRevFlag :' + ISNULL(CONVERT(VARCHAR(5),@VariableRemoteRevFlag),'')+ ' @YellowFlag :' + ISNULL(CONVERT(VARCHAR(5),@YellowFlag),'')+ ' @Network :' + ISNULL(CONVERT(VARCHAR(5),@Network),'')+ ' @MinCharge :' + ISNULL(CONVERT(VARCHAR(5),@MinCharge),'')
 

  SELECT  --@Region - HXRates
    @Region = Region,  
    @Region2 = RegionB,  
    @Region3 = RegionC,  
    @Region4 = RegionD,  
    @Region5 = RegionE,  
    @Region6 = RegionF,  
    @Region7 = RegionG,  
    @Region8 = RegionH,  
    @Region9 = RegionI,  
    @Region10 = RegionJ,  
    @Region11 = RegionK,  
    @Region12 = RegionL,  
    @Region13 = RegionM,  
    @Region14 = RegionN,  
    @Region15 = RegionO,  
    @Region16 = RegionP,  
    @Region17 = RegionQ,  
    @Region18 = RegionR,  
    @Region19 = RegionS,  
    @Region20 = RegionT,  
    @Region21 = RegionU,  
    @Region22 = RegionV,  
    @Region23 = RegionW,  
    @Region24 = RegionX,  
    @Region25 = RegionY,  
    @Region26 = RegionZ,  
    @BlendedRegion = BlendedRegion,  
    @NetworkA = NetworkA,  
    @NetworkB = NetworkB,  
    @NetworkC = NetworkC,  
    @NetworkD = NetworkD,  
    @NetworkE = NetworkE,  
    @NetworkF = NetworkF,  
    @NetworkG = NetworkG,  
    @NetworkH = NetworkH,  
    @NetworkI = NetworkI,  
    @NetworkJ = NetworkJ,  
    @NetworkK = NetworkK,  
    @NetworkL = NetworkL,  
    @NetworkM = NetworkM,  
    @NetworkN = NetworkN,  
    @NetworkO = NetworkO,  
    @NetworkP = NetworkP,  
    @NetworkQ = NetworkQ,  
    @NetworkR = NetworkR,  
    @NetworkS = NetworkS,  
    @NetworkT = NetworkT,  
    @NetworkU = NetworkU,  
    @NetworkV = NetworkV,  
    @NetworkW = NetworkW,  
    @NetworkX = NetworkX,  
    @NetworkY = NetworkY,  
    @NetworkZ = NetworkZ,  
    @BlendedNetwork = BlendedNetwork,  
    @RemoteRev = ISNULL(RemoteRev, 0),  
    @RemoteRev2 = ISNULL(RemoteRev2, 0),  
    @RemoteRev3 = ISNULL(RemoteRev3, 0),  
    @RemoteRev4 = ISNULL(RemoteRev4, 0),  
    @RemoteRev5 = ISNULL(RemoteRev5, 0),  
    @RemoteRev6 = ISNULL(RemoteRev6, 0),  
    @RemoteRev7 = ISNULL(RemoteRev7, 0),  
    @RemoteRev8 = ISNULL(RemoteRev8, 0),  
    @RemoteRev9 = ISNULL(RemoteRev9, 0),  
    @RemoteRev10 = ISNULL(RemoteRev10, 0),  
    @RemoteRevB = ISNULL(RemoteRevB, 0),  
    @RemoteRevC = ISNULL(RemoteRevC, 0),  
    @RemoteRevD = ISNULL(RemoteRevD, 0),  
    @RemoteRevE = ISNULL(RemoteRevE, 0),  
    @RemoteRevF = ISNULL(RemoteRevF, 0),  
    @RemoteRevG = ISNULL(RemoteRevG, 0),  
    @RemoteRevH = ISNULL(RemoteRevH, 0),  
    @RemoteRevI = ISNULL(RemoteRevI, 0),  
    @RemoteRevJ = ISNULL(RemoteRevJ, 0),  
    @RemoteRevK = ISNULL(RemoteRevK, 0),  
    @RemoteRevL = ISNULL(RemoteRevL, 0),  
    @RemoteRevM = ISNULL(RemoteRevM, 0),  
    @RemoteRevN = ISNULL(RemoteRevN, 0),  
    @RemoteRevO = ISNULL(RemoteRevO, 0),  
    @RemoteRevP = ISNULL(RemoteRevP, 0),  
    @RemoteRevQ = ISNULL(RemoteRevQ, 0),  
    @RemoteRevR = ISNULL(RemoteRevR, 0),  
    @RemoteRevS = ISNULL(RemoteRevS, 0),  
    @RemoteRevT = ISNULL(RemoteRevT, 0),  
    @RemoteRevU = ISNULL(RemoteRevU, 0),  
    @RemoteRevV = ISNULL(RemoteRevV, 0),  
    @RemoteRevW = ISNULL(RemoteRevW, 0),  
    @RemoteRevX = ISNULL(RemoteRevX, 0),  
    @RemoteRevY = ISNULL(RemoteRevY, 0),  
    @RemoteRevZ = ISNULL(RemoteRevZ, 0),  
    @RemoteRevBlended = ISNULL(BlendedRemoteRev, 0),  
    @RemoteExp = ISNULL(RemoteExp, 0),  
    @RateStateID = StateID  
  FROM [HDN_Providence].[dbo].HXRates  
  WHERE Zip = @DestZip  
  
  
    PRINT ISNULL('--HXRates-- Select  * from HXRates  WHERE ZIP = @DestZip ','') + ' @DestZip :' + ISNULL(CONVERT(VARCHAR(5),@DestZip),'')
 
  
  SELECT  
    @RowCount = @@RowCount  
  
 
  IF @YellowFlag = 1  
  BEGIN  
    SET @Region = @BlendedRegion  
    SET @RemoteRev = @RemoteRevBlended  
    SET @PreferredProvider = @BlendedNetwork  
  END  
  
  IF @YellowFlag <> 1  
  BEGIN  
    IF @Network = 'A'  
    BEGIN  
      --Set @Region = @Region    
      --Set @RemoteRev = @RemoteRev    
      SET @PreferredProvider = @NetworkA  
    END  
    IF @Network = 'B'  
    BEGIN  
      SET @Region = @Region2  
      SET @RemoteRev = @RemoteRevB  
      SET @PreferredProvider = @NetworkB  
    END  
    IF @Network = 'C'  
    BEGIN  
      SET @Region = @Region3  
      SET @RemoteRev = @RemoteRevC  
      SET @PreferredProvider = @NetworkC  
    END  
    IF @Network = 'D'  
    BEGIN  
      SET @Region = @Region4  
      SET @RemoteRev = @RemoteRevD  
      SET @PreferredProvider = @NetworkD  
    END  
    IF @Network = 'E'  
    BEGIN  
      SET @Region = @Region5  
      SET @RemoteRev = @RemoteRevE  
      SET @PreferredProvider = @NetworkE  
    END  
    IF @Network = 'F'  
    BEGIN  
      SET @Region = @Region6  
      SET @RemoteRev = @RemoteRevF  
      SET @PreferredProvider = @NetworkF  
   END  
    IF @Network = 'G'  
    BEGIN  
      SET @Region = @Region7  
      SET @RemoteRev = @RemoteRevG  
      SET @PreferredProvider = @NetworkG  
    END  
    IF @Network = 'H'  
    BEGIN  
      SET @Region = @Region8  
      SET @RemoteRev = @RemoteRevH  
      SET @PreferredProvider = @NetworkH  
    END  
    IF @Network = 'I'  
    BEGIN  
      SET @Region = @Region9  
      SET @RemoteRev = @RemoteRevI  
      SET @PreferredProvider = @NetworkI  
    END  
    IF @Network = 'J'  
    BEGIN  
      SET @Region = @Region10  
      SET @RemoteRev = @RemoteRevJ  
      SET @PreferredProvider = @NetworkJ  
    END  
    IF @Network = 'K'  
    BEGIN  
      SET @Region = @Region11  
      SET @RemoteRev = @RemoteRevK  
      SET @PreferredProvider = @NetworkK  
    END  
    IF @Network = 'L'  
    BEGIN  
      SET @Region = @Region12  
      SET @RemoteRev = @RemoteRevL  
      SET @PreferredProvider = @NetworkL  
    END  
    IF @Network = 'M'  
    BEGIN  
      SET @Region = @Region13  
      SET @RemoteRev = @RemoteRevM  
      SET @PreferredProvider = @NetworkM  
    END  
    IF @Network = 'N'  
    BEGIN  
      SET @Region = @Region14  
      SET @RemoteRev = @RemoteRevN  
      SET @PreferredProvider = @NetworkN  
    END  
    IF @Network = 'O'  
    BEGIN  
      SET @Region = @Region15  
      SET @RemoteRev = @RemoteRevO  
      SET @PreferredProvider = @NetworkO  
    END  
    IF @Network = 'P'  
    BEGIN  
      SET @Region = @Region16  
      SET @RemoteRev = @RemoteRevP  
      SET @PreferredProvider = @NetworkP  
    END  
    IF @Network = 'Q'  
    BEGIN  
      SET @Region = @Region17  
      SET @RemoteRev = @RemoteRevQ  
      SET @PreferredProvider = @NetworkQ  
    END  
    IF @Network = 'R'  
    BEGIN  
      SET @Region = @Region18  
      SET @RemoteRev = @RemoteRevR  
      SET @PreferredProvider = @NetworkR  
    END  
    IF @Network = 'S'  
    BEGIN  
      SET @Region = @Region19  
      SET @RemoteRev = @RemoteRevS  
      SET @PreferredProvider = @NetworkS  
    END  
    IF @Network = 'T'  
    BEGIN  
      SET @Region = @Region20  
      SET @RemoteRev = @RemoteRevT  
      SET @PreferredProvider = @NetworkT  
    END  
    IF @Network = 'U'  
    BEGIN  
      SET @Region = @Region21  
      SET @RemoteRev = @RemoteRevU  
      SET @PreferredProvider = @NetworkU  
    END  
    IF @Network = 'V'  
    BEGIN  
      SET @Region = @Region22  
      SET @RemoteRev = @RemoteRevV  
      SET @PreferredProvider = @NetworkV  
    END  
    IF @Network = 'W'  
    BEGIN  
      SET @Region = @Region23  
      SET @RemoteRev = @RemoteRevW  
      SET @PreferredProvider = @NetworkW  
    END  
    IF @Network = 'X'  
    BEGIN  
      SET @Region = @Region24  
      SET @RemoteRev = @RemoteRevX  
      SET @PreferredProvider = @NetworkX  
    END  
    IF @Network = 'Y'  
    BEGIN  
      SET @Region = @Region25  
      SET @RemoteRev = @RemoteRevY  
      SET @PreferredProvider = @NetworkY  
    END  
    IF @Network = 'Z'  
    BEGIN  
      SET @Region = @Region26  
      SET @RemoteRev = @RemoteRevZ  
      SET @PreferredProvider = @NetworkZ  
    END  
  END  
  
  IF @VariableRemoteRevFlag > 0  
  BEGIN  
    IF @TypeCode = 1  
    BEGIN  
      SET @RemoteRev = @RemoteRev  
    END  
    IF @TypeCode = 2  
    BEGIN  
      SET @RemoteRev = @RemoteRev2  
    END  
    IF @TypeCode = 3  
    BEGIN  
      SET @RemoteRev = @RemoteRev3  
    END  
    IF @TypeCode = 4  
    BEGIN  
      SET @RemoteRev = @RemoteRev4  
    END  
    IF @TypeCode = 5  
    BEGIN  
      SET @RemoteRev = @RemoteRev5  
    END  
    IF @TypeCode = 6  
    BEGIN  
      SET @RemoteRev = @RemoteRev6  
    END  
    IF @TypeCode = 7  
    BEGIN  
      SET @RemoteRev = @RemoteRev7  
    END  
    IF @TypeCode = 8  
    BEGIN  
      SET @RemoteRev = @RemoteRev8  
    END  
    IF @TypeCode = 9  
    BEGIN  
      SET @RemoteRev = @RemoteRev9  
    END  
    IF @TypeCode = 10  
    BEGIN  
      SET @RemoteRev = @RemoteRev10  
    END  
  END  
  /*    
   End    
  */  
  
 
   
  IF (@RemoteExp <> 0  
    OR @RemoteRev <> 0)  
  BEGIN  
  
    IF (@RatingTypeID = 1)  
     OR (@RatingTypeID = 3) -- Region    
    BEGIN  
      SELECT TOP 1  
        @RemoteDiscount = ISNULL(RemoteDisc, 1)  
      FROM [HDN_Providence].[dbo].CustomerRatesRevenue  
      WHERE CustomerID = @CustomerID  
      AND Region = @Region  
      AND ServiceLevelID = @ServiceLevel  
      AND MileageRange = @MileageRange  
      AND EffectiveDate < @DelDate  
      ORDER BY EffectiveDate DESC  
      
      
    END  
  
    IF (@RatingTypeID = 2)  
      OR (@RatingTypeID = 4) -- State to State      
    BEGIN  
      SELECT TOP 1  
        @RemoteDiscount = ISNULL(RemoteDisc, 1)  
      FROM [HDN_Providence].[dbo].CustomerRatesRevenue  
      WHERE CustomerID = @CustomerID  
      AND StateID = @RateStateID  
      AND ServiceLevelID = @ServiceLevel  
      AND MileageRange = @MileageRange  
      AND EffectiveDate < @DelDate  
      ORDER BY EffectiveDate DESC  
    END  
  
    -- if match not found, default to 100% remote rev and comp discount    
    SELECT  
      @RemoteDiscount = ISNULL(@RemoteDiscount, 1)  
  
    IF @RemoteDiscount > 0  
    BEGIN  
      SELECT  
        @RemoteRev = @RemoteRev - (@RemoteRev * @RemoteDiscount)  
    END  
  
  END  
  
  
  /* Remote Charges calculation ends */  
  ----------------------------------------------------------------------------------------    
  
  -- calculate Delivery Charge next    
  
  
  SET @KeepGoing = 1  
  -- in this section, we will use the next variable    
  -- so we can keep the excess Mileage cost separate    
  SET @ExcessMilesCustomerCost = 0  
  
  IF @RatingTypeID = 1  
    OR @RatingTypeID = 2  
  BEGIN  
  
    WHILE @KeepGoing = 1  
    BEGIN  
  
      IF @RatingTypeID = 1 -- Region , Rate, CustomerRatesRevenue     
      BEGIN  
        SELECT TOP 1  
          @DeliveryCharge1 = ISNULL(Rate1, 0),  
          @DeliveryCharge2 = ISNULL(Rate2, 0),  
          @DeliveryCharge3 = ISNULL(Rate3, 0),  
          @DeliveryCharge4 = ISNULL(Rate4, 0),  
          @DeliveryCharge5 = ISNULL(Rate5, 0),  
          @DeliveryCharge6 = ISNULL(Rate6, 0),  
          @DeliveryCharge7 = ISNULL(Rate7, 0),  
          @DeliveryCharge8 = ISNULL(Rate8, 0),  
          @DeliveryCharge9 = ISNULL(Rate9, 0),  
          @DeliveryCharge10 = ISNULL(Rate10, 0),  
          @DeliveryCharge11 = ISNULL(Rate11, 0),  
          @DeliveryCharge12 = ISNULL(Rate12, 0),  
          @DeliveryCharge13 = ISNULL(Rate13, 0),  
          @DeliveryCharge14 = ISNULL(Rate14, 0),  
          @DeliveryCharge15 = ISNULL(Rate15, 0),  
          @DeliveryCharge16 = ISNULL(Rate16, 0),  
          @DeliveryCharge17 = ISNULL(Rate17, 0),  
          @DeliveryCharge18 = ISNULL(Rate18, 0),  
          @DeliveryCharge19 = ISNULL(RAte19, 0),  
          @DeliveryCharge20 = ISNULL(Rate20, 0)  
        FROM [HDN_Providence].[dbo].CustomerRatesRevenue  
        WHERE CustomerID = @CustomerID  
        AND Region = @Region  
        AND ServiceLevelID = @ServiceLevel  
        AND MileageRange = @MileageRange  
        AND EffectiveDate < @DelDate  
        ORDER BY EffectiveDate DESC  
        
            PRINT ISNULL('-- Region , Rate, CustomerRatesRevenue -- FROM CustomerRatesRevenue  
        WHERE CustomerID ','') + ' @CustomerID :' + ISNULL(CONVERT(VARCHAR(5),@CustomerID),'')+ ' @Region :' + ISNULL(CONVERT(VARCHAR(5),@Region),'')+ ' @ServiceLevel :' + ISNULL(CONVERT(VARCHAR(5),@ServiceLevel),'')+ ' @MileageRange :' + ISNULL(CONVERT(VARCHAR(5),@MileageRange),'')+ ' @DelDate :' + ISNULL(CONVERT(VARCHAR(5),@DelDate),'')
        
      END  
  
      IF @RatingTypeID = 2 -- State to State, Rate, CustomerRatesRevenue      
      BEGIN  
        SELECT TOP 1  
          @DeliveryCharge1 = ISNULL(Rate1, 0),  
          @DeliveryCharge2 = ISNULL(Rate2, 0),  
          @DeliveryCharge3 = ISNULL(Rate3, 0),  
          @DeliveryCharge4 = ISNULL(Rate4, 0),  
          @DeliveryCharge5 = ISNULL(Rate5, 0),  
          @DeliveryCharge6 = ISNULL(Rate6, 0),  
          @DeliveryCharge7 = ISNULL(Rate7, 0),  
          @DeliveryCharge8 = ISNULL(Rate8, 0),  
          @DeliveryCharge9 = ISNULL(Rate9, 0),  
          @DeliveryCharge10 = ISNULL(Rate10, 0),  
          @DeliveryCharge11 = ISNULL(Rate11, 0),  
          @DeliveryCharge12 = ISNULL(Rate12, 0),  
          @DeliveryCharge13 = ISNULL(Rate13, 0),  
          @DeliveryCharge14 = ISNULL(Rate14, 0),  
          @DeliveryCharge15 = ISNULL(Rate15, 0),  
          @DeliveryCharge16 = ISNULL(Rate16, 0),  
          @DeliveryCharge17 = ISNULL(Rate17, 0),  
          @DeliveryCharge18 = ISNULL(Rate18, 0),  
          @DeliveryCharge19 = ISNULL(Rate19, 0),  
          @DeliveryCharge20 = ISNULL(Rate20, 0)  
        FROM [HDN_Providence].[dbo].CustomerRatesRevenue  
        WHERE CustomerID = @CustomerID  
        AND StateID = @RateStateID  
        AND ServiceLevelID = @ServiceLevel  
        AND MileageRange = @MileageRange  
        AND EffectiveDate < @DelDate  
        ORDER BY EffectiveDate DESC  
        
            PRINT ISNULL('-- State to State, Rate, CustomerRatesRevenue -- SELECT TOP 1 * FROM CustomerRatesRevenue  WHERE CustomerID ','') + ' @CustomerID :' + ISNULL(CONVERT(VARCHAR(5),@CustomerID),'')+ ' @RateStateID :' + ISNULL(CONVERT(VARCHAR(5),@RateStateID),'')+ ' @ServiceLevel :' + ISNULL(CONVERT(VARCHAR(5),@ServiceLevel),'')+ ' @MileageRange :' + ISNULL(CONVERT(VARCHAR(5),@MileageRange),'')+ ' @DelDate :' + ISNULL(CONVERT(VARCHAR(5),@DelDate),'')
        
      END  
  
  
      -- @TypeCode falls through from the beginning of the procedure    
      
      
      IF @TypeCode = 1  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge11  
      END  
      IF @TypeCode = 12  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge12  
      END  
      IF @TypeCode = 13  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge13  
      END  
      IF @TypeCode = 14  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge14  
      END  
      IF @TypeCode = 15  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge15  
      END  
      IF @TypeCode = 16  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge16  
      END  
      IF @TypeCode = 17  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge17  
      END  
      IF @TypeCode = 18  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge18  
      END  
      IF @TypeCode = 19  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge19  
      END  
      IF @TypeCode = 20  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge20  
      END
      
         PRINT '--@TypeCode:-- @DeliveryCharge Comes from CustomerRatesRevenue ' + CONVERT(VARCHAR(5), @TypeCode) + ' @DeliveryCharge :' +   CONVERT(VARCHAR(5),@DeliveryCharge)
  
  
      ----------------------------------------------------------------------------------------    
      IF @MileageRange > 1  
        AND @MilesPlusDelivChgFlag = 1  
      -- NOTE: if it is mileagerange 1, it is not valid that the    
      -- Plus Delivery Charge box be checked.    
      BEGIN  
  
        -- Pounds or Hundred Weight    
        IF @ExcessMilesUOM = 'LB'  
          OR @ExcessMilesUOM = 'CWT'  
        BEGIN  
          SELECT  
            @UOMMultiplier = Multiplier  
          FROM [HDN_Providence].[dbo].UnitofMeasure  
          WHERE UnitofMeasureCD = @ExcessMilesUOM  
          SET @DeliveryCharge = (@TotalWeight / @UOMMultiplier) * @DeliveryCharge  
          
            PRINT ISNULL(' -- Pounds or Hundred Weight -- ','') + ' @DeliveryCharge :' + ISNULL(CONVERT(VARCHAR(5),@DeliveryCharge),'')
        
          
        END  
  
  
        -- Miles    
        IF @ExcessMilesUOM = 'MI'  
        BEGIN  
          -- cost times excess miles    
          -- I need to account for 3 or more mileage breaks where    
          -- the mileage falls into the 3rd break and both the 2nd and 3rd    
          -- are "Plus Delivery Charge".    
          IF @FMMiles > @EndMiles  
          BEGIN  
            SET @wrkMiles = @EndMiles - @StartMiles + 1  
          END  
          ELSE  
          BEGIN  
            SET @wrkMiles = @FMMiles - @StartMiles + 1  
          END  
          SET @DeliveryCharge = @DeliveryCharge * @wrkMiles  
          
           PRINT ISNULL(' --cost times excess miles-- ','') + ' @DeliveryCharge :' + ISNULL(CONVERT(VARCHAR(5),@DeliveryCharge),'')
        END  
  
        -- accumulate excess mileage costs here    
        SET @ExcessMilesCustomerCost = @ExcessMilesCustomerCost + @DeliveryCharge  
        SET @DeliveryCharge = 0  
  
        SET @MileageRange = @MileageRange - 1  
  
        SELECT  
          @MilesPlusDelivChgFlag = PlusDelivChgFlag,  
          @StartMiles = StartMiles,  
          @EndMiles = EndMiles,  
          @ExcessMilesUOM = ExcessMilesUOM  
        FROM [HDN_Providence].[dbo].AccountMileageBreaks  
        WHERE CustomerID = @CustomerID  
        AND TypeCode = @MileageRange
        
              PRINT ISNULL('-- accumulate excess mileage costs here--','') + ' @MilesPlusDelivChgFlag :' + ISNULL(CONVERT(VARCHAR(5),@MilesPlusDelivChgFlag),'')+ ' @StartMiles :' + ISNULL(CONVERT(VARCHAR(5),@StartMiles),'')+ ' @EndMiles :' + ISNULL(CONVERT(VARCHAR(5),@EndMiles),'')+ ' @ExcessMilesUOM :' + ISNULL(CONVERT(VARCHAR(5),@ExcessMilesUOM),'')+ ' @MileageRange :' + ISNULL(CONVERT(VARCHAR(5),@MileageRange),'')  
      END    

      ELSE  
  
      BEGIN  
        SET @KeepGoing = 0  
      END  
  
    END -- WHILE @KeepGoing = 1    
  
    ----------------------------------------------------------------------------------------    
  
    -- the "Keep Going" loop above will handle the Mileage Breaks.  We will "back thru"    
    -- the mileage breaks until we hit one without the "Plus Delivery Charge" checked,    
    -- or until we get to the first one.    
  
    -- at this point, @ExcessMilesCustomerCost and @ExcessMilesPartnerCost contain the excess     
    -- mileage costs, and @DeliveryCharge and @PartnerCost contain the rate to use/process.    
    -- of course, that rate could be "Plus Delivery Charge" on the Weight Range.    
  
    SET @WasPlusDeliv = 0  
    SET @PlusDelivCustomerCost = 0  
  
    WHILE @TypeCode > 1  
      AND @PlusDelivChgFlag = 1  
    BEGIN  
      -- we are here if we have a weight range "Plus Delivery Charge"    
  
      -- pass this flag to avoid overcharging if per pound or CWT calc    
      SET @WasPlusDeliv = 1  
  
      SELECT  
        @UOMMultiplier = Multiplier  
      FROM [HDN_Providence].[dbo].UnitofMeasure  
      WHERE UnitofMeasureCD = @UOM  
  
      -- Pounds or Hundred Weight    
      IF @UOM = 'LB'  
        OR @UOM = 'CWT'  
      BEGIN  
        -- since this is "Plus Delivery Charge", we are dealing with an "excess" charge,     
        -- so we need to calculate the excess weight here    
        IF @TotalWeight > @EndWgt  
        BEGIN  
          SET @wrkWeight = @EndWgt - @StartWgt + 1  
        END  
        ELSE  
        BEGIN  
          SET @wrkWeight = @TotalWeight - @StartWgt + 1  
        END  
      END  
  
      IF @UOM = 'LB'  
        OR @UOM = 'CWT'  
      BEGIN  
        SET @DeliveryCharge = (@wrkWeight / @UOMMultiplier) * @DeliveryCharge  
        
         PRINT ISNULL('--1033 --@UOM = ''LB'' OR @UOM = ''CWT''-- ','') + ' @DeliveryCharge :' + ISNULL(CONVERT(VARCHAR(5),@DeliveryCharge),'')
        
      END  
  
      -- Flat Rate will fall through (@DeliveryCharge=@DeliveryCharge, @PartnerCost=@PartnerCost)    
  
      SET @PlusDelivCustomerCost = @PlusDelivCustomerCost + @DeliveryCharge  
      SET @DeliveryCharge = 0  
  
      SET @TypeCode = @TypeCode - 1  
  
      SELECT  
        @UOM = UnitofMeasureCD,  
        @PlusDelivChgFlag = PlusDelivChgFlag,  
        @StartWgt = StartWgt,  
        @EndWgt = EndWgt  
      FROM [HDN_Providence].[dbo].AccountWgtBreaks  
      WHERE CustomerID = @CustomerID  
      AND TypeCode = @TypeCode  
      ORDER BY TypeCode  
  
  
      -- now get the appropriate rate for the new @TypeCode    
      IF @TypeCode = 1  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge11  
      END  
      IF @TypeCode = 12  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge12  
      END  
      IF @TypeCode = 13  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge13  
      END  
      IF @TypeCode = 14  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge14  
      END  
      IF @TypeCode = 15  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge15  
      END  
      IF @TypeCode = 16  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge16  
      END  
      IF @TypeCode = 17  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge17  
      END  
      IF @TypeCode = 18  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge18  
      END  
      IF @TypeCode = 19  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge19  
      END  
      IF @TypeCode = 20  
      BEGIN  
        SET @DeliveryCharge = @DeliveryCharge20  
      END  
  
    END -- WHILE @TypeCode > 1 and @PlusDelivChgFlag = 1    
  
  
    -- at this point, we will have the ExcellMilesCustomerCost,    
    -- the PlusDelivCustomerCost and the CustomerCost.    
    -- the CustomerCost needs to be adjusted for UOM.    
  
  
    SELECT  
      @UOMMultiplier = Multiplier  
    FROM [HDN_Providence].[dbo].UnitofMeasure  
    WHERE UnitofMeasureCD = @UOM  
  
  
    -- Pounds or Hundred Weight    
    IF @UOM = 'LB'  
      OR @UOM = 'CWT'  
    BEGIN  
      IF @WasPlusDeliv = 1  
      BEGIN  
        -- since there was a "Plus Delivery Charge", we already dealt with the "excess" charge,     
        -- so our calculation here can only cover the current weight range    
        IF @TotalWeight > @EndWgt  
        BEGIN  
          -- Set @wrkWeight = @EndWgt - @StartWgt + 1    
          SET @wrkWeight = @EndWgt  
        END  
        ELSE  
        BEGIN  
          -- Set @wrkWeight = @TotalWeight - @StartWgt + 1    
          SET @wrkWeight = @TotalWeight  
        END  
      END  
    END  
  
    -- Pounds or Hundred Weight    
    IF @UOM = 'LB'  
      OR @UOM = 'CWT'  
    BEGIN  
      SET @DeliveryCharge = (@TotalWeight / @UOMMultiplier) * @DeliveryCharge  
      
       PRINT ISNULL('--1178 --@UOM = ''LB'' OR @UOM = ''CWT''-- ','') + ' @DeliveryCharge :' + ISNULL(CONVERT(VARCHAR(5),@DeliveryCharge),'')
    END  
  
    
    -- Flat Rate will fall through (@DeliveryCharge=@DeliveryCharge, @PartnerCost=@PartnerCost)    
  
    -- after the CustomerCost and PartnerCost is adjusted, add the ExcessMiles and    
    -- PlusDeliv costs    
    SET @DeliveryCharge = @DeliveryCharge + @ExcessMilesCustomerCost + @PlusDelivCustomerCost  
  
   PRINT ISNULL('---1188 --@UOM = ''LB'' OR @UOM = ''CWT''-- ','') + ' @DeliveryCharge :' + ISNULL(CONVERT(VARCHAR(5),@DeliveryCharge),'')
  
  END  -- If @RatingTypeID = 1 or @RatingTypeID = 2    
  
  
  ----------------------------------------------------------------------------------------    
  -- if there is an origin zip, then calculate a Transportation Charge    
  IF ISNULL(@OriginZip, '') <> ''  
  BEGIN  
    ----------------------------------------------------------------------------------------    
  
  
    SELECT  
      @TransMarkup = ISNULL(TransMarkup, 0),  
      @CustomerTransDiscount = ISNULL(CustomerTransDisc, 0)  
    FROM [HDN_Providence].[dbo].Customer  
    WHERE CustomerID = @CustomerID  
  
    -- @TransTypeID is now passed into the routine.  There are some    
    -- customers that do not have a trans type assigned.  When that is the    
    -- case, we need to set he "UsingDefault" flag so it will caclulate    
    -- a fuel surcharge on the transportation.     
    SELECT  
      @NbrofTransTypes = COUNT(*)  
    FROM [HDN_Providence].[dbo].CustomerTransType  
    WHERE CustomerID = @CustomerID  
  
    IF @NbrofTransTypes = 0  
    BEGIN  
      SET @UsingDefaultTransTypeID = 1  
    END  
  
    SELECT  
      @TransProvider = Description,  
      @OneTimeRate = OneTimeRate,  
      @TransPartnerID = PartnerID  
    FROM [HDN_Providence].[dbo].TransportationType  
    WHERE TransTypeID = @TransTypeID  
    

  
    -- the @PreferredProvider variable contains the partner id    
    ------------------------------------------------------------------------    
    -- the provider will be the destination for the transportation activity.    
    -- we will assume that the destination zip that they enter is the    
    -- end consumer's zip code    
    ------------------------------------------------------------------------    
    -- 10/27/04: in the New York area, BDI Laguna wants to do business    
    -- only with S&D Transfer, and not with Santini.        
    IF @CustomerID = 212  
      AND @PreferredProvider = 137  
    BEGIN  
      SELECT  
        @PreferredProvider = 278  
    END  
  
    -- 3/12/2010: BDI wants to use S&D in Connecticut    
    IF @CustomerID = 212  
      AND @PreferredProvider = 1437  
    BEGIN  
      SELECT  
        @PreferredProvider = 278  
    END  
  
    -- 4/20/2005: Santini took over the LC (partner 141)    
    IF @CustomerID = 212  
      AND @PreferredProvider = 141  
    BEGIN  
      SELECT  
        @PreferredProvider = 278  
    END  
  
    -- 9/12/2006: all orders from this customer go to Eglin AFB,    
    -- and Terrell Magee wants them all going thru the Pensacola agency    
    IF @CustomerID = 489  
    BEGIN  
      SELECT  
        @PreferredProvider = 250  
    END  
  
    -- 9/14/2005: Terrell Magee wants all orders for this customer going thru the Pensacola agency    
    IF @CustomerID = 592  
    BEGIN  
      SELECT  
        @PreferredProvider = 250  
    END  
  
    -- 9/14/2005: Terrell Magee wants all orders for this customer going thru the Panama City agency    
    IF @CustomerID = 593  
    BEGIN  
      SELECT  
        @PreferredProvider = 157  
    END  
  
    -- 6/19/2008: this customer is the Scholastic Books customer, and all orders    
    -- are delivered by Santini, but they need to go to BOS, so we use this     
    -- internal Santini partner ID.    
    IF @CustomerID = 462  
    BEGIN  
      SELECT  
        @PreferredProvider = 1033  
    END  
  
  
    SELECT  
      @wrkOriginState = StateID  
    FROM [HDN_Providence].[dbo].HXRates  
    WHERE Zip = @OriginZip  
  
    -- origin should always be the same    
    SELECT  
      @wrkConsumerOriginState = @wrkOriginState  
  
    -- wrkDestState will be the provider state    
    SELECT  
      @wrkDestState = a.stateid  
    FROM [HDN_Providence].[dbo].Partner p,  
         PartnerLocation pl,  
         Address a  
    WHERE p.partnerid = pl.partnerid  
    AND pl.addressid = a.addressid  
    AND pl.primarylocation = 1  
    AND p.partnerid = @PreferredProvider  
  
    SELECT  
      @wrkConsumerDestState = StateID  
    FROM [HDN_Providence].[dbo].HXRates  
    WHERE Zip = @DestZip  
  
    /*    
    insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)    
     Values('@PreferredProvider=',@PreferredProvider, 0, GetDate(),'bergc')    
    insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)    
     Values('@wrkDestState=',@wrkDestState, 0, GetDate(),'bergc')    
    insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)    
     Values('@wrkConsumerDestState=',@wrkConsumerDestState, 0, GetDate(),'bergc')    
    */  
  
    ------------------------------------------------------------------------    
    -- 3/11/2009 new Worldwide Logistics tariff using the furniture rates    
    IF @TransPartnerID = 1514  
      AND @TransTypeID = 42  
    BEGIN  
  
      SET @OriginZip = LEFT(@OriginZip, 3)  
      SET @DestZip = LEFT(@DestZip, 3)  
  
      SELECT  
        @FurnitureTransID = FurnitureTransID  
      FROM [HDN_Providence].[dbo].FurnitureTrans  
      WHERE StartZip = @OriginZip  
      AND EndZip = @DestZip  
      AND PartnerID = @TransPartnerID     --1514    
  
      -- *** look here ***    
      -- ***    
      
      SET @CaseWeight = 0  
      SET @UpholsteryWeight = @TotalWeight  
  
      IF @CaseWeight > 0  
      BEGIN  
        SET @CommodityCode = 'CASE'  
  
        SELECT  
          @TypeCode = TypeCode  
        FROM [HDN_Providence].[dbo].TransWGTBreaks  
        WHERE PartnerID = @TransPartnerID   --1514    
        AND TransTypeID = @TransTypeID   --42    
        AND @CaseWeight BETWEEN StartWgt AND EndWgt  
      END  
  
      IF @UpholsteryWeight > 0  
      BEGIN  
        SET @CommodityCode = 'UPHOLSTERY'  
  
        SELECT  
          @TypeCode = TypeCode  
        FROM [HDN_Providence].[dbo].TransWGTBreaks  
        WHERE PartnerID = @TransPartnerID   --1514    
        AND TransTypeID = @TransTypeID   --42    
        AND @UpholsteryWeight BETWEEN StartWgt AND EndWgt  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11,  
        @Rate12 = Rate12  
      FROM [HDN_Providence].[dbo].FurnitureRates  
      WHERE FurnitureTransID = @FurnitureTransID  
      AND TransTypeID = @TransTypeID   --42    
      AND CommodityCode = @CommodityCode  
  
      -- at this time, they only have 4 weight breaks, but I will leave all     
      -- of these in, in case it changes in the future.    
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @TransCharge = @Rate10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @TransCharge = @Rate11  
      END  
      IF @TypeCode = 12  
      BEGIN  
        SET @TransCharge = @Rate12  
      END  
  
      -- for this tariff, the rates are defined per hundred weight    
      SET @TransCharge = @TransCharge * @UpholsteryWeight / 100  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- 3/4/04: round trans charges to nearest dollar    
      SET @TransCharge = ROUND(@TransCharge, 0)  
  
    END  
    -- end Worldwide Logistics tariff    
    ------------------------------------------------------------------------    
  
    ------------------------------------------------------------------------    
    -- begin STI tariffs    
  
    -- 8/30/2004: don't send new NAVL Spa rate through this logic    
    -- there are a bunch of others we don't send through here either    
    IF @TransPartnerID = 208  
      AND @OneTimeRate = 0  
      AND @TransTypeID <> 8  
      AND @TransTypeID <> 16  
      AND @TransTypeID <> 17  
      AND @TransTypeID <> 19  
      AND @TransTypeID <> 25  
      AND @TransTypeID <> 26  
      AND @TransTypeID <> 27  
      AND @TransTypeID <> 22  
      AND @TransTypeID <> 36  
      AND @TransTypeID <> 38  
      AND @TransTypeID <> 41  
      AND @TransTypeID <> 43  
      AND @TransTypeID <> 70  
      AND @TransTypeID <> 252  
    BEGIN  
  
      -- 6/17/2011: added "STI 31" tariff which works just like     
      -- transtypeid 1 (STI 4) except that the rates are different    
      IF @TransTypeID = 1  
        OR @TransTypeID = 102  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
  
        -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 will calculate customer    
        -- charges based on the customer address, and the partner    
        -- charges based on the provider address    
        SELECT  
          @ConsumerStateTariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkConsumerOriginState  
        AND DestStateID = @wrkConsumerDestState  
      END  
  
      IF @TransTypeID = 4  
      BEGIN  
        SELECT  
          @Tariff = GSTariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
      END  
  
      IF @TransTypeID = 6  
      BEGIN  
        SELECT  
          @Tariff = ZoneTariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
      END  
  
      IF @TransTypeID = 10  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
      END  
  
      IF @TransTypeID = 11  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
  
        -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 will calculate customer    
        -- charges based on the customer address, and the partner    
        -- charges based on the provider address    
        SELECT  
          @ConsumerStateTariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkConsumerOriginState  
        AND DestStateID = @wrkConsumerDestState  
      END  
  
      IF @TransTypeID = 12  
      BEGIN  
        SELECT  
          @Tariff = YellowTariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
      END  
  
      IF @TransTypeID = 14  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
      END  
  
      IF @TransTypeID = 32  
      BEGIN  
        SELECT  
          @Tariff = Sec24Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
      END  
  
      IF @TransTypeID = 18  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
  
        -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 will calculate customer    
        -- charges based on the customer address, and the partner    
        -- charges based on the provider address    
        SELECT  
          @ConsumerStateTariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkConsumerOriginState  
        AND DestStateID = @wrkConsumerDestState  
      END  
  
      IF @TransTypeID = 20  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
  
        -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 will calculate customer    
        -- charges based on the customer address, and the partner    
        -- charges based on the provider address    
        SELECT  
          @ConsumerStateTariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkConsumerOriginState  
        AND DestStateID = @wrkConsumerDestState  
      END  
  
      IF @TransTypeID = 21  
      BEGIN  
        SET @Tariff = 'A'  
        SET @ConsumerStateTariff = 'A'  
      END  
  
      -- 10/29/2007: new transtype    
      IF @TransTypeID = 23  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
  
        -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 will calculate customer    
        -- charges based on the customer address, and the partner    
        -- charges based on the provider address    
        SELECT  
          @ConsumerStateTariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkConsumerOriginState  
        AND DestStateID = @wrkConsumerDestState  
      END  
  
      -- 3/24/2017: New tariff for Exact Direct    
      IF @TransTypeID = 366  
      BEGIN  
        SELECT  
          @Tariff = ExactDirectTariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
        SET @ConsumerStateTariff = @Tariff  
  
  
      END  
      -- put the other exceptions here for Michigan    
      IF @wrkDestState = 'MI'  
        AND @wrkDestZip3 IN ('480', '481', '482', '483', '485', '489', '495', '492')  
      BEGIN  
        SET @Tariff = 'E'  
      END  
  
      IF @wrkDestState = 'MI'  
        AND @wrkDestZip3 IN ('496', '494', '493', '491', '493', '490', '488', '486', '487', '484')  
      BEGIN  
        SET @Tariff = 'G'  
      END  
      IF @wrkDestState = 'MI'  
        AND @wrkDestZip3 IN ('497', '498', '499')  
      BEGIN  
        SET @Tariff = 'H'  
        
   
      END  
  
      -- put the other exceptions here for Colorado    
      IF @wrkDestState = 'CO'  
        AND @wrkDestZip3 IN ('800', '801', '802', '803', '805', '806')  
      BEGIN  
        SET @Tariff = 'N'  
      END  
      IF @wrkDestState = 'CO'  
        AND @wrkDestZip3 IN ('804', '807', '808', '809', '810', '811', '812', '813', '814', '815', '816')  
      BEGIN  
        SET @Tariff = 'Q'  
      END  
      -- put the other exceptions here for New York    
      IF @wrkDestState = 'NY'  
        AND @wrkDestZip3 IN ('100', '101', '102', '103', '104', '105', '106', '107', '108', '109', '110',  
        '111', '112', '113', '114', '115', '116', '117', '118', '119')  
      BEGIN  
        SET @Tariff = 'H'  
      END  
  
      IF @wrkDestState = 'NY'  
        AND @wrkDestZip3 IN ('120', '121', '122', '123', '124', '125', '126', '127', '128', '129')  
      BEGIN  
        SET @Tariff = 'I'  
      END  
  
      IF @wrkDestState = 'NY'  
        AND @wrkDestZip3 IN ('130', '131', '132', '133', '134', '135', '136', '137', '138', '139', '140',  
        '141', '142', '143', '144', '145', '146', '147', '148', '149')  
      BEGIN  
        SET @Tariff = 'J'  
      END  
  
      -- put the other exceptions here for Pennsylvania    
      IF @wrkDestState = 'PA'  
        AND @wrkDestZip3 IN ('150', '151', '152', '153', '154', '155', '156', '157', '158', '159', '160',  
        '161', '162', '163', '164', '165', '166', '167', '168', '169')  
      BEGIN  
        SET @Tariff = 'F'  
      END  
  
      IF @wrkDestState = 'PA'  
        AND @wrkDestZip3 IN ('170', '171', '172', '173', '174', '175', '176', '177', '178', '179', '180',  
        '181', '182', '183', '184', '185', '186', '187', '188', '189', '190', '191', '192', '193', '194', '195', '196')  
      BEGIN  
        SET @Tariff = 'H'  
         
      END  
      -- 3/30/2017 Need to get weight rates for Exact Direct: Harry Mims  
      if @TransTypeID = 366  
SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
     WHERE PartnerID = @TransPartnerID  
       
      And TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      -- only 3 weight ranges for this customer      
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
  
  
      -- 6/18/2012: new transtype 136 for STI 36A - Wine Cellar    
      -- 6/18/2012: new transtype 137 for STI 36B - Wine Cellar    
      IF @TransTypeID = 136  
        OR @TransTypeID = 137  
      BEGIN  
        SET @Tariff = @wrkDestState  
        SET @ConsumerStateTariff = @wrkConsumerDestState  
        -- Tariff 36A is for up to 1099 pounds    
        -- Tariff 36B is for 1100 pounds and up    
        -- do an edit here to ensure they don't mess up    
        IF @TotalWeight < 1100  
        BEGIN  
          SET @TransTypeID = 136  
        END  
        IF @TotalWeight > 1099  
        BEGIN  
          SET @TransTypeID = 137  
        END  
      END  
  
      -- 12/19/2013: added transtype 155 (STI - M.T.I.)    
      -- 3/13/2015: aadded #249 for Design Furnishings    
      IF @TransTypeID = 24  
        OR @TransTypeID = 28  
        OR @TransTypeID = 29  
        OR @TransTypeID = 30  
        OR @TransTypeID = 31  
        OR @TransTypeID = 33  
        OR @TransTypeID = 34  
        OR @TransTypeID = 37  
        OR @TransTypeID = 44  
        OR @TransTypeID = 54  
        OR @TransTypeID = 55  
        OR @TransTypeID = 65  
        OR @TransTypeID = 117  
        OR @TransTypeID = 118  
        OR @TransTypeID = 119  
        OR @TransTypeID = 125  
        OR @TransTypeID = 128  
        OR @TransTypeID = 155  
        OR @TransTypeID = 249  
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkOriginState  
        AND DestStateID = @wrkDestState  
  
        -- 4/2/2007: transtypes 1 and 11 and 18 and 20 and 23 and 24 will calculate customer    
        -- charges based on the customer address, and the partner    
        -- charges based on the provider address    
        SELECT  
          @ConsumerStateTariff = Tariff  
        FROM [HDN_Providence].[dbo].NAVLTrans  
        WHERE OriginStateID = @wrkConsumerOriginState  
        AND DestStateID = @wrkConsumerDestState  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      -- 4/2/2007: needed for trans types 1 and 11 and 18 and 20 and 23 and 24    
      SELECT  
        @CSMinimumCharge = MinimumCharge,  
        @CSRate1 = Rate1,  
        @CSRate2 = Rate2,  
        @CSRate3 = Rate3,  
        @CSRate4 = Rate4,  
        @CSRate5 = Rate5,  
        @CSRate6 = Rate6,  
        @CSRate7 = Rate7,  
        @CSRate8 = Rate8,  
        @CSRate9 = Rate9,  
        @CSRate10 = Rate10,  
        @CSRate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @ConsumerStateTariff  
      AND TransTypeID = @TransTypeID  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
        SET @CSCustomerTransCharge = @CSRate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
        SET @CSCustomerTransCharge = @CSRate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
        SET @CSCustomerTransCharge = @CSRate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
        SET @CSCustomerTransCharge = @CSRate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
        SET @CSCustomerTransCharge = @CSRate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
        SET @CSCustomerTransCharge = @CSRate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
        SET @CSCustomerTransCharge = @CSRate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
        SET @CSCustomerTransCharge = @CSRate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
        SET @CSCustomerTransCharge = @CSRate9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @TransCharge = @Rate10  
        SET @CSCustomerTransCharge = @CSRate10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @TransCharge = @Rate11  
        SET @CSCustomerTransCharge = @CSRate11  
      END  
  
      -- 6/18/2012: Trans Type 137 is also per hundred weight    
      IF @TransTypeID = 12  
        OR @TransTypeID = 137  
        OR @TransTypeID = 366  
      BEGIN  
        SET @TransCharge = @TransCharge * @TotalWeight / 100  
        IF @TransCharge < @MinimumCharge  
        BEGIN  
          SET @TransCharge = @MinimumCharge  
        
        END  
      END  
  
      IF @TransTypeID = 1  
        OR @TransTypeID = 11  
        OR @TransTypeID = 18  
        OR @TransTypeID = 20  
        OR @TransTypeID = 23  
        OR @TransTypeID = 24  
        OR @TransTypeID = 28  
        OR @TransTypeID = 29  
        OR @TransTypeID = 30  
       OR @TransTypeID = 31  
        OR @TransTypeID = 32  
        OR @TransTypeID = 33  
        OR @TransTypeID = 34  
        OR @TransTypeID = 37  
        OR @TransTypeID = 44  
        OR @TransTypeID = 54  
        OR @TransTypeID = 55  
        OR @TransTypeID = 65  
        OR @TransTypeID = 102  
      BEGIN  
        SET @TransCharge = @CSCustomerTransCharge  
      END  
  
      -- discount does not apply to one time cost    
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
    END  
  
  
    -- 8/30/2004: add new NAVL Spa trans rate    
    IF @TransTypeID = 8  
    BEGIN  
      SET @TransCharge = ROUND(275, 0)  
      -- discount does not apply to one time cost    
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
    END  
  
  
    -- 4/18/2006: added NEW NEW NAVL Spa trans rate    
    IF @TransTypeID = 16  
    BEGIN  
      SELECT  
        @Tariff = Tariff  
      FROM [HDN_Providence].[dbo].NAVLTrans  
      WHERE OriginStateID = @wrkOriginState  
      AND DestStateID = @wrkDestState  
  
      SELECT  
        @TransCharge = Rate1  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
    END  
  
  
    -- 3/9/2007: added NEW Spa Tariff 7b trans rate    
    IF @TransTypeID = 17  
    BEGIN  
      SELECT  
        @Tariff = SpaTariff7b  
      FROM [HDN_Providence].[dbo].NAVLTrans  
      WHERE OriginStateID = @wrkOriginState  
      AND DestStateID = @wrkDestState  
  
      SELECT  
        @TransCharge = Rate1  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
    END  
  
  
    -- 7/30/2007: added Tariff 11 trans rate    
    IF @TransTypeID = 19  
    BEGIN  
  
      SET @TransCharge = 0  
      IF @TotalWeight < 101  
      BEGIN  
        SET @TransCharge = ROUND(86, 0)  
      END  
      IF @TotalWeight > 100  
        AND @TotalWeight < 451  
      BEGIN  
        SET @TransCharge = ROUND(136, 0)  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
    END  
  
  
    -- 11/4/2007: added Tariff Section 15 trans rate    
    IF @TransTypeID = 25  
    BEGIN  
  
      SET @TransCharge = 0  
  
      IF @wrkDestState = 'IL'  
      BEGIN  
        IF @TotalWeight < 1900  
        BEGIN  
          SET @TransCharge = ROUND(47.50, 2)  
        END  
        IF @TotalWeight > 1899  
          AND @TotalWeight < 3001  
        BEGIN  
          SET @TransCharge = ROUND(107, 2)  
        END  
      END  
  
      IF @wrkDestState = 'WI'  
      BEGIN  
        IF @TotalWeight < 1900  
        BEGIN  
          SET @TransCharge = ROUND(52, 2)  
        END  
        IF @TotalWeight > 1899  
          AND @TotalWeight < 3001  
        BEGIN  
          SET @TransCharge = ROUND(120, 2)  
        END  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
    END  
  
  
    -- 1/18/2008: added Tariff Section 18 trans rate    
    IF @TransTypeID = 26  
    BEGIN  
      SET @TransCharge = 0  
    END  
  
  
    -- 1/18/2008: added Tariff Section 18A trans rate    
    IF @TransTypeID = 27  
    BEGIN  
      SET @TransCharge = 0  
    END  
  
  
    -- 1/18/2008: added Tariff Section 18A trans rate    
    -- 11/25/2008: added Tariff for Canon (38)    
    -- 1/28/2009: new Sears/Kmart trans rate    
    IF @TransTypeID = 36  
      OR @TransTypeID = 38  
      OR @TransTypeID = 41  
    BEGIN  
      SET @TransCharge = ROUND(1, 2)  
    --set @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)    
    END  
  
  
    -- 1/28/2009: new Sears/Kmart trans rate    
    -- 6/29/2009: set to 0/110 for now.  this is the 0-499 pounds rete    
    IF @TransTypeID = 43  
    BEGIN  
      SET @TransCharge = ROUND(0, 2)  
    --set @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)    
    END  
  
  
    -- 1/21/2008: NOTE - this used to be a "TM" tariff, but they converted it to STI because    
    -- the customer did not want to ship with "brand X".    
    IF @TransPartnerID = 208  
      AND @TransTypeID = 22  
    BEGIN  
      SELECT  
        @TMOriginTransZone = TMTransZone  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
  
      SELECT  
        @TMDestTransZone = TMTransZone  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      SELECT  
        @Tariff = Tariff  
      FROM [HDN_Providence].[dbo].TMTrans  
      WHERE OriginZone = @TMOriginTransZone  
      AND DestZone = @TMDestTransZone  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
  
      -- for this tariff, the rates are defined per hundred weight    
      SET @TransCharge = @TransCharge * @TotalWeight / 100  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
  
    END  
  
  
    -- 6/10/2014: New tariff for Roche Bobois    
    -- it is Zonal with new zones not used before    
    IF @TransPartnerID = 208  
      AND @TransTypeID = 219  
    BEGIN  
      -- use the TM variables so we don't need to define others    
      SELECT  
        @TMOriginTransZone = STITransZone  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
  
      SELECT  
        @TMDestTransZone = STITransZone  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      -- use the new Tariff3 field in TMTrans    
      SELECT  
        @Tariff = Tariff3  
      FROM [HDN_Providence].[dbo].TMTrans  
      WHERE OriginZone = @TMOriginTransZone  
      AND DestZone = @TMDestTransZone  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
  
      -- for this tariff, the rates are defined per hundred weight    
      SET @TransCharge = @TransCharge * @TotalWeight / 100  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
  
    END  
  
    -- 3/26/2015: New tariff for Transit Systems    
    -- it is Zonal with new zones not used before    
    IF @TransPartnerID = 208  
      AND @TransTypeID = 252  
    BEGIN  
      -- use the TM variables so we don't need to define others    
      SELECT  
        @TMOriginTransZone = STITransZone10  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
  
      SELECT  
        @TMDestTransZone = STITransZone10  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      -- use the Tariff field in the new STITrans table    
      SELECT  
        @Tariff = Tariff  
      FROM [HDN_Providence].[dbo].STITrans  
      WHERE OriginZone = @TMOriginTransZone  
      AND DestZone = @TMDestTransZone  
  
      -- 3/26/2015: currently they have 3 weight ranges    
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
  
      -- for this tariff, the rates are defined per hundred weight    
      SET @TransCharge = @TransCharge * @TotalWeight / 100  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
  
    END  
  
  
    -- transtype 70 = Million Dollar Rustic    
    IF @TransTypeID = 70  
    BEGIN  
      SELECT  
        @Tariff = Sec30Tariff  
      FROM [HDN_Providence].[dbo].NAVLTrans  
      WHERE OriginStateID = @wrkOriginState  
      AND DestStateID = @wrkDestState  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      -- for this tariff, the rates are defined per hundred weight    
      SET @wrkTransCharge1 = @Rate1 * @TotalWeight / 100  
      SET @wrkTransCharge2 = @Rate2 * @TotalWeight / 100  
  
      SET @TransCharge = @wrkTransCharge1  
  
      IF @wrkTransCharge2 < @TransCharge  
      BEGIN  
        SET @TransCharge = @wrkTransCharge2  
      END  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
    END  
  
  
    IF @TransPartnerID = 208  
    BEGIN  
      -- 2/12/2007: new way to calculate markup    
      -- set @TransCharge = (1+@TransMarkup)*@TransCharge    
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
      -- 3/4/04: round trans charges to nearest dollar    
      SET @TransCharge = ROUND(@TransCharge, 0)  
    END  
  
  
    ------------------------------------------------------------------------    
    -- TM tariffs    
    ------------------------------------------------------------------------    
  
    -- 2/26/2010: new "TM" tariff for Spring Air    
    IF @TransPartnerID IN (214, 2487)  AND @TransTypeID = 53  
    BEGIN  
      SET @TransCharge = ROUND(85, 2)  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest dollar   
      SET @TransCharge = ROUND(@TransCharge, 0)  
    END  
  
    -- 2/14/2013: new "TM" tariff for Sears White Space    
    IF @TransPartnerID IN (214, 2487)  AND @TransTypeID = 147  
    BEGIN  
      SET @TransCharge = ROUND(0, 2)  
    END  
  
    -- 4/2/2014: new "TM" tariff forSleepers in Seattle (acct #2506)    
    IF @TransPartnerID IN (214, 2487)  AND @TransTypeID = 162  
    BEGIN  
      -- for this customer the TM rate varies by service level    
      IF @ServiceLevel = 1  
      BEGIN  
        SET @TransCharge = ROUND(197.44, 2)  
      END  
      IF @ServiceLevel = 2  
      BEGIN  
        SET @TransCharge = ROUND(190.94, 2)  
      END  
      IF @ServiceLevel = 3  
      BEGIN  
        SET @TransCharge = ROUND(164.94, 2)  
      END  
  
      -- and there is a standard charge for > 350 pounds    
      IF @TotalWeight > 350  
      BEGIN  
        SET @TransCharge = @TransCharge + ((@TotalWeight - 350) * .29)  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest dollar    
      SET @TransCharge = ROUND(@TransCharge, 2)  
    END    
  
    -- 120/14/2009: new "TM" tariff with dummy rate    
    IF @TransPartnerID IN (214, 2487) AND @TransTypeID = 52  
    BEGIN  
      SET @TransCharge = ROUND(1, 2)  
    END  
  
    -- 12/12/2013: new "TM" tariff for Allied Aeroform    
    IF @TransPartnerID IN (214, 2487) AND @TransTypeID = 154  
    BEGIN  
      SET @TransCharge = ROUND(122, 2)  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      SET @TransCharge = ROUND(@TransCharge, 2)  
    END  
  
    -- 2/07/2012: new "TM" tariff for Boyd Mattresses    
    IF @TransPartnerID IN (214, 2487) AND @TransTypeID = 113  
    BEGIN  
      SET @TransCharge = ROUND(145, 2)  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      SET @TransCharge = ROUND(@TransCharge, 2)  
    END  
  
  
    -- 8/18/2011: new "TM" tariff for IRS Auctions    
    IF @TransPartnerID IN (214, 2487) AND @TransTypeID = 104  
    BEGIN  
      SELECT  
        @Tariff = TMRateBasis2  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      SELECT  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      -- only 4 weight ranges for this customer    
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- 3/4/04: round trans charges to nearest dollar    
      SET @TransCharge = ROUND(@TransCharge, 0)  
  
    END  
    
    -- 10/19/2011: new "TM" tariff for TV Lift Cabinet    
    -- 2/1/2012: new TM tariff for Topgrill Patio Furniture    
    -- 4/18/2012: new tariffs for FDSI    
    IF @TransPartnerID IN (214, 2487) AND (@TransTypeID = 107 OR @TransTypeID = 108 OR @TransTypeID = 112 OR @TransTypeID = 131 OR @TransTypeID = 132)  
    BEGIN  
      SELECT  
        @Tariff = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      IF @TransTypeID = 131  
        OR @TransTypeID = 132  
      BEGIN  
        SELECT  
          @Tariff = TMRateBasis3  
        FROM [HDN_Providence].[dbo].State  
        WHERE StateID = @wrkDestState  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      -- only 3 weight ranges for this customer    
      -- 2/1/2012: there are 5 weight breaks for transtypeid 112 & 131 & 132    
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
  
      -- the rates for this customer are per hunderd weight    
      SET @TransCharge = @TransCharge * @TotalWeight / 100  
  
      -- and are subject to a minimum    
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      IF @TransTypeID = 107  
        OR @TransTypeID = 108  
      BEGIN  
        -- 3/4/04: round trans charges to nearest dollar    
        SET @TransCharge = ROUND(@TransCharge, 0)  
      END  
  
    END  
  
    -- 10/3/2008: new "TM" tariff, same as Section 17 (transtype 22)    
    -- 7/23/2013: new "TM" tariff for DCG Stores (transtype 150)    
    -- 12/19/2013: new "TM" tariff for M.T.I. (156) is similar    
    -- 3/3/2014: new "TM" tariff for Liberty Safe (157) is similar    
    -- 11/6/2014: new TM tariff for Grow Healthy Vending (247) is similar    
    IF @TransPartnerID IN (214, 2487) AND(@TransTypeID = 35 OR @TransTypeID = 150 OR @TransTypeID = 156 OR @TransTypeID = 157 OR @TransTypeID = 247)  
    BEGIN  
      SELECT  
        @TMOriginTransZone = TMTransZone  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
      
       PRINT ISNULL('-- SELECT @TMOriginTransZone = TMTransZone FROM [HDN_Providence].[dbo].State WHERE StateID = @wrkOriginState--','') + ' @wrkOriginState :' + ISNULL(CONVERT(VARCHAR(5),@wrkOriginState),'')+ ' @TMOriginTransZone :' + ISNULL(CONVERT(VARCHAR(5),@TMOriginTransZone),'')
  
      SELECT  
        @TMDestTransZone = TMTransZone  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
      
      PRINT ISNULL('-- SELECT @TMDestTransZone = TMTransZone FROM [HDN_Providence].[dbo].State WHERE StateID = @wrkDestState --','') + ' @wrkDestState :' + ISNULL(CONVERT(VARCHAR(5),@wrkDestState),'')+ ' @TMDestTransZone :' + ISNULL(CONVERT(VARCHAR(5),@TMDestTransZone),'')
  
      IF @TransTypeID = 35  
        OR @TransTypeID = 150  
        OR @TransTypeID = 157  
        OR @TransTypeID = 247  
        
      BEGIN  
        SELECT  
          @Tariff = Tariff  
        FROM [HDN_Providence].[dbo].TMTrans  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
        
         PRINT ISNULL('-- SELECT @Tariff = Tariff FROM [HDN_Providence].[dbo].TMTrans WHERE OriginZone = @TMOriginTransZone AND DestZone = @TMDestTransZone -- ','') + ' @TMOriginTransZone :' + ISNULL(CONVERT(VARCHAR(5),@TMOriginTransZone),'')+ ' @TMDestTransZone :' + ISNULL(CONVERT(VARCHAR(5),@TMDestTransZone),'')+ ' @Tariff :' + ISNULL(CONVERT(VARCHAR(5),@Tariff),'')
      
      END  
  
      -- there is one difference between tariff and tariff2    
      IF @TransTypeID = 156  
      BEGIN  
        SELECT  
          @Tariff = Tariff2  
        FROM [HDN_Providence].[dbo].TMTrans  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
      
            PRINT ISNULL('-- SELECT * FROM [HDN_Providence].[dbo].NAVLRates WHERE RateCode = @Tariff AND TransTypeID = @TransTypeID ---  ','') + ' @Tariff :' + ISNULL(CONVERT(VARCHAR(5),@Tariff),'')+ ' @TransTypeID :' + ISNULL(CONVERT(VARCHAR(5),@TransTypeID),'')
        
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
      
       PRINT ISNULL('--SELECT @TypeCode = TypeCode FROM [HDN_Providence].[dbo].TransWGTBreaks WHERE PartnerID = @TransPartnerID AND TransTypeID = @TransTypeID AND @TotalWeight BETWEEN StartWgt AND EndWgt -- ','') + ' @TransPartnerID :' + ISNULL(CONVERT(VARCHAR(5),@TransPartnerID),'')+ ' @TransTypeID :' + ISNULL(CONVERT(VARCHAR(5),@TransTypeID),'')+ ' @TotalWeight :' + ISNULL(CONVERT(VARCHAR(5),@TotalWeight),'') + ' @TypeCode :' + ISNULL(CONVERT(VARCHAR(5),@TypeCode),'')
      
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
  
      IF @TransTypeID = 35  
        OR @TransTypeID = 150  
      BEGIN  
        -- for these tariffs, the rates are defined per hundred weight 
        
           PRINT ISNULL(CONVERT(VARCHAR(250),'--IF @TransTypeID = 35 --@TransCharge  = @TransCharge * @TotalWeight / 100 -- '),'')+ ' @TypeCode :' + ISNULL(CONVERT(VARCHAR(5),@TypeCode),'')  + ' @TransCharge :' + ISNULL(CONVERT(VARCHAR(5),@TransCharge),'')+ ' @TotalWeight :' + ISNULL(CONVERT(VARCHAR(5),@TotalWeight),'')
           
              
        SET @TransCharge = @TransCharge * @TotalWeight / 100  
            
         
      END  
  
      IF @TransTypeID = 156  
        OR @TransTypeID = 157  
        OR @TransTypeID = 247  
      BEGIN  
        -- for this tariff, the rates are defined per pound    
        SET @TransCharge = @TransCharge * @TotalWeight  
      END  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- 3/4/04: round trans charges to nearest dollar    
      SET @TransCharge = ROUND(@TransCharge, 0)  
    END  
  
  
    -- 6/29/2010: new TM Tariff for Healthcare Intl    
    -- 1/12/2011: new TM Tariff (76) for Boyd Specialty Sleep    
    -- 1/25/2011: new TM Tariff (77) for Amini Furniture and Goedeker Superstore    
    -- 9/12/2012: new TM Tariff (143) for Wynn International    
    -- 11/12/2012: new TM Tariff (146) for Fusion Specialties    
    IF @TransPartnerID IN (214, 2487)  
      AND (@TransTypeID = 66  
      OR @TransTypeID = 69  
      OR @TransTypeID = 76  
      OR @TransTypeID = 77  
      OR @TransTypeID = 143  
      OR @TransTypeID = 146)  
    BEGIN  
      -- Note: everything comes from one origin    
      SELECT  
        @Tariff = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      IF @TransTypeID = 146  
      BEGIN  
        SELECT  
          @Tariff = TMRateBasis3  
        FROM [HDN_Providence].[dbo].State  
        WHERE StateID = @wrkDestState  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @TransCharge = @Rate10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @TransCharge = @Rate11  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest cent    
      SET @TransCharge = ROUND(@TransCharge, 2)  
  
      IF @TransTypeID = 76  
      BEGIN  
        -- round trans charges to nearest dollar for this customer    
        SET @TransCharge = ROUND(@TransCharge, 0)  
      END  
  
    END  
  
  
    -- 3/13/2015: new transtye 250 for TM - Design Furnishings    
    IF @TransPartnerID IN (214, 2487)  
      AND (@TransTypeID = 56  
      OR @TransTypeID = 57  
      OR @TransTypeID = 60  
      OR @TransTypeID = 62  
      OR @TransTypeID = 63  
      OR @TransTypeID = 64  
      OR @TransTypeID = 67  
      OR @TransTypeID = 71  
      OR @TransTypeID = 74  
      OR @TransTypeID = 75  
      OR @TransTypeID = 97  
      OR @TransTypeID = 98  
      OR @TransTypeID = 100  
      OR @TransTypeID = 101  
      OR @TransTypeID = 103  
      OR @TransTypeID = 106  
      OR @TransTypeID = 109  
      OR @TransTypeID = 110  
      OR @TransTypeID = 129  
      OR @TransTypeID = 133  
      OR @TransTypeID = 134  
      OR @TransTypeID = 135  
      OR @TransTypeID = 148  
      OR @TransTypeID = 149  
      OR @TransTypeID = 250)  
    BEGIN  
      SELECT  
        @TMOriginTransZone = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
  
      SELECT  
        @TMDestTransZone = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      -- 3/28/2012: new transtype 129 for TM - Poliform    
      -- 6/18/2012: new transtype 133 for TM - Poliform FL    
      -- 6/18/2012: new transtype 134 for TM - Poliform IL    
      -- 6/18/2012: new transtype 135 for TM - Poliform CA    
      IF @TransTypeID = 129  
        OR @TransTypeID = 133  
        OR @TransTypeID = 134  
        OR @TransTypeID = 135  
      BEGIN  
        SET @Tariff = @wrkDestState  
        -- do a little edit here to see if they chose the correct trans type    
        IF @wrkOriginState = 'NJ'  
        BEGIN  
          SET @TransTypeID = 129  
        END  
        IF @wrkOriginState = 'FL'  
        BEGIN  
          SET @TransTypeID = 133  
        END  
        IF @wrkOriginState = 'IL'  
        BEGIN  
          SET @TransTypeID = 134  
        END  
        IF @wrkOriginState = 'CA'  
        BEGIN  
          SET @TransTypeID = 135  
        END  
      END  
  
      -- 3/13/2015: new transtpye for Design Furnishings    
      IF @TransTypeID = 250  
      BEGIN  
        SET @Tariff = @wrkDestState  
      END  
  
      IF @TransTypeID = 56  
        OR @TransTypeID = 57  
      BEGIN  
        -- use Tariff2 for these customers    
        SELECT  
          @Tariff = Tariff2  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
      IF @TransTypeID = 60  
        OR @TransTypeID = 62  
        OR @TransTypeID = 64  
        OR @TransTypeID = 67  
        OR @TransTypeID = 100  
        OR @TransTypeID = 103  
      BEGIN  
        -- use Tariff1 for Lifestyles and Fort Knox and Furniture From Home    
        -- also for Restonic and Action Lock and Safe & ASAP Safes    
        SELECT  
          @Tariff = Tariff1  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
      IF @TransTypeID = 63  
      BEGIN  
        -- use Tariff4 for Lynden Coinstar    
        SELECT  
          @Tariff = Tariff4  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
      IF @TransTypeID = 71  
        OR @TransTypeID = 74  
        OR @TransTypeID = 75  
        OR @TransTypeID = 97  
      BEGIN  
        -- use Tariff5 for Custom Companies, Fitness Blowout, Transgroup Worldwide Logistics, and Cozy Pure    
        SELECT  
          @Tariff = Tariff5  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
      IF @TransTypeID = 98  
        OR @TransTypeID = 101  
      BEGIN  
        -- use Tariff6 for DA Stores    
        -- use Tariff6 for 1 Stop Camera and Electronics    
        SELECT  
          @Tariff = Tariff6  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
  
      IF @TransTypeID = 106  
        OR @TransTypeID = 110  
      BEGIN  
        -- use TMRateBasis3 for this customer    
        SELECT  
          @TMOriginTransZone = TMRateBasis3  
        FROM [HDN_Providence].[dbo].State  
    WHERE StateID = @wrkOriginState  
  
        SELECT  
          @TMDestTransZone = TMRateBasis3  
        FROM [HDN_Providence].[dbo].State  
        WHERE StateID = @wrkDestState  
  
        -- use Tariff7 for this customer    
        SELECT  
          @Tariff = Tariff7  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
  
      -- 2/25/2013: new transtypes for Dreamworks, use Tariff8    
      IF @TransTypeID = 148  
        OR @TransTypeID = 149  
      BEGIN  
        -- do a little edit here to see if they chose the correct trans type    
        -- I don't know where all of their warehouses are, but they are in at least these 6 states    
        IF @wrkOriginState = 'CA'  
          OR @wrkOriginState = 'WA'  
        BEGIN  
          SET @TransTypeID = 149  
        END  
        IF @wrkOriginState = 'VA'  
          OR @wrkOriginState = 'NC'  
          OR @wrkOriginState = 'NJ'  
          OR @wrkOriginState = 'NY'  
        BEGIN  
          SET @TransTypeID = 148  
        END  
  
        -- use TMRateBasis3 for this customer    
        SELECT  
          @TMOriginTransZone = TMRateBasis3  
        FROM [HDN_Providence].[dbo].State  
        WHERE StateID = @wrkOriginState  
  
        --  use consumer destination state for this customer    
        SELECT  
          @TMDestTransZone = TMRateBasis3  
        FROM [HDN_Providence].[dbo].State  
        WHERE StateID = @wrkConsumerDestState  
  
        -- use Tariff8 for this customer    
        SELECT  
          @Tariff = Tariff8  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
  
      IF @TransTypeID = 109  
      BEGIN  
        -- use Tariff7 for this customer    
        --Select @Tariff = Tariff7     
        -- 8/21/2012: use tariff6 - tariff7 was an error    
        SELECT  
          @Tariff = Tariff6  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11,  
        @Rate12 = Rate12,  
        @Rate13 = Rate13  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
   IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @TransCharge = @Rate10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @TransCharge = @Rate11  
      END  
      IF @TypeCode = 12  
      BEGIN  
        SET @TransCharge = @Rate12  
      END  
      IF @TypeCode = 13  
      BEGIN  
        SET @TransCharge = @Rate13  
      END  
  
      IF @TransTypeID = 56  
        OR @TransTypeID = 57  
        OR @TransTypeID = 60  
        OR @TransTypeID = 62  
      BEGIN  
        -- for these tariffs, the rates are defined per hundred weight    
        -- but Lynden Coinstar (63) and Furniture Fro Home (64) and Restonic (67)    
        -- and the others are the actual dollar amount    
        SET @TransCharge = @TransCharge * @TotalWeight / 100  
      END  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest cent    
      SET @TransCharge = ROUND(@TransCharge, 2)  
  
    END  
  
  
    -- 4/19/2010: new "TM" tariff for Nebraska Furniture    
    IF @TransPartnerID IN (214, 2487)  
      AND @TransTypeID = 58  
    BEGIN  
      SELECT  
        @TMOriginTransZone = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
  
      SELECT  
        @TMDestTransZone = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      IF @TransTypeID = 58  
      BEGIN  
        -- use Tariff3 for this customer    
        SELECT  
          @Tariff = Tariff3  
        FROM [HDN_Providence].[dbo].TMRateBasis  
        WHERE OriginZone = @TMOriginTransZone  
        AND DestZone = @TMDestTransZone  
      END  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
  
      -- for this tariff, the rates are defined per hundred weight    
      SET @TransCharge = @TransCharge * @TotalWeight / 100  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest cent    
      SET @TransCharge = ROUND(@TransCharge, 2)  
    END  
  
  
    -- 4/19/2010: new "TM" tariff for Woodland Creek    
    IF @TransPartnerID IN (214, 2487)  
      AND @TransTypeID = 59  
    BEGIN  
      SELECT  
        @TMOriginTransZone = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkOriginState  
  
      SELECT  
        @TMDestTransZone = TMRateBasis  
      FROM [HDN_Providence].[dbo].State  
      WHERE StateID = @wrkDestState  
  
      -- use Tariff1 for this customer    
      SELECT  
        @Tariff = Tariff1  
      FROM [HDN_Providence].[dbo].TMRateBasis  
      WHERE OriginZone = @TMOriginTransZone  
      AND DestZone = @TMDestTransZone  
  
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = @Tariff  
      AND TransTypeID = @TransTypeID  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @TransCharge = @Rate10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @TransCharge = @Rate11  
      END  
  
      IF @TotalWeight > 1000  
      BEGIN  
        -- for this tariff, the final rate (Rate11) is defined as per hundred weight    
        -- the others (Rate1 - Rate10) are actual flat rates    
        SET @TransCharge = @TransCharge * @TotalWeight / 100  
      END  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest cent    
      SET @TransCharge = ROUND(@TransCharge, 2)  
  
    END  
  
  
    -- 4/20/2010: new "TM" tariff for Treasure Rooms    
    -- 1/20/2012: new "TM" tariff for Natural Bed Store    
    IF @TransPartnerID IN (214, 2487)  
      AND (@TransTypeID = 61  
      OR @TransTypeID = 111)  
    BEGIN  
      -- for this customer, there is only 1 ratecode    
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11  
      FROM [HDN_Providence].[dbo].NAVLRates  
      WHERE RateCode = 'A'  
      AND TransTypeID = @TransTypeID  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
      END  
  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest cent    
      SET @TransCharge = ROUND(@TransCharge, 2)  
  
    END  
  
    ------------------------------------------------------------------------    
    -- tariff for CRST LTL    
    -- 6/9/2015: added tariff for Sun Welding Safes (258)    
    -- the calculation is the same, but the rates and origins are different    
  
    IF @TransTypeID IN (248, 258)  
    BEGIN  
  
      ------------------------------------------------------------------------    
      -- the provider will be the destination for the transportation activity.    
      -- we will assume that the destination zip that they enter is the    
      -- end consumer's zip code    
      -- the @PreferredProvider variable contains the partner id    
  
      SELECT  
        @DestZip = a.zip  
      FROM [HDN_Providence].[dbo].Partner p,  
           PartnerLocation pl,  
           Address a  
      WHERE p.partnerid = pl.partnerid  
      AND pl.addressid = a.addressid  
      AND pl.primarylocation = 1  
      AND p.partnerid = @PreferredProvider  
  
      -- this tariff is Zip to Zip with the rates in the CRSTRates table    
      SELECT  
        @MinimumCharge = MinimumCharge,  
        @Rate1 = Rate1,  
        @Rate2 = Rate2,  
        @Rate3 = Rate3,  
        @Rate4 = Rate4,  
        @Rate5 = Rate5,  
        @Rate6 = Rate6,  
        @Rate7 = Rate7,  
        @Rate8 = Rate8,  
        @Rate9 = Rate9,  
        @Rate10 = Rate10,  
        @Rate11 = Rate11,  
        @Rate12 = Rate12,  
        @Rate13 = Rate13  
      FROM [HDN_Providence].[dbo].CRSTRates  
      WHERE TransTypeID = @TransTypeID  
      AND OriginZip = @OriginZip  
      AND DestZip = @DestZip  
  
  
      SELECT  
        @TypeCode = TypeCode  
      FROM [HDN_Providence].[dbo].TransWGTBreaks  
      WHERE PartnerID = @TransPartnerID  
      AND TransTypeID = @TransTypeID  
      AND @TotalWeight BETWEEN StartWgt AND EndWgt  
  
      IF @TypeCode = 1  
      BEGIN  
        SET @TransCharge = @Rate1  
      END  
      IF @TypeCode = 2  
      BEGIN  
        SET @TransCharge = @Rate2  
      END  
      IF @TypeCode = 3  
      BEGIN  
        SET @TransCharge = @Rate3  
      END  
      IF @TypeCode = 4  
      BEGIN  
        SET @TransCharge = @Rate4  
      END  
      IF @TypeCode = 5  
      BEGIN  
        SET @TransCharge = @Rate5  
      END  
      IF @TypeCode = 6  
      BEGIN  
        SET @TransCharge = @Rate6  
      END  
      IF @TypeCode = 7  
      BEGIN  
        SET @TransCharge = @Rate7  
      END  
      IF @TypeCode = 8  
      BEGIN  
        SET @TransCharge = @Rate8  
      END  
      IF @TypeCode = 9  
      BEGIN  
        SET @TransCharge = @Rate9  
      END  
      IF @TypeCode = 10  
      BEGIN  
        SET @TransCharge = @Rate10  
      END  
      IF @TypeCode = 11  
      BEGIN  
        SET @TransCharge = @Rate11  
      END  
      IF @TypeCode = 12  
      BEGIN  
        SET @TransCharge = @Rate12  
      END  
      IF @TypeCode = 13  
      BEGIN  
        SET @TransCharge = @Rate13  
      END  
  
      -- this tariff is per hindred weight    
      SET @TransCharge = @TransCharge * @TotalWeight  
      IF @TransCharge < @MinimumCharge  
      BEGIN  
        SET @TransCharge = @MinimumCharge  
      END  
  
  
      SET @TransCharge = @TransCharge - (@TransCharge * @CustomerTransDiscount)  
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      -- round trans charges to nearest cent    
      SET @TransCharge = ROUND(@TransCharge, 2)  
  
    END  
  
    ------------------------------------------------------------------------    
  
    IF @TransPartnerID = 265  
    BEGIN  
      SET @TransCharge = ROUND(0, 0)  
    END  
  
  
    -- 8/10/2004: handle VDS 3rd party trans    
    IF @TransPartnerID = 317  
    BEGIN  
     SET @TransCharge = ROUND(93, 0)  
  
      -- 2/12/2007: new way to calculate markup    
      -- set @TransCharge = (1+@TransMarkup)*@TransCharge    
      SET @TransCharge = @TransCharge / (1 - @TransMarkup)  
  
      SET @TransCharge = ROUND(@TransCharge, 0)  
  
    END  
  
    -- 1/19/2009: HP is no longer a customer.    
    -- We now use partner 274 to indicate YRCW transtypes.      
    -- (generally used when mastercustomerid = 567.)    
    -- We will create the ProjectPartner record, and we will     
    -- create the ProjectActivities and ProjectRates records    
    -- with customercost and partnercost of 0.    
    -- 9/30/2004: handle Yellow (HP) 3rd party trans    
    IF @TransPartnerID = 274  
    BEGIN  
      -- 1/19/2009: set charges to 0    
      SET @TransCharge = ROUND(0, 0)  
    END  
  
    -- 9/28/2011: added Sears Logistics    
    IF @TransPartnerID = 2043  
    BEGIN  
      SET @TransCharge = ROUND(0, 0)  
    END  
  
    -- 8/21/2013: added Reddaway    
    IF @TransPartnerID = 2197  
    BEGIN  
      SET @TransCharge = ROUND(0, 0)  
    END  
  
  
  ----------------------------------------------------------------------------------------    
  END  
  /* END Transportation Charge calculation */  
  ----------------------------------------------------------------------------------------    
  
  -- 10/11/2016: we added some excess mileage permanent accessorials earlier    
  -- this year and now realize that they do not add into the shipment quote.    
  -- this will be a problem.    
  -- so, I added code here to see if they are used and if they are, and if    
  -- they apply to this quote, we will calculate that accessorial charge    
  -- and add it to the Delivery Charge.    
  
  SELECT  
    @ExcessMileage = ISNULL(ExcessMileage, 0),  
    @ExcessMileageID = ISNULL(ExcessMileageID, 0),  
    @ExcessMileageQty = ISNULL(ExcessMileageQty, 0),  
    @ExcessMileageStartMiles = ISNULL(ExcessMileageStartMiles, 0),  
    @ExcessMileageEndMiles = ISNULL(ExcessMileageEndMiles, 0),  
    @ExcessMileageID2 = ISNULL(ExcessMileageID2, 0),  
    @ExcessMileageQty2 = ISNULL(ExcessMileageQty2, 0),  
    @ExcessMileageStartMiles2 = ISNULL(ExcessMileageStartMiles2, 0),  
    @ExcessMileageEndMiles2 = ISNULL(ExcessMileageEndMiles2, 0),  
    @ExcessMileageID3 = ISNULL(ExcessMileageID3, 0),  
    @ExcessMileageQty3 = ISNULL(ExcessMileageQty3, 0),  
    @ExcessMileageStartMiles3 = ISNULL(ExcessMileageStartMiles3, 0),  
    @ExcessMileageEndMiles3 = ISNULL(ExcessMileageEndMiles3, 0)  
  -- 8/10/2012: change to get the template from the project      
  FROM [HDN_Providence].[dbo].Customer  
  INNER JOIN CustomerTemplate  
    ON Customer.CustomerID = CustomerTemplate.CustomerID  
    AND CustomerTemplate.Active = 1  
  WHERE Customer.CustomerID = @CustomerID  
  
  
  SET @PermAccessID = 0  
  
  /* new we need to handle the Excess Mileage Permanent Accessorials */  
  IF @ExcessMileage = 1  
    AND @ExcessMileageID > 0  
    AND @FMMiles BETWEEN @ExcessMileageStartMiles AND @ExcessMileageEndMiles  
  BEGIN  
    SET @PermAccessID = @ExcessMileageID  
    SET @PermAccessQty = @ExcessMileageQty  
  END  
  
  IF @ExcessMileage = 1  
    AND @ExcessMileageID2 > 0  
    AND @FMMiles BETWEEN @ExcessMileageStartMiles2 AND @ExcessMileageEndMiles2  
  BEGIN  
    SET @PermAccessID = @ExcessMileageID2  
    SET @PermAccessQty = @ExcessMileageQty2  
  END  
  
  IF @ExcessMileage = 1  
    AND @ExcessMileageID3 > 0  
    AND @FMMiles BETWEEN @ExcessMileageStartMiles3 AND @ExcessMileageEndMiles3  
  BEGIN  
    SET @PermAccessID = @ExcessMileageID3  
    SET @PermAccessQty = @ExcessMileageQty3  
  END  
  
  IF @PermAccessID > 0  
  BEGIN  
    -- need some if the code from USP_CalculateAccessorialCharge here    
  
    SELECT  
      @ActivityID = ActivityID,  
      @ActivityDescription = ProjectDescription,  
      @UnitOfMeasureCD = UnitOfMeasureCD,  
      @CustomerRateUnitOfMeasure = CustomerRateUnitOfMeasure,  
      @CustomerRateValue = CustomerRateValue,  
      @CustomerMinimumUOM = CustomerMinimumUOM,  
      @CustomerMinimumRateValue = CustomerMinimumRateValue  
    FROM [HDN_Providence].[dbo].Accessories  
    WHERE AccessorialID = @PermAccessID  
  
  
    SET @CustomerCost = 0  
    SET @CustomerMinimum = 0  
    SET @Units = 1  
  
  
    -- First, Calculate the Customer Cost    
    -- Pounds or Hundred Weight    
    IF @CustomerRateUnitOfMeasure = 'LB'  
      OR @CustomerRateUnitOfMeasure = 'CWT'  
    BEGIN  
      SELECT  
        @UOMMultiplier = Multiplier  
      FROM [HDN_Providence].[dbo].UnitofMeasure  
      WHERE UnitofMeasureCD = @CustomerRateUnitOfMeasure  
      SET @CustomerCost = (@TotalWeight / @UOMMultiplier) * @CustomerRateValue  
      SET @Units = (@TotalWeight / @UOMMultiplier)  
    END  
  
   
    -- Miles    
    IF @CustomerRateUnitOfMeasure = 'MI'  
    BEGIN  
    
  
      SET @CustomerCost = @FMMiles * @CustomerRateValue  
      SET @Units = @FMMiles  
      
          PRINT CONVERT(VARCHAR(100),'--@CustomerRateUnitOfMeasure = ''MI''--') +  ' @FMMiles :' + CONVERT(VARCHAR(5),ISNULL( @FMMiles,'')) + ' @CustomerRateValue :' + CONVERT(VARCHAR(5),ISNULL( @CustomerRateValue,'')) + ' @@CustomerCost :' + CONVERT(VARCHAR(5),ISNULL( @CustomerCost,''))
    END  
  
    -- Flat Rate    
    IF @CustomerRateUnitOfMeasure = 'FR'  
    BEGIN  
      SET @CustomerCost = @CustomerRateValue  
      SET @Units = 1  
    END  
  
  
    IF @CustomerMinimumRateValue > 0  
    -- if there is a minimum defined, calculate it    
    BEGIN  
      -- Pounds or Hundred Weight    
      IF @CustomerMinimumUOM = 'LB'  
        OR @CustomerMinimumUOM = 'CWT'  
      BEGIN  
        SELECT  
          @UOMMultiplier = Multiplier  
        FROM [HDN_Providence].[dbo].UnitofMeasure  
        WHERE UnitofMeasureCD = @CustomerMinimumUOM  
        SET @CustomerMinimum = (@TotalWeight / @UOMMultiplier) * @CustomerMinimumRateValue  
      END  
  
  
      -- Miles    
      IF @CustomerMinimumUOM = 'MI'  
      BEGIN  
        SET @CustomerMinimum = @FMMiles * @CustomerMinimumRateValue  
      END  
  
      -- Flat Rate    
      IF @CustomerMinimumUOM = 'FR'  
      BEGIN  
        SET @CustomerMinimum = @CustomerMinimumRateValue  
      END  
  
      
    END  
  
    IF @CustomerCost < @CustomerMinimum  
    BEGIN  
      SET @CustomerCost = @CustomerMinimum  
      SET @CustomerRateValue = @CustomerMinimum  
      SET @Units = 1  
    END  
  
   PRINT CONVERT(VARCHAR(100),'--@DeliveryCharge = @DeliveryCharge + @CustomerCost--') +  ' @DeliveryCharge :' + CONVERT(VARCHAR(5),ISNULL( @DeliveryCharge,'')) + ' @CustomerCost :' + CONVERT(VARCHAR(5),ISNULL( @CustomerCost,''))
   
    SET @DeliveryCharge = @DeliveryCharge + @CustomerCost  
    
   
  
  END  
  
  ----------------------------------------------------------------------------------------    
  
  IF @CustomerID <> 1270  
    AND @CustomerID <> 1665  
    AND @CustomerID <> 1820  
  -- 10/8/2012: for these customers, the fuel surcharge is built into the rates,    
  -- so we will skip the fuel surcharge calculation for them    
  -- they are setup with rates only so that we can pay the providers    
  BEGIN  
  
    -- get Fuel Surcharges    
    SELECT  
      @ApplyFuelSurcharge = ISNULL(ApplyFuelSurcharge, 0),  
      @DeliveryEffDate = ISNULL(DeliverySurchargeEffDate, '1/1/1990'),  
      @TransEffDate = ISNULL(TransSurchargeEffDate, '1/1/1990'),  
      @FuelSurchargeID = FuelSurchargeID  
    FROM [HDN_Providence].[dbo].Customer  
    WHERE CustomerId = @CustomerId  
  
    SET @Surcharge = 0  
    SET @TransSurcharge = 0  
  
    IF @ApplyFuelSurcharge > 0  
    BEGIN  
      SELECT TOP 1  
        @Surcharge = ISNULL(Surcharge, 0),  
        @TransSurcharge = ISNULL(TransSurcharge, 0)  
      FROM [HDN_Providence].[dbo].FuelSurchargeRates  
      WHERE ISNULL(CustomerID, 0) = @FuelSurchargeID  
      AND EffectiveDate <= @DelDate  
      ORDER BY EffectiveDate DESC  
    END  
  
    -- if we are using the default Tran Type, that is because    
    -- this customer does not ship with us regularly.    
    -- when we have this situation, Trudy says to charge    
    -- the Trans Fuel Surcharge from STANDARD RATES (-1)    
  
    IF @UsingDefaultTransTypeID = 1  
    BEGIN  
      SELECT TOP 1  
        @TransSurcharge = ISNULL(TransSurcharge, 0)  
      FROM FuelSurchargeRates  
      WHERE ISNULL(CustomerID, 0) = -1  
      AND EffectiveDate <= @DelDate  
      ORDER BY EffectiveDate DESC  
    END  
  
  
    SELECT  
      @Surcharge = ISNULL(@Surcharge, 0)  
    SELECT  
      @TransSurcharge = ISNULL(@TransSurcharge, 0)  
  
    IF @Surcharge > 0  
      AND @DeliveryEffDate <= @DelDate  
    BEGIN  
      IF @ApplyFuelSurcharge = 1  
        OR @ApplyFuelSurcharge = 3 -- Delivery or Both    
      BEGIN  
        -- Delivery Charge 
        
          PRINT CONVERT(VARCHAR(100),'--@FuelSurcharge = (@DeliveryCharge + @RemoteRev) * @Surcharge--') +  ' @DeliveryCharge :' + CONVERT(VARCHAR(5),ISNULL( @DeliveryCharge,'')) + ' @RemoteRev :' + CONVERT(VARCHAR(5),ISNULL( @RemoteRev,''))  + ' @Surcharge :' + CONVERT(VARCHAR(5),ISNULL( @Surcharge,''))
           
        SET @FuelSurcharge = (@DeliveryCharge + @RemoteRev) * @Surcharge  
        
        
      END  
    END  
  
    IF @TransSurcharge > 0  
      AND @TransEffDate <= @DelDate  
    BEGIN  
      IF @ApplyFuelSurcharge = 2  
        OR @ApplyFuelSurcharge = 3  -- Transportation or Both    
      BEGIN  
        SET @TransFuelSurcharge = @TransCharge * @TransSurcharge  
      END  
    END  
  
    -- when we are using the default Trans Type, they won't have    
    -- the applyfuelsurcharge set for Trans, so we need to force it.    
    IF @UsingDefaultTransTypeID = 1  
    BEGIN  
      SET @TransFuelSurcharge = @TransCharge * @TransSurcharge  
    END  
  
  END  
  
   --SELECT 
   --@QuoteID = @@IDENTITY
  
   --SELECT 
   --@QuoteID as 'QuoteID',
   --@CustomerID as 'CustomerId',
   --@OriginZip as 'OriginZip',
   --@DestZip as 'DestZip',
   --@TotalWeight as 'TotalWeight', 
   --@ServiceLevel as 'ServiceLevel',
   --@TransTypeID as 'TransTypeId',
   --@FMMiles as 'FMMiles',
   --@RemoteRev as 'RemoteRevenue',
   --@DeliveryCharge as 'DeliveryCharge',
   --@FuelSurcharge as 'FuelSurcharge',
   --@TransCharge as 'TransCharge',
   --@TransFuelSurcharge as 'TransFuelSurcharge',
   --@UserID as 'UserId',
   --GETDATE() as 'CreateDate'
  
  ----------------------------------------------------------------------------------------    
  --INSERT INTO Quote (CustomerID, OriginZip, DestZip, TotalWeight, ServiceLevelID, TransTypeID, FMMiles,  
  --RemoteRev, DeliveryCharge, FuelSurcharge, TransCharge, TransFuelSurcharge, CreatedBy, CreateDate)  
  --  VALUES (@CustomerID, @OriginZip, @DestZip, @TotalWeight, @ServiceLevel, @TransTypeID, @FMMiles, @RemoteRev, @DeliveryCharge, @FuelSurcharge, @TransCharge, @TransFuelSurcharge, @UserID, GETDATE())  
  
  
  
  UPDATE Quote set OriginZip = @OriginZip , DestZip = @DestZip , TotalWeight = @TotalWeight 
          ,DeliveryCharge = @DeliveryCharge ,FuelSurcharge = @FuelSurcharge ,RemoteRev = @RemoteRev,
          TransCharge = @TransCharge,TransFuelSurcharge = TransFuelSurcharge,CreateDate = GETDATE()
          where QuoteID= @QuoteID 

  --SELECT  
  --  @QuoteID = @@IDENTITY  
  
  --SELECT  
    --@QuoteID AS 'QuoteID',  
   -- @RemoteRev AS 'RemoteRevenue',  
   -- @DeliveryCharge AS 'DeliveryCharge',  
    --@FuelSurcharge AS 'FuelSurcharge',  
    --@TransCharge AS 'TransCharge',  
    --@TransFuelSurcharge AS 'TransFuelSurcharge'  
      
      
    SET NOCOUNT OFF   
      
  END
  
  END

GO

