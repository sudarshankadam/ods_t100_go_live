
CREATE PROCEDURE [dbo].[usp_PP_Quote_InsertProductOrderInvNew] 
@QuoteID int,  
@Accountnumber int,
@Quantity int,
@ProductTypeID int,
@TypeCode int,
@ProductNumber varchar(30),
@ProductDesc varchar(100),
@Weight int,
@Pieces int,
@Cubes float,
@Parts int,
@ModelNumber varchar(30),
@SerialNumber varchar(100),
@Meter varchar(50),
@Condition varchar(150),
@NMFC varchar(30),
@Itn varchar(40),
@ItnInDateTime datetime,
@Height float,
@Width float,
@Length float,                             
@TotalWeight int,
@InstructionPath varchar(100),
@VideoPath varchar(100),
@CreatedBy varchar(10)      

AS  
 
Declare @QuantityVal int     
SET @QuantityVal = @Quantity


  
--DELETE FROM OrderInv  
--WHERE OrderInv.AccountNumber = @Accountnumber  
--	AND OrderInv.ProjectID = @ProjectID  
  
--EXEC sp_xml_preparedocument @XMLArrayDoc output, @XMLArray  
  
     
WHILE @QuantityVal > 0        
 BEGIN  
   INSERT INTO Orderinv(QuoteID,ProductTypeID,AccountNumber,ProductNumber,
		NMFC,ModelNumber,SerialNumber,Meter,Condition,
		TypeCode,ProductDescription,Weight,Pieces,Cube,Parts,InstructionPath,Itn,ItnInDateTime,Height,Width,Length,VideoPath,CreatedBy)      
   VALUES (@QuoteID,@ProductTypeID,@AccountNumber,@ProductNumber,
		@NMFC,@ModelNumber,@SerialNumber,@Meter,@Condition,
		@TypeCode,@ProductDesc,@Weight,@Pieces,@Cubes,@Parts,@InstructionPath,@ITN,@ITNInDateTime,@Height,@Width,@Length,@VideoPath,@CreatedBy)      
	SET @QuantityVal = @QuantityVal - 1  
 END     
   
SELECT @TotalWeight = SUM(weight) FROM OrderInv WHERE QuoteID = @QuoteID  
  
UPDATE Quote   
SET ApproximateWeight = @TotalWeight   
WHERE QuoteID = @QuoteID

GO

