

------SAMPLE
/*
EXEC [dbo].[UpdateDataStoreData] @company_id = 'T100',@section = 'stops'
*/
CREATE PROCEDURE [dbo].[UpdateDataStoreData]
@company_id varchar(10) = 'T100',
@section varchar(20) = 'ALL'
AS
BEGIN

    
    --DELETE FROM ods.stops WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM ods.movement_assets WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM ods.[orderMaster] WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM [ODS].[OD_Orders] WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM [ODS].[OD_route_details] WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM ods.movements WHERE company_id IN (SELECT * FROM string_split(@company_id,','))    
    --DELETE FROM ods.agency_link WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM [ODS].[responsibility_history] WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    --DELETE FROM [ODS].hierarchy WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
    
    IF @section IN ('All','Orders')
    BEGIN
        TRUNCATE TABLE ods.[orderMaster]

        INSERT INTO ods.[orderMaster]
        (
            [order_id],
            [company_id],
            [customer_id],
            customer_name,
            [current_movement_id],
            [origin_stop_id],
            [dest_stop_id],
            [order_status],
            [order_mode],
            [order_mode_id],
            [agency_id],
            [order_source],
            [total_distance],
            [load_weight],
            [last_modified_date],
            ordered_date,
            agent_payee_id,
            order_value,
            high_value,
            operations_user,
            created_by,
            [order_type],
            order_pieces,
            total_charge,
            other_charges_total,
            equipment_type_id,
            is_agent_captive_order,
            carrierOtherPaySum,
            orders_Blnum,
            total_pay,
            temperature_max,
            temperature_min,
            freight_charge
        )
        SELECT DISTINCT
               o.id AS order_id,
               o.company_id,
               o.customer_id,
               (
                   SELECT TOP 1
                          name
                   FROM [QA-McLeod-RPT].lme_qa.dbo.customer
                   WHERE id = o.customer_id
                         AND company_id = o.company_id
               ) customer_name,
               o.curr_movement_id AS current_movement_id,
               orderMovement.origin_stop_id,
               orderMovement.dest_stop_id destination_stop_id,
               o.status AS order_status,
               CASE order_mode
                   WHEN 'T' THEN
                       'FTLF'
                   WHEN 'P' THEN
                       'FLB_LTL'
                   ELSE
                       NULL
               END order_mode,
               order_mode,
               agency_id,
               'LME' order_source,
               bill_distance total_distance,
               [weight] load_weight,
               GETDATE(),
               ordered_date,
               agent_payee_id,
               o.order_value,
               o.high_value,
               o.operations_user,
               o.entered_user_id,
               o.order_type_id,
               o.pieces,
               o.total_charge,
               o.otherchargetotal,
               (SELECT top 1 equipType.descr FROM [QA-McLeod-RPT].lme_qa.dbo.equipment_type equipType WHERE equipType.id = o.equipment_type_id and
			    equipType.company_id = o.company_id ) AS equipment_type_id,
               o.is_agent_captive_order,
               (SELECT SUM(amount) as otherPaySum
		        FROM [QA-McLeod-RPT].lme_qa.dbo.driver_extra_pay
		        WHERE driver_extra_pay.company_id = o.company_Id AND
			    driver_extra_Pay.order_id = o.id
		        group by driver_extra_Pay.order_id       
                ) as carrierOtherPaySum,
                o.blnum,
                orderMovement.override_pay_amt + (SELECT SUM(amount) as otherPaySum
		        FROM [QA-McLeod-RPT].lme_qa.dbo.driver_extra_pay
		        WHERE driver_extra_pay.company_id = o.company_Id AND
			    driver_extra_Pay.order_id = o.id
		        group by driver_extra_Pay.order_id       
                ) total_pay,
                o.temperature_max,
                o.temperature_min,
                o.freight_charge
        FROM [QA-McLeod-RPT].lme_qa.dbo.orders o
            JOIN [QA-McLeod-RPT].lme_qa.dbo.movement_order mo
                ON o.id = mo.order_id AND o.company_id = mo.company_id
            JOIN [QA-McLeod-RPT].lme_qa.dbo.movement orderMovement
                ON mo.movement_id = orderMovement.id AND orderMovement.company_id = mo.company_id
                   AND o.curr_movement_id = orderMovement.id
        WHERE o.company_id IN (SELECT * FROM string_split(@company_id,','))
        --AND orderMovement.status <> 'V'
              AND ordered_date > DATEADD(m, -2, GETDATE())
    END

    DECLARE @OrderList TABLE(order_id varchar(20))
    INSERT INTO @OrderList SELECT order_id FROM ODS.orderMaster

    IF @section IN ('All','OD')
    BEGIN
        TRUNCATE TABLE [ODS].[OD_Orders]
    
        ---_GET OD ORDERS
        INSERT INTO [ODS].[OD_Orders]
        SELECT 
        [company_id]
          ,id as [order_id]
          ,[axle_weight1]
          ,[axle_weight2]
          ,[axle_weight3]
          ,[axle_weight4]
          ,[axle_weight5]
          ,[axle_weight6]
          ,[axle_weight7]
          ,[axle_weight8]
          ,[axle_weight9]
          ,[overall_weight]
          ,[overall_width]
          ,[overhang_back]
          ,[overall_height]
          ,[overall_length]
          ,[kingpin_length]
          ,[overhang_front]
          ,[is_agent_captive_order]
          ,[tractor_axle_spacing1]
          ,[tractor_axle_spacing2]
          ,[tractor_axle_spacing3]
          ,[tractor_axle_spacing4]
          ,[trailer_axle_spacing1]
          ,[trailer_axle_spacing2]
          ,[trailer_axle_spacing3]
          ,[trailer_axle_spacing4]
          ,[trailer_axle_spacing5]
          ,[ordered_date]
          ,(
            SELECT id as [order_id]
          ,[customer_id]
          ,[route_cost1]
          ,[route_cost1_c]
          ,[route_cost1_d]
          ,[route_cost1_n]
          ,[route_cost1_r]
          ,[route_cost2]
          ,[route_cost2_c]
          ,[route_cost2_d]
          ,[route_cost2_n]
          ,[route_cost2_r]
          ,[route_cost3]
          ,[route_cost3_c]
          ,[route_cost3_d]
          ,[route_cost3_n]
          ,[route_cost3_r]
          ,[route_cost4]
          ,[route_cost4_c]
          ,[route_cost4_d]
          ,[route_cost4_n]
          ,[route_cost4_r]
          ,[route_state_locality1]
          ,[route_state_locality2]
          ,[route_state_locality3]
          ,[route_state_locality4]
          ,[route_date1]
          ,[route_date2]
          ,[route_date3]
          ,[route_date4]
          ,[route_notes1]
          ,[route_notes2]
          ,[route_notes3]
          ,[route_notes4]
          ,[route_rcvd_date1]
          ,[route_rcvd_date2]
          ,[route_rcvd_date3]
          ,[route_rcvd_date4]
          ,[route_state_locality6]
          ,[route_state_locality5]
          ,[route_date6]
          ,[route_date5]
          ,[route_notes6]
          ,[route_notes5]
          ,[route_rcvd_date6]
          ,[route_rcvd_date5]
          ,[route_cost5]
          ,[route_cost6]
      FROM [QA-McLeod-RPT].lme_qa.dbo.orders
      WHERE id = o.id AND company_id = o.company_id
      FOR JSON PATH, INCLUDE_NULL_VALUES

            )
          FROM [QA-McLeod-RPT].lme_qa.dbo.orders o
        WHERE order_type_id = 'ODPERM' AND company_id IN (SELECT * FROM string_split(@company_id,','))
        ----------

        TRUNCATE TABLE [ODS].[OD_route_details]
    
    INSERT INTO [ODS].[OD_route_details]
    SELECT 
          o.company_id
          ,o.id order_id
          ,o.customer_id
          ,s1.location_id Origin_Location_Id
          ,s1.city_name Origin_City
          ,s1.state Origin_State
          ,s1.zip_code Origin_Zip
          ,s2.location_id Dest_Location_Id
          ,s1.city_name Dest_City
          ,s1.state Dest_State
          ,s1.zip_code Dest_Zip      
          ,[route_cost1]
          ,[route_cost1_c]
          ,[route_cost1_d]
          ,[route_cost1_n]
          ,[route_cost1_r]
          ,[route_cost2]
          ,[route_cost2_c]
          ,[route_cost2_d]
          ,[route_cost2_n]
          ,[route_cost2_r]
          ,[route_cost3]
          ,[route_cost3_c]
          ,[route_cost3_d]
          ,[route_cost3_n]
          ,[route_cost3_r]
          ,[route_cost4]
          ,[route_cost4_c]
          ,[route_cost4_d]
          ,[route_cost4_n]
          ,[route_cost4_r]
          ,[route_state_locality1]
          ,[route_state_locality2]
          ,[route_state_locality3]
          ,[route_state_locality4]
          ,[route_date1]
          ,[route_date2]
          ,[route_date3]
          ,[route_date4]
          ,[route_notes1]
          ,[route_notes2]
          ,[route_notes3]
          ,[route_notes4]
          ,[route_rcvd_date1]
          ,[route_rcvd_date2]
          ,[route_rcvd_date3]
          ,[route_rcvd_date4]
          ,[route_state_locality6]
          ,[route_state_locality5]
          ,[route_date6]
          ,[route_date5]
          ,[route_notes6]
          ,[route_notes5]
          ,[route_rcvd_date6]
          ,[route_rcvd_date5]
          ,[route_cost5]
          ,[route_cost6]
          ,getdate() created_date 
          ,1 as is_current
    FROM [QA-McLeod-RPT].lme_qa.dbo.orders o 
    JOIN [QA-McLeod-RPT].lme_qa.dbo.stop s1 ON s1.order_id = o.id AND s1.company_id = o.company_id AND s1.order_sequence = 1
    JOIN [QA-McLeod-RPT].lme_qa.dbo.stop s2 ON s2.order_id = o.id AND s2.company_id = o.company_id AND s2.order_sequence = (SELECT MAX(order_sequence) FROM [QA-McLeod-RPT].lme_qa.dbo.stop WHERE order_id = o.id)
    WHERE o.order_type_id = 'ODPERM' AND o.company_id IN (SELECT * FROM string_split(@company_id,','))
    END
---------
    IF @section IN ('All','movements')
    BEGIN
        TRUNCATE TABLE ods.movements

        ---movements
        INSERT INTO ods.movements
        (
            [movement_id],
            [company_id],
            [order_id],
            [is_brokered],
            [brokerage_status],
            [origin_stop_id],
            [dest_stop_id],
            [movement_status],
            [dispatch_status],
            [is_empty_move],
            [last_modified_date],
            pro_number,
            move_distance,
            carrierPay,
            capacity_rep,
            carrier_contact,
            carrier_phone,
            carrier_email,
            preassign_sequence
        )
        SELECT m.id movement_id,
               m.company_id,
               mo.order_id,
               m.brokerage AS is_brokered,
               m.brokerage_status,
               m.origin_stop_id,
               m.dest_stop_id destination_stop_id,
               m.status AS movement_status,
               NULL AS dispatch_status,
               NULL AS is_empty_move,
               GETDATE(),
               m.pro_nbr pro_number,
               m.move_distance,
               m.override_pay_amt,
               m.capacity_rep,
               m.carrier_contact,
               m.carrier_phone,
               m.carrier_email,
               m.preassign_sequence
        FROM [QA-McLeod-RPT].lme_qa.dbo.movement m
            JOIN [QA-McLeod-RPT].lme_qa.dbo.movement_order mo
                ON mo.movement_id = m.id AND mo.company_id = m.company_id AND mo.id <> 'JRM06102021a'
        WHERE m.company_id IN (SELECT * FROM string_split(@company_id,','))
              AND order_id IN
                  (
                      SELECT order_id FROM @OrderList
                  )
    END

    IF @section IN ('All','stops')
    BEGIN
        TRUNCATE TABLE ods.stops
        
        INSERT INTO ods.stops
        (
            [company_id],
            [stop_id],
            [order_id],
            [movement_id],
            [sched_arrival],
            [sched_departure],
            [original_arrival],
            [original_departure],
            [actual_arrival],
            [actual_departure],
            [city],
            [state],
            [zip],
            [location_code],
            location_name,
            [stop_type],
            [lattitude],
            [longitude],
            [last_modified_date],
            [order_sequence],
            [movement_sequence]
        )
        SELECT origin.[company_id],
               origin.id AS stop_id,
               origin.order_id,
               origin.movement_id,
               origin.sched_arrive_early,
               origin.sched_arrive_late,
               origin.orig_sched_early,
               origin.orig_sched_late,
               origin.actual_arrival,
               origin.actual_departure,
               origin.city_name,
               origin.state,
               origin.zip_code,
               origin.location_id,
               origin.location_name,
               origin.stop_type,
               origin.latitude,
               origin.longitude,
               GETDATE(),
               origin.order_sequence,
               origin.movement_sequence
        FROM [QA-McLeod-RPT].lme_qa.dbo.stop origin
        WHERE origin.company_id IN (SELECT * FROM string_split(@company_id,','))
              AND origin.order_id IN
                  (
                      SELECT order_id FROM @OrderList
                  )
        --ORDER BY origin.order_id,
        --         origin.order_sequence,
        --         origin.movement_sequence
    END

    IF @section IN ('All','RF')
    BEGIN
        TRUNCATE TABLE ods.agency_link

        INSERT INTO ODS.agency_link
        SELECT *
        FROM [QA-McLeod-RPT].lme_qa.dbo.agency_link
        WHERE company_id IN (SELECT * FROM string_split(@company_id,','))

        TRUNCATE TABLE [ODS].hierarchy

        INSERT INTO [ODS].hierarchy
        SELECT * FROM [QA-McLeod-RPT].lme_qa.dbo.hierarchy WHERE company_id IN (SELECT * FROM string_split(@company_id,','))

    
        TRUNCATE TABLE [ODS].[responsibility_history]

        INSERT INTO ODS.responsibility_history
        SELECT DISTINCT
               company_id,
               hierarchy_level_1 AS [hiearchy_code],
               1 AS Hiearchy_level,
               NULL AS parent_hierarchy,
               responsible_role,
               row_id,
               row_type
        FROM [QA-McLeod-RPT].lme_qa.dbo.responsible_hist
        WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
        UNION
        SELECT DISTINCT
               company_id,
               hierarchy_level_2 AS [hiearchy_code],
               2 AS Hiearchy_level,
               hierarchy_level_1 AS parent_hierarchy,
               responsible_role,
               row_id,
               row_type
        FROM [QA-McLeod-RPT].lme_qa.dbo.responsible_hist
        WHERE company_id IN (SELECT * FROM string_split(@company_id,','))
        ORDER BY row_id
    END

    IF @section IN ('All','assets')
    BEGIN
        TRUNCATE TABLE ods.movement_assets

        INSERT INTO ODS.movement_assets
        (
            company_id,
            [movement_id],
            [order_id],
            [tractor_id],
            [driver1_id],
            [driver2_id],
            [trailer1_id],
            [trailer2_id],
            [carrier_id],
            [assignment_status],
            [last_modified_date],
            [created_date],
            [equipment_group_id],
            tractor_last_location,
            driver_dot_info
        )
        SELECT m.company_id,
                m.movement_id,
               m.order_id,
               tractor.equipment_id tractor_id,
               driver1.equipment_id driver1_id,
               driver2.equipment_id driver2_id,
               trailer1.equipment_id trailer1_id,
               trailer2.equipment_id trailer2_id,
               mv.override_payee_id carrier_id,
               CASE
                   WHEN cont.is_preassignment = 'Y' THEN
                       'Preassigned'
                   WHEN cont.is_preassignment = 'N' THEN
                       'Assigned'
                   ELSE
                       NULL
               END assignment_status,
               GETDATE() assignment_modified_date,
               GETDATE(),
               tractor.equipment_group_id,
               (SELECT top 1 RTRIM(city_name) + ', ' + RTRIM([state]) + ' @ ' + CAST(call_date_time AS VARCHAR(30))
               FROM [QA-McLeod-RPT].lme_qa.dbo.callin WHERE tractor_id = tractor.equipment_id AND company_id = tractor.company_id
		       ORDER BY call_date_time DESC) tractor_last_location,
               (SELECT id,license_date, physical_date,mvr_date, security_date,hm126c_date,hazmat_date,medical_cert_expire 
               FROM [QA-McLeod-RPT].lme_qa.dbo.driver WHERE id = driver1.equipment_id AND company_id = driver1.company_id 
               FOR JSON PATH, INCLUDE_NULL_VALUES) driver_dot_info
        FROM [QA-McLeod-RPT].lme_qa.dbo.movement_order m
            INNER JOIN [QA-McLeod-RPT].lme_qa.dbo.movement mv
                ON m.movement_id = mv.id
                   AND m.company_id = mv.company_id
            LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.equipment_item tractor
                ON mv.equipment_group_id = tractor.equipment_group_id
                   AND tractor.equipment_type_id = 'T'
            LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.equipment_item driver1
                ON mv.equipment_group_id = driver1.equipment_group_id
                   AND driver1.equipment_type_id = 'D'
                   AND driver1.type_sequence = '0'
            LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.equipment_item driver2
                ON mv.equipment_group_id = driver2.equipment_group_id
                   AND driver2.equipment_type_id = 'D'
                   AND driver2.type_sequence = '1'
            LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.equipment_item trailer1
                ON mv.equipment_group_id = trailer1.equipment_group_id
                   AND trailer1.equipment_type_id = 'L'
                   AND trailer1.type_sequence = '0'
            LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.equipment_item trailer2
                ON mv.equipment_group_id = trailer2.equipment_group_id
                   AND trailer2.equipment_type_id = 'L'
                   AND trailer2.type_sequence = '1'
            LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.continuity cont
                ON m.movement_id = cont.movement_id
                   AND m.company_id = cont.company_id
                   AND cont.equipment_id = tractor.equipment_id
                   AND cont.equipment_type_id = 'T'
        WHERE m.company_id IN (SELECT * FROM string_split(@company_id,','))
              AND m.order_id IN
                  (
                      SELECT order_id FROM @OrderList
                  )

    END
END

GO

