
CREATE PROCEDURE [dbo].[ssp_PP_UserRoles_GetAllRoles]        
@UserID varchar(10)
            
 AS            

--Select R.Role_NAME From Role R ,User_Role UR ,User_Master UM
--Where R.Role_Id = UR.Role_Id and UR.User_Info_Id = UM.User_Info_Id and UM.Login_Id = @UserID

Select Distinct AT.Account_Type as Role_NAME from User_Master UM ,User_System_Account USA, Account_Master AM ,Account_Type AT
Where 
UM.User_Info_Id = USA.User_Info_Id        And
USA.Account_Id  = AM.Account_Id           And
AM.Account_Type_Id = AT.Account_Type_Id   AND
UM.Login_Id = @UserID

GO

