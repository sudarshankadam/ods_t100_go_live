  
  
CREATE procedure [dbo].[usp_PP_InsertFinalMileQuote]    
  
@AccountNumber int,            
@ProjectName varchar(100),   
           
--Destination Info           
@Address1 varchar(50),            
@Address2 varchar(50),            
@City varchar(50),            
@State varchar(2),            
@Zip varchar(50),            
@Country int,            
@Contact varchar(100),            
@MainPhone varchar(50),            
@CellPhone varchar(50),            
@WorkPhone varchar(50),            
@Email varchar(200),            
    
--Order Information      
@ServiceLevel int,
@Freight datetime,            
@DelDate datetime,            
@DelTime varchar(20),      
@ProjectComment varchar(500),     
         
--ProjectPartner            
@PartnerID int,     
        
--OrderInv             
--@ProductTypeIds text,            
--@ProductIDQTY text,  
  
--Creator            
@CreatedBy varchar(50),      
  
@FMMiles float,  
@TemplateID int, 
@contractTypeId int, 
@Name varchar(50),
@FirstMileFlag int,  
@TransTypeId int,  
@RetVal integer=0 output      
    
AS            
         
    
Declare @AddressID int            
Declare @AddressTypeID int            
Declare @QuoteID int            
Declare @CommentID int            
Declare @ContactID int    
          
Declare @RCPRate numeric(9)      
Declare @AdditionalItem numeric(9)      
Declare @AdditionalItemExp numeric(9)      
    
declare @sSQL varchar(8000)      
Declare @Err int      
  
  
Declare @ActivityDescription varchar(50)    
Declare @ProjectRateID int    
--Declare @TemplateID int    
    
Declare @CommValue float  
Declare @CommType varchar(50)    
    
Declare @XMLArrayDoc int          
Declare @TotalWeight numeric        
   
Declare @OrderTypeID int  
  
Declare @BusinessTypeID int  

-- setting the date format will be removed upon uat Umakanth
--   SELECT @Freight = DATEADD(day, DATEDIFF(day, 0, @Freight), '21:30:00')
--   SELECT @DelDate = DATEADD(day, DATEDIFF(day, 0, @DelDate), '21:30:00')

If @contractTypeId = 1  
Set @AddressTypeID = 8  -- Project Destination  Address type   

If @contractTypeId = 5 
Set @AddressTypeID = 8  -- Project Origin  Address type          
    
BEGIN TRANSACTION      
    
If @Err <> 0 goto ERR_HANDLER    
    
--select @CommValue = CommissionValue, @CommType = CommissionType   
--from customer where customerid = @AccountNumber    
  
select @CommValue = CommissionValue, @CommType = CommissionType, @BusinessTypeID = BusinessTypeID  
from customer where customerid = @AccountNumber    
    
    
--Insert into address table            
    
Insert into Address(AddressTypeID,Address1,Address2,City,StateID,Zip,            
      CountryID,Mainphone,CellPhone,WorkPhone,Email,Name,            
      CreatedBy,CreateDate)            
Values(@AddressTypeID,@Address1,@Address2,@City,@State,@Zip,@Country,            
 @Mainphone,@CellPhone,@WorkPhone,@Email,@Name,@CreatedBy,getdate())            
          
    
Select @AddressID = @@IDENTITY, @Err=@@error              
    
If @Err <> 0 goto ERR_HANDLER     
    
-- 12/22/2005: set the ordertype based on flag  
If @FirstMileFlag = 0  
Begin  
 -- Final Mile  
 Set @OrderTypeID = 1  
End  
  
If @FirstMileFlag = 1  
Begin  
 -- First Mile  
 Set @OrderTypeID = 5  
End  
  
-- 5/29/2007: new "Order Swap" order type  
If @FirstMileFlag = 2  
Begin  
 -- Order Swap  
 Set @OrderTypeID = 7  
End  

-- Insert into project table          
--If @AccountNumber = 1871  
  -- 6/14/2011: for the Sears Outlet Customer, we need to put what they  
  -- send us as the CarrierProNumber into the TransOrderRef field  
--  Begin  
-- Insert into Project (CustomerID,InvoiceNowFlag,KioskFlag,RecordStatusId,CreatedBy)          
--		  Values(@AccountNumber,0,0,0,'UmaTanvi') 
		  
-- DEC 11 Umakanth Modified Project Table inputs for basic contract
-- OrderStatus ID as Open-10 for now --Umakanth
-- Insert into project table          
If @AccountNumber = 1871  
  -- 6/14/2011: for the Sears Outlet Customer, we need to put what they  
  -- send us as the CarrierProNumber into the TransOrderRef field  
  Begin  
 Insert into Quote (CustomerID,ProjectName,ProjectComment,AddressID,            
        FreightAvailable,DeliveryDate,DeliveryTime,CreatedBy,CreateDate,ProjectTypeId,OrderTypeID, 
         ServiceLevelID,FMMiles,TransTypeId,OrderStatusID,Active,TemplateId)              
 Values(@AccountNumber,@projectName,@ProjectComment,@AddressID,           
    @Freight,@DelDate,@DelTime,@CreatedBy,getdate(),10,@OrderTypeID,@ServiceLevel,@FMMiles,@TransTypeId,10,1,@TemplateID)               
  End  
Else  
  Begin  
 Insert into Quote (CustomerID,ProjectName,ProjectComment,AddressID,          
        FreightAvailable,DeliveryDate,DeliveryTime,CreatedBy,CreateDate,ProjectTypeId,OrderTypeID,  
        ServiceLevelID,FMMiles,TransTypeId,OrderStatusID,Active,TemplateId)              
 Values(@AccountNumber,@projectName,@ProjectComment,@AddressID,            
    @Freight,@DelDate,@DelTime,@CreatedBy,getdate(),10,@OrderTypeID,@ServiceLevel,@FMMiles,@TransTypeId,10,1,@TemplateID)               
  End   
------------------------------------------------------------------		               
 -- End   
        
Select @QuoteID = @@IDENTITY ,@Err=@@error              
    
If @Err <> 0 goto ERR_HANDLER 


      
--Project location table   
Insert into projectLocation (QuoteID,AddressID,AddressTypeID,CreatedBy,CreateDate)    
Values(@QuoteID,@AddressID,@AddressTypeID,@CreatedBy,getdate())  

If @@error <> 0 goto ERR_HANDLER

--ProjectPartner table            
Insert into ProjectPartner (QuoteID,PartnerID,CreatedBy,CreateDate)            
Values(@QuoteID,@PartnerID,@CreatedBy,getdate())            
    
If @@error <> 0 goto ERR_HANDLER  

-- 7/21/2011: add STI - Canada as a partner to all Home Express orders  
-- that are going to Canada  
If @BusinessTypeID = 1 and @Country = 2 and @PartnerID <> 734  
 Begin  
  Insert into ProjectPartner (QuoteID,PartnerID,PrimaryPartner,CreatedBy,CreateDate)             
  Values(@QuoteID,734,0,@CreatedBy,getdate())            
  If @@error <> 0 goto ERR_HANDLER      
 End  

 if @ProjectComment <> ''            
 begin      
 -- Insert into Comments            
    
 Insert into Comment(Comment,Name,CommentDate,CreatedBy,CreateDate)            
 Values(@ProjectComment,@CreatedBy,getdate(),@CreatedBy,getdate())            
    
 Select @CommentID = @@IDENTITY,@Err=@@error              
    
 If @Err <> 0 goto ERR_HANDLER      
           
 -- Insert into ProjectComments            
 Insert into ProjectComments(QuoteID,CommentID,CreatedBy,CreateDate)            
 Values(@QuoteID,@CommentID,@CreatedBy,getDate())            
    
 If @@Error <> 0 goto ERR_HANDLER      
 End 

 --Insert into Contact            
Insert into Contact(ContactTypeID,Position,Name,Createdby,CreateDate)            
Values(25,'Unknown',@Contact,@CreatedBy,getdate())            
    
Select @ContactID = @@IDENTITY,@Err=@@error              
    
If @Err <> 0 goto ERR_HANDLER      
          
      
--Insert into projectContact            
    
Insert into projectContacts(QuoteID,CustomerID,ContactID,CreatedBy,CreateDate)            
Values (@QuoteID,@AccountNumber,@ContactID,@CreatedBy,getdate())            
    
If @@Error <> 0 goto ERR_HANDLER 

--- Start Umakanth next
select @RCPRate = isNull(RCPRate,0),@AdditionalItem = isnull(AdditionalItem,0),    
@AdditionalItemExp = isnull(AdditionalItemExp,0) from customer       
where customerid = @AccountNumber      
    
If @@Error <> 0 goto ERR_HANDLER      
    
if @RCPRate <> 0       
 Begin       
    
 Select @ActivityDescription = Description from Activity where ActivityID = 38    
    
 Insert into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOFmeasureCD,RateValue,    
 Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,    
 InvoicedFlag,RateActionTypeID,createdby,createDate)    
 Values(@QuoteID,@PartnerID,38,'FR',0,1,    
 @RCPRate,'FR',0,@ActivityDescription,0,3,@CreatedBy,getdate())    
    
 Select  @ProjectRateID = @@IDENTITY, @Err=@@error              
    
 If @Err <> 0 goto ERR_HANDLER      
    
-- 2/3/04: use freight available date instead of deldate, because deldate is not required  
 insert into projectactivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,    
 CustomerCost,partnercost,MinimumUsed,createdby,createdate)      
 values (@ProjectRateID,@Freight,1,0,@RCPRate,0,0,@CreatedBy,getdate())      
    
 If @@Error <> 0 goto ERR_HANDLER      
End      

/* Remote Charges Calculation is now in the Usp_CalculateAddlItemCharge_New procedure */  
  
    
--Calculate Delivery Charge - July 7 2003    
-- 2/3/04: use freight available date instead of deldate, because deldate is not required  
-- 3/10/2011: pass Zip and Country instead of Region and State,  
-- and no longer pass rowcount  
-- Exec Usp_CalculateAddlItemCharge_New @ProductIDQTY, @AccountNumber,@Zip,@Country,@ProjectID,@PartnerID,@Freight,@CreatedBy  
-- Exec Usp_CalculateAddlItemCharge_New @ProductIDQTY, @AccountNumber,@Region,@RateStateID,@ProjectID,@PartnerID,@Freight,@CreatedBy,@Rowcount    
If @@Error <> 0 goto ERR_HANDLER      

---- End Umakanth
 
    
if @@error = 0       
BEGIN      
  Commit Transaction      
  Set @RetVal= @QuoteID        
END      
    
ERR_HANDLER:      
   IF @@TRANCOUNT > 0        
   BEGIN       
    Rollback transaction      
   END  
  
  
Return @RetVal

GO

