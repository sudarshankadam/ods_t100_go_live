      
CREATE Procedure [dbo].[USP_QUOTE_CalculateAccessorialCharge]            
@QuoteID int,      
@PartnerID int,      
@DelDate DATETIME,       
@PermAccessID int,      
@PermAccessQTY int,       
@TotalWeight numeric,      
@TotalCube numeric(12,2),      
@TotalSquareFeet numeric(18,2),      
@TotalLinearFeet numeric(18,2),      
@TotalQuantity int,      
@TotalPieces int,      
@FMMiles float,      
@ProductID int,      
@CreatedBy varchar(50)      
      
AS      
      
Declare @ActivityID int          
Declare @ActivityDescription varchar(50)            
Declare @UnitOfMeasureCD varchar(3)          
Declare @CustomerRateUnitOfMeasure varchar(3)          
Declare @CustomerRateValue float            
Declare @PartnerRateValue float            
Declare @PartnerUOM varchar(3)      
Declare @CustomerMinimumUOM varchar(3)      
Declare @CustomerMinimumRateValue float      
Declare @PartnerMinimumUOM varchar(3)      
Declare @PartnerMinimumRateValue float      
Declare @RateActionTypeID int          
      
      
Declare @CustomerCost float      
Declare @CustomerMinimum float      
Declare @PartnerCost float      
Declare @PartnerMinimum float      
Declare @MinimumUsed int      
      
Declare @UOMMultiplier int      
Declare @Margin smallmoney             
Declare @ProjectRateID int            
Declare @Units float      
      
--4/5/2012: new variables for table driven additional time on site calculation      
Declare @RecordExists int      
Declare @ServiceLevelID int      
Declare @AssemblyMinutes int      
Declare @NumberofPeople int      
Declare @CustomerID int      
Declare @Hours float      
Declare @CustomerUOM varchar(3)      
Declare @1PersonCustomerRateValue float      
Declare @1PersonCustomerRateUOM varchar(3)      
Declare @1PersonPartnerRateValue float      
Declare @1PersonPartnerRateUOM varchar(3)      
Declare @2PersonCustomerRateValue float      
Declare @2PersonCustomerRateUOM varchar(3)      
Declare @2PersonPartnerRateValue float      
Declare @2PersonPartnerRateUOM varchar(3)      
Declare @3PersonCustomerRateValue float      
Declare @3PersonCustomerRateUOM varchar(3)      
Declare @3PersonPartnerRateValue float      
Declare @3PersonPartnerRateUOM varchar(3)      
Declare @4PersonCustomerRateValue float      
Declare @4PersonCustomerRateUOM varchar(3)      
Declare @4PersonPartnerRateValue float      
Declare @4PersonPartnerRateUOM varchar(3)      
Declare @CustomerMaximumUom varchar(3)      
Declare @CustomerMaximumRateValue float      
Declare @PartnerMaximumUOM varchar(3)      
Declare @PartnerMaximumRateValue float      
      
--insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)      
-- Values('USP_CalculateAccessorialCharge','enter procedure', @QuoteID, GetDate(),'bergc')      
      
      
Select  @ActivityID = ActivityID,      
  @ActivityDescription = ProjectDescription,          
   @UnitOfMeasureCD = UnitOfMeasureCD,          
        @CustomerRateUnitOfMeasure = CustomerRateUnitOfMeasure,          
        @CustomerRateValue = CustomerRateValue,          
        @PartnerRateValue = PartnerRateValue,          
  @PartnerUOM = PartnerUOM,      
  @CustomerMinimumUOM = CustomerMinimumUOM,      
  @CustomerMinimumRateValue = CustomerMinimumRateValue,      
  @PartnerMinimumUOM = PartnerMinimumUOM,      
  @PartnerMinimumRateValue = PartnerMinimumRateValue,      
     @RateActionTypeID = RateActionTypeID          
From [HDN_Providence].[dbo].Accessories       
Where AccessorialID = @PermAccessID          
      
      
Set @CustomerCost = 0      
Set @CustomerMinimum = 0      
Set @PartnerCost = 0      
Set @PartnerMinimum = 0      
Set @MinimumUsed = 0      
Set @Units = 1      
      
      
-- First, Calculate the Customer Cost      
-- Pounds or Hundred Weight      
if @CustomerRateUnitOfMeasure = 'LB' or @CustomerRateUnitOfMeasure = 'CWT'          
  Begin        
   Select @UOMMultiplier = Multiplier from [HDN_Providence].[dbo].UnitofMeasure where UnitofMeasureCD = @CustomerRateUnitOfMeasure        
 Set @CustomerCost = (@TotalWeight/@UOMMultiplier ) * @CustomerRateValue        
 Set @Units = (@TotalWeight/@UOMMultiplier )      
  End        
      
-- Cubes      
if @CustomerRateUnitOfMeasure = 'CU'         
  Begin        
 Set @CustomerCost = @TotalCube * @CustomerRateValue        
 Set @Units = @TotalCube      
  End       
      
-- Square Feet      
if @CustomerRateUnitOfMeasure = 'SF'         
  Begin        
 Set @CustomerCost = @TotalSquareFeet * @CustomerRateValue        
 Set @Units = @TotalSquareFeet      
  End        
      
-- Linear Feet      
if @CustomerRateUnitOfMeasure = 'LF'         
  Begin        
 Set @CustomerCost = @TotalLinearFeet * @CustomerRateValue        
 Set @Units = @TotalLinearFeet      
  End        
      
-- Each      
if @CustomerRateUnitOfMeasure = 'EA'       
  Begin        
 Set @CustomerCost = @TotalQuantity * @CustomerRateValue        
 Set @Units = @TotalQuantity      
     
  End        
      
-- Per Piece      
if @CustomerRateUnitOfMeasure = 'PC'          
  Begin        
 Set @CustomerCost = @TotalPieces * @CustomerRateValue        
 Set @Units = @TotalPieces      
  End        
      
-- Miles      
if @CustomerRateUnitOfMeasure = 'MI'          
  Begin        
 Set @CustomerCost = @FMMiles * @CustomerRateValue      
 Set @Units = @FMMiles      
  End        
      
-- Flat Rate      
if @CustomerRateUnitOfMeasure = 'FR'          
  Begin        
  Set @CustomerCost = @CustomerRateValue      
 Set @Units = 1      
  End        
      
      
--insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)      
-- Values('@CustomerRateUnitOfMeasure=',@CustomerRateUnitOfMeasure, 0, GetDate(),'bergc')      
      
-- 5/1/2012: TDM = Table Driven by Minutes - new calculation method for the complicated      
-- additional time on site calculation      
if @CustomerRateUnitOfMeasure = 'TDM'          
  Begin       
        
 -- this will vary by service level      
 Select @ServiceLevelID = ServiceLevelID      
 From Quote      
 Where QuoteID = @QuoteID      
        
 -- initialize these values - we may not find a match      
 Set @CustomerCost = 0      
  Set @PartnerCost = 0      
 Set @Units = 1      
       
 Select @AssemblyMinutes = sum(p.AssemblyMinutes)       
 From OrderInv oi      
 inner join [HDN_Providence].[dbo].Product p ON oi.ProductTypeID = p.ProductTypeID      
 Where oi.QuoteID = @QuoteID      
       
 Select @ActivityDescription = ltrim(rtrim(@ActivityDescription)) + ' - ' + convert(varchar(10), @AssemblyMinutes) + ' Minutes'      
--insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)      
-- Values('@ActivityDescription=',@ActivityDescription, 0, GetDate(),'bergc')      
       
  Select @NumberofPeople = max(p.NumberofPeople)       
 From OrderInv oi      
 inner join [HDN_Providence].[dbo].Product p ON oi.ProductTypeID = p.ProductTypeID      
 Where oi.QuoteID = @QuoteID      
       
 Select @CustomerID = CustomerID From Quote Where QuoteID = @QuoteID      
       
 -- we will calculate as any part of an hour is a full hour      
 Set @Hours = (@AssemblyMinutes + 29)      
 Set @Hours = round(@Hours / 60, 0)      
      
 Select @RecordExists = Count(*)      
 From [HDN_Providence].[dbo].Accessories_OnSiteMinutes      
 Where CustomerID = @CustomerID      
 and ActivityID = @ActivityID      
 and ServiceLevelID = @ServiceLevelID      
 and @AssemblyMinutes between StartMinutes and EndMinutes      
       
 -- if they don't have anything set up for this customer,      
 -- activity, servicelevel, and range of minutes, we won't      
 -- create an activity      
 If @RecordExists = 0      
   Begin      
  Set @CustomerRateValue = 0      
  Set @CustomerMinimumRateValue = 0      
  Set @CustomerRateUnitOfMeasure = 'FR'      
   Set @PartnerRateValue = 0      
  Set @PartnerMinimumRateValue = 0      
   Set @PartnerUOM = 'FR'          
   End      
 Else      
   Begin      
  -- Set the additional fields we need from the table      
  Select  @1PersonCustomerRateValue = OnePersonCustomerRateValue,      
    @1PersonCustomerRateUOM = OnePersonCustomerRateUOM,      
    @1PersonPartnerRateValue= OnePersonPartnerRateValue,      
    @1PersonPartnerRateUOM = OnePersonPartnerRateUOM,      
    @2PersonCustomerRateValue = TwoPersonCustomerRateValue,      
    @2PersonCustomerRateUOM = TwoPersonCustomerRateUOM,      
    @2PersonPartnerRateValue= TwoPersonPartnerRateValue,      
    @2PersonPartnerRateUOM = TwoPersonPartnerRateUOM,      
    @3PersonCustomerRateValue = ThreePersonCustomerRateValue,      
    @3PersonCustomerRateUOM = ThreePersonCustomerRateUOM,      
    @3PersonPartnerRateValue= ThreePersonPartnerRateValue,      
    @3PersonPartnerRateUOM = ThreePersonPartnerRateUOM,      
    @4PersonCustomerRateValue = FourPersonCustomerRateValue,      
    @4PersonCustomerRateUOM = FourPersonCustomerRateUOM,      
    @4PersonPartnerRateValue = FourPersonPartnerRateValue,      
    @4PersonPartnerRateUOM = FourPersonPartnerRateUOM,      
    @CustomerMaximumUom = CustomerMaximumUom,      
    @CustomerMinimumUom = CustomerMinimumUom,      
    @CustomerMaximumRateValue = CustomerMaximumRateValue,      
    @CustomerMinimumRateValue = CustomerMinimumRateValue,      
    @PartnerMaximumUOM = PartnerMaximumUOM,      
    @PartnerMinimumUOM = PartnerMinimumUOM,      
    @PartnerMaximumRateValue = PartnerMaximumRateValue,      
    @PartnerMinimumRateValue = PartnerMinimumRateValue         
  From [HDN_Providence].[dbo].Accessories_OnSiteMinutes      
  Where CustomerID = @CustomerID      
  and ActivityID = @ActivityID      
  and ServiceLevelID = @ServiceLevelID      
  and @AssemblyMinutes between StartMinutes and EndMinutes      
       
  If @NumberofPeople = 1      
    Begin      
   Set @CustomerRateValue = @1PersonCustomerRateValue      
    Set @PartnerRateValue = @1PersonPartnerRateValue      
   Set @CustomerUOM = @1PersonCustomerRateUOM      
    Set @PartnerUOM = @1PersonPartnerRateUOM      
    End      
  If @NumberofPeople = 2      
    Begin      
   Set @CustomerRateValue = @2PersonCustomerRateValue      
    Set @PartnerRateValue = @2PersonPartnerRateValue      
   Set @CustomerUOM = @2PersonCustomerRateUOM      
    Set @PartnerUOM = @2PersonPartnerRateUOM      
    End      
  If @NumberofPeople = 3      
    Begin      
   Set @CustomerRateValue = @3PersonCustomerRateValue      
    Set @PartnerRateValue = @3PersonPartnerRateValue      
   Set @CustomerUOM = @3PersonCustomerRateUOM      
    Set @PartnerUOM = @3PersonPartnerRateUOM      
    End      
  If @NumberofPeople = 4      
    Begin      
   Set @CustomerRateValue = @4PersonCustomerRateValue      
    Set @PartnerRateValue = @4PersonPartnerRateValue      
   Set @CustomerUOM = @4PersonCustomerRateUOM      
    Set @PartnerUOM = @4PersonPartnerRateUOM      
    End      
         
  Set @CustomerCost = @CustomerRateValue      
  Set @CustomerRateUnitOfMeasure = 'FR'      
  Set @UnitOfMeasureCD = @PartnerUOM      
   End      
           
 -- I am only expecting Hours (HR) and Flat Rate (FR) at this time        
 If @CustomerUOM = 'HR'      
   Begin      
  Set @CustomerCost = @CustomerCost * @Hours      
  Set @CustomerRateUnitOfMeasure = 'HR'      
  Set @Units = @Hours      
   End      
      
 -- also, I am going to ignore the CustomerMaximumRateValue for now      
 -- it is only used in CPM.  Should I remove it from the table?      
       
 -- I am in position now to fall thru for the Customer Minimum check      
 -- I am also set up for Partner Cost as well as the Partner Minimum      
  End        
      
      
      
--insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)      
-- Values('@CustomerCost=',@CustomerCost, 0, GetDate(),'bergc')      
--insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)      
-- Values('@CustomerMinimumRateValue=',@CustomerMinimumRateValue, 0, GetDate(),'bergc')      
      
      
If @CustomerMinimumRateValue > 0      
  -- if there is a minimum defined, calculate it      
  Begin      
 -- Pounds or Hundred Weight      
 if @CustomerMinimumUOM = 'LB' or @CustomerMinimumUOM = 'CWT'          
   Begin        
    Select @UOMMultiplier = Multiplier from [HDN_Providence].[dbo].UnitofMeasure where UnitofMeasureCD = @CustomerMinimumUOM        
  Set @CustomerMinimum = (@TotalWeight/@UOMMultiplier ) * @CustomerMinimumRateValue        
   End        
      
 -- Cubes      
 if @CustomerMinimumUOM = 'CU'         
   Begin        
  Set @CustomerMinimum = @TotalCube * @CustomerMinimumRateValue        
   End       
      
 -- Square Feet      
 if @CustomerMinimumUOM = 'SF'         
   Begin        
  Set @CustomerMinimum = @TotalSquareFeet * @CustomerMinimumRateValue        
   End        
      
 -- Linear Feet      
 if @CustomerMinimumUOM = 'LF'         
   Begin        
  Set @CustomerMinimum = @TotalLinearFeet * @CustomerMinimumRateValue        
   End        
      
 -- Each      
 if @CustomerMinimumUOM = 'EA'       
   Begin        
  Set @CustomerMinimum = @TotalQuantity * @CustomerMinimumRateValue        
   End        
      
 -- Per Piece      
 if @CustomerMinimumUOM = 'PC'          
   Begin        
  Set @CustomerMinimum = @TotalPieces * @CustomerMinimumRateValue        
   End        
      
 -- Miles      
 if @CustomerMinimumUOM = 'MI'          
   Begin        
  Set @CustomerMinimum = @FMMiles * @CustomerMinimumRateValue      
   End        
      
 -- Flat Rate      
 if @CustomerMinimumUOM = 'FR'          
   Begin        
   Set @CustomerMinimum = @CustomerMinimumRateValue      
   End        
      
 -- Hourly does not show as an option in OMS.  The only      
 -- way it will be set is if we get it from the       
 -- Accessories_OnSiteMinutes table.      
 -- Hours      
 if @CustomerMinimumUOM = 'HR'          
   Begin        
   Set @CustomerMinimum = @CustomerMinimumRateValue * @Hours      
   End        
  End      
      
      
If @CustomerCost < @CustomerMinimum      
  Begin      
 Set @CustomerCost = @CustomerMinimum      
 Set @CustomerRateValue = @CustomerMinimum      
 Set @MinimumUsed = 1      
 Set @Units = 1      
  End      
      
      
      
      
-- Now, Calculate the Partner Cost      
-- Pounds or Hundred Weight      
if @PartnerUOM = 'LB' or @PartnerUOM = 'CWT'          
  Begin        
   Select @UOMMultiplier = Multiplier from [HDN_Providence].[dbo].UnitofMeasure where UnitofMeasureCD = @PartnerUOM        
 Set @PartnerCost = (@TotalWeight/@UOMMultiplier ) * @PartnerRateValue        
  End        
      
-- Cubes      
if @PartnerUOM = 'CU'         
  Begin        
 Set @PartnerCost = @TotalCube * @PartnerRateValue        
  End       
      
-- Square Feet      
if @PartnerUOM = 'SF'         
  Begin        
 Set @PartnerCost = @TotalSquareFeet * @PartnerRateValue        
  End        
      
-- Linear Feet      
if @PartnerUOM = 'LF'         
  Begin        
 Set @PartnerCost = @TotalLinearFeet * @PartnerRateValue        
  End        
      
-- Each      
if @PartnerUOM = 'EA'       
  Begin        
 Set @PartnerCost = @TotalQuantity * @PartnerRateValue        
  End        
      
-- Per Piece      
if @PartnerUOM = 'PC'          
  Begin        
 Set @PartnerCost = @TotalPieces * @PartnerRateValue        
  End        
      
-- Miles      
if @PartnerUOM = 'MI'          
  Begin        
 Set @PartnerCost = @FMMiles * @PartnerRateValue      
  End        
      
-- Flat Rate      
if @PartnerUOM = 'FR'          
  Begin        
  Set @PartnerCost = @PartnerRateValue      
  End        
      
-- Hourly does not show as an option in OMS.  The only way      
-- it will be set will be in the Accessories_OnSiteMinutes      
-- table.      
-- Hours      
If @PartnerUOM = 'HR'      
  Begin      
 Set @PartnerCost = @PartnerRateValue * @Hours      
  End      
      
      
If @PartnerMinimumRateValue > 0      
  -- if there is a minimum defined, calculate it      
  Begin      
 -- Pounds or Hundred Weight      
 if @PartnerMinimumUOM = 'LB' or @PartnerMinimumUOM = 'CWT'          
   Begin        
    Select @UOMMultiplier = Multiplier from [HDN_Providence].[dbo].UnitofMeasure where UnitofMeasureCD = @PartnerMinimumUOM        
  Set @PartnerMinimum = (@TotalWeight/@UOMMultiplier ) * @PartnerMinimumRateValue        
   End        
      
 -- Cubes      
 if @PartnerMinimumUOM = 'CU'         
   Begin        
  Set @PartnerMinimum = @TotalCube * @PartnerMinimumRateValue        
   End       
      
 -- Square Feet      
 if @PartnerMinimumUOM = 'SF'         
   Begin        
  Set @PartnerMinimum = @TotalSquareFeet * @PartnerMinimumRateValue        
   End        
      
 -- Linear Feet      
 if @PartnerMinimumUOM = 'LF'         
   Begin        
  Set @PartnerMinimum = @TotalLinearFeet * @PartnerMinimumRateValue        
   End        
      
 -- Each      
 if @PartnerMinimumUOM = 'EA'       
   Begin        
  Set @PartnerMinimum = @TotalQuantity * @PartnerMinimumRateValue        
   End        
      
 -- Per Piece      
 if @PartnerMinimumUOM = 'PC'          
   Begin        
  Set @PartnerMinimum = @TotalPieces * @PartnerMinimumRateValue        
   End        
      
 -- Miles      
 if @PartnerMinimumUOM = 'MI'          
   Begin        
  Set @PartnerMinimum = @FMMiles * @PartnerMinimumRateValue      
   End        
      
 -- Flat Rate      
 if @PartnerMinimumUOM = 'FR'          
   Begin        
   Set @PartnerMinimum = @PartnerMinimumRateValue      
   End        
       
  -- Hourly does not show as an option in OMS.  The only      
 -- way it will be set is if we get it from the       
 -- Accessories_OnSiteMinutes table.      
 -- Hours      
 if @PartnerMinimumUOM = 'HR'          
   Begin        
   Set @PartnerMinimum = @PartnerMinimumRateValue * @Hours      
   End        
 End      
      
If @PartnerCost < @PartnerMinimum      
  Begin      
 Set @PartnerCost = @PartnerMinimum      
 Set @PartnerRateValue = @PartnerMinimum      
 Set @MinimumUsed = 1      
  End      
      
      
--Calc Margin and insert record into ProjectRates and ProjectActivites tables                
If @PartnerCost <> 0         
  Begin        
 Select @Margin  = round(((@CustomerCost - @PartnerCost)*100/@PartnerCost)/100,4)          
  End          
        
If @PartnerCost = 0         
  Begin        
 Select @Margin  = 1         
  End 
  
 --    insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)                              
 --Values('@ProductQuantityAss=',CAST(@PermAccessID AS VARCHAR(10)) + ' : ' + CAST(@CustomerCost AS VARCHAR(10)) + ' : ' + CAST(@PartnerCost AS VARCHAR(10)), @QuoteID, GetDate(),'bergc')            
           
-- 4/26/2012: do not create the activity if no charge or cost      
If @CustomerCost <> 0 or @PartnerCost <> 0       
  Begin 
  BEGIN TRY           
 Insert Into ProjectRates (QuoteID,PartnerID,ActivityID,UnitOfMeasureCD,RateValue,            
     Margin,CustomerRateValue,CustomerRateUnitOfMeasure,TailGate,ProjectRateDescription,            
     InvoicedFlag,RateActionTypeID,CreatedBy,CreateDate,ProductID,AccessorialFlag)            
 Values(@QuoteID,@PartnerID,@ActivityID,@UnitOfMeasureCD,@PartnerRateValue,      
   @Margin,@CustomerRateValue,@CustomerRateUnitOfMeasure,0,@ActivityDescription,      
   0,@RateActionTypeID,@CreatedBy,getdate(),@ProductID,1)      
--Values(@QuoteID,@PartnerID,@ActivityID,@UnitOfMeasureCD,@PartnerCost,      
--  @Margin,@CustomerCost,@CustomerRateUnitOfMeasure,0,@ActivityDescription,      
--  0,@RateActionTypeID,@CreatedBy,getdate(),@ProductID,1)      
             
 Select  @ProjectRateID = @@IDENTITY            
       
    if @CustomerRateUnitOfMeasure = 'EA'    
    BEGIN     
     Insert Into ProjectActivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,            
     CustomerCost,PartnerCost,MinimumUsed,CreatedBy,CreateDate)              
 values (@ProjectRateID,@DelDate,@Units,0,      
   @CustomerCost*@PermAccessQTY,@PartnerCost*@PermAccessQTY,@MinimumUsed,@CreatedBy,getdate())     
     END        
      ELSE         
    BEGIN    
     Insert Into ProjectActivities(ProjectRateID,ActivityDate,Units,DoNotpayAgentFlag,            
     CustomerCost,PartnerCost,MinimumUsed,CreatedBy,CreateDate)              
 values (@ProjectRateID,@DelDate,@Units*@PermAccessQTY,0,      
   @CustomerCost*@PermAccessQTY,@PartnerCost*@PermAccessQTY,@MinimumUsed,@CreatedBy,getdate())     
    END  
    
   END TRY         
        
BEGIN CATCH         
        
--insert into AppErrorLog(Application, Error, ProjectID, CreateDate, CreatedBy)                              
 --Values('USP_CalculateAccessorialCharge=',ERROR_MESSAGE() + ' ' + CAST(ERROR_LINE() as VARCHAR(10)), @QuoteID, GetDate(),'Anudeep')         
        
END CATCH                  
      
  End

GO

