


--SELECT * FROM [ODS].[vControlTower_Workflow_Status]
CREATE VIEW [ODS].[vControlTower_Workflow_Status]
AS
    SELECT 
        o.order_id,
        m.movement_id,
        ma.driver1_id,
        ma.driver2_id,
        m.carrier_id,
        ma.tractor_id,
        ma.trailer1_id,
        ma.trailer2_id,
        order_RF.OrderRF_code,
        movement_RF.movementRF_code,
        m.order_movement_execution_division,
        o.order_division,
        CASE 
            WHEN m.is_brokered = 'Y' AND brokerage_status IN ('05', '06', '7', '10', '15', '20' ) THEN 'Y' 
            WHEN m.is_brokered = 'N' AND ISNULL(o.freight_charge,0) > 0 THEN 'Y'
            ELSE 'N' 
            END AS Customer_Rating_Done,
        CASE 
            WHEN m.is_brokered = 'Y' AND brokerage_status IN ('20','30','40','50','55','60','65','66','70','75','80','90','95','99') THEN 'Y' 
            WHEN m.[order_movement_execution_division] <> o.order_division
                        AND ma.assignment_status IS NULL THEN 'W'
            WHEN m.is_brokered = 'N' AND ma.assignment_status IS NOT NULL THEN 'Y'
            ELSE 'N' END AS Carrier_Rating_Done,
        ----------
        CASE 
            WHEN o.order_division <> m.order_movement_execution_division AND ma.assignment_status IS NULL AND wfst.workflow_status_code IN ('10') THEN '20' 
            WHEN o.order_division <> m.order_movement_execution_division AND ma.assignment_status IS NOT NULL AND wfst.workflow_status_code = '20' THEN '30'
            ELSE wfst.workflow_status_code 
        END workflow_status_code ,
        CASE
            WHEN o.order_division <> m.order_movement_execution_division AND ma.assignment_status IS NOT NULL AND wfst.workflow_status_code = '20' THEN 'PICKUP' 
            ELSE wfst.next_action
        END next_action,
        ------------
        ma.assignment_status
	FROM ODS.orderMaster o WITH(NOLOCK)
	LEFT JOIN ODS.movements m WITH(NOLOCK)
	ON o.order_id = m.order_id AND o.company_id = m.company_id
	LEFT JOIN ODS.movement_assets ma WITH(NOLOCK)
	ON o.order_id = ma.order_id
		AND m.movement_id = ma.movement_id
	LEFT JOIN
	(
		SELECT row_id,
			STRING_AGG(RTRIM(hierarchy_code), ',') AS OrderRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'O' AND row_id IN (SELECT order_id FROM ODS.orderMaster)
		GROUP BY row_id,company_id
	) order_RF
	ON order_RF.row_id = o.order_id AND order_RF.company_id = o.company_id
	LEFT JOIN
	(
		SELECT row_id,STRING_AGG(RTRIM(hierarchy_code), ',') AS movementRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'M' AND row_id IN (SELECT movement_id FROM ODS.movements)
		GROUP BY row_id,company_id
	) movement_RF
	ON movement_RF.row_id = m.movement_id AND movement_RF.company_id = m.company_id
    LEFT OUTER JOIN [ODS].[order_movement_workflow_status] wfst
        ON m.order_id = wfst.order_id AND m.movement_id = wfst.movement_id
    WHERE o.order_status IN ('A','P','D')

GO

