





---SELECT * FROM [ODS].[ControlTower_DispatchOrders_FleetView] ORDER BY ISNULL(tractor_id,carrier_id), orderOriginpickUpDate
CREATE VIEW [ODS].[ControlTower_DispatchOrders_FleetView]
AS
    SELECT 
    ma.tractor_id,
    ma.driver1_id,
    ma.driver2_id,
    m.carrier_id,
    ma.trailer1_id,
    ma.trailer2_id,
    m.is_brokered,
    m.brokerage_status,
    ma.driver_dot_info,
    origin.sched_arrival AS orderOriginPickUpDate,
    dest.sched_arrival AS orderDestPickUpDate,
    RTRIM(origin.city) AS origin_city,
    RTRIM(origin.state) AS origin_state,
    RTRIM(origin.zip) AS origin_zip,
    origin.location_code AS origin_location_code,
    origin.location_name AS origin_location_name,
    RTRIM(dest.city) AS dest_city,
    RTRIM(dest.state) AS dest_state,
    RTRIM(dest.zip) AS dest_zip,
    LEFT(RTRIM(origin.zip),3) AS origin_zip3,
    LEFT(RTRIM(dest.zip),3) AS dest_zip3,
    dest.location_code AS dest_location_code,
    dest.location_name AS dest_location_name,
    ma.tractor_last_location,
    o.order_id,
    m.movement_id,
    
    o.customer_id,
    o.customer_name,
    o.order_status,
    (
        SELECT MIN(movement_sequence) ms
        FROM ODS.stops WITH(NOLOCK)
        WHERE (order_id = o.order_id)
                AND (movement_id = m.movement_id)
                AND (actual_arrival IS NOT NULL)
    ) AS current_stop_sequence,
    (
        SELECT COUNT(*) AS Cnt
        FROM ODS.stops AS stops_1 WITH(NOLOCK)
        WHERE (order_id = o.order_id)
                AND (movement_id = m.movement_id)
    ) AS stop_count,
    o.agent_payee_id,
    order_RF.OrderRF_code,
    movement_RF.movementRF_code,
    ----
    o.total_distance AS distance,
    o.order_value,
    m.pro_number,
    m.movement_status,
    m.move_distance,
    o.high_value,
    o.operations_user,
    o.created_by,
    o.equipment_type_id AS trailer_type,
    o.load_weight AS order_weight,
    o.order_type,
    o.order_mode,
    o.order_pieces,       
    o.order_mode_id,
    origin.lattitude originLattitude,
    origin.longitude originLongitude,
    dest.lattitude destinationLattitude,
    dest.longitude destinationLongitude,
    (SELECT RTRIM(s.stop_id) id,
		    s.movement_id movementId,
		    s.sched_arrival scheduledArrival,
		    s.sched_departure scheduledDeparture,
		    s.actual_arrival actualArrival,
		    s.actual_departure actualDeparture,
		    NULL address,
		    RTRIM(city) cityName,
		    RTRIM(state) state,
		    RTRIM(zip) zipCode,
		    NULL AS eta,
		    s.order_id orderId,
		    s.stop_type stopType,
		    RTRIM(servicefail.dsp_comment) serviceFailComment,
		    servicefail.minutes_late serviceFailMinutesLate
    FROM ODS.stops s WITH(NOLOCK)
	    LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.servicefail servicefail
		    ON s.stop_id = servicefail.stop_id
			    AND servicefail.company_id = o.company_id
    WHERE s.order_id = o.order_id
    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER) AS all_stops_info,
    o.ordered_date,
    NULL AS scheduling_conflict,
    NULL AS conflict_start_date,
    NULL AS conflict_end_date,
    m.preassign_sequence,
    o.company_id,
    movement_RF.movementRF_code AS order_movement_execution_division,
    origin.zone_id as origin_zone_id,
    dest.zone_id as dest_zone_id,
    o.is_agent_captive_order,
    'N' as is_partial_shipment,
    ma.assignment_status,
    'N' as brokered_not_preferred,
    NULL AS daysOut,
    m.carrier_id as carrier_name,
    NULL as fleet_id,
    o.ready_to_bill,
    o.bill_date,
    CASE WHEN o.ops_review_status = 'Y' THEN o.last_modified_date ELSE NULL END AS ops_closed_date,
    origin.appt_required origin_appt_required,
    origin.appt_status_code origin_appt_status_code,
    origin.appt_confirmed as origin_appt_confirmed,
    dest.appt_required dest_appt_required,
    dest.appt_status_code dest_appt_status_code,
    dest.appt_confirmed as dest_appt_confirmed,
    CASE WHEN o.order_type IN ('ODPERM','HRISK') THEN 10 ELSE 100 END AS order_sort_priority,
    CASE WHEN CAST(m.[xmitted2driver] as date) = CAST(getdate() as date) 
        THEN 'Y'
        ELSE 'N' 
    END AS driver_contacted_today,
    m.[xmitted2driver] as driver_contact_date,
    wfst.[workflow_status_code],
    wfst.[next_action],
    origin.showas_address origin_showas_address,
    origin.showas_address2 origin_showas_address2,
    origin.showas_city_id origin_showas_city_id,
    origin.showas_city_name origin_showas_city_name,
    origin.showas_location_id origin_showas_location_id,
    origin.showas_location_name origin_showas_location_name,
    origin.showas_state origin_showas_state,
    origin.showas_zip_code origin_showas_zip_code,
    dest.showas_address dest_showas_address,
    dest.showas_address2 dest_showas_address2,
    dest.showas_city_id dest_showas_city_id,
    dest.showas_city_name dest_showas_city_name,
    dest.showas_location_id dest_showas_location_id,
    dest.showas_location_name dest_showas_location_name,
    dest.showas_state dest_showas_state,
    dest.showas_zip_code dest_showas_zip_code,
        o.[subject_order_number],
        o.[subject_order_status],
        o.[subject_order_void_date]
    
    -------------
    FROM ODS.orderMaster AS o WITH(NOLOCK)
    LEFT OUTER JOIN ODS.movements AS m WITH(NOLOCK)
        ON o.order_id = m.order_id AND o.company_id = m.company_id
    LEFT OUTER JOIN ODS.stops AS origin WITH(NOLOCK)
        ON origin.order_id = m.order_id
		AND ISNULL(o.origin_stop_id,m.origin_stop_id) = origin.stop_id
           AND origin.movement_id = m.movement_id
    LEFT OUTER JOIN ODS.stops AS dest WITH(NOLOCK)
        ON dest.order_id = m.order_id
		AND ISNULL(o.dest_stop_id,m.dest_stop_id) = dest.stop_id
           AND dest.movement_id = m.movement_id
    LEFT OUTER JOIN ODS.movement_assets AS ma WITH(NOLOCK)
        ON ma.order_id = m.order_id
           AND ma.movement_id = m.movement_id
    LEFT OUTER JOIN
    (
        SELECT row_id,
			STRING_AGG(RTRIM(hierarchy_code), ',') AS OrderRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'O' AND row_id IN (SELECT order_id FROM ODS.orderMaster)
		GROUP BY row_id,company_id
    ) AS order_RF
        ON order_RF.row_id = o.order_id
    LEFT OUTER JOIN
    (
        SELECT row_id,STRING_AGG(RTRIM(hierarchy_code), ',') AS movementRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'M' AND row_id IN (SELECT movement_id FROM ODS.movements)
		GROUP BY row_id,company_id
    ) AS movement_RF
        ON movement_RF.row_id = o.current_movement_id
    LEFT OUTER JOIN [ODS].[order_movement_workflow_status] wfst
        ON m.order_id = wfst.order_id AND m.movement_id = wfst.movement_id
    WHERE
    ----Non brokered
    (
        o.order_status IN ('A','P')
        AND m.is_brokered = 'N'
        AND ma.assignment_status IS NOT NULL
    ) --- unplanned orders
    OR
    ----Brokered
    (
        m.is_brokered = 'Y'
        AND m.brokerage_status IN ( '30','40','50','55','60','65','66','70','75' )
    )

GO

