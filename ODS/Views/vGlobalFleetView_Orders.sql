












/*
SELECT TOP 100 * FROM [ODS].[vGlobalFleetView_Orders]  
*/
CREATE VIEW [ODS].[vGlobalFleetView_Orders]
AS
SELECT o.company_id,
       o.customer_id,
       o.customer_name,
       CASE 
                WHEN order_status = 'P' AND order_type = 'ODPERM' THEN 2 
                WHEN order_status = 'P' AND order_type = 'HRISK' THEN 1 
                ELSE 0 
        END AS driver_sort_rank,
       o.order_id,
       m.movement_id,
       m.is_brokered,
       m.brokerage_status,
       o.order_status,
       o.hazmat,
       CASE o.order_status WHEN 'A' THEN 'Available' WHEN 'P' THEN 'Progress' WHEN 'V' THEN 'Void' WHEN 'D' THEN 'Delivered' WHEN 'Q' THEN 'Quote' END AS order_status_description,
       RTRIM(order_origin.city) AS origin_city,
       RTRIM(order_origin.state) AS origin_state,
       RTRIM(order_origin.zip) AS origin_zip,
       origin.location_code AS origin_location_code,
       origin.location_name AS origin_location_name,
       origin.sched_departure AS order_Origin_pickUpDate,
       origin.actual_arrival AS order_Origin_Actual_pickUpDate,
       RTRIM(order_dest.city) AS dest_city,
       RTRIM(order_dest.state) AS dest_state,
       RTRIM(order_dest.zip) AS dest_zip,
       RTRIM(origin.city) AS movement_origin_city,
       RTRIM(origin.state) AS movement_origin_state,
       RTRIM(origin.zip) AS movement_origin_zip,
       RTRIM(dest.city) AS movement_dest_city,
       RTRIM(dest.state) AS movement_dest_state,
       RTRIM(dest.zip) AS movement_dest_zip,
       LEFT(RTRIM(origin.zip),3) AS origin_zip3,
       LEFT(RTRIM(dest.zip),3) AS dest_zip3,
       dest.location_code AS dest_location_code,
       dest.location_name AS dest_location_name,
       dest.sched_departure AS order_Dest_DeliveryDate,
       dest.actual_arrival AS order_Dest_Actual_DeliveryDate,
       (
           SELECT MIN(movement_sequence) AS Expr1
           FROM ODS.stops WITH(NOLOCK) 
           WHERE (order_id = o.order_id)
                 AND (movement_id = m.movement_id)
                 AND (actual_arrival IS NOT NULL)
       ) AS current_stop_sequence,
       (
           SELECT COUNT(*) AS Expr1
           FROM ODS.stops AS stops_1 WITH(NOLOCK)
           WHERE (order_id = o.order_id)
                 AND (movement_id = m.movement_id)
       ) AS stop_count,
       ma.driver1_id,
       ma.driver2_id,
       ma.driver_dot_info,
       m.carrier_id,
       ma.tractor_id,
       ma.trailer1_id,
       ma.trailer2_id,
       o.agent_payee_id,
       order_RF.OrderRF_code,
       movement_RF.movementRF_code,
       -----------
       o.total_distance AS distance,
       o.order_value,
       m.pro_number,
       m.movement_status,
       m.move_distance,
       o.high_value,
       o.operations_user,
       o.created_by,
       NULL AS trailer_type,
       o.load_weight AS order_weight,
       o.order_type,
       o.order_mode,
       o.order_pieces,
       o.order_mode_id,
       origin.lattitude originLattitude,
       origin.longitude originLongitude,
       dest.lattitude destinationLattitude,
       dest.longitude destinationLongitude,
       (
            SELECT 
			    RTRIM(s.stop_id) id,
			    s.movement_id movementId,
			    s.sched_arrival scheduledArrival,
			    s.sched_departure scheduledDeparture,
			    s.actual_arrival actualArrival,
			    s.actual_departure actualDeparture,
			    NULL address,
			    RTRIM(city) cityName,
			    RTRIM(state) state,
			    RTRIM(zip) zipCode,
			    s.order_id orderId,
			    s.stop_type stopType,
                s.eta,
                s.[move_dist_from_previous]
       --         ,
			    --RTRIM(servicefail.dsp_comment) serviceFailComment,
			    --servicefail.minutes_late serviceFailMinutesLate
            FROM ODS.stops s WITH(NOLOCK)
                --LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.servicefail servicefail
                --    ON s.stop_id = servicefail.stop_id
                --        AND servicefail.company_id = 'TMS'
            WHERE s.order_id = o.order_id
            FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
        ) AS all_stops_info,
        o.total_charge,
        origin.has_hot_comment as OriginHasHotComment,
        origin.hot_comments AS origin_hot_comment,
        dest.[has_hot_comment] as DestHasHotComment,
        dest.hot_comments AS dest_hot_comment,
        m.carrier_contact,
        m.carrier_phone,
        m.carrier_email,
        NULL AS AssignedDriverEmail,
        NULL AS AssignedDriverCellphone,
        o.equipment_type_id TrailerType,
        o.last_modified_date LastUpdateDate,
        o.total_pay,
        o.ordered_date as  BillDate, -----Update this
        o.ops_review_status,
        --CASE 
        --    WHEN m.is_brokered = 'Y' AND brokerage_status IN ('05', '06', '7', '10', '15', '20' ) THEN 'Y' 
        --    WHEN m.is_brokered = 'N' AND ISNULL(o.freight_charge,0) > 0 THEN 'Y'
        --    ELSE 'N' 
        --    END AS Customer_Rating_Done,
        --CASE 
        --    WHEN m.is_brokered = 'Y' AND brokerage_status IN ('20','30','40','50','55','60','65','66','70','75','80','90','95','99') THEN 'Y' 
        --    WHEN m.[order_movement_execution_division] <> o.order_division
        --                AND ma.assignment_status IS NULL THEN 'W'
        --    WHEN m.is_brokered = 'N' AND ma.assignment_status IS NOT NULL THEN 'Y'
        --    ELSE 'N' END AS Carrier_Rating_Done,
        m.order_movement_execution_division,
        origin.zone_id as origin_zone_id,
        dest.zone_id as dest_zone_id,
        o.is_agent_captive_order,
        'N' as is_partial_shipment,
        ma.assignment_status,
        'N' as brokered_not_preferred,
        NULL AS daysOut,
        m.carrier_id as carrier_name,
        NULL as fleet_id,
        o.ready_to_bill,
        o.bill_date,
        CASE WHEN o.ops_review_status = 'Y' THEN o.last_modified_date ELSE NULL END AS ops_closed_date,
        o.freight_charge,
        origin.appt_required origin_appt_required,
        origin.appt_status_code origin_appt_status_code,
        origin.appt_confirmed as origin_appt_confirmed,
        dest.appt_required dest_appt_required,
        dest.appt_status_code dest_appt_status_code,
        dest.appt_confirmed as dest_appt_confirmed,
        o.order_division,
        CASE WHEN o.order_type = 'ODPERM' THEN 10 WHEN o.order_type = 'HRISK' THEN 20 ELSE 100 END AS order_sort_priority,
        CASE WHEN CAST(m.[xmitted2driver] as date) = CAST(getdate() as date) 
            THEN 'Y'
            ELSE 'N' 
        END AS driver_contacted_today,
        m.[xmitted2driver] as driver_contact_date,
        wfst.[workflow_status_code],
        wfst.[next_action],
        m.preassign_sequence
    FROM ODS.orderMaster AS o WITH(NOLOCK)
    LEFT OUTER JOIN ODS.movements AS m WITH(NOLOCK)
        ON o.order_id = m.order_id AND o.company_id = m.company_id AND m.movement_id = o.current_movement_id
    LEFT OUTER JOIN ODS.stops AS origin WITH(NOLOCK)
        ON o.order_id = origin.order_id
           AND origin.stop_id = m.origin_stop_id
           AND origin.movement_id = m.movement_id
    LEFT OUTER JOIN ODS.stops AS dest WITH(NOLOCK)
        ON dest.order_id = m.order_id
           AND dest.stop_id = m.dest_stop_id
           AND dest.movement_id = m.movement_id
    LEFT OUTER JOIN ODS.stops AS order_origin WITH(NOLOCK)
        ON o.order_id = order_origin.order_id
           AND o.origin_stop_id = order_origin.stop_id
    LEFT OUTER JOIN ODS.stops AS order_dest WITH(NOLOCK)
        ON order_dest.order_id = o.order_id
           AND order_dest.stop_id = o.dest_stop_id
    LEFT OUTER JOIN ODS.movement_assets AS ma WITH(NOLOCK)
        ON ma.order_id = m.order_id 
           AND ma.movement_id = m.movement_id
    LEFT OUTER JOIN
    (
        SELECT row_id,
			STRING_AGG(RTRIM(hierarchy_code), ',') AS OrderRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'O' AND row_id IN (SELECT order_id FROM ODS.orderMaster)
		GROUP BY row_id,company_id
    ) AS order_RF
        ON order_RF.row_id = o.order_id
    LEFT OUTER JOIN
    (
        SELECT row_id,STRING_AGG(RTRIM(hierarchy_code), ',') AS movementRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'M' AND row_id IN (SELECT movement_id FROM ODS.movements)
		GROUP BY row_id,company_id
    ) AS movement_RF
        ON movement_RF.row_id = o.current_movement_id
    LEFT OUTER JOIN [ODS].[order_movement_workflow_status] wfst
        ON m.order_id = wfst.order_id AND m.movement_id = wfst.movement_id
WHERE o.order_division = 'FLAT' AND order_status IN ('A','P')

GO

