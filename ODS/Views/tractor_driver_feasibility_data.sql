


CREATE VIEW [ODS].[tractor_driver_feasibility_data]
AS 
----SELECT * FROM ODS.[tractor_driver_feasibility_data] WHERE company_id = 'TMS' AND tractor_id = '103468'
SELECT ma.company_id,
       o.order_id,
       ma.movement_id,
       ma.tractor_id,
       ma.driver1_id driver_id,
       ma.driver2_id,
       ma.trailer1_id,
       ma.trailer2_id,
       ma.tractor_last_location,
       CASE WHEN ma.assignment_status = 'preassigned' THEN 'Y' ELSE 'N' END preassigned,
       origin.city originCity,
       dest.city destinationCity,
       origin.sched_arrival scheduled_pickup,
       dest.sched_departure scheduled_dropoff,
       m.move_distance distance,
       origin.actual_departure actual_pickup,
       dest.actual_departure actual_dropoff,
       origin.longitude originLongitude,
       origin.lattitude originLatitude,
       dest.longitude destinationLongitude,
       dest.lattitude destinationLatitude,
       dest.projected_arrival,
       dest.eta_distance,
       o.order_status
FROM ODS.movement_assets ma WITH(NOLOCK)
    LEFT JOIN ODS.movements m WITH(NOLOCK)
        ON ma.movement_id = m.movement_id
            AND m.company_id = ma.company_id
            AND m.order_id = ma.order_id
    JOIN ODS.orderMaster o WITH(NOLOCK)
        ON ma.order_id = o.order_id
           AND ma.company_id = o.company_id
    JOIN ODS.stops origin WITH(NOLOCK)
        ON o.origin_stop_id = origin.stop_id
           AND origin.company_id = o.company_id
    JOIN ODS.stops dest WITH(NOLOCK)
        ON o.dest_stop_id = dest.stop_id
           AND dest.company_id = o.company_id
WHERE o.order_status IN ( 'A', 'P' )
    --AND ma.company_id = 'TMS'
    --AND ma.tractor_id = '103468  '
UNION ALL

SELECT ma.company_id,
       o.order_id,
       ma.movement_id,
       ma.tractor_id,
       ma.driver1_id driver_id,
       ma.driver2_id,
       ma.trailer1_id,
       ma.trailer2_id,
       ma.tractor_last_location,
       CASE WHEN ma.assignment_status = 'preassigned' THEN 'Y' ELSE 'N' END preassigned,
       origin.city originCity,
       dest.city destinationCity,
       origin.sched_arrival scheduled_pickup,
       dest.sched_departure scheduled_dropoff,
       m.move_distance distance,
       origin.actual_departure actual_pickup,
       dest.actual_departure actual_dropoff,
       origin.longitude originLongitude,
       origin.lattitude originLatitude,
       dest.longitude destinationLongitude,
       dest.lattitude destinationLatitude,
       dest.projected_arrival,
       dest.eta_distance,
       o.order_status
FROM ODS.movement_assets ma WITH(NOLOCK)
    LEFT JOIN ODS.movements m WITH(NOLOCK)
        ON m.movement_id = ma.movement_id
            AND m.company_id = ma.company_id
            AND m.order_id = ma.order_id
    LEFT JOIN ODS.orderMaster o WITH(NOLOCK)
        ON ma.order_id = o.order_id
           AND ma.company_id = o.company_id
    LEFT JOIN ODS.stops origin WITH(NOLOCK)
        ON o.origin_stop_id = origin.stop_id
           AND origin.company_id = o.company_id
    JOIN ODS.stops dest WITH(NOLOCK)
        ON o.dest_stop_id = dest.stop_id
           AND dest.company_id = o.company_id
WHERE o.order_status IN ( 'D' )
--AND ma.company_id = 'TMS' AND ma.tractor_id = '103468'

GO

