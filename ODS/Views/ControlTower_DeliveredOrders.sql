



--SELECT * FROM [ODS].[ControlTower_DeliveredOrders] ORDER BY order_id
CREATE VIEW [ODS].[ControlTower_DeliveredOrders]
AS
   SELECT o.company_id,
       o.customer_id,
       o.customer_name,
       o.order_id,
       m.movement_id,
       m.is_brokered,
       m.brokerage_status,       
       o.order_status as order_status_description,
       RTRIM(origin.city) AS origin_city,
       RTRIM(origin.state) AS origin_state,
       RTRIM(origin.zip) AS origin_zip,
       origin.location_code AS origin_location_code,
       origin.location_name AS origin_location_name,
       origin.sched_arrival AS orderOriginpickUpDate,
       RTRIM(dest.city) AS dest_city,
       RTRIM(dest.state) AS dest_state,
       RTRIM(dest.zip) AS dest_zip,
       RTRIM(origin.city) AS movement_origin_city,
       RTRIM(origin.state) AS movement_origin_state,
       RTRIM(origin.zip) AS movement_origin_zip,
       RTRIM(dest.city) AS movement_dest_city,
       RTRIM(dest.state) AS movement_dest_state,
       RTRIM(dest.zip) AS movement_dest_zip,
       LEFT(RTRIM(origin.zip),3) AS origin_zip3,
       LEFT(RTRIM(dest.zip),3) AS dest_zip3,
       dest.location_code AS dest_location_code,
       dest.location_name AS dest_location_name,
       dest.sched_arrival AS orderDestpickUpDate,
       (
           SELECT MIN(movement_sequence)
           FROM ODS.stops
           WHERE order_id = o.order_id
                 AND movement_id = o.current_movement_id
                 AND actual_arrival IS NOT NULL
       ) AS current_stop_sequence,
       (
           SELECT COUNT(*)
           FROM ODS.stops
           WHERE order_id = o.order_id
                 AND movement_id = o.current_movement_id
       ) AS stop_count,
       ma.driver1_id,
       ma.driver2_id,
       ma.driver_dot_info,
       m.carrier_id,
       ma.tractor_id,
       ma.trailer1_id,
       ma.trailer2_id,
       order_RF.OrderRF_code,
       movement_RF.movementRF_code,
       --------
	   o.total_distance orders_bill_distance,
       o.order_value,
       m.pro_number,
	   m.movement_status,
       m.move_distance,
       o.high_value,
       o.operations_user,
       o.created_by,
       NULL AS trailer_type,
       o.load_weight AS order_weight,
	   o.order_type,
       o.order_mode,
       o.order_pieces,
       o.order_status,
       NULL AS orders_Freight_charge,
       origin.sched_arrival pickUpDate,
       dest.sched_arrival DeliveryDate,
	   o.current_movement_id,
       o.total_charge AS orders_Total_charge,
       origin.has_hot_comment as OriginHasHotComment,
        origin.hot_comments AS origin_hot_comment,
        dest.[has_hot_comment] as DestHasHotComment,
        dest.hot_comments AS dest_hot_comment,
	   o.equipment_type_id AS equipment_type,
	   NULL AS order_commodity,
        o.carrier_pay AS carrierPay,
        o.total_pay AS totalPay,
        o.carrierOtherPaySum AS carrierOtherPaySum,
        o.on_hold AS orders_On_hold,
        o.hold_reason AS orders_Hold_reason,
        o.orders_Blnum AS orders_Blnum,
        m.is_brokered brokered,
        m.brokerage_status AS movement_brokerage_status,
        o.ready_to_bill,
        o.bill_date,
        CASE WHEN o.ops_review_status = 'Y' THEN o.last_modified_date ELSE NULL END AS ops_closed_date,
        order_mode_id,
        origin.lattitude originLattitude,
        origin.longitude originLongitude,
        dest.lattitude destLattitude,
        dest.longitude destLongitude,
        NULL AS movement_capacity_rep,
        NULL AS movement_carrier_contact,
        NULL AS movement_carrier_phone,
        (
            SELECT 
			    RTRIM(s.stop_id) id,
			    s.movement_id movementId,
			    s.sched_arrival scheduledArrival,
			    s.sched_departure scheduledDeparture,
			    s.actual_arrival actualArrival,
			    s.actual_departure actualDeparture,
			    NULL address,
			    RTRIM(city) cityName,
			    RTRIM(state) state,
			    RTRIM(zip) zipCode,
			    NULL AS eta,
			    s.order_id orderId,
			    s.stop_type stopType,
			    RTRIM(servicefail.dsp_comment) serviceFailComment,
			    servicefail.minutes_late serviceFailMinutesLate
            FROM ODS.stops s
                LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.servicefail servicefail
                    ON s.stop_id = servicefail.stop_id
                        AND servicefail.company_id = 'TMS'
            WHERE s.order_id = o.order_id
            FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
        ) AS all_stops_info,
        NULL AS orders_Temperature_max,
        NULL AS orders_Temperature_min,
        NULL AS portal_operations_last_update_dt,
        agent_payee_id AS movement_Override_payee_id, ---UPDATE THIS TO MOVEMENT PAYEE ID
        order_mode AS orders_Ltl,
        NULL movement_Pro_nbr,
        o.created_by portal_order_create_user_id,
        NULL AS movement_Carrier_email,
        agent_payee_id AS orders_Agent_payee_id,
        o.ordered_date AS portal_order_create_dt,
        o.operations_user portal_operations_user,
        'N' AS originAppt_required,                   --------GET APPOINTMENT REQ in DATA
        NULL AS originConfirmed,
        'N' AS destinationAppt_required,
        NULL AS destinationConfirmed,
        NULL AS orders_Equipment_type_id,
        o.[ops_review_status] orders_Ops_rev_complete,
        'Y' AS Customer_Rating_Done,
        'Y' AS Carrier_Rating_Done,
        movement_RF.movementRF_code AS order_movement_execution_division,
        origin.zone_id as origin_zone_id,
        dest.zone_id as dest_zone_id,
        o.is_agent_captive_order,
        'N' as is_partial_shipment,
        ma.assignment_status,
        'N' as brokered_not_preferred,
        NULL AS daysOut,
        m.carrier_id as carrier_name,
        NULL as fleet_id,
        origin.appt_required origin_appt_required,
        origin.appt_status_code origin_appt_status_code,
        origin.appt_confirmed as origin_appt_confirmed,
        dest.appt_required dest_appt_required,
        dest.appt_status_code dest_appt_status_code,
        dest.appt_confirmed as dest_appt_confirmed,
        CASE WHEN o.order_type IN ('ODPERM','HRISK') THEN 10 ELSE 100 END AS order_sort_priority,
        CASE WHEN CAST(m.[xmitted2driver] as date) = CAST(getdate() as date) 
            THEN 'Y'
            ELSE 'N' 
        END AS driver_contacted_today,
        m.[xmitted2driver] as driver_contact_date,
        wfst.[workflow_status_code],
        wfst.[next_action],
        origin.showas_address origin_showas_address,
        origin.showas_address2 origin_showas_address2,
        origin.showas_city_id origin_showas_city_id,
        origin.showas_city_name origin_showas_city_name,
        origin.showas_location_id origin_showas_location_id,
        origin.showas_location_name origin_showas_location_name,
        origin.showas_state origin_showas_state,
        origin.showas_zip_code origin_showas_zip_code,
        dest.showas_address dest_showas_address,
        dest.showas_address2 dest_showas_address2,
        dest.showas_city_id dest_showas_city_id,
        dest.showas_city_name dest_showas_city_name,
        dest.showas_location_id dest_showas_location_id,
        dest.showas_location_name dest_showas_location_name,
        dest.showas_state dest_showas_state,
        dest.showas_zip_code dest_showas_zip_code,
        o.[subject_order_number],
        o.[subject_order_status],
        o.[subject_order_void_date],
        m.preassign_sequence
--------------
    FROM ODS.orderMaster o
    LEFT JOIN ODS.movements m
        ON o.order_id = m.order_id AND o.current_movement_id = m.movement_id AND o.company_id = m.company_id
    LEFT JOIN ODS.stops origin
        ON o.order_id = origin.order_id
		AND ISNULL(o.origin_stop_id,m.origin_stop_id) = origin.stop_id
    LEFT JOIN ODS.stops dest
        ON o.order_id = dest.order_id
		AND ISNULL(o.dest_stop_id,m.dest_stop_id) = dest.stop_id
    LEFT JOIN ODS.movement_assets ma
        ON o.order_id = ma.order_id
           AND o.current_movement_id = ma.movement_id
    LEFT JOIN
    (
        SELECT row_id,
			STRING_AGG(RTRIM(hierarchy_code), ',') AS OrderRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'O' AND row_id IN (SELECT order_id FROM ODS.orderMaster)
		GROUP BY row_id,company_id
    ) order_RF
        ON order_RF.row_id = o.order_id
    LEFT JOIN
    (
        SELECT row_id,STRING_AGG(RTRIM(hierarchy_code), ',') AS movementRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'M' AND row_id IN (SELECT movement_id FROM ODS.movements)
		GROUP BY row_id,company_id
    ) movement_RF
        ON movement_RF.row_id = o.current_movement_id
    LEFT OUTER JOIN [ODS].[order_movement_workflow_status] wfst
        ON m.order_id = wfst.order_id AND m.movement_id = wfst.movement_id
WHERE o.order_status = 'D'

GO

