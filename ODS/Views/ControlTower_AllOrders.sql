












--SELECT * FROM [ODS].[ControlTower_AllOrders] WHERE company_id = 'T100'
CREATE VIEW [ODS].[ControlTower_AllOrders]
AS
    SELECT 
        o.order_id,
        o.company_id,
        o.customer_id,
        o.customer_name,
        CASE o.order_status WHEN 'A' THEN 'Available' WHEN 'P' THEN 'Progress' WHEN 'V' THEN 'Void' WHEN 'D' THEN 'Delivered' WHEN 'Q' THEN 'Quote' END AS order_status_description,
        RTRIM(origin.city) + ' ' + RTRIM(origin.state) + ', ' + RTRIM(origin.zip) AS order_origin_address,
        RTRIM(origin.city) origin_city,
        RTRIM(origin.state) origin_state,
        RTRIM(origin.zip) origin_zip,
        RTRIM(dest.city) + ' ' + RTRIM(dest.state) + ', ' + RTRIM(dest.zip) AS order_destination_address,
        RTRIM(dest.city) destination_city,
        RTRIM(dest.state) destination_state,
        RTRIM(dest.zip) destination_zip,
        RTRIM(origin.city) AS movement_origin_city,
        RTRIM(origin.state) AS movement_origin_state,
        RTRIM(origin.zip) AS movement_origin_zip,
        RTRIM(dest.city) AS movement_dest_city,
        RTRIM(dest.state) AS movement_dest_state,
        RTRIM(dest.zip) AS movement_dest_zip,
        o.total_charge AS orders_Total_charge,
        o.other_charges_total AS orders_other_charges_total,
        o.equipment_type_id AS equipment_type,
        origin.location_name AS origin_location_name,
        origin.location_code AS origin_location_id,
        dest.location_name AS destination_location_name,
        dest.location_code AS destination_location_id,
        NULL AS order_commodity,
        o.total_distance orders_bill_distance,
        o.total_charge AS orders_Freight_charge,  ---needs review
        origin.sched_arrival pickUpDate,
        dest.sched_arrival DeliveryDate,
        origin.actual_arrival ActualPickUpDate,
        dest.actual_departure ActualDeliveryDate,
        o.current_movement_id,
        o.order_value,
        o.order_type,
        o.order_status,
        (
            SELECT MIN(movement_sequence)
            FROM ODS.stops WITH(NOLOCK)
            WHERE order_id = o.order_id
                    AND movement_id = o.current_movement_id
                    AND actual_arrival IS NOT NULL
        ) AS current_stop_sequence,
        (
            SELECT COUNT(*)
            FROM ODS.stops WITH(NOLOCK)
            WHERE order_id = o.order_id
                    AND movement_id = o.current_movement_id
        ) AS stop_count,
        (
            SELECT COUNT(*)
            FROM ODS.movements WITH(NOLOCK)
            WHERE order_id = o.order_id                    
        ) AS movement_count,
        ma.driver1_id,
        ma.driver2_id,
        m.carrier_id,
        ma.tractor_id,
       ma.trailer1_id,
       ma.trailer2_id,
       o.agent_payee_id,
        order_RF.OrderRF_code,
        movement_RF.movementRF_code,
        ----
        m.carrierPay AS carrierPay,
        o.total_pay AS totalPay,
        carrierOtherPaySum,
        o.on_hold AS orders_On_hold,
        o.hold_reason AS orders_Hold_reason,
        orders_Blnum AS orders_Blnum,
        m.is_brokered brokered,
        m.brokerage_status AS movement_brokerage_status,
        o.ready_to_bill,
        o.bill_date,
        CASE WHEN o.ops_review_status = 'Y' THEN o.last_modified_date ELSE NULL END AS ops_closed_date,
        order_mode,
        order_mode_id,
        origin.lattitude originLattitude,
        origin.longitude originLongitude,
        dest.lattitude destLattitude,
        dest.longitude destLongitude,
        operations_user,
        m.capacity_rep AS movement_capacity_rep,
        m.carrier_contact AS movement_carrier_contact,
        m.carrier_phone AS movement_carrier_phone,
        (
            SELECT 
			    RTRIM(s.stop_id) id,
			    s.movement_id movementId,
			    s.sched_arrival scheduledArrival,
			    s.sched_departure scheduledDeparture,
			    s.actual_arrival actualArrival,
			    s.actual_departure actualDeparture,
			    NULL address,
			    RTRIM(city) cityName,
			    RTRIM(state) state,
			    RTRIM(zip) zipCode,
			    NULL AS eta,
			    s.order_id orderId,
			    s.stop_type stopType,
                s.movement_sequence,
			    RTRIM(servicefail.dsp_comment) serviceFailComment,
			    servicefail.minutes_late serviceFailMinutesLate
            FROM ODS.stops s WITH(NOLOCK)
                LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.servicefail servicefail
                    ON s.stop_id = servicefail.stop_id
                        AND servicefail.company_id = o.company_id
            WHERE s.order_id = o.order_id
            FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
        ) AS all_stops_info,
        o.temperature_max AS orders_Temperature_max,
        o.temperature_min AS orders_Temperature_min,
        o.last_modified_date AS portal_operations_last_update_dt,
        agent_payee_id AS movement_Override_payee_id, ---UPDATE THIS TO MOVEMENT PAYEE ID
        order_mode AS orders_Ltl,
        m.pro_number AS movement_Pro_nbr,
        o.order_pieces,
        o.load_weight AS order_weight,
        o.created_by portal_order_create_user_id,
        m.carrier_email AS movement_Carrier_email,
        agent_payee_id AS orders_Agent_payee_id,
        m.movement_status,
        o.ordered_date AS portal_order_create_dt,
        o.operations_user portal_operations_user,
        'N' AS originAppt_required,                   --------GET APPOINTMENT REQ in DATA
        NULL AS originConfirmed,
        'N' AS destinationAppt_required,
        NULL AS destinationConfirmed,
        o.equipment_type_id AS orders_Equipment_type_id,
        o.[ops_review_status] orders_Ops_rev_complete,
        o.agency_id orders_agency_id,
        origin.stop_type origin_stop_type,
        NULL as movement_fleet_manager,
        o.total_distance movement_Fuel_distance  ---TBD
        ,
        CASE 
            WHEN ISNULL(m.is_brokered,'N') = 'Y' AND brokerage_status IN ('05', '06', '7', '10', '15', '20','30','40','50','55','60','65','66','70','75','80','90','95','99') THEN 'Y' 
            WHEN ISNULL(m.is_brokered,'N') = 'N' AND ISNULL(o.freight_charge,0) > 0 THEN 'Y'
            ELSE 'N' 
            END AS Customer_Rating_Done,
        CASE 
            WHEN ISNULL(m.is_brokered,'N') = 'Y' AND brokerage_status IN ('20','30','40','50','55','60','65','66','70','75','80','90','95','99') THEN 'Y' 
            WHEN ISNULL(m.is_brokered,'N') = 'N' AND o.order_division <> m.order_movement_execution_division --NOT IN (SELECT * FROM STRING_SPLIT(movementRF_code,',') WHERE value IN ('INTG','EXPD','DEDW','DEDE','FLAT'))
                        AND ma.assignment_status IS NULL THEN 'W'
            WHEN ISNULL(m.is_brokered,'N') = 'N' AND ma.assignment_status IS NOT NULL THEN 'Y'
            ELSE 'N' END AS Carrier_Rating_Done,
        o.freight_charge,
        ISNULL((SELECT top 1 value FROM STRING_SPLIT(movementRF_code,',') WHERE value IN ('INTG','EXPD','DEDW','DEDE','FLAT') 
        AND value <> o.order_division),m.order_movement_execution_division) order_movement_execution_division,
        o.order_division,
        CASE WHEN o.order_type IN ('ODPERM','HRISK') THEN 10 ELSE 100 END AS order_sort_priority,
        CASE WHEN CAST(m.[xmitted2driver] as date) = CAST(getdate() as date) 
            THEN 'Y'
            ELSE 'N' 
        END AS driver_contacted_today,
        m.[xmitted2driver] as driver_contact_date,
        ----------
        CASE 
            WHEN o.order_division <> m.order_movement_execution_division AND ma.assignment_status IS NULL AND wfst.workflow_status_code IN ('10') THEN '20' 
            WHEN o.order_division <> m.order_movement_execution_division AND ma.assignment_status IS NOT NULL AND wfst.workflow_status_code = '20' THEN '30'
            ELSE wfst.workflow_status_code 
        END workflow_status_code ,
        CASE
            WHEN o.order_division <> m.order_movement_execution_division AND ma.assignment_status IS NOT NULL AND wfst.workflow_status_code = '20' THEN 'PICKUP' 
            ELSE wfst.next_action
        END next_action,
        ------------
        ma.assignment_status,
        origin.showas_address origin_showas_address,
        origin.showas_address2 origin_showas_address2,
        origin.showas_city_id origin_showas_city_id,
        origin.showas_city_name origin_showas_city_name,
        origin.showas_location_id origin_showas_location_id,
        origin.showas_location_name origin_showas_location_name,
        origin.showas_state origin_showas_state,
        origin.showas_zip_code origin_showas_zip_code,
        dest.showas_address dest_showas_address,
        dest.showas_address2 dest_showas_address2,
        dest.showas_city_id dest_showas_city_id,
        dest.showas_city_name dest_showas_city_name,
        dest.showas_location_id dest_showas_location_id,
        dest.showas_location_name dest_showas_location_name,
        dest.showas_state dest_showas_state,
        dest.showas_zip_code dest_showas_zip_code,
        o.[subject_order_number],
        o.[subject_order_status],
        o.[subject_order_void_date],
        origin.has_hot_comment as OriginHasHotComment,
        origin.hot_comments AS origin_hot_comment,
        dest.[has_hot_comment] as DestHasHotComment,
        dest.hot_comments AS dest_hot_comment,
        (SELECT STRING_AGG(RTRIM(ref.reference_number),',')
        FROM  [QA-McLeod-RPT].lme_qa.dbo.reference_number ref 
        WHERE ref.stop_id IN (SELECT stop_id from ODS.stops s WHERE s.order_id = o.order_id)
        AND ref.company_id = o.company_id) order_stop_reference_numbers

	FROM ODS.orderMaster o WITH(NOLOCK)
	LEFT JOIN ODS.movements m WITH(NOLOCK)
	ON o.order_id = m.order_id AND o.current_movement_id = m.movement_id AND o.company_id = m.company_id
	LEFT JOIN ODS.stops origin WITH(NOLOCK)
	ON o.order_id = origin.order_id
		AND ISNULL(o.origin_stop_id,m.origin_stop_id) = origin.stop_id
	LEFT JOIN ODS.stops dest WITH(NOLOCK)
	ON o.order_id = dest.order_id
		AND ISNULL(o.dest_stop_id,m.dest_stop_id) = dest.stop_id
	LEFT JOIN ODS.movement_assets ma WITH(NOLOCK)
	ON o.order_id = ma.order_id
		AND o.current_movement_id = ma.movement_id
	LEFT JOIN
	(
		SELECT row_id,
			STRING_AGG(RTRIM(hierarchy_code), ',') AS OrderRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'O' AND row_id IN (SELECT order_id FROM ODS.orderMaster)
		GROUP BY row_id,company_id
	) order_RF
	ON order_RF.row_id = o.order_id AND order_RF.company_id = o.company_id
	LEFT JOIN
	(
		SELECT row_id,STRING_AGG(RTRIM(hierarchy_code), ',') AS movementRF_code, company_id
		FROM ODS.responsibility_history rh WITH(NOLOCK)
		WHERE rh.row_type = 'M' AND row_id IN (SELECT movement_id FROM ODS.movements)
		GROUP BY row_id,company_id
	) movement_RF
	ON movement_RF.row_id = o.current_movement_id AND movement_RF.company_id = o.company_id
    LEFT OUTER JOIN [ODS].[order_movement_workflow_status] wfst WITH(NOLOCK)
        ON m.order_id = wfst.order_id AND m.movement_id = wfst.movement_id

GO

