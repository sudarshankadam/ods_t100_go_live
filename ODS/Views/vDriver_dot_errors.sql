-- ODS.vDriver_dot_errors source

CREATE VIEW [ODS].[vDriver_dot_errors]
AS
SELECT *
FROM
(
    SELECT d.driver_id,
           CASE
               WHEN DATEDIFF(day, GETDATE(), license_date)
                    BETWEEN 1 AND 30 THEN
                   'CDL expires ' + CONVERT(VARCHAR,license_date,101)
               WHEN DATEDIFF(day, GETDATE(), license_date) < 0 THEN
                   'CDL expired ' + CONVERT(VARCHAR,license_date,101)
               ELSE
                   NULL
           END AS License_Error,
           CASE
               WHEN DATEDIFF(day, GETDATE(), license_date)
                    BETWEEN 1 AND 30 THEN
                   'Warn and Continue'
               WHEN DATEDIFF(day, GETDATE(), license_date) < 0 THEN
                   'Stop Dispatch'
               ELSE
                   NULL
           END License_Allow_Dispatch,
           CASE
               WHEN DATEDIFF(day, GETDATE(), physical_date)
                    BETWEEN 1 AND 30 THEN
                   'Physical due by ' + CONVERT(VARCHAR,physical_date,101)
               WHEN DATEDIFF(day, GETDATE(), physical_date) < 0 THEN
                   'Physical was due by ' + CONVERT(VARCHAR,physical_date,101)
               ELSE
                   NULL
           END AS physical_Error,
           CASE
               WHEN DATEDIFF(day, GETDATE(), physical_date)
                    BETWEEN 1 AND 30 THEN
                   'Warn and Continue'
               WHEN DATEDIFF(day, GETDATE(), physical_date) < 0 THEN
                   'Stop Dispatch'
               ELSE
                   NULL
           END physical_Allow_Dispatch,
           CASE
               WHEN DATEDIFF(day, GETDATE(), mvr_date)
                    BETWEEN 1 AND 30 THEN
                   'MVR date expires ' + CONVERT(VARCHAR,mvr_date,101)
               WHEN DATEDIFF(day, GETDATE(), mvr_date) < 0 THEN
                   'MVR date expired ' + CONVERT(VARCHAR,mvr_date,101)
               ELSE
                   NULL
           END AS mvr_date_Error,
           CASE
               WHEN DATEDIFF(day, GETDATE(), mvr_date)
                    BETWEEN 1 AND 30 THEN
                   'Warn and Continue'
               WHEN DATEDIFF(day, GETDATE(), mvr_date) < 0 THEN
                   'Stop Dispatch'
               ELSE
                   NULL
           END mvr_date_Allow_Dispatch,
           CASE
               WHEN DATEDIFF(day, GETDATE(), hm126c_date)
                    BETWEEN 1 AND 30 THEN
                   'hm126c expires ' + CONVERT(VARCHAR,hm126c_date,101)
               WHEN DATEDIFF(day, GETDATE(), hm126c_date) < 0 THEN
                   'hm126c expired ' + CONVERT(VARCHAR,hm126c_date,101)
               ELSE
                   NULL
           END AS hm126c_date_Error,
           CASE
               WHEN DATEDIFF(day, GETDATE(), hm126c_date)
                    BETWEEN 1 AND 30 THEN
                   'Warn and Continue'
               WHEN DATEDIFF(day, GETDATE(), hm126c_date) < 0 THEN
                   'Stop Dispatch'
               ELSE
                   NULL
           END hm126c_date_Allow_Dispatch,
           CASE
               WHEN DATEDIFF(day, GETDATE(), hazmat_date)
                    BETWEEN 1 AND 30 THEN
                   'Hazmat cert date expires ' + CONVERT(VARCHAR,hazmat_date,101)
               WHEN DATEDIFF(day, GETDATE(), hazmat_date) < 0 THEN
                   'Hazmat cert date expired ' + CONVERT(VARCHAR,hazmat_date,101)
               ELSE
                   NULL
           END AS hazmat_date_Error,
           CASE
               WHEN DATEDIFF(day, GETDATE(), hazmat_date)
                    BETWEEN 1 AND 30 THEN
                   'Warn and Continue'
               WHEN DATEDIFF(day, GETDATE(), hazmat_date) < 0 THEN
                   'Stop Dispatch'
             ELSE
                   NULL
           END hazmat_date_Allow_Dispatch,
           CASE
               WHEN DATEDIFF(day, GETDATE(), medical_cert_expire)
                    BETWEEN 1 AND 30 THEN
                   'Medical Certificate expires ' + CONVERT(VARCHAR,medical_cert_expire,101)
               WHEN DATEDIFF(day, GETDATE(), medical_cert_expire) < 0 THEN
                   'Medical Certificate expired ' + CONVERT(VARCHAR,medical_cert_expire,101)
               ELSE
                   NULL
           END AS medical_certificate_date_Error,
           CASE
               WHEN DATEDIFF(day, GETDATE(), medical_cert_expire)
                    BETWEEN 1 AND 30 THEN
                   'Warn and Continue'
               WHEN DATEDIFF(day, GETDATE(), medical_cert_expire) < 0 THEN
                   'Stop Dispatch'
               ELSE
                   NULL
           END medical_cert_expire_Allow_Dispatch,
           d.group_id as fleet_id,
           fm.agency_id as fleet_agency_id,
           CASE WHEN d.operation_type IN ('C','S') THEN 'Y' ELSE 'N' END as is_codriver
        
    FROM ODS.driver_master d WITH (NOLOCK) JOIN ODS.fleet_master fm WITH(NOLOCK) ON d.driver_id = fm.asset_id AND fm.asset_type = 'D'
    WHERE is_active = 'Y'
) DOT_Flags
WHERE COALESCE(
                  License_Error,
                  License_Allow_Dispatch,
                  physical_Error,
                  physical_Allow_Dispatch,
                  mvr_date_Error,
                  mvr_date_Allow_Dispatch,
                  hm126c_date_Error,
                  hm126c_date_Allow_Dispatch,
                  hazmat_date_Error,
                  hazmat_date_Allow_Dispatch,
                  medical_certificate_date_Error,
                  medical_cert_expire_Allow_Dispatch,
                  NULL
              ) IS NOT NULL;

GO

