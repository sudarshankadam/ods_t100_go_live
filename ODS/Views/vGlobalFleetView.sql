
-- ODS.vGlobalFleetView source

-- ODS.vGlobalFleetView source

-- ODS.vGlobalFleetView source

/*
SELECT * FROM [ODS].[vGlobalFleetView]  WHERE fleet_id = 'DSOSA'
*/
CREATE VIEW [ODS].[vGlobalFleetView]
AS
    SELECT ROW_NUMBER() OVER(order by fm.fleet_id) SrNo,* FROM (
    SELECT       fm.fleet_id,
           fm.fleet_descr,
           fm.[agency_id],
           fm.company_id,
           ----Tractor Card
           tr.id tractor_id,
           
           tr.inspection_date tractor_inspection_date,
           tr.outservice_date tractor_outservice_date,
           tr.service_status tractor_service_status,
           tr.owner tractor_owner,
           tr.pay_owner tractor_pay_owner,
           tr.owner_op tractor_owner_op,
           ------
           ----driver card
           CASE WHEN fm.asset_type = 'D' THEN fm.asset_id ELSE d.driver_id END AS driver_id,
           ISNULL(RTRIM(d.first_name) + ' ', '') + ISNULL(RTRIM(d.name), '') driver_name,
           d.status_code AS driver_status,
           ISNULL(d.cell_phone,d.phone) driver_phone_number,
           d.email AS driver_email,
           d.driver_last_location,
        --shophine edit begin 
        --commented the below one for driver_eld_location and updated to include date
--           (SELECT top 1 near_s_city FROM [QA-McLeod-RPT].lme_qa.dbo.mc_position WHERE driver_id = d.driver_id ORDER BY position_date DESC) driver_eld_location,
          (SELECT top 1 near_s_city+', '+ Format(position_date,'MM/dd/yyyy HH:mm') FROM [QA-McLeod-RPT].lme_qa.dbo.mc_position WHERE driver_id = d.driver_id ORDER BY position_date DESC) driver_eld_location,
     	 --shophine edit end
           
          (SELECT top 1 equipment_type_id FROM ODS.equipment_issued e  WITH(NOLOCK) WHERE asset_type = 'Driver' AND equipment_type_id = 'FELM' AND issued_to_asset = d.driver_id) ELD_type,
           ---Driver Messages
           --SELECT * FROM mc_message WHERE driver_id = 'GWEEKLE ' AND is_current = 'Y' AND message_type = 'I' ORDER BY message_recvd_date DESC
           ISNULL(RTRIM(d.city) + ', ', '') + ISNULL(RTRIM(d.state), '') driver_home,
           d.avl_drive_hours driver_avl_drive_hours,
           d.is_active driver_is_active,
           d.pta_date driver_pta_date,
           d.next_home_date driver_next_home_date,
           d.home_duration driver_home_duration,
           --d.fleet_manager driver_manager,
           d.driver_manager_name driver_manager,
           d.driver_manager_name,
           d.avl_as_of_date driver_avl_as_of_date,
           d.license_date driver_license_date,
           '' AS driver_DOT_ALERTS_JSON,
           CASE WHEN tr.driver1_id IS NOT NULL THEN 'Y' ELSE 'N' END AS driver_has_tractor,
           CASE WHEN tr.driver2_id IS NOT NULL THEN 'Y' ELSE 'N' END AS is_team_driver,
           tr.driver2_id as codriver_id,
           ISNULL(RTRIM(cd.first_name) + ' ', '') + ISNULL(RTRIM(cd.name), '') codriver_name,
           (SELECT TOP 1 mobile_phone
               FROM    [QA-McLeod-RPT].lme_qa.dbo.contact c
               WHERE
                      c.parent_row_id = tr.driver2_id
                      and c.company_id =  'T100'
                      and c.parent_row_type = 'd'
                      and c.title in ('DRIVER','SELF')
                      and c.is_active = 'y'
              ORDER BY sequence ) AS codriver_phone,
           -----Trailer Card
           tl.id trailer1_id,
           tl.statuscode trailer_status,
           tl.trailer_type,
           tl.length_of trailer_length,
           tl.weight trailer_weight,
           tl.width trailer_width,
           tl.is_active trailer_is_active,
           
           
           ------- Order Card
           ---Order indicators
           o.high_value order_high_value,
           o.operations_user,
           NULL AS ops_user_phone,
           NULL AS ops_user_email,
           o.agent_payee_id as booking_source,
           --------
           --CASE 
           --     WHEN order_status = 'P' AND order_type = 'ODPERM' THEN 2 
           --     WHEN order_status = 'P' AND order_type = 'HRISK' THEN 1 
           --     ELSE 0 
           -- END AS sort_rank,
           ISNULL(SUM(driver_sort_rank) OVER(PARTITION BY o.driver1_id),0) driver_sort_rank,
           --------
           ISNULL(o.order_id,'') as order_id,
           o.order_type,
           o.customer_id,
           o.customer_name,
           ISNULL(o.order_status,'')order_status,
           o.order_status_description,
           ISNULL(o.movement_id,'') as movement_id,
           o.origin_location_code origin_location_id,
           o.origin_location_name,
           o.origin_city origin_city,
           o.origin_state origin_state,
           o.origin_zip origin_zip,
           o.dest_location_name dest_location_id,
           o.dest_location_name,
           o.dest_city,
           o.dest_state,
           o.dest_zip,
           o.assignment_status,
           o.order_Origin_pickUpDate,
           o.order_Origin_Actual_pickUpDate,
           o.order_Dest_DeliveryDate,
           o.order_Dest_Actual_DeliveryDate,
           o.stop_count,
           o.all_stops_info,
           o.current_stop_sequence,
           o.distance order_distance,
           ISNULL(o.order_sort_priority,0) order_sort_priority,
           o.hazmat,
        o.preassign_sequence
    FROM ODS.fleet_master fm  WITH(NOLOCK)
        LEFT JOIN ODS.tractor_master tr  WITH(NOLOCK)
            ON fm.asset_id = tr.id
        JOIN ODS.driver_master d  WITH(NOLOCK)
            ON tr.driver1_id = d.driver_id
        LEFT JOIN ODS.vGlobalFleetView_Orders o  WITH(NOLOCK)
            ON fm.asset_id = (CASE
                                  WHEN fm.asset_type = 'T' THEN
                                      O.tractor_id
                                  ELSE
                                      O.driver1_id
                              END
                             )
        LEFT JOIN ODS.trailer_master tl  WITH(NOLOCK)
            ON o.trailer1_id = tl.id
        LEFT JOIN ODS.driver_master cd  WITH(NOLOCK)
            ON tr.driver2_id = cd.driver_id
    WHERE fm.asset_id NOT IN (SELECT asset_id FROM ODS.fleet_drivers_without_tractors())
    UNION
    SELECT       fm.fleet_id,
           fm.fleet_descr,
           fm.[agency_id],
           fm.company_id,
           ----Tractor Card
           tr.id tractor_id,
           
           tr.inspection_date tractor_inspection_date,
           tr.outservice_date tractor_outservice_date,
           tr.service_status tractor_service_status,
           tr.owner tractor_owner,
           tr.pay_owner tractor_pay_owner,
           tr.owner_op tractor_owner_op,
           ------
           ----driver card
           CASE WHEN fm.asset_type = 'D' THEN fm.asset_id ELSE d.driver_id END AS driver_id,
           ISNULL(RTRIM(d.first_name) + ' ', '') + ISNULL(RTRIM(d.name), '') driver_name,
           d.status_code AS driver_status,
           ISNULL(d.cell_phone,d.phone) driver_phone_number,
           d.email AS driver_email,
           d.driver_last_location,
           (SELECT top 1 near_s_city FROM [QA-McLeod-RPT].lme_qa.dbo.mc_position WHERE driver_id = d.driver_id ORDER BY position_date DESC) driver_eld_location,
           (SELECT top 1 equipment_type_id FROM ODS.equipment_issued e  WITH(NOLOCK) WHERE asset_type = 'Driver' AND equipment_type_id = 'FELM' AND issued_to_asset = d.driver_id) ELD_type,
           ---Driver Messages
           --SELECT * FROM mc_message WHERE driver_id = 'GWEEKLE ' AND is_current = 'Y' AND message_type = 'I' ORDER BY message_recvd_date DESC
           ISNULL(RTRIM(d.city) + ', ', '') + ISNULL(RTRIM(d.state), '') driver_home,
           d.avl_drive_hours driver_avl_drive_hours,
           d.is_active driver_is_active,
           d.pta_date driver_pta_date,
           d.next_home_date driver_next_home_date,
           d.home_duration driver_home_duration,
           --d.fleet_manager driver_manager,
           d.driver_manager_name driver_manager,
           d.driver_manager_name,
           d.avl_as_of_date driver_avl_as_of_date,
           d.license_date driver_license_date,
           '' AS driver_DOT_ALERTS_JSON,
           CASE WHEN tr.driver1_id IS NOT NULL THEN 'Y' ELSE 'N' END AS driver_has_tractor,
           CASE WHEN tr.driver2_id IS NOT NULL THEN 'Y' ELSE 'N' END AS is_team_driver,
           tr.driver2_id as codriver_id,
           ISNULL(RTRIM(cd.first_name) + ' ', '') + ISNULL(RTRIM(cd.name), '') codriver_name,
           (SELECT TOP 1 mobile_phone
               FROM    [QA-McLeod-RPT].lme_qa.dbo.contact c
               WHERE
                      c.parent_row_id = tr.driver2_id
                      and c.company_id =  'T100'
                      and c.parent_row_type = 'd'
                      and c.title in ('DRIVER','SELF')
                      and c.is_active = 'y'
              ORDER BY sequence ) AS codriver_phone,
           -----Trailer Card
           tl.id trailer1_id,
           tl.statuscode trailer_status,
           tl.trailer_type,
           tl.length_of trailer_length,
           tl.weight trailer_weight,
           tl.width trailer_width,
           tl.is_active trailer_is_active,
           
           
           ------- Order Card
           ---Order indicators
           o.high_value order_high_value,
           o.operations_user,
           NULL AS ops_user_phone,
           NULL AS ops_user_email,
           o.agent_payee_id as booking_source,
           --------
           --CASE 
           --     WHEN order_status = 'P' AND order_type = 'ODPERM' THEN 2 
           --     WHEN order_status = 'P' AND order_type = 'HRISK' THEN 1 
           --     ELSE 0 
           -- END AS sort_rank,
           ISNULL(SUM(driver_sort_rank) OVER(PARTITION BY o.driver1_id),0) driver_sort_rank,
           --------
           ISNULL(o.order_id,'') as order_id,
           o.order_type,
           o.customer_id,
           o.customer_name,
           ISNULL(o.order_status,'')order_status,
           o.order_status_description,
           ISNULL(o.movement_id,'') as movement_id,
           o.origin_location_code origin_location_id,
           o.origin_location_name,
           o.origin_city origin_city,
           o.origin_state origin_state,
           o.origin_zip origin_zip,
           o.dest_location_name dest_location_id,
           o.dest_location_name,
           o.dest_city,
           o.dest_state,
           o.dest_zip,
           o.assignment_status,
           o.order_Origin_pickUpDate,
           o.order_Origin_Actual_pickUpDate,
           o.order_Dest_DeliveryDate,
           o.order_Dest_Actual_DeliveryDate,
           o.stop_count,
           o.all_stops_info,
           o.current_stop_sequence,
           o.distance order_distance,
           ISNULL(o.order_sort_priority,0) order_sort_priority,
           o.hazmat,
        o.preassign_sequence
    FROM ODS.fleet_master fm  WITH(NOLOCK)
        LEFT JOIN ODS.tractor_master tr  WITH(NOLOCK)
            ON fm.asset_id = tr.id  AND fm.fleet_id = tr.fleet_id
        LEFT JOIN ODS.driver_master d  WITH(NOLOCK)
            ON fm.asset_id = d.driver_id AND fm.fleet_id = d.group_id
        LEFT JOIN ODS.vGlobalFleetView_Orders o  WITH(NOLOCK)
            ON fm.asset_id = (CASE
                                  WHEN fm.asset_type = 'T' THEN
                                      O.tractor_id
                                  ELSE
                                      O.driver1_id
                              END
                             )
        LEFT JOIN ODS.trailer_master tl  WITH(NOLOCK)
            ON o.trailer1_id = tl.id
        LEFT JOIN ODS.driver_master cd  WITH(NOLOCK)
            ON tr.driver2_id = cd.driver_id
    WHERE fm.asset_id IN (SELECT asset_id FROM ODS.fleet_drivers_without_tractors())
    )fm;

GO

