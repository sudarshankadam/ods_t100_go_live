-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [ODS].[fleet_drivers_without_tractors]
()
RETURNS TABLE 
AS
RETURN 
(
	SELECT d.driver_id asset_id
    FROM ODS.driver_master d
    LEFT JOIN ODS.tractor_master t WITH(NOLOCK)
    ON d.driver_id = t.driver1_id 
    WHERE t.id IS NULL
    AND d.group_id IS NOT NULL
    AND d.group_id = 'OLAN'
    UNION
    SELECT t.id as asset_id FROM ODS.tractor_master t WITH(NOLOCK) WHERE fleet_id IS NOT NULL AND driver1_id IS NULL
)

GO

