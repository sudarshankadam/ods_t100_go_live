-- =============================================
-- Author:		Sudarshan Kadam
-- Create date: 8/10/2021
-- Description:	Function to return movement list for RF codes
-- Sample:
/*
SELECT FilterItem FROM [ODS].[getFilterByRF]('C1008043,C1008048, 1002, 1003,C1008368,CDUMDATG,INTG,1000,FLAT', 'Agent','INTG')
SELECT FilterItem FROM [ODS].[getFilterByRF]('1002, 1003,1000,FLAT', 'Customer','FLAT')

SELECT OrderRF_code,movementRF_code,* 
FROM ODS.ControlTower_AllOrders 
WHERE order_id IN (SELECT FilterItem FROM [ODS].[getFilterByRF]('1001', 'Agent','FLAT'))
*/
-- =============================================
CREATE FUNCTION [ODS].[getFilterByRF] 
(	
	@UserRFList varchar(max), 
	@UserType varchar(10) = NULL,
    @Division varchar(10) = NULL
)
RETURNS
@ItemList TABLE
(
     FilterItem varchar(100)
)
AS
BEGIN
    DECLARE @FinalRFList varchar(MAX) = ''
    IF @Division IS NULL
    BEGIN
        SET @FinalRFList = @UserRFList
    END
    ELSE IF @Division = 'INTG'
    BEGIN
        SELECT @FinalRFList = LTRIM(value)+','+ @FinalRFList
        FROM string_split(@UserRFList,',') s
        JOIN ODS.hierarchy h ON RTRIM(LTRIM(s.value)) = h.id AND h.company_id = 'T100'
        WHERE --parent_id IN ('AGENTFL') OR id = 'FLAT'
        parent_id IN ('AGENTIS','CUSTIS') OR id = 'INTG'
    END
    ELSE IF @Division = 'FLAT'
    BEGIN
        SELECT @FinalRFList = LTRIM(value)+','+ @FinalRFList
        FROM string_split(@UserRFList,',') s
        JOIN ODS.hierarchy h ON RTRIM(LTRIM(s.value)) = h.id AND h.company_id = 'T100'
        WHERE parent_id IN ('AGENTFL') OR id = 'FLAT'
        --parent_id IN ('AGENTIS','CUSTIS') OR id = 'INTEG'
    END
    ELSE
    BEGIN
        SET @FinalRFList = @UserRFList
    END

    ---Now get the list of orders for given RF list
    IF @UserType IN ('Customer','CUST','CUSX', 'TGMT', 'TGMX')
    BEGIN
        INSERT INTO @ItemList
        SELECT row_id FROM ODS.responsibility_history
        WHERE hierarchy_code IN (SELECT * FROM string_split(@FinalRFList,','))
        AND row_type = 'O'
    END
    ELSE
    BEGIN
        INSERT INTO @ItemList
        SELECT row_id FROM ODS.responsibility_history
        WHERE hierarchy_code IN (SELECT * FROM string_split(@FinalRFList,','))
        AND row_type = 'O' AND row_id IS NOT NULL
        UNION
        SELECT distinct m.order_id FROM ODS.responsibility_history rh join ODS.movements m on rh.row_id = m.movement_id
        WHERE hierarchy_code IN (SELECT * FROM string_split(@FinalRFList,','))
        AND row_type = 'M' AND row_id IS NOT NULL
    END
    
    RETURN
END

GO

