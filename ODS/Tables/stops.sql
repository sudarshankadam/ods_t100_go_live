CREATE TABLE [ODS].[stops] (
    [stopMasterId]            INT            IDENTITY (1, 1) NOT NULL,
    [stop_id]                 VARCHAR (50)   NOT NULL,
    [order_id]                VARCHAR (50)   NULL,
    [movement_id]             VARCHAR (50)   NULL,
    [sched_arrival]           DATETIME       NULL,
    [sched_departure]         DATETIME       NULL,
    [original_arrival]        DATETIME       NULL,
    [original_departure]      DATETIME       NULL,
    [actual_arrival]          DATETIME       NULL,
    [actual_departure]        DATETIME       NULL,
    [city]                    VARCHAR (50)   NULL,
    [state]                   VARCHAR (50)   NULL,
    [zip]                     VARCHAR (50)   NULL,
    [location_code]           VARCHAR (50)   NULL,
    [stop_type]               VARCHAR (50)   NULL,
    [lattitude]               VARCHAR (50)   NULL,
    [longitude]               VARCHAR (50)   NULL,
    [last_modified_date]      DATETIME       NULL,
    [order_sequence]          INT            NULL,
    [movement_sequence]       INT            NULL,
    [location_name]           VARCHAR (100)  NULL,
    [company_id]              VARCHAR (50)   NULL,
    [zone_id]                 VARCHAR (50)   NULL,
    [appt_required]           VARCHAR (1)    NULL,
    [appt_status_code]        VARCHAR (50)   NULL,
    [appt_confirmed]          VARCHAR (1)    NULL,
    [has_hot_comment]         VARCHAR (1)    NULL,
    [hot_comments]            VARCHAR (MAX)  NULL,
    [projected_arrival]       DATETIME       NULL,
    [eta_distance]            INT            NULL,
    [showas_address]          VARCHAR (500)  NULL,
    [showas_address2]         VARCHAR (500)  NULL,
    [showas_city_id]          VARCHAR (50)   NULL,
    [showas_city_name]        VARCHAR (250)  NULL,
    [showas_location_id]      VARCHAR (50)   NULL,
    [showas_location_name]    VARCHAR (250)  NULL,
    [showas_state]            VARCHAR (50)   NULL,
    [showas_zip_code]         VARCHAR (50)   NULL,
    [eta]                     DATETIME       NULL,
    [move_dist_from_previous] DECIMAL (8, 2) NULL,
    CONSTRAINT [PK_stops] PRIMARY KEY CLUSTERED ([stopMasterId] ASC)
);


GO

CREATE NONCLUSTERED INDEX [ids_order_movement_only]
    ON [ODS].[stops]([order_id] ASC, [movement_id] ASC);


GO

CREATE NONCLUSTERED INDEX [idx_stops_order_id]
    ON [ODS].[stops]([stop_id] ASC, [movement_id] ASC, [order_id] ASC);


GO

