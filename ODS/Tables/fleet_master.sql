CREATE TABLE [ODS].[fleet_master] (
    [fleet_id]     VARCHAR (50)  NOT NULL,
    [company_id]   VARCHAR (50)  NOT NULL,
    [fleet_descr]  VARCHAR (MAX) NULL,
    [asset_type]   VARCHAR (1)   NULL,
    [asset_id]     VARCHAR (50)  NOT NULL,
    [asset_status] VARCHAR (1)   NULL,
    [agency_id]    VARCHAR (50)  NULL,
    CONSTRAINT [PK_fleet_master_1] PRIMARY KEY CLUSTERED ([fleet_id] ASC, [company_id] ASC, [asset_id] ASC)
);


GO

