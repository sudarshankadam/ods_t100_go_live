CREATE TABLE [ODS].[notification_type_master] (
    [notification_type_id]          INT           NOT NULL,
    [notification_type]             VARCHAR (50)  NULL,
    [event_type]                    VARCHAR (MAX) NULL,
    [notification_message_template] VARCHAR (MAX) NULL,
    [expiry_time_hours]             INT           NULL,
    [description]                   VARCHAR (MAX) NULL,
    [last_modified_date]            DATETIME      NULL,
    CONSTRAINT [PK_notification_type_master] PRIMARY KEY CLUSTERED ([notification_type_id] ASC)
);


GO

