CREATE TABLE [ODS].[notification_publisher_master] (
    [notification_publisher_id] INT NOT NULL,
    [notification_type_id]      INT NULL,
    [publisher_group_id]        INT NULL,
    CONSTRAINT [PK_notification_publisher_master] PRIMARY KEY CLUSTERED ([notification_publisher_id] ASC)
);


GO

