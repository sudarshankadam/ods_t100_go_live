CREATE TABLE [ODS].[order_movement_workflow_status] (
    [order_id]              VARCHAR (50)  NOT NULL,
    [movement_id]           VARCHAR (50)  NOT NULL,
    [company_id]            VARCHAR (50)  NULL,
    [workflow_type]         VARCHAR (50)  NULL,
    [workflow_status_code]  VARCHAR (50)  NULL,
    [workflwo_status_descr] VARCHAR (100) NULL,
    [modified_date]         DATETIME      NULL,
    [is_updated_by_portal]  VARCHAR (1)   NULL,
    [updated_by_user]       VARCHAR (50)  NULL,
    [next_action]           VARCHAR (200) NULL,
    CONSTRAINT [PK_order_movement_workflow_status] PRIMARY KEY CLUSTERED ([order_id] ASC, [movement_id] ASC)
);


GO

