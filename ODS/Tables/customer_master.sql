CREATE TABLE [ODS].[customer_master] (
    [customer_id]   VARCHAR (50)    NOT NULL,
    [company_id]    VARCHAR (50)    NOT NULL,
    [location_id]   VARCHAR (50)    NULL,
    [location_name] VARCHAR (50)    NULL,
    [address1]      VARCHAR (500)   NULL,
    [address2]      VARCHAR (500)   NULL,
    [city]          VARCHAR (50)    NULL,
    [city_id]       VARCHAR (50)    NULL,
    [state_id]      VARCHAR (2)     NULL,
    [zip_code]      VARCHAR (50)    NULL,
    [credit_limit]  DECIMAL (18, 8) NULL,
    [credit_status] VARCHAR (1)     NULL
);


GO

