CREATE TABLE [ODS].[rate_assign_templates] (
    [id]                INT           NOT NULL,
    [template_id]       VARCHAR (20)  NOT NULL,
    [category]          VARCHAR (50)  NULL,
    [template_json]     VARCHAR (MAX) NULL,
    [rule_description]  VARCHAR (MAX) NULL,
    [rule_display_text] VARCHAR (MAX) NULL,
    [last_modified]     DATETIME      NULL,
    CONSTRAINT [PK_rate_assign_templates] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO

