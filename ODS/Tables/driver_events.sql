CREATE TABLE [ODS].[driver_events] (
    [company_id]        CHAR (4)      NOT NULL,
    [driver_id]         CHAR (8)      NULL,
    [remark]            VARCHAR (100) NULL,
    [event_code]        CHAR (4)      NULL,
    [event_status]      CHAR (1)      NULL,
    [city]              CHAR (50)     NULL,
    [city_id]           INT           NULL,
    [driver_mgr_id]     CHAR (10)     NULL,
    [entry_date]        DATETIME      NULL,
    [start_date]        DATETIME      NULL,
    [end_date]          DATETIME      NULL,
    [entered_by]        CHAR (10)     NULL,
    [equipment_type_id] CHAR (1)      NULL,
    [location_id]       CHAR (8)      NULL,
    [notify_start_date] DATETIME      NULL,
    [notify_end_date]   DATETIME      NULL,
    [priority]          CHAR (1)      NULL,
    [request_date]      DATETIME      NULL,
    [state]             CHAR (2)      NULL,
    [zip_code]          CHAR (10)     NULL,
    [allow_dispatch]    VARCHAR (1)   NULL,
    [event_name]        VARCHAR (250) NULL
);


GO

