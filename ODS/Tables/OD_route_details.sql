CREATE TABLE [ODS].[OD_route_details] (
    [OD_route_details_Id]    INT             IDENTITY (1, 1) NOT NULL,
    [company_id]             VARCHAR (4)     NOT NULL,
    [order_id]               VARCHAR (50)    NOT NULL,
    [customer_id]            VARCHAR (50)    NULL,
    [Origin_Location_Id]     VARCHAR (50)    NULL,
    [Origin_City]            VARCHAR (50)    NULL,
    [Origin_State]           VARCHAR (50)    NULL,
    [Origin_Zip]             VARCHAR (50)    NULL,
    [Dest_Location_Id]       VARCHAR (50)    NULL,
    [Dest_City]              VARCHAR (50)    NULL,
    [Dest_State]             VARCHAR (50)    NULL,
    [Dest_Zip]               VARCHAR (50)    NULL,
    [route_cost1]            DECIMAL (16, 2) NULL,
    [route_cost1_c]          VARCHAR (3)     NULL,
    [route_cost1_d]          DATETIME        NULL,
    [route_cost1_n]          DECIMAL (16, 2) NULL,
    [route_cost1_r]          DECIMAL (8, 4)  NULL,
    [route_cost2]            DECIMAL (16, 2) NULL,
    [route_cost2_c]          VARCHAR (3)     NULL,
    [route_cost2_d]          DATETIME        NULL,
    [route_cost2_n]          DECIMAL (16, 2) NULL,
    [route_cost2_r]          DECIMAL (8, 4)  NULL,
    [route_cost3]            DECIMAL (16, 2) NULL,
    [route_cost3_c]          VARCHAR (3)     NULL,
    [route_cost3_d]          DATETIME        NULL,
    [route_cost3_n]          DECIMAL (16, 2) NULL,
    [route_cost3_r]          DECIMAL (8, 4)  NULL,
    [route_cost4]            DECIMAL (16, 2) NULL,
    [route_cost4_c]          VARCHAR (3)     NULL,
    [route_cost4_d]          DATETIME        NULL,
    [route_cost4_n]          DECIMAL (16, 2) NULL,
    [route_cost4_r]          DECIMAL (8, 4)  NULL,
    [route_state_locality1]  VARCHAR (2)     NULL,
    [route_state_locality2]  VARCHAR (2)     NULL,
    [route_state_locality3]  VARCHAR (2)     NULL,
    [route_state_locality4]  VARCHAR (2)     NULL,
    [route_date1]            DATETIME        NULL,
    [route_date2]            DATETIME        NULL,
    [route_date3]            DATETIME        NULL,
    [route_date4]            DATETIME        NULL,
    [route_notes1]           VARCHAR (MAX)   NULL,
    [route_notes2]           VARCHAR (MAX)   NULL,
    [route_notes3]           VARCHAR (MAX)   NULL,
    [route_notes4]           VARCHAR (MAX)   NULL,
    [route_rcvd_date1]       DATETIME        NULL,
    [route_rcvd_date2]       DATETIME        NULL,
    [route_rcvd_date3]       DATETIME        NULL,
    [route_rcvd_date4]       DATETIME        NULL,
    [route_state_locality6]  VARCHAR (2)     NULL,
    [route_state_locality5]  VARCHAR (2)     NULL,
    [route_date6]            DATETIME        NULL,
    [route_date5]            DATETIME        NULL,
    [route_notes6]           VARCHAR (MAX)   NULL,
    [route_notes5]           VARCHAR (MAX)   NULL,
    [route_rcvd_date6]       VARCHAR (MAX)   NULL,
    [route_rcvd_date5]       DATETIME        NULL,
    [route_cost5]            DECIMAL (16, 2) NULL,
    [route_cost6]            DECIMAL (16, 2) NULL,
    [created_date]           DATETIME        NULL,
    [is_current]             INT             NULL,
    [route_permit_type1]     VARCHAR (1)     NULL,
    [route_permit_america6]  VARCHAR (1)     NULL,
    [route_permit_america5]  VARCHAR (1)     NULL,
    [route_permit_america4]  VARCHAR (1)     NULL,
    [route_permit_america3]  VARCHAR (1)     NULL,
    [route_permit_america2]  VARCHAR (1)     NULL,
    [route_permit_america1]  VARCHAR (1)     NULL,
    [route_permit_type6]     VARCHAR (1)     NULL,
    [route_permit_type5]     VARCHAR (1)     NULL,
    [route_permit_type4]     VARCHAR (1)     NULL,
    [route_permit_type3]     VARCHAR (1)     NULL,
    [route_permit_type2]     VARCHAR (1)     NULL,
    [route_permit_america7]  VARCHAR (1)     NULL,
    [route_permit_america9]  VARCHAR (1)     NULL,
    [route_permit_america8]  VARCHAR (1)     NULL,
    [route_permit_america15] VARCHAR (1)     NULL,
    [route_permit_america14] VARCHAR (1)     NULL,
    [route_permit_america13] VARCHAR (1)     NULL,
    [route_permit_america12] VARCHAR (1)     NULL,
    [route_permit_america11] VARCHAR (1)     NULL,
    [route_permit_america10] VARCHAR (1)     NULL,
    [route_rcvd_date9]       DATETIME        NULL,
    [route_rcvd_date8]       DATETIME        NULL,
    [route_rcvd_date7]       DATETIME        NULL,
    [route_cost15]           DECIMAL (16, 2) NULL,
    [route_cost15_c]         DECIMAL (16, 2) NULL,
    [route_cost15_d]         DECIMAL (16, 2) NULL,
    [route_cost15_n]         DECIMAL (16, 2) NULL,
    [route_cost15_r]         DECIMAL (16, 2) NULL,
    [route_cost14]           DECIMAL (16, 2) NULL,
    [route_cost14_c]         DECIMAL (16, 2) NULL,
    [route_cost14_d]         DECIMAL (16, 2) NULL,
    [route_cost14_n]         DECIMAL (16, 2) NULL,
    [route_cost14_r]         DECIMAL (16, 2) NULL,
    [route_cost13]           DECIMAL (16, 2) NULL,
    [route_cost13_c]         DECIMAL (16, 2) NULL,
    [route_cost13_d]         DECIMAL (16, 2) NULL,
    [route_cost13_n]         DECIMAL (16, 2) NULL,
    [route_cost13_r]         DECIMAL (16, 2) NULL,
    [route_cost12]           DECIMAL (16, 2) NULL,
    [route_cost12_c]         DECIMAL (16, 2) NULL,
    [route_cost12_d]         DECIMAL (16, 2) NULL,
    [route_cost12_n]         DECIMAL (16, 2) NULL,
    [route_cost12_r]         DECIMAL (16, 2) NULL,
    [route_cost11]           DECIMAL (16, 2) NULL,
    [route_cost11_c]         DECIMAL (16, 2) NULL,
    [route_cost11_d]         DECIMAL (16, 2) NULL,
    [route_cost11_n]         DECIMAL (16, 2) NULL,
    [route_cost11_r]         DECIMAL (16, 2) NULL,
    [route_cost10]           DECIMAL (16, 2) NULL,
    [route_cost10_c]         DECIMAL (16, 2) NULL,
    [route_cost10_d]         DECIMAL (16, 2) NULL,
    [route_cost10_n]         DECIMAL (16, 2) NULL,
    [route_cost10_r]         DECIMAL (16, 2) NULL,
    [route_cost9]            DECIMAL (16, 2) NULL,
    [route_cost9_c]          DECIMAL (16, 2) NULL,
    [route_cost9_d]          DECIMAL (16, 2) NULL,
    [route_cost9_n]          DECIMAL (16, 2) NULL,
    [route_cost9_r]          DECIMAL (16, 2) NULL,
    [route_cost8]            DECIMAL (16, 2) NULL,
    [route_cost8_c]          DECIMAL (16, 2) NULL,
    [route_cost8_d]          DECIMAL (16, 2) NULL,
    [route_cost8_n]          DECIMAL (16, 2) NULL,
    [route_cost8_r]          DECIMAL (16, 2) NULL,
    [route_cost7]            DECIMAL (16, 2) NULL,
    [route_cost7_c]          DECIMAL (16, 2) NULL,
    [route_cost7_d]          DECIMAL (16, 2) NULL,
    [route_cost7_n]          DECIMAL (16, 2) NULL,
    [route_cost7_r]          DECIMAL (16, 2) NULL,
    [route_date9]            DATETIME        NULL,
    [route_date8]            DATETIME        NULL,
    [route_date7]            DATETIME        NULL,
    [route_permit_type15]    VARCHAR (1)     NULL,
    [route_permit_type14]    VARCHAR (1)     NULL,
    [route_permit_type13]    VARCHAR (1)     NULL,
    [route_permit_type12]    VARCHAR (1)     NULL,
    [route_permit_type11]    VARCHAR (1)     NULL,
    [route_permit_type10]    VARCHAR (1)     NULL,
    [permit_notes9]          VARCHAR (MAX)   NULL,
    [permit_notes8]          VARCHAR (MAX)   NULL,
    [permit_notes7]          VARCHAR (MAX)   NULL,
    [route_rcvd_date15]      DATETIME        NULL,
    [route_rcvd_date14]      DATETIME        NULL,
    [route_rcvd_date13]      DATETIME        NULL,
    [route_rcvd_date12]      DATETIME        NULL,
    [route_rcvd_date11]      DATETIME        NULL,
    [route_rcvd_date10]      DATETIME        NULL,
    [permit_notes15]         VARCHAR (MAX)   NULL,
    [permit_notes14]         VARCHAR (MAX)   NULL,
    [permit_notes13]         VARCHAR (MAX)   NULL,
    [permit_notes12]         VARCHAR (MAX)   NULL,
    [permit_notes11]         VARCHAR (MAX)   NULL,
    [permit_notes10]         VARCHAR (MAX)   NULL,
    [route_permit_type9]     VARCHAR (1)     NULL,
    [route_permit_type8]     VARCHAR (1)     NULL,
    [route_permit_type7]     VARCHAR (1)     NULL,
    [route_notes9]           VARCHAR (MAX)   NULL,
    [route_notes8]           VARCHAR (MAX)   NULL,
    [route_notes7]           VARCHAR (MAX)   NULL,
    [route_notes15]          VARCHAR (MAX)   NULL,
    [route_notes14]          VARCHAR (MAX)   NULL,
    [route_notes13]          VARCHAR (MAX)   NULL,
    [route_notes12]          VARCHAR (MAX)   NULL,
    [route_notes11]          VARCHAR (MAX)   NULL,
    [route_notes10]          VARCHAR (MAX)   NULL,
    [route_date15]           DATETIME        NULL,
    [route_date14]           DATETIME        NULL,
    [route_date13]           DATETIME        NULL,
    [route_date12]           DATETIME        NULL,
    [route_date11]           DATETIME        NULL,
    [route_date10]           DATETIME        NULL,
    CONSTRAINT [PK_OD_route_details_1] PRIMARY KEY CLUSTERED ([OD_route_details_Id] ASC)
);


GO

CREATE NONCLUSTERED INDEX [IX_OD_Origin_dest]
    ON [ODS].[OD_route_details]([Origin_Location_Id] ASC, [Dest_Location_Id] ASC);


GO

