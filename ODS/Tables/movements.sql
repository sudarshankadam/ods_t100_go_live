CREATE TABLE [ODS].[movements] (
    [movementMasterId]                  INT             IDENTITY (1, 1) NOT NULL,
    [movement_id]                       VARCHAR (50)    NULL,
    [company_id]                        VARCHAR (50)    NULL,
    [order_id]                          VARCHAR (50)    NULL,
    [is_brokered]                       CHAR (1)        NULL,
    [brokerage_status]                  VARCHAR (50)    NULL,
    [origin_stop_id]                    VARCHAR (50)    NULL,
    [dest_stop_id]                      VARCHAR (50)    NULL,
    [movement_status]                   VARCHAR (50)    NULL,
    [dispatch_status]                   VARCHAR (50)    NULL,
    [is_empty_move]                     CHAR (1)        NULL,
    [last_modified_date]                DATETIME        NULL,
    [pro_number]                        VARCHAR (50)    NULL,
    [move_distance]                     DECIMAL (18, 2) NULL,
    [carrierPay]                        DECIMAL (18, 2) NULL,
    [capacity_rep]                      VARCHAR (50)    NULL,
    [carrier_contact]                   VARCHAR (50)    NULL,
    [carrier_phone]                     VARCHAR (50)    NULL,
    [carrier_email]                     VARCHAR (MAX)   NULL,
    [preassign_sequence]                INT             NULL,
    [order_movement_execution_division] VARCHAR (50)    NULL,
    [carrier_id]                        VARCHAR (50)    NULL,
    [xmitted2driver]                    DATETIME        NULL,
    CONSTRAINT [PK_movements] PRIMARY KEY CLUSTERED ([movementMasterId] ASC)
);


GO

CREATE NONCLUSTERED INDEX [_dta_index_movements_19_1289771652__K4_K3_K2_K7_K8_K20]
    ON [ODS].[movements]([order_id] ASC, [company_id] ASC, [movement_id] ASC, [origin_stop_id] ASC, [dest_stop_id] ASC, [preassign_sequence] ASC);


GO

CREATE NONCLUSTERED INDEX [idx_movements_order_id]
    ON [ODS].[movements]([order_id] ASC);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UK_movement_id]
    ON [ODS].[movements]([movement_id] ASC, [order_id] ASC, [company_id] ASC);


GO

