CREATE TABLE [ODS].[Document_log] (
    [id]                     INT             IDENTITY (1, 1) NOT NULL,
    [order_id]               VARCHAR (50)    NULL,
    [doc_type]               VARCHAR (50)    NULL,
    [doc_type_sr_no]         INT             NULL,
    [doc_status]             VARCHAR (50)    NULL,
    [doc_name]               VARCHAR (250)   NULL,
    [upload_date]            DATETIME        NULL,
    [catalog_date]           DATETIME        NULL,
    [last_modified]          DATETIME        NULL,
    [user_id]                VARCHAR (50)    NULL,
    [doc_type_sr_no_version] INT             NULL,
    [batch_id]               VARCHAR (250)   NULL,
    [doc_Reference]          VARCHAR (250)   NULL,
    [doc_data]               VARBINARY (MAX) NULL,
    [pending_notification]   VARCHAR (1)     NULL,
    CONSTRAINT [PK_ODS.Document_log] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO

