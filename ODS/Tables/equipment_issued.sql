CREATE TABLE [ODS].[equipment_issued] (
    [id]                INT           IDENTITY (1, 1) NOT NULL,
    [company_id]        VARCHAR (50)  NULL,
    [issued_to_asset]   VARCHAR (50)  NULL,
    [asset_type]        VARCHAR (50)  NULL,
    [equipment_type_id] VARCHAR (50)  NULL,
    [equipment_descr]   VARCHAR (500) NULL,
    [expiration_date]   DATETIME      NULL,
    [quantity]          INT           NULL,
    [memo]              VARCHAR (MAX) NULL,
    [comments]          VARCHAR (MAX) NULL,
    [issued_date]       DATETIME      NULL,
    [status]            VARCHAR (50)  NULL
);


GO

