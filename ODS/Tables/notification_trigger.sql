CREATE TABLE [ODS].[notification_trigger] (
    [id]                 INT           IDENTITY (1, 1) NOT NULL,
    [notificationTypeId] INT           NULL,
    [eventType]          VARCHAR (200) NULL,
    [messageText]        VARCHAR (MAX) NULL,
    [bookingAgency]      VARCHAR (100) NULL,
    [fleetAgency]        VARCHAR (100) NULL,
    [expiry]             DATETIME      NULL,
    [orderId]            VARCHAR (20)  NULL,
    [movementId]         VARCHAR (20)  NULL,
    [role]               VARCHAR (200) NULL,
    [fleetId]            VARCHAR (20)  NULL,
    [driverId]           VARCHAR (20)  NULL,
    [modified_date]      DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO

