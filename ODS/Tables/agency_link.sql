CREATE TABLE [ODS].[agency_link] (
    [company_id]  CHAR (4)      NOT NULL,
    [agency_id]   CHAR (8)      NULL,
    [description] VARCHAR (250) NULL,
    [id]          CHAR (32)     NOT NULL,
    [row_id]      CHAR (32)     NULL,
    [row_type]    CHAR (1)      NULL,
    PRIMARY KEY CLUSTERED ([company_id] ASC, [id] ASC)
);


GO

