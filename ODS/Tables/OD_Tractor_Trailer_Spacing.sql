CREATE TABLE [ODS].[OD_Tractor_Trailer_Spacing] (
    [id]                    INT            IDENTITY (1, 1) NOT NULL,
    [company_id]            VARCHAR (4)    NOT NULL,
    [tractor_id]            VARCHAR (50)   NOT NULL,
    [trailer_id]            VARCHAR (50)   NULL,
    [tractor_axle_spacing1] DECIMAL (7, 1) NULL,
    [tractor_axle_spacing2] DECIMAL (7, 1) NULL,
    [tractor_axle_spacing3] DECIMAL (7, 1) NULL,
    [tractor_axle_spacing4] DECIMAL (7, 1) NULL,
    [trailer_axle_spacing1] DECIMAL (7, 1) NULL,
    [trailer_axle_spacing2] DECIMAL (7, 1) NULL,
    [trailer_axle_spacing3] DECIMAL (7, 1) NULL,
    [trailer_axle_spacing4] DECIMAL (7, 1) NULL,
    [trailer_axle_spacing5] DECIMAL (7, 1) NULL,
    [last_modified]         DATETIME       NULL,
    [last_order_id]         VARCHAR (50)   NULL,
    [kingpin_length]        DECIMAL (7, 1) NULL,
    PRIMARY KEY CLUSTERED ([company_id] ASC, [tractor_id] ASC)
);


GO

