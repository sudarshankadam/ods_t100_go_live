CREATE TABLE [ODS].[notification_group_master] (
    [notification_group_id]   INT          NOT NULL,
    [is_active]               VARCHAR (1)  NULL,
    [notification_group_name] VARCHAR (50) NULL,
    CONSTRAINT [PK_notification_group_master] PRIMARY KEY CLUSTERED ([notification_group_id] ASC)
);


GO

