CREATE TABLE [ODS].[hierarchy] (
    [company_id]      CHAR (4)      NOT NULL,
    [description]     VARCHAR (250) NULL,
    [hierarchy_level] INT           NULL,
    [id]              CHAR (8)      NOT NULL,
    [parent_id]       CHAR (8)      NULL
);


GO

