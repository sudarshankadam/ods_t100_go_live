CREATE TABLE [ODS].[notification_subscriber_master] (
    [notification_subscriber_id] INT NOT NULL,
    [notification_type_id]       INT NULL,
    [subscriber_group_id]        INT NULL,
    CONSTRAINT [PK_notification_subscriber_master] PRIMARY KEY CLUSTERED ([notification_subscriber_id] ASC)
);


GO

