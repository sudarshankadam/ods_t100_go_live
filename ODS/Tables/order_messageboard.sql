CREATE TABLE [ODS].[order_messageboard] (
    [id]                    INT           IDENTITY (1, 1) NOT NULL,
    [order_Id]              VARCHAR (50)  NULL,
    [message_Id]            VARCHAR (200) NULL,
    [movement_id]           VARCHAR (50)  NULL,
    [message_type]          VARCHAR (50)  NULL,
    [message_created_date]  DATETIME      NULL,
    [message_details]       VARCHAR (MAX) NULL,
    [entered_by]            VARCHAR (100) NULL,
    [od_status_on_entry]    VARCHAR (50)  NULL,
    [channel_type]          VARCHAR (50)  NULL,
    [message_from_email_id] VARCHAR (200) NULL,
    [message_to]            VARCHAR (200) NULL,
    [message_to_email_id]   VARCHAR (200) NULL,
    CONSTRAINT [PK_order_messageboard] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [IX_order_messageboard] UNIQUE NONCLUSTERED ([order_Id] ASC, [message_Id] ASC)
);


GO

