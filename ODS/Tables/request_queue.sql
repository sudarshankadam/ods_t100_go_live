CREATE TABLE [ODS].[request_queue] (
    [id]                 VARCHAR (250) NOT NULL,
    [payload]            VARCHAR (MAX) NULL,
    [function_descr]     VARCHAR (800) NULL,
    [modified_date]      DATETIME      NULL,
    [failure_msg_header] VARCHAR (MAX) NULL,
    [user_id]            VARCHAR (50)  NULL,
    CONSTRAINT [PK_request_queue] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO

