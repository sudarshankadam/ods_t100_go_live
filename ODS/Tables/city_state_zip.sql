CREATE TABLE [ODS].[city_state_zip] (
    [company_id]         CHAR (4)        NOT NULL,
    [airport_code]       CHAR (3)        NULL,
    [alk_id]             INT             NULL,
    [central_location]   CHAR (1)        NULL,
    [county]             VARCHAR (45)    NULL,
    [id]                 INT             NOT NULL,
    [inactive]           CHAR (1)        NULL,
    [isi_id]             INT             NULL,
    [latitude]           DECIMAL (10, 6) NULL,
    [longitude]          DECIMAL (10, 6) NULL,
    [name]               CHAR (50)       NULL,
    [pcm_window_city_id] INT             NULL,
    [rmm_id]             INT             NULL,
    [state_id]           CHAR (2)        NULL,
    [three_digit_zip]    CHAR (3)        NULL,
    [timezone_id]        CHAR (3)        NULL,
    [zip_code]           CHAR (10)       NULL,
    [zone_id]            CHAR (3)        NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO

