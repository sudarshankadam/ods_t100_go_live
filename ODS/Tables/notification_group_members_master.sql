CREATE TABLE [ODS].[notification_group_members_master] (
    [notification_group_members_id] INT           NOT NULL,
    [notification_group_id]         INT           NULL,
    [members]                       VARCHAR (200) NULL,
    CONSTRAINT [PK_notification_group_members_master] PRIMARY KEY CLUSTERED ([notification_group_members_id] ASC)
);


GO

