CREATE TABLE [ODS].[orderMaster] (
    [orderMasterId]           INT             IDENTITY (1, 1) NOT NULL,
    [order_id]                VARCHAR (50)    NOT NULL,
    [company_id]              VARCHAR (500)   NOT NULL,
    [customer_id]             VARCHAR (50)    NULL,
    [customer_name]           VARCHAR (500)   NULL,
    [current_movement_id]     VARCHAR (50)    NULL,
    [origin_stop_id]          VARCHAR (50)    NULL,
    [dest_stop_id]            VARCHAR (50)    NULL,
    [order_status]            VARCHAR (50)    NULL,
    [order_type]              VARCHAR (50)    NULL,
    [agency_id]               VARCHAR (50)    NULL,
    [order_source]            VARCHAR (50)    NULL,
    [total_distance]          DECIMAL (18, 2) NULL,
    [load_weight]             DECIMAL (18, 2) NULL,
    [last_modified_date]      DATETIME        NULL,
    [ordered_date]            DATETIME        NULL,
    [agent_payee_id]          VARCHAR (50)    NULL,
    [order_value]             DECIMAL (18, 2) NULL,
    [high_value]              VARCHAR (50)    NULL,
    [operations_user]         VARCHAR (50)    NULL,
    [created_by]              VARCHAR (50)    NULL,
    [order_mode]              VARCHAR (50)    NULL,
    [order_mode_id]           VARCHAR (50)    NULL,
    [order_pieces]            INT             NULL,
    [on_hold]                 CHAR (1)        NULL,
    [hold_reason]             VARCHAR (500)   NULL,
    [carrier_pay]             DECIMAL (18, 2) NULL,
    [total_pay]               DECIMAL (18, 2) NULL,
    [carrier_other_pay]       DECIMAL (18, 2) NULL,
    [ops_review_status]       VARCHAR (100)   NULL,
    [total_charge]            DECIMAL (18, 2) NULL,
    [other_charges_total]     DECIMAL (18, 2) NULL,
    [equipment_type_id]       VARCHAR (50)    NULL,
    [is_agent_captive_order]  CHAR (1)        NULL,
    [carrierOtherPaySum]      DECIMAL (18, 2) NULL,
    [orders_Blnum]            VARCHAR (50)    NULL,
    [temperature_max]         DECIMAL (18, 2) NULL,
    [temperature_min]         DECIMAL (18, 2) NULL,
    [freight_charge]          DECIMAL (18, 2) NULL,
    [order_division]          VARCHAR (50)    NULL,
    [bill_date]               DATETIME        NULL,
    [ready_to_bill]           VARCHAR (1)     NULL,
    [subject_order_number]    VARCHAR (50)    NULL,
    [subject_order_status]    VARCHAR (50)    NULL,
    [subject_order_void_date] DATETIME        NULL,
    [hazmat]                  VARCHAR (1)     NULL,
    CONSTRAINT [PK_orderMaster] PRIMARY KEY CLUSTERED ([orderMasterId] ASC),
    CONSTRAINT [UK_order_id] UNIQUE NONCLUSTERED ([order_id] ASC)
);


GO

