CREATE TABLE [ODS].[order_drafts] (
    [draft_id]      INT           IDENTITY (1, 1) NOT NULL,
    [agency_id]     VARCHAR (50)  NULL,
    [customer_Id]   VARCHAR (50)  NULL,
    [draft_descr]   VARCHAR (250) NULL,
    [order_details] VARCHAR (MAX) NULL,
    [draft_status]  VARCHAR (50)  NULL,
    [created_by]    VARCHAR (50)  NULL,
    [created_date]  DATETIME      NULL,
    CONSTRAINT [PK_order_drafts] PRIMARY KEY CLUSTERED ([draft_id] ASC)
);


GO

