CREATE TABLE [ODS].[responsibility_history] (
    [id]                    INT           IDENTITY (1, 1) NOT NULL,
    [company_id]            VARCHAR (50)  NOT NULL,
    [hierarchy_code]        VARCHAR (100) NULL,
    [hierarchy_level]       INT           NULL,
    [parent_hierarchy_code] VARCHAR (100) NULL,
    [responsible_role]      VARCHAR (2)   NULL,
    [row_id]                VARCHAR (50)  NULL,
    [row_type]              VARCHAR (2)   NULL,
    CONSTRAINT [PK_responsibility_history] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO

CREATE NONCLUSTERED INDEX [IX_responsibility_history]
    ON [ODS].[responsibility_history]([row_id] ASC, [row_type] ASC, [hierarchy_code] ASC);


GO

CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [ODS].[responsibility_history]([row_type] ASC, [row_id] ASC, [hierarchy_code] ASC);


GO

