CREATE TABLE [ODS].[rate_assign_rules] (
    [Id]                          INT          IDENTITY (1, 1) NOT NULL,
    [Order_Division]              VARCHAR (50) NULL,
    [Order_Mode]                  VARCHAR (50) NULL,
    [Order_Type]                  VARCHAR (50) NULL,
    [Create_Order]                VARCHAR (50) NULL,
    [Order_Workflow]              VARCHAR (50) NULL,
    [Is_agent_captive_order]      VARCHAR (4)  NULL,
    [LineHaul_template]           VARCHAR (50) NULL,
    [Additional_Charges_template] VARCHAR (50) NULL,
    [Tiered_Rates_template]       VARCHAR (50) NULL,
    [Fuel_Surcharge_template]     VARCHAR (50) NULL,
    [Loadboard_template]          VARCHAR (50) NULL,
    [last_modified]               DATETIME     NULL,
    CONSTRAINT [PK_Rate_Assign_Properties] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO

