CREATE TABLE [ODS].[movement_assets] (
    [AssetsMasterId]        INT           IDENTITY (1, 1) NOT NULL,
    [movement_id]           VARCHAR (50)  NULL,
    [order_id]              VARCHAR (50)  NULL,
    [tractor_id]            VARCHAR (50)  NULL,
    [trailer1_id]           VARCHAR (50)  NULL,
    [trailer2_id]           VARCHAR (50)  NULL,
    [driver1_id]            VARCHAR (50)  NULL,
    [driver2_id]            VARCHAR (50)  NULL,
    [carrier_id]            VARCHAR (50)  NULL,
    [assignment_status]     VARCHAR (50)  NULL,
    [last_modified_date]    DATETIME      NULL,
    [created_date]          DATETIME      NULL,
    [equipment_group_id]    CHAR (32)     NULL,
    [tractor_last_location] VARCHAR (200) NULL,
    [company_id]            VARCHAR (50)  NULL,
    [driver_dot_info]       VARCHAR (MAX) NULL,
    CONSTRAINT [PK_movement_assets] PRIMARY KEY CLUSTERED ([AssetsMasterId] ASC)
);


GO

CREATE NONCLUSTERED INDEX [IX_FleetView_Index]
    ON [ODS].[movement_assets]([movement_id] ASC, [order_id] ASC)
    INCLUDE([tractor_id], [trailer1_id], [trailer2_id], [driver1_id], [driver2_id], [carrier_id], [assignment_status], [tractor_last_location]);


GO

