
/* SAMPLE
EXEC [ODS].[UpdateRFHist] @section = 'debug'
*/
CREATE PROCEDURE [ODS].[UpdateRFHist]
@company_id varchar(10) = 'T100',
@section varchar(20) = 'ALL',
@FromDate datetime = NULL
AS
BEGIN
--TRUNCATE TABLE  [ODS].[responsibility_history]
        SELECT DISTINCT * INTO #RFTemp
        FROM (
            SELECT DISTINCT
                   company_id,
                   hierarchy_level_1 AS hierarchy_code,
                   1 AS hierarchy_level,
                   NULL AS parent_hierarchy,
                   responsible_role,
                   row_id,
                   row_type
            FROM [QA-McLeod-RPT].lme_qa.dbo.responsible_hist
            WHERE company_id = 'T100'
            UNION
            SELECT DISTINCT
                   company_id,
                   hierarchy_level_2 AS hierarchy_code,
                   2 AS hierarchy_level,
                   hierarchy_level_1 AS parent_hierarchy,
                   responsible_role,
                   row_id,
                   row_type
            FROM [QA-McLeod-RPT].lme_qa.dbo.responsible_hist
            WHERE company_id = 'T100'
        ) RF
        WHERE hierarchy_code IS NOT NULL
        --SELECT * FROM #RFTemp
        --SELECT top 10 * FROM ODS.responsibility_history
        IF @section = 'debug'
            SELECT * FROM #RFTemp WHERE row_id = '2547931'

        ;WITH newData
        AS (SELECT * FROM #RFTemp)
        MERGE [ODS].[responsibility_history] AS t
        USING newData AS s
        ON t.row_id = s.row_id AND t.row_type = s.row_type AND t.[company_id] = s.[company_id]  AND t.hierarchy_level = s.hierarchy_level
            AND t.responsible_role = s.responsible_role AND ISNULL(s.parent_hierarchy,'') = ISNULL(t.parent_hierarchy_code,'') AND t.hierarchy_code =  s.hierarchy_code
        WHEN MATCHED AND EXISTS 
            (
                SELECT s.company_id,s.hierarchy_code, s.hierarchy_level,s.parent_hierarchy,s.responsible_role,s.row_id,s.row_type
                EXCEPT 
                SELECT t.company_id,t.hierarchy_code, t.hierarchy_level,t.parent_hierarchy_code,t.responsible_role,t.row_id,t.row_type
            )
        THEN UPDATE
        SET t.hierarchy_code = s.[hierarchy_code]
        ,t.parent_hierarchy_code = s.parent_hierarchy
        ,responsible_role = s.responsible_role
        ,row_id = s.row_id
        ,row_type = s.row_type
        WHEN NOT MATCHED BY TARGET THEN
        INSERT
        (
            company_id,
            hierarchy_code,
            hierarchy_level,
            parent_hierarchy_code,
            responsible_role,
            row_id,
            row_type
        )
        VALUES
        (
            company_id,
            hierarchy_code,
            hierarchy_level,
            parent_hierarchy,
            responsible_role,
            row_id,
            row_type
        )
        WHEN NOT MATCHED BY SOURCE 
        THEN 
        DELETE;
    PRINT 'RF Update RowCount: '   + CAST(@@RowCount AS VARCHAR(100))    
   
END

GO

