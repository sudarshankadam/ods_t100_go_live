/*
Req:
Input: 
1. Radius
2. Location
    2a: Lat long
    2b: city state
    2c: zip
3. datetime of target avalibility
4. Buffer time

Output:
1. Tractor and driver list
    1a: Active T/D with no current assignments 
    1b: T/D with active loads that will reach the desired location at end of the current load
    1c: T/D with pre-assignments that will reach the desired location at end of the assigned load
2, Available Order List

--SAMPLE
EXEC [ODS].[radius_search_orders_equipment]
@Radius = 150
,@PickupLat = NULL
,@PickupLong = NULL
,@SourceLocation = NULL
,@PickupCity = 'WARSAW'
,@PickupState = 'IN'
,@PickupZip = '46580'
,@PickupDateFrom = NULL
,@PickupDateTo = NULL
,@OrderId = NULL
,@ResponseType = 'assets'

EXEC [ODS].[radius_search_orders_equipment]
@Radius = 150
,@OrderId = '0030548'
,@ResponseType = 'assets'
,@debug=1

*/
CREATE PROCEDURE [ODS].[radius_search_orders_equipment]
@Radius INT = 150
,@PickupLat decimal(18,6) = NULL
,@PickupLong decimal(18,6) = NULL
,@SourceLocation varchar(50) = NULL
,@PickupCity varchar(50) = NULL
,@PickupState varchar(50) = NULL
,@PickupZip varchar(50) = NULL
,@PickupDateFrom datetime = NULL
,@PickupDateTo datetime = NULL
,@OrderId varchar(50) = NULL
,@ResponseType varchar(10) = NULL ---orders, assets
,@debug int = NULL

AS
BEGIN
    SET @ResponseType = ISNULL(@ResponseType, 'assets')
    
    IF @SourceLocation IS NOT NULL
        BEGIN
            ---Provided that LME is taking care of accuracy of lat long
            PRINT 'Inside Source Location'
            SELECT @PickupLat = latitude,
                   @PickupLong = longitude
            FROM ODS.locationMaster
            WHERE company_id = 'T100'
                  AND id = @SourceLocation --fill in location id from location table (or order entry) here.
        END
        ELSE IF @PickupCity IS NOT NULL
                AND @PickupZip IS NOT NULL
        BEGIN
            PRINT 'Inside Pickup City Zip'
            SELECT @PickupLat = latitude,
                   @PickupLong = longitude
            FROM ods.[city_state_zip]
            WHERE name = @PickupCity
                  AND zip_code = @PickupZip
        END

        -----Step 1: list of active T/D with no assignments
        --SELECT  driver_id, SUBSTRING(driver_last_location, 1, CHARINDEX(',',driver_last_location  ))
        --FROM ODS.driver_master 
        --WHERE is_active = 'Y' 
        --AND driver_id NOT IN (SELECT driver1_id FROM ODS.movement_assets WHERE driver1_id IS NOT NULL)
    IF @ResponseType = 'assets'
    BEGIN
        IF @OrderId IS NOT NULL --- override all other info if order id is passed
        BEGIN
            PRINT 'WHEN OrderId is NOT NULL..'
            SELECT @PickupLat = originLattitude,
                   @PickupLong = originLongitude,
                   @PickupCity = origin_city,
                   @PickupZip = origin_zip,
                   @PickupState = origin_state,
                   @PickupDateFrom = pickUpDate
            FROM ODS.ControlTower_AllOrders
            WHERE order_id = @OrderId

            PRINT 'Selected order info:
                '
            PRINT 'Pickup Location: ' + @PickupCity + ' ' + @PickupState + ' ' + @PickupZip + ' on '
                  + CAST(@PickupDateFrom AS VARCHAR(30))

        END
        
        
        DECLARE @DriverList TABLE (driver_id VARCHAR(50), driver_distance_in_miles decimal)
        ------------Get drivers that will be nearby the pickup point on or after given date
        INSERT INTO @DriverList
        SELECT DISTINCT driver1_id,MIN(distance_in_miles) OVER(PARTITION BY driver1_id) distance_in_miles 
        FROM (
            SELECT DISTINCT driver1_id, driver2_id --,tractor_id,trailer1_id, trailer2_id
            ,MAX(ISNULL(ActualDeliveryDate, DeliveryDate)) OVER(PARTITION BY driver1_id)  DeliveryDate --, assignment_status 
            --,o.destLattitude, o.destLongitude
            ,CAST(GEOGRAPHY::Point(@PickupLat, @PickupLong, 4326).STDistance(GEOGRAPHY::Point(o.destLattitude,o.destLongitude,4326))/ 1609.344 AS DECIMAL(8, 1)) distance_in_miles
            FROM ODS.ControlTower_AllOrders o
            WHERE CAST(DeliveryDate as date) >= CAST(@PickupDateFrom as date) AND o.order_status IN ('A','P')
            AND assignment_status IS NOT NULL
        ) orderData
        WHERE distance_in_miles <= @Radius
        ORDER BY distance_in_miles ASC
        
        IF @debug = 1 SELECT * FROM @DriverList

        ---- get list of drivers
        SELECT tm.id tractor_id,
               A.*,
               A.driver_last_location as tractor_last_location,tm.status tractor_status
        FROM
        (
            SELECT driver_info.*,
                   lm.latitude,
                   lm.longitude,
                   ISNULL( driver_distance_in_miles, CAST(GEOGRAPHY::Point(@PickupLat, @PickupLong, 4326).STDistance(GEOGRAPHY::Point(latitude,longitude,4326)) / 1609.344 AS DECIMAL(8, 1))) distance_in_miles
            FROM
            (
                SELECT d.driver_id,
                       driver_last_location,
                       ISNULL(RTRIM(SUBSTRING(driver_last_location, 1, CHARINDEX(',', driver_last_location) - 1)), city) city,
                       ISNULL(LTRIM(SUBSTRING(driver_last_location,CHARINDEX(',', driver_last_location)+1,3)), state) state,
                       ISNULL(LTRIM(SUBSTRING(driver_last_location,CHARINDEX(',', driver_last_location)+5,6)), zip) zip,
                       need_codriver,
                       return_home_flag,
                       next_home_date,
                       pta_date,
                       cell_phone,
                       is_active,
                       pta_date as next_available,
                       dl.driver_distance_in_miles
                FROM ODS.driver_master d WITH(NOLOCK) JOIN @DriverList dl ON d.driver_id = dl.driver_id
                WHERE d.is_active = 'Y') as driver_info
                JOIN ODS.[city_state_zip] lm WITH(NOLOCK)
                    ON driver_info.city = lm.name
                       AND driver_info.state = lm.state_id
                       AND driver_info.zip = lm.zip_code
                       AND lm.latitude IS NOT NULL
                
        ) A
            JOIN ODS.tractor_master tm WITH(NOLOCK)
                ON tm.driver1_id = A.driver_id
        WHERE distance_in_miles <= @Radius
        ORDER BY distance_in_miles ASC

    END
    --SELECT tm.id tractor_id, driver1_id, driver2_id, ISNULL(dm1.driver_last_location,RTRIM(dm1.city)+','+RTRIM(dm1.state)+','+ RTRIM(dm1.zip))  last_location 
    --FROM ODS.tractor_master tm left join ODS.driver_master dm1 on tm.driver1_id = dm1.driver_id
    --WHERE service_status = 'I' --AND id NOT IN (SELECT tractor_id FROM ODS.movement_assets WHERE tractor_id IS NOT NULL)
    --AND allocation IS NULL

    ELSE IF @ResponseType = 'orders'
    BEGIN
        --SELECT * FROM ODS.tractor_master
        PRINT 'Inside Response type orders'
        
        SELECT order_list.distance_in_miles,
               order_list.*
        FROM
        (
            SELECT o.order_id,
                   o.customer_name,
                   o.order_mode,
                   CAST(GEOGRAPHY::Point(@PickupLat, @PickupLong, 4326).STDistance(GEOGRAPHY::Point(
                                                                                                       pickup.lattitude,
                                                                                                       pickup.longitude,
                                                                                                       4326
                                                                                                   )
                                                                                  ) / 1609.344 AS DECIMAL(8, 1)) distance_in_miles,
                   o.order_type,
                   o.agent_payee_id,
                   o.order_division,
                   pickup.location_name pickup_location,
                   pickup.city pickup_city,
                   pickup.state pickup_state,
                   pickup.zip pickup_zip,
                   pickup.zone_id pickup_zone_id,
                   pickup.sched_arrival pickup_sched_arrival,
                   pickup.lattitude pickup_latitude,
                   pickup.longitude pickup_longitude,
                   delivery.location_name delivery_location,
                   delivery.city delivery_city,
                   delivery.state delivery_state,
                   delivery.zip delivery_zip,
                   delivery.zone_id delivery_zone_id,
                   delivery.sched_arrival delivery_sched_arrival,
                   delivery.lattitude delivery_latitude,
                   delivery.longitude delivery_longitude
            FROM ODS.orderMaster o WITH(NOLOCK)
                JOIN ODS.stops pickup WITH(NOLOCK)
                    ON o.company_id = pickup.company_id
                       AND o.order_id = pickup.order_id
                       AND o.origin_stop_id = pickup.stop_id
                JOIN ODS.stops delivery WITH(NOLOCK)
                    ON o.company_id = delivery.company_id
                       AND o.order_id = delivery.order_id
                       AND o.dest_stop_id = delivery.stop_id
            WHERE o.order_status = 'A'
                  ---Other filters to be added here (e.g. area, region, zip, zone etc)
        --AND pickup.zone_id = ''
        ) order_list
        WHERE CAST(order_list.distance_in_miles AS INT) <= @Radius
        ORDER BY order_list.distance_in_miles

    /* --------2. Search Equipment in radius from given location
            ---2.1 Driver equipment search

            SELECT d.id driver_id, d.name,
            ISNULL(
                    (SELECT top 1 RTRIM(city_name)  + ISNULL(+ ', ' + RTRIM([state]),'') + ISNULL(+ ', ' + RTRIM(zip_code),'') 
                    FROM [QA-McLeod-RPT].lme_qa.dbo.callin WHERE driver_id = d.id AND company_id = d.company_id ORDER BY call_date_time DESC) 
                    ,RTRIM(d.city)  + ISNULL(+ ', ' + RTRIM(d.[state]),'')+ ISNULL(+ ', ' + RTRIM(d.zip),'')
                ) driver_last_location
            FROM [QA-McLeod-RPT].lme_qa.dbo.driver d 
            WHERE d.company_id = 'TMS' AND d.is_active = 'Y'

            SELECT d.id tractor_id, 
            (SELECT top 1 RTRIM(city_name)  + ISNULL(+ ', ' + RTRIM([state]),'') + ISNULL(+ ', ' + RTRIM(zip_code),'') 
            FROM [QA-McLeod-RPT].lme_qa.dbo.callin WHERE tractor_id = d.id AND company_id = d.company_id ORDER BY call_date_time DESC) tractor_last_location
            FROM [QA-McLeod-RPT].lme_qa.dbo.tractor d 
            WHERE d.company_id = 'TMS' AND d.service_status = 'A'


            SELECT *
            FROM  [QA-McLeod-RPT].lme_qa.dbo.equipment_issued e 
            WHERE e.company_id = 'TMS' AND e.status = 'I' AND e.parent_row_type = 'T'


            */
    END
END

GO

