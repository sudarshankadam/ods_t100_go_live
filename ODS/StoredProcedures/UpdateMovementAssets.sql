-- =============================================
-- Author: Sudarshan Kadam
-- Create date: 27-Aug-2021
-- Description: Get CDC data from CRSTReporting and Merge with ODS.OrderMaster
-- =============================================
CREATE PROCEDURE [ODS].[UpdateMovementAssets]
@OverrideFromDate DATETIME = NULL
AS
BEGIN
    SET NOCOUNT ON;
    ---CREATE Temp table
    CREATE TABLE #ChangedAssetsData
    (
        [company_id] [varchar](50) NULL,
        [movement_id] [varchar](50) NULL,
	    [order_id] [varchar](50) NULL,
	    [tractor_id] [varchar](50) NULL,
	    [trailer1_id] [varchar](50) NULL,
	    [trailer2_id] [varchar](50) NULL,
	    [driver1_id] [varchar](50) NULL,
	    [driver2_id] [varchar](50) NULL,
	    [carrier_id] [varchar](50) NULL,
	    [assignment_status] [varchar](50) NULL,
	    [last_modified_date] [datetime] NULL,
	    [created_date] [datetime] NULL,
	    [equipment_group_id] [char](32) NULL,
	    [tractor_last_location] [varchar](200) NULL,	    
	    [driver_dot_info] [varchar](max) NULL
    )
        
    IF @OverrideFromDate IS NOT NULL
		SET @OverrideFromDate = DATEADD(DAY, -2, @OverrideFromDate)
    ----Get change data in temp table
    INSERT INTO #ChangedAssetsData
    (
        company_id,
        [movement_id],
        [order_id],
        [tractor_id],
        [driver1_id],
        [driver2_id],
        [trailer1_id],
        [trailer2_id],
        [carrier_id],
        [assignment_status],
        [last_modified_date],
        [created_date],
        [equipment_group_id],
        tractor_last_location
    )
    EXEC [QA-McLeod-RPT].[CRSTReporting].ODS.[GetChangesForMovementAssets] -- @OverrideFromDate = @OverrideFromDate

    IF @OverrideFromDate IS NOT NULL
        SELECT * FROM #ChangedAssetsData
    ELSE
    BEGIN
        ;WITH newData
        AS (SELECT * FROM #ChangedAssetsData)
        MERGE [ODS].Movement_Assets AS t
        USING newData AS s
        ON t.movement_id = s.movement_id AND t.[company_id] = s.[company_id]
        WHEN MATCHED AND EXISTS
        (
            SELECT 
                s.[company_id],
                s.[movement_id],
                s.[order_id],
                s.[tractor_id],
                s.[driver1_id],
                s.[driver2_id],
                s.[trailer1_id],
                s.[trailer2_id],
                s.[carrier_id],
                s.[assignment_status],
                s.[equipment_group_id],
                s.tractor_last_location
            EXCEPT
            SELECT 
                t.company_id,
                t.[movement_id],
                t.[order_id],
                t.[tractor_id],
                t.[driver1_id],
                t.[driver2_id],
                t.[trailer1_id],
                t.[trailer2_id],
                t.[carrier_id],
                t.[assignment_status],
                t.[equipment_group_id],
                t.tractor_last_location
        ) THEN
            UPDATE SET 
		    t.company_id = s.company_id,
            t.[movement_id] = s.[movement_id],
            t.[order_id] = s.[order_id],
            t.[tractor_id] = s.[tractor_id],
            t.[driver1_id] = s.[driver1_id],
            t.[driver2_id] = s.[driver2_id],
            t.[trailer1_id] = s.[trailer1_id],
            t.[trailer2_id] = s.[trailer2_id],
            t.[carrier_id] = s.[carrier_id],
            t.[assignment_status] = s.[assignment_status],
            t.[last_modified_date] = s.[last_modified_date],
            t.[created_date] = s.[created_date],
            t.[equipment_group_id] = s.[equipment_group_id],
            t.tractor_last_location = s.tractor_last_location
    
        WHEN NOT MATCHED BY TARGET THEN
            INSERT
            (
                company_id,
                [movement_id],
                [order_id],
                [tractor_id],
                [driver1_id],
                [driver2_id],
                [trailer1_id],
                [trailer2_id],
                [carrier_id],
                [assignment_status],
                [last_modified_date],
                [created_date],
                [equipment_group_id],
                tractor_last_location
            )
            VALUES
            (
                s.company_id,
                s.[movement_id],
                s.[order_id],
                s.[tractor_id],
                s.[driver1_id],
                s.[driver2_id],
                s.[trailer1_id],
                s.[trailer2_id],
                s.[carrier_id],
                s.[assignment_status],
                s.[last_modified_date],
                s.[created_date],
                s.[equipment_group_id],
                s.tractor_last_location
            );

        PRINT 'Asset Rows affected: ' + CAST(@@RowCount AS VARCHAR(100))
    END
END

GO

