-- =============================================
-- Author: Sudarshan Kadam
-- Create date: 27-Aug-2021
-- Description: Get CDC data from CRSTReporting and Merge with ODS.OrderMaster
-- =============================================
CREATE PROCEDURE [ODS].[UpdateFleetMaster]
@OverrideFromDate DATETIME = NULL
AS
BEGIN
    SET NOCOUNT ON;
    ---CREATE Temp table
   CREATE TABLE #ChangedFleetData
    (
        fleet_id [varchar](50) NOT NULL,
        company_id [varchar](50) NOT NULL,
        fleet_descr [varchar](500) NULL,
        asset_type [varchar](50) NULL,
        asset_id [varchar](50) NULL,
        asset_status [varchar](50) NULL,
        agency_id [varchar](50) NULL
    )
    
    IF @OverrideFromDate IS NOT NULL
		SET @OverrideFromDate = DATEADD(DAY, -2, @OverrideFromDate)
    ----Get change data in temp table
    INSERT INTO #ChangedFleetData
    EXEC [QA-McLeod-RPT].[CRSTReporting].ODS.[GetChangesForFleet] @OverrideFromDate = @OverrideFromDate

    --SELECT RTRIM(f.id) AS fleet_id,
    --        f.company_id,
    --        RTRIM(f.description) fleet_descr,
    --        RTRIM(assets.asset_type) asset_type,
    --        RTRIM(assets.asset_id) asset_id,
    --        RTRIM(assets.asset_status) asset_status,
    --        RTRIM(p.id) agency_id
    --    INTO #ChangedFleetData
    --    FROM [QA-McLeod-RPT].lme_qa.dbo.fleet f
    --        LEFT JOIN [QA-McLeod-RPT].lme_qa.dbo.payee p ON f.id = p.agent_fleet_code AND f.company_id = p.company_id 
    --        AND p.id <> 'DANMBIAL'
    --        JOIN
    --        (
    --            SELECT id asset_id,
    --                    group_id fleet_id,
    --                    company_id,
    --                    'D' asset_type,
    --                    is_active asset_status
    --            FROM [QA-McLeod-RPT].lme_qa.dbo.driver
    --            WHERE group_id IS NOT NULL
    --                    AND company_id = 'T100'
    --            UNION
    --            SELECT id asset_id,
    --                    fleet_id,
    --                    company_id,
    --                    'T' asset_type,
    --                    service_status asset_status
    --            FROM [QA-McLeod-RPT].lme_qa.dbo.tractor
    --            WHERE fleet_id IS NOT NULL
    --                    AND company_id = 'T100'
    --        ) assets
    --            ON f.id = assets.fleet_id
    --    WHERE f.company_id = 'T100'
    
    ;WITH newData
    AS (SELECT * FROM #ChangedFleetData)
    MERGE [ODS].fleet_master AS t
    USING newData AS s
    ON t.asset_type = s.asset_type AND t.asset_id = s.asset_id
    WHEN MATCHED AND EXISTS
                     (
                         SELECT
                           s.[fleet_id]
                          ,'T100'
                          ,s.[fleet_descr]
                          ,s.[asset_type]
                          ,s.[asset_id]
                          ,s.[asset_status]
                          ,s.[agency_id]
                         EXCEPT
                         SELECT t.[fleet_id]
          ,t.[company_id]
          ,t.[fleet_descr]
          ,t.[asset_type]
          ,t.[asset_id]
          ,t.[asset_status]
          ,t.[agency_id]
                     ) 
    THEN
        UPDATE SET 
	   [fleet_id]       = s.[fleet_id]
      ,[fleet_descr]    = s.[fleet_descr]
      ,[asset_type]     = s.[asset_type]
      ,[asset_id]       = s.[asset_id]
      ,[asset_status]   = s.[asset_status]
      ,[agency_id]      = s.[agency_id]
    WHEN NOT MATCHED BY TARGET THEN
        INSERT
        (
            [fleet_id]
          ,[company_id]
          ,[fleet_descr]
          ,[asset_type]
          ,[asset_id]
          ,[asset_status]
          ,[agency_id]
        )
        VALUES
        (
	       s.[fleet_id]
          ,'T100'
          ,s.[fleet_descr]
          ,s.[asset_type]
          ,s.[asset_id]
          ,s.[asset_status]
          ,s.[agency_id]
        );
    --WHEN NOT MATCHED BY SOURCE 
    --    THEN 
    --    DELETE;

    PRINT 'Rows affected: ' + CAST(@@RowCount AS VARCHAR(100))
END

GO

