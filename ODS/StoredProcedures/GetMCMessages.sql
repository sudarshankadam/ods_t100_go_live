-- =============================================
-- Author: Sudarshan Kadam
-- Create date: 27-Aug-2021
-- Description: Get CDC data from CRSTReporting and Merge with ODS.OrderMaster
---SAMPLE
/*
EXEC [ODS].[UpdateStops] @OverrideFromDate = '2021-10-15 10:45:06.407'
*/
-- =============================================
CREATE PROCEDURE [ODS].GetMCMessages
@OverrideFromDate DATETIME = NULL
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @MaxMessageId varchar(50) 
    SELECT @MaxMessageId = MAX(message_id) FROM ODS.order_messageboard WHERE channel_type = 'ELDInbound'

    SELECT mcm.id message_id, mcm.order_id, mcm.movement_id, mc_unit_no, driver_id entered_by, 
    mc_form_number, message_text message_details, message_recvd_date, mc_form_definition
    INTO #mc_message_temp
    FROM [QA-McLeod-RPT].lme_qa.dbo.mc_message mcm JOIN [QA-McLeod-RPT].lme_qa.dbo.driver d
    ON mcm.company_id = d.company_id
    AND mcm.driver_id = d.id
    WHERE mc_form_definition LIKE '%freeform%'
    AND message_type = 'I'
    AND mcm.company_id = 'T100'
    AND mcm.id > ISNULL(@MaxMessageId,0)

    INSERT INTO ODS.order_messageboard
    (message_id, order_id, movement_id, entered_by, message_created_date, message_details,channel_type )
    SELECT message_id,order_id, movement_id, entered_by,message_recvd_date,message_details, 'ELDInbound' channel_type
    FROM #mc_message_temp
    PRINT 'Messages Added: ' + CAST(@@RowCount AS VARCHAR(100))

    INSERT INTO ODS.notification_trigger
    (notificationTypeId, messageText, bookingAgency, fleetAgency, expiry, orderId, movementId, role,fleetId,driverId,modified_date)
    SELECT 5, message_details, NULL,(SELECT top 1 CASE WHEN agency_id = 'OLANSISD' THEN '1002' ELSE agency_id END  FROM ODS.fleet_master WHERE asset_type = 'D' AND asset_id = t.entered_by) fleetAgency
    ,NULL, order_id, movement_id, NULL, (SELECT top 1 fleet_id FROM ODS.fleet_master WHERE asset_type = 'D' AND asset_id = t.entered_by) fleet_id
    ,t.entered_by, t.message_recvd_date
    FROM #mc_message_temp t

    PRINT 'Triggers Added: ' + CAST(@@RowCount AS VARCHAR(100))
    
    DROP TABLE IF EXISTS  #mc_message_temp
    
END

GO

