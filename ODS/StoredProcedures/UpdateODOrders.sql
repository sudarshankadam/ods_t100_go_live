CREATE PROCEDURE [ODS].[UpdateODOrders]
@company_id varchar(10) = 'T100',
@section varchar(20) = 'ALL',
@FromDate datetime = NULL
AS
BEGIN
    
        SELECT 
            o.company_id
            ,o.id order_id
            ,o.customer_id
            ,s1.location_id Origin_Location_Id
            ,s1.city_name Origin_City
            ,s1.state Origin_State
            ,s1.zip_code Origin_Zip
            ,s2.location_id Dest_Location_Id
            ,s1.city_name Dest_City
            ,s1.state Dest_State
            ,s1.zip_code Dest_Zip      
            ,[route_cost1]
            ,[route_cost1_c]
            ,[route_cost1_d]
            ,[route_cost1_n]
            ,[route_cost1_r]
            ,[route_cost2]
            ,[route_cost2_c]
            ,[route_cost2_d]
            ,[route_cost2_n]
            ,[route_cost2_r]
            ,[route_cost3]
            ,[route_cost3_c]
            ,[route_cost3_d]
            ,[route_cost3_n]
            ,[route_cost3_r]
            ,[route_cost4]
            ,[route_cost4_c]
            ,[route_cost4_d]
            ,[route_cost4_n]
            ,[route_cost4_r]
            ,[route_state_locality1]
            ,[route_state_locality2]
            ,[route_state_locality3]
            ,[route_state_locality4]
            ,[route_date1]
            ,[route_date2]
            ,[route_date3]
            ,[route_date4]
            ,[route_notes1]
            ,[route_notes2]
            ,[route_notes3]
            ,[route_notes4]
            ,[route_rcvd_date1]
            ,[route_rcvd_date2]
            ,[route_rcvd_date3]
            ,[route_rcvd_date4]
            ,[route_state_locality6]
            ,[route_state_locality5]
            ,[route_date6]
            ,[route_date5]
            ,[route_notes6]
            ,[route_notes5]
            ,[route_rcvd_date6]
            ,[route_rcvd_date5]
            ,[route_cost5]
            ,[route_cost6]
            ,o.ordered_date created_date 
            ,1 as is_current
            ,route_permit_type1
            ,route_permit_america6
            ,route_permit_america5
            ,route_permit_america4
            ,route_permit_america3
            ,route_permit_america2
            ,route_permit_america1
            ,route_permit_type6
            ,route_permit_type5
            ,route_permit_type4
            ,route_permit_type3
            ,route_permit_type2
        INTO #routeTemp
        FROM [QA-McLeod-RPT].lme_qa.dbo.orders o 
        JOIN [QA-McLeod-RPT].lme_qa.dbo.stop s1 ON s1.order_id = o.id AND s1.company_id = o.company_id AND s1.id = o.shipper_stop_id
        JOIN [QA-McLeod-RPT].lme_qa.dbo.stop s2 ON s2.order_id = o.id AND s2.company_id = o.company_id AND s2.id = o.consignee_stop_id
        WHERE o.order_type_id = 'ODPERM' AND o.company_id = 'T100' --AND o.ordered_date > DATEADD(DAY,-1,getdate())
        
        ;WITH newData
        AS (SELECT * FROM #routeTemp WHERE route_state_locality1 IS NOT NULL)
        MERGE [ODS].OD_route_details AS t
        USING newData AS s
        ON t.Origin_Location_Id = s.Origin_Location_Id AND t.Dest_Location_Id = s.Dest_Location_Id AND t.[company_id] = s.[company_id]
        WHEN MATCHED 
        THEN UPDATE
        SET order_id = s.order_id
        ,customer_id = s.customer_id
        ,[route_cost1] = s.[route_cost1]
        ,[route_cost1_c] = s.[route_cost1_c]
        ,[route_cost1_d] = s.[route_cost1_d]
        ,[route_cost1_n] = s.[route_cost1_n]
        ,[route_cost1_r] = s.[route_cost1_r]
        ,[route_cost2] = s.[route_cost2]
        ,[route_cost2_c] = s.[route_cost2_c]
        ,[route_cost2_d] = s.[route_cost2_d]
        ,[route_cost2_n] = s.[route_cost2_n]
        ,[route_cost2_r] = s.[route_cost2_r]
        ,[route_cost3] = s.[route_cost3]
        ,[route_cost3_c] = s.[route_cost3_c]
        ,[route_cost3_d] = s.[route_cost3_d]
        ,[route_cost3_n] = s.[route_cost3_n]
        ,[route_cost3_r] = s.[route_cost3_r]
        ,[route_cost4] = s.[route_cost4]
        ,[route_cost4_c] = s.[route_cost4_c]
        ,[route_cost4_d] = s.[route_cost4_d]
        ,[route_cost4_n] = s.[route_cost4_n]
        ,[route_cost4_r] = s.[route_cost4_r]
        ,[route_state_locality1] = s.[route_state_locality1]
        ,[route_state_locality2] = s.[route_state_locality2]
        ,[route_state_locality3] = s.[route_state_locality3]
        ,[route_state_locality4] = s.[route_state_locality4]
        ,[route_date1] = s.[route_date1]
        ,[route_date2] = s.[route_date2]
        ,[route_date3] = s.[route_date3]
        ,[route_date4] = s.[route_date4]
        ,[route_notes1] = s.[route_notes1]
        ,[route_notes2] = s.[route_notes2]
        ,[route_notes3] = s.[route_notes3]
        ,[route_notes4] = s.[route_notes4]
        ,[route_rcvd_date1] = s.[route_rcvd_date1]
        ,[route_rcvd_date2] = s.[route_rcvd_date2]
        ,[route_rcvd_date3] = s.[route_rcvd_date3]
        ,[route_rcvd_date4] = s.[route_rcvd_date4]
        ,[route_state_locality6] = s.[route_state_locality6]
        ,[route_state_locality5] = s.[route_state_locality5]
        ,[route_date6] = s.[route_date6]
        ,[route_date5] = s.[route_date5]
        ,[route_notes6] = s.[route_notes6]
        ,[route_notes5] = s.[route_notes5]
        ,[route_rcvd_date6] = s.[route_rcvd_date6]
        ,[route_rcvd_date5] = s.[route_rcvd_date5]
        ,[route_cost5] = s.[route_cost5]
        ,[route_cost6] = s.[route_cost6]
        ,route_permit_type1 = s.route_permit_type1
        ,route_permit_america6 = s.route_permit_america6
        ,route_permit_america5 = s.route_permit_america5
        ,route_permit_america4 = s.route_permit_america4
        ,route_permit_america3 = s.route_permit_america3
        ,route_permit_america2 = s.route_permit_america2
        ,route_permit_america1 = s.route_permit_america1
        ,route_permit_type6 = s.route_permit_type6
        ,route_permit_type5 = s.route_permit_type5
        ,route_permit_type4 = s.route_permit_type4
        ,route_permit_type3 = s.route_permit_type3
        ,route_permit_type2 = s.route_permit_type2
        
        WHEN NOT MATCHED BY TARGET THEN
        INSERT
        (
            company_id
            ,order_id
            ,customer_id
            ,Origin_Location_Id
            ,Origin_City
            ,Origin_State
            ,Origin_Zip
            ,Dest_Location_Id
            ,Dest_City
            ,Dest_State
            ,Dest_Zip      
            ,[route_cost1]
            ,[route_cost1_c]
            ,[route_cost1_d]
            ,[route_cost1_n]
            ,[route_cost1_r]
            ,[route_cost2]
            ,[route_cost2_c]
            ,[route_cost2_d]
            ,[route_cost2_n]
            ,[route_cost2_r]
            ,[route_cost3]
            ,[route_cost3_c]
            ,[route_cost3_d]
            ,[route_cost3_n]
            ,[route_cost3_r]
            ,[route_cost4]
            ,[route_cost4_c]
            ,[route_cost4_d]
            ,[route_cost4_n]
            ,[route_cost4_r]
            ,[route_state_locality1]
            ,[route_state_locality2]
            ,[route_state_locality3]
            ,[route_state_locality4]
            ,[route_date1]
            ,[route_date2]
            ,[route_date3]
            ,[route_date4]
            ,[route_notes1]
            ,[route_notes2]
            ,[route_notes3]
            ,[route_notes4]
            ,[route_rcvd_date1]
            ,[route_rcvd_date2]
            ,[route_rcvd_date3]
            ,[route_rcvd_date4]
            ,[route_state_locality6]
            ,[route_state_locality5]
            ,[route_date6]
            ,[route_date5]
            ,[route_notes6]
            ,[route_notes5]
            ,[route_rcvd_date6]
            ,[route_rcvd_date5]
            ,[route_cost5]
            ,[route_cost6]
            ,created_date 
            ,is_current
            ,route_permit_type1
            ,route_permit_america6
            ,route_permit_america5
            ,route_permit_america4
            ,route_permit_america3
            ,route_permit_america2
            ,route_permit_america1
            ,route_permit_type6
            ,route_permit_type5
            ,route_permit_type4
            ,route_permit_type3
            ,route_permit_type2
        )
        VALUES
        (
            company_id
            ,order_id
            ,customer_id
            ,Origin_Location_Id
            ,Origin_City
            ,Origin_State
            ,Origin_Zip
            ,Dest_Location_Id
            ,Dest_City
            ,Dest_State
            ,Dest_Zip      
            ,[route_cost1]
            ,[route_cost1_c]
            ,[route_cost1_d]
            ,[route_cost1_n]
            ,[route_cost1_r]
            ,[route_cost2]
            ,[route_cost2_c]
            ,[route_cost2_d]
            ,[route_cost2_n]
            ,[route_cost2_r]
            ,[route_cost3]
            ,[route_cost3_c]
            ,[route_cost3_d]
            ,[route_cost3_n]
            ,[route_cost3_r]
            ,[route_cost4]
            ,[route_cost4_c]
            ,[route_cost4_d]
            ,[route_cost4_n]
            ,[route_cost4_r]
            ,[route_state_locality1]
            ,[route_state_locality2]
            ,[route_state_locality3]
            ,[route_state_locality4]
            ,[route_date1]
            ,[route_date2]
            ,[route_date3]
            ,[route_date4]
            ,[route_notes1]
            ,[route_notes2]
            ,[route_notes3]
            ,[route_notes4]
            ,[route_rcvd_date1]
            ,[route_rcvd_date2]
            ,[route_rcvd_date3]
            ,[route_rcvd_date4]
            ,[route_state_locality6]
            ,[route_state_locality5]
            ,[route_date6]
            ,[route_date5]
            ,[route_notes6]
            ,[route_notes5]
            ,[route_rcvd_date6]
            ,[route_rcvd_date5]
            ,[route_cost5]
            ,[route_cost6]
            ,created_date 
            ,is_current
            ,route_permit_type1
            ,route_permit_america6
            ,route_permit_america5
            ,route_permit_america4
            ,route_permit_america3
            ,route_permit_america2
            ,route_permit_america1
            ,route_permit_type6
            ,route_permit_type5
            ,route_permit_type4
            ,route_permit_type3
            ,route_permit_type2
        );
    PRINT 'OD Route RowCount: '   + CAST(@@RowCount AS VARCHAR(100))    
   
END

GO

