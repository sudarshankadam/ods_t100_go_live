-- =============================================
-- Author: Sudarshan Kadam
-- Create date: 27-Aug-2021
-- Description: Get CDC data from CRSTReporting and Merge with ODS.OrderMaster
-- =============================================
CREATE PROCEDURE [ODS].[UpdateMovements]
@OverrideFromDate DATETIME = NULL
AS
BEGIN
    SET NOCOUNT ON;
    ---CREATE Temp table
   CREATE TABLE #ChangedMovementsData
    (
        [movement_id] [varchar](50) NOT NULL,
	    [order_id] [varchar](50) NULL,
        [company_id] [varchar](50) NOT NULL,
        is_brokered [varchar](2) NULL,
        brokerage_status [varchar](500) NULL,
        [origin_stop_id] [varchar](50) NULL,
        [dest_stop_id] [varchar](50) NULL,
        [movement_status] [varchar](50) NULL,
        dispatch_status [varchar](50) NULL,
        is_empty_move [varchar](2) NULL,
        pro_number [varchar](100) NULL,
        move_distance [decimal](18, 2) NULL,
        carrier_pay [decimal](18, 2) NULL,
        capacity_rep [varchar](50) NULL,
        carrier_contact [varchar](50) NULL,
        carrier_phone [varchar](50) NULL,
        carrier_email [varchar](max) NULL,
        preassign_sequence INT NULL,        
        order_movement_execution_division [varchar](50) NULL,
        carrier_id varchar(50) NULL,
        xmitted2driver datetime,
        __$start_lsn BINARY(10)
    )
    
    IF @OverrideFromDate IS NOT NULL
		SET @OverrideFromDate = DATEADD(DAY, -2, @OverrideFromDate)
    ----Get change data in temp table
    INSERT INTO #ChangedMovementsData
    (
        movement_id,company_id,order_id,is_brokered,brokerage_status,origin_stop_id,dest_stop_id
	   ,movement_status,dispatch_status,is_empty_move,pro_number,move_distance,
       carrier_pay,capacity_rep,carrier_contact,carrier_phone,carrier_email,preassign_sequence,
       carrier_id,xmitted2driver,order_movement_execution_division ,__$start_lsn
    )
    EXEC [QA-McLeod-RPT].[CRSTReporting].ODS.[GetChangesForMovements] @OverrideFromDate = @OverrideFromDate

    UPDATE C
    SET order_movement_execution_division = rh.hierarchy_code
    FROM #ChangedMovementsData C JOIN ODS.responsibility_history rh
    ON c.movement_id = rh.row_id 
    WHERE rh.row_type ='M' AND rh.hierarchy_code IN ('DEDE','DEDW','EXPD','FLAT','INTG')

    --SELECT * FROM #ChangedMovementsData
    
    ;WITH newData
    AS (SELECT * FROM #ChangedMovementsData)
    MERGE [ODS].Movements AS t
    USING newData AS s
    ON t.movement_id = s.movement_id AND t.[company_id] = s.[company_id] AND t.order_id = s.order_id
    WHEN MATCHED AND EXISTS
                     (
                         SELECT s.movement_id,s.company_id,s.order_id,s.is_brokered,s.brokerage_status,s.origin_stop_id,s.dest_stop_id
	   ,s.movement_status,s.dispatch_status,s.is_empty_move,s.pro_number,s.move_distance
       ,s.carrier_pay,s.capacity_rep,s.carrier_contact,s.carrier_phone,s.carrier_email,s.preassign_sequence,s.order_movement_execution_division,s.carrier_id,s.xmitted2driver
                         EXCEPT
                         SELECT t.movement_id,t.company_id,t.order_id,t.is_brokered,t.brokerage_status,t.origin_stop_id,t.dest_stop_id
	   ,t.movement_status,t.dispatch_status,t.is_empty_move,t.pro_number,t.move_distance
       ,t.carrierPay,t.capacity_rep,t.carrier_contact,t.carrier_phone,t.carrier_email,t.preassign_sequence, t.order_movement_execution_division,t.carrier_id,t.xmitted2driver
                     ) THEN
        UPDATE SET 
		 movement_id= s.movement_id
		,company_id= s.company_id
		,order_id= s.order_id
		,is_brokered= s.is_brokered
		,brokerage_status= s.brokerage_status
		,origin_stop_id= s.origin_stop_id
		,[dest_stop_id]= s.dest_stop_id
		,movement_status= s.movement_status
		,dispatch_status= s.dispatch_status
		,is_empty_move= s.is_empty_move
		,pro_number= s.pro_number
		,move_distance= s.move_distance
        ,carrierPay = s.carrier_pay
        ,capacity_rep= s.capacity_rep
        ,carrier_contact= s.carrier_contact
        ,carrier_phone= s.carrier_phone
        ,carrier_email= s.carrier_email
        ,preassign_sequence = s.preassign_sequence
        ,order_movement_execution_division = s.order_movement_execution_division
        ,carrier_id = s.carrier_id
        ,xmitted2driver = s.xmitted2driver
    WHEN NOT MATCHED BY TARGET THEN
        INSERT
        (
            [movement_id]
		 ,[company_id]
		 ,[order_id]
		 ,[is_brokered]
		 ,[brokerage_status]
		 ,[origin_stop_id]
		 ,[dest_stop_id]
		 ,[movement_status]
		 ,[dispatch_status]
		 ,[is_empty_move]
		 ,[last_modified_date]
		 ,[pro_number]
		 ,[move_distance],carrierPay,capacity_rep,carrier_contact,carrier_phone,carrier_email,preassign_sequence ,
         order_movement_execution_division,carrier_id,xmitted2driver
        )
        VALUES
        (
		  s.[movement_id]
		 ,s.[company_id]
		 ,s.[order_id]
		 ,s.[is_brokered]
		 ,s.[brokerage_status]
		 ,s.[origin_stop_id]
		 ,s.[dest_stop_id]
		 ,s.[movement_status]
		 ,s.[dispatch_status]
		 ,s.[is_empty_move]
		 ,GETDATE()
		 ,s.[pro_number]
		 ,s.[move_distance]
         ,s.carrier_pay
         ,s.capacity_rep
         ,s.carrier_contact
         ,s.carrier_phone
         ,s.carrier_email
         ,s.preassign_sequence
         ,s.order_movement_execution_division
         ,s.carrier_id
         ,s.xmitted2driver
        );

    PRINT 'Rows affected: ' + CAST(@@RowCount AS VARCHAR(100))
END

GO

