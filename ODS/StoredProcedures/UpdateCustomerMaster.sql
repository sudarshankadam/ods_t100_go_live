-- =============================================
-- Author: Sudarshan Kadam
-- Create date: 27-Aug-2021
-- Description: Get CDC data from CRSTReporting and Merge with ODS.OrderMaster
---SAMPLE
/*
EXEC [ODS].[UpdateStops] @OverrideFromDate = '2021-10-15 10:45:06.407'
*/
-- =============================================
CREATE PROCEDURE [ODS].[UpdateCustomerMaster]
@OverrideFromDate DATETIME = NULL
AS
BEGIN
    SET NOCOUNT ON;
    ---CREATE Temp table
   SELECT id as customer_id, [company_id]
      ,[location_id]
      ,NULL [location_name]
      ,address1
      ,address2
      ,city
      ,[city_id]
      ,[state_id]
      ,[zip_code]
      ,[credit_limit]
      ,credit_status  
    INTO #ChangedCustomerData
    FROM [QA-McLeod-RPT].lme_qa.dbo.customer WHERE company_id = 'T100' AND is_active = 'Y'
    

    
    IF @OverrideFromDate IS NOT NULL
		SET @OverrideFromDate = DATEADD(DAY, -2, @OverrideFromDate)
    ------Get change data in temp table
    INSERT INTO #ChangedStopsData
    (
        [stop_id],
	    [order_id],
	    [movement_id],
	    [sched_arrival] ,
	    [sched_departure] ,
	    [original_arrival] ,
	    [original_departure] ,
	    [actual_arrival] ,
	    [actual_departure] ,
	    [city],
	    [state],
	    [zip],
        [location_id],
	    [location_name],
	    [stop_type],
	    [lattitude],
	    [longitude],
	    [order_sequence] ,
	    [movement_sequence] ,
	    [company_id],
	    [zone_id],
	    [appt_required] ,
	    [appt_status_code],
        appt_confirmed,
        origin_is_hot_comment,
        origin_all_comments,
        projected_arrival,
        eta_distance,
        __$start_lsn
    )
    EXEC [QA-McLeod-RPT].[CRSTReporting].ODS.[GetChangesForStops] @OverrideFromDate = @OverrideFromDate

    --SELECT * FROM #ChangedStopsData
    
    ;WITH newData
    AS (SELECT * FROM #ChangedStopsData)
    MERGE [ODS].Stops AS t
    USING newData AS s
    ON t.stop_id = s.stop_id
       AND t.[company_id] = s.[COMPANY_ID]
    WHEN MATCHED AND EXISTS
                     (
                         SELECT s.[stop_id],
	                            s.[order_id],
	                            s.[movement_id],
	                            s.[sched_arrival] ,
	                            s.[sched_departure] ,
	                            s.[original_arrival] ,
	                            s.[original_departure] ,
	                            s.[actual_arrival] ,
	                            s.[actual_departure] ,
	                            s.[city],
	                            s.[state],
	                            s.[zip],
                                s.[location_id],
	                            s.[location_name],
	                            s.[stop_type],
	                            s.[lattitude],
	                            s.[longitude],
	                            s.[order_sequence] ,
	                            s.[movement_sequence] ,
	                            s.[company_id],
	                            s.[zone_id],
	                            s.[appt_required] ,
	                            s.[appt_status_code],
                                s.appt_confirmed,
                                s.origin_is_hot_comment,
                                s.projected_arrival,
                                s.eta_distance
                         EXCEPT
                         SELECT t.[stop_id],
	                            t.[order_id],
	                            t.[movement_id],
	                            t.[sched_arrival] ,
	                            t.[sched_departure] ,
	                            t.[original_arrival] ,
	                            t.[original_departure] ,
	                            t.[actual_arrival] ,
	                            t.[actual_departure] ,
	                            t.[city],
	                            t.[state],
	                            t.[zip],
                                t.[location_code],
	                            t.[location_name],
	                            t.[stop_type],
	                            t.[lattitude],
	                            t.[longitude],
	                            t.[order_sequence] ,
	                            t.[movement_sequence] ,
	                            t.[company_id],
	                            t.[zone_id],
	                            t.[appt_required] ,
	                            t.[appt_status_code],
                                t.appt_confirmed,
                                t.[has_hot_comment],
                                t.projected_arrival,
                                t.eta_distance
                     ) THEN
        UPDATE SET 
		[stop_id] = s.[stop_id],
	    [order_id] = s.[order_id],
	    [movement_id] = s.[movement_id],
	    [sched_arrival] = s.[sched_arrival] ,
	    [sched_departure] = s.[sched_departure] ,
	    [original_arrival] = s.[original_arrival] ,
	    [original_departure] = s.[original_departure] ,
	    [actual_arrival] = s.[actual_arrival] ,
	    [actual_departure] = s.[actual_departure] ,
	    [city] = s.[city],
	    [state] = s.[state],
	    [zip] = s.[zip],
        [location_code] = s.[location_id],
	    [location_name] = s.[location_name],
	    [stop_type] = s.[stop_type],
	    [lattitude] = s.[lattitude],
	    [longitude] = s.[longitude],
	    [order_sequence] = s.[order_sequence] ,
	    [movement_sequence] = s.[movement_sequence] ,
	    [company_id] = s.[company_id],
	    [zone_id] = s.[zone_id],
	    [appt_required] = s.[appt_required] ,
	    [appt_status_code] = s.[appt_status_code],
        appt_confirmed = s.appt_confirmed,
        [last_modified_date] = GETDATE(),
        [has_hot_comment] = s.origin_is_hot_comment,
        [hot_comments] = s.origin_all_comments,
        projected_arrival = s.projected_arrival,
        eta_distance = s.eta_distance
    WHEN NOT MATCHED BY TARGET THEN
        INSERT
        (
            [stop_id],
	        [order_id],
	        [movement_id],
	        [sched_arrival] ,
	        [sched_departure] ,
	        [original_arrival] ,
	        [original_departure] ,
	        [actual_arrival] ,
	        [actual_departure] ,
	        [city],
	        [state],
	        [zip],
            [location_code],
	        [location_name],
	        [stop_type],
	        [lattitude],
	        [longitude],
	        [order_sequence] ,
	        [movement_sequence] ,
	        [company_id],
	        [zone_id],
	        [appt_required] ,
	        [appt_status_code] ,
            appt_confirmed,
            [last_modified_date],
            [has_hot_comment],
            [hot_comments],
            projected_arrival,
            eta_distance
        )
        VALUES
        (
            s.[stop_id],
            s.[order_id],
            s.[movement_id],
            s.[sched_arrival] ,
            s.[sched_departure] ,
            s.[original_arrival] ,
            s.[original_departure] ,
            s.[actual_arrival] ,
            s.[actual_departure] ,
            s.[city],
            s.[state],
            s.[zip],
            s.[location_id],
            s.[location_name],
            s.[stop_type],
            s.[lattitude],
            s.[longitude],
            s.[order_sequence] ,
            s.[movement_sequence] ,
            s.[company_id],
            s.[zone_id],
            s.[appt_required] ,
            s.[appt_status_code],
            s.appt_confirmed,
            GETDATE(),
            origin_is_hot_comment,
            origin_all_comments,
            projected_arrival,
            eta_distance
        );

    PRINT 'Rows affected: ' + CAST(@@RowCount AS VARCHAR(100))
    
    CREATE TABLE #stopComments
    (   
        stop_id varchar(max),
        order_id varchar(50),
        has_hot_comment varchar(1),
        hot_comments varchar(Max)
    )

    INSERT INTO #stopComments
    EXEC [QA-McLeod-RPT].[CRSTReporting].ODS.GetStopHotComments 

    ;WITH newCommentData AS (SELECT * FROM #stopComments)
    MERGE ODS.stops t
    USING newCommentData as s
    ON s.stop_id = t.stop_id AND s.order_id = t.order_id
    WHEN MATCHED THEN 
    UPDATE  SET 
    has_hot_comment = s.has_hot_comment,
    hot_comments = s.hot_comments;
    
    PRINT 'Rows affected: ' + CAST(@@RowCount AS VARCHAR(100))
END

GO

